

$.ajax({
    url: base_url+'dashboard/notificacoes/',
    dataType: 'json',
})
.done(function(response) {

    var s1 = parseInt(response.ta);
    var s2 = parseInt(response.tce);
    var s3 = parseInt(response.contvenc);
    var s4 = parseInt(response.cont20avenc);
    
    var s5 = s1 + s2 + s3 + s4;

    $("#somaNotificacao").text(s5);
    $("#tceNotificacao").text(response.tce);
    $("#taNotificacao").text(response.ta);
    $("#contVencNotificacao").text(response.contvenc);
    $("#cont20aVencNotificacao").text(response.cont20avenc);
});


$('#estado').change(function(event) {

	var estado = $(this).val();


	$.ajax({
		url: base_url+'usuario/cidadesPorId',
		type: 'POST',
		dataType: 'html',
		data: {estado: estado, token: token },
	})
	.done(function(resultado) {
		$('#cidade').html(resultado);
	});
});

$('.classData').daterangepicker({
	singleDatePicker: true,
	locale: {
      format: 'DD-MM-YYYY'
    }
});

// Column selectors
$('.datatable-button-html5-columns-estudante').DataTable({
    "ordering": false,
    "order": [],
    buttons: {            
        dom: {
            button: {
                className: 'btn btn-default'
            }
        },
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible' // columns: [0, 1, 2, 5]
                }
            },
            {
                extend: 'colvis',
                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                className: 'btn-icon'
            }
        ]
    },
    processing: true,
    serverSide: true,
    ajax: base_url+"estudante/listaAjax"
});

$('.summernoteEmails').summernote();

$('#enviarMalaDireta').click(function(event) {
    var descricao = $('.summernoteEmails').code();
    $('#descricao').val(descricao);
});


$('#estudanteEmails').select2({

        placeholder: 'Estudantes',
        ajax: {
            url: base_url+'emails/selecionarEstudantes/',
            dataType: 'json',
            data: function (term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function (data, page) {
                return { results: data.results };
            }

        },
        tags:true,
        multiple: true,
        tokenSeparators: [',']


  });

$('#empresas').select2({

        placeholder: 'Empresa',
        ajax: {
            url: base_url+'contratoestudante/selecionarEmpresas/',
            dataType: 'json',
            data: function (term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function (data, page) {
                return { results: data.results };
            }

        },
        tags:true,
        multiple: true,
        tokenSeparators: [',']


  });

$(document).ready(function(){


        var tamanho = $(".cnpj").val().length;

        if(tamanho < 12 ){

            $(".cnpj").inputmask("999.999.999-99");

        } else {

            $(".cnpj").inputmask("99.999.999/9999-99");
        }

});

$('#salualcep').change(function(event) {
      
      var cep = $(this).val();

      $.ajax({
        url: 'http://viacep.com.br/ws/'+cep+'/json/',
        type: 'get',
        dataType: 'json',
      })
      .done(function(result) {
          if(result.erro != true){
              $('#saluallogra').val(result.logradouro)
              $('#salualbairr').val(result.bairro)
              $('#salualestad').val(result.uf)
              $('#salualcidad').val(result.localidade)
          }
      });
      
});