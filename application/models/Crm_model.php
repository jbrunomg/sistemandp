<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model {

	private $tabela  = 'tbcrm';
	private $id      = 'pcrmcodig';
	private $visivel = 'crm_visivel';

	public function listar()
	{
		$this->db->select('*');		
		$this->db->where($this->visivel, 1);
		$this->db->order_by($this->id, "desc");	
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');	
		$this->db->join('tbcrm_email','tbcrm.pcrmcodig =  tbcrm_email.crm_id','LEFT');	
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($tabela, $dados)
	{	

		$this->db->insert($tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}







}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */