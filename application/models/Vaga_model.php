<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaga_model extends CI_Model {

	private $tabela  = 'tbestvagas';
	private $id      = 'pestvacodig';
	private $visivel = 'vaga_visivel';

	public function listar()
	{
		$this->db->select('tb_cidades.nome, tbestvagas.*');
		$this->db->join('tb_cidades','tbestvagas.sestvacidad =  tb_cidades.id');	
		$this->db->where($this->visivel, 1);
		$this->db->order_by("tbestvagas.pestvacodig", "desc");	
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}
	public function tadasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}
	public function todosCursos()
	{
		$this->db->select('*');	
		$this->db->where('curso_visivel', 1);	
		$this->db->order_by("scurcunome", "asc");				
		return $this->db->get('tbcurcursos')->result();
	}
	public function todasEmpresas()
	{
		$this->db->select('*');	
		$this->db->where('empresa_visivel', 1);
		$this->db->order_by("sempemrazao", "asc");				
		return $this->db->get('tbempempres')->result();

	}

	public function pegarDadosDaEmpresa($id)
	{
		$this->db->select('sempemlogra as logradouro, sempemcep as CEP, sempembairr as bairro, sempemcidad as cidade, sempemuf as uf');	
		$this->db->where('pempemcodig', $id);
		return $this->db->get('tbempempres')->result();
	}



}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */