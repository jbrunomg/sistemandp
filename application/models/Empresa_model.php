<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {

	private $tabela  = 'tbempempres';
	private $id      = 'pempemcodig';
	private $visivel = 'empresa_visivel';

	public function listar()
	{	
	/*
	$sql = ' "Ativo" AS situacao, tbempempres.* FROM tbempempres WHERE `empresa_visivel` = 1  AND pempemcodig IN ( 
   			SELECT DISTINCT(tbcontrestempr.`iempemcodig`) FROM tbcontrestempr WHERE tbcontrestempr.`contrestempr_status` = "Ativo" AND tbcontrestempr.`contrato_visivel` = 1 AND tbcontrestempr.ialualcodig <> 0) 
   			UNION
   			SELECT "Inativo" AS situacao, tbempempres.* FROM tbempempres WHERE `empresa_visivel` = 1  AND pempemcodig NOT IN ( 
   			SELECT DISTINCT(tbcontrestempr.`iempemcodig`) FROM tbcontrestempr WHERE tbcontrestempr.`contrestempr_status` = "Ativo" AND tbcontrestempr.`contrato_visivel` = 1 AND tbcontrestempr.ialualcodig <> 0) ';
	*/
	
	/* Mudança de SQL necessário para pega empresa com 01 contrato recindido no periodo atual - 250522 */
	$sql = '  "Ativo" AS `situacao`,  tbempempres.* FROM  tbempempres 
				WHERE `empresa_visivel` = 1 
 	 		AND pempemcodig IN (
 	 			SELECT DISTINCT(tbcontrestempr.`iempemcodig`)  FROM  tbcontrestempr 
				WHERE tbcontrestempr.`contrestempr_status` = "Ativo" 
					AND tbcontrestempr.`contrato_visivel` = 1     
					AND tbcontrestempr.ialualcodig <> 0 ) 
    
  			UNION
  
		SELECT   "Ativo" AS `situacao`,  tbempempres.* FROM  tbempempres 
		 	WHERE `empresa_visivel` = 1 
		AND pempemcodig IN (    
			SELECT  DISTINCT(tbcontrestempr.`iempemcodig`) FROM tbcontrestempr
			WHERE tbcontrestempr.`contrestempr_status` = "Rescindido"
			    AND ( MONTH(`contrestempr_data_rescisao`) = MONTH(CURDATE()) AND YEAR(`contrestempr_data_rescisao`) = YEAR(CURDATE()) )       
				/* AND MONTH(`contrestempr_data_rescisao`) = MONTH(CURDATE()) */
				AND tbcontrestempr.`iempemcodig` NOT IN (
					SELECT DISTINCT(tbcontrestempr.`iempemcodig`) FROM  tbcontrestempr 
			WHERE tbcontrestempr.`contrestempr_status` = "Ativo" 
			    AND tbcontrestempr.`contrato_visivel` = 1     
			    AND tbcontrestempr.ialualcodig <> 0 ) )   
    
 			UNION
  
  		SELECT  "Inativo" AS situacao,  tbempempres.*  FROM  tbempempres
  		   WHERE `empresa_visivel` = 1 
    	AND pempemcodig NOT IN  (
    		SELECT DISTINCT(tbcontrestempr.`iempemcodig`) FROM  tbcontrestempr 
   			WHERE tbcontrestempr.`contrestempr_status` = "Ativo" 
		      AND tbcontrestempr.`contrato_visivel` = 1 
		      AND tbcontrestempr.ialualcodig <> 0      
      		
      		UNION 

       		SELECT  DISTINCT(tbcontrestempr.`iempemcodig`) FROM tbcontrestempr
 			WHERE tbcontrestempr.`contrestempr_status` = "Rescindido"       
				/* AND MONTH(`contrestempr_data_rescisao`) = MONTH(CURDATE()) */
				AND ( MONTH(`contrestempr_data_rescisao`) = MONTH(CURDATE()) AND YEAR(`contrestempr_data_rescisao`) = YEAR(CURDATE()) ) 
				AND tbcontrestempr.`iempemcodig` NOT IN (
				SELECT DISTINCT(tbcontrestempr.`iempemcodig`) FROM  tbcontrestempr 
		    WHERE tbcontrestempr.`contrestempr_status` = "Ativo" 
			    AND tbcontrestempr.`contrato_visivel` = 1     
			    AND tbcontrestempr.ialualcodig <> 0 ) ) 
		    ';

		$this->db->select($sql);

		$this->db->order_by("situacao");		

		return $this->db->get()->result();	
	}

	public function listarNotificacao($notificacao)
	{

	if ($notificacao == 'vencido') {
	$where =  "  WHERE F.data_term < CURRENT_DATE	 GROUP BY sempemfanta ";
	}else{
	$where =  " WHERE F.data_term BETWEEN CURDATE() and CURDATE() + INTERVAL 30 /*20*/ DAY  GROUP BY sempemfanta ";
	}

	$query = $this->db->query(" SELECT 
		'Alerta' AS `situacao`,
		COUNT(sempemfanta)  AS `total`,
		F.data_term,
		F.salualnome,
		F.pempemcodig,
		F.sempemfanta,
		F.sempemcnpj, 
		F.sempemtel01,
		F.sempememail,
		F.sempemrespo
		FROM 
		(SELECT 
		CASE
		    WHEN ((`contrestempr_data_rescisao` <> '0000-00-00') AND (`contrestempr_data_rescisao` <> '1970-01-01')) THEN contrestempr_data_rescisao
		    WHEN ((`contrestempr_data_renIII` <> '0000-00-00') AND (`contrestempr_data_renIII` <> '1970-01-01')) THEN contrestempr_data_renIII
		    WHEN ((`contrestempr_data_renII` <> '0000-00-00') AND (`contrestempr_data_renII` <> '1970-01-01')) THEN contrestempr_data_renII
		    WHEN ((`contrestempr_data_renI` <> '0000-00-00') AND (`contrestempr_data_renI` <> '1970-01-01')) THEN contrestempr_data_renI
		    WHEN ((`contrestempr_data_term` <> '0000-00-00') AND (`contrestempr_data_term` <> '1970-01-01')) THEN contrestempr_data_term
		    ELSE 'False'
		END AS data_term, 
		 tba.`salualnome`, tbe.`pempemcodig`,
		 tbe.`sempemfanta`, tbe.`sempemcnpj`, tbe.`sempemtel01`, tbe.`sempememail`, tbe.`sempemrespo`
		FROM
		  tbcontrestempr AS tbc
		INNER JOIN `tbalualunos` AS tba ON `tba`.`palualcodig` =  `tbc`.`ialualcodig`
		INNER JOIN `tbempempres` AS tbe ON `tbe`.`pempemcodig` =  `tbc`.`iempemcodig`
		  WHERE 
		  `contrestempr_status` = 'Ativo'
		  AND `contrato_visivel` = 1) AS F". $where
	
	);

    return $query->result();

}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarPorCnpj($cnpj)
	{
		$this->db->select('sempemrazao');		
		$this->db->where('sempemcnpj',$cnpj);		
		return $this->db->get($this->tabela)->result();
	}

	public function pegarContratoPorId($id)
	{
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome,  tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		// $this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$this->db->where($this->visivel, 1);
		$this->db->where('contrato_visivel', 1);
		$this->db->where('iempemcodig',$id);
		// $this->db->order_by("tbcontrestempr.contrestempr_id", "desc");
		$this->db->order_by("tbalualunos.salualnome", "asc");	
		return $this->db->get('tbcontrestempr')->result();	
	}

	public function checarDemonstrativoId($mes,$ano,$empresa)
	{

		$this->db->select('*');	

		$this->db->where('demonstrativoempr_visivel', 1);
		$this->db->where('demonstrativoempr_mes',$mes);	
		$this->db->where('demonstrativoempr_ano',$ano);
		$this->db->where('demonstrativoempr_empresa_id',$empresa);

		return $this->db->get('tbdemonstrativoempr')->result();

	}

	public function estornoDemonstrativoId($lastmes,$ano,$empresa)
	{

		$this->db->select(' `salualnome`, ((sempemvalor/30)*`contrestempr_estorno_dias`) AS estorno , `contrestempr_data_rescisao` ');	

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');

		$this->db->where('contrestempr_estorno', 1);
		$this->db->where('MONTH(contrestempr_data_rescisao)',$lastmes);	
		$this->db->where('YEAR(contrestempr_data_rescisao)',$ano);
		$this->db->where('iempemcodig',$empresa);

		return $this->db->get('tbcontrestempr')->result();

	}


	public function cadastrarDemonstrativo($dados)
	{
		$this->db->insert('tbdemonstrativoempr', $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function pegarDemonstrativoId_OLD($empresa)
	{	
		$sql = '(SELECT
				  SUM(`tbempempres`.`sempemvalor`) 
				FROM
				  `tbcontrestempr` 
				  JOIN `tbempempres` 
				    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
				  JOIN `tbalualunos` 
				    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
				  JOIN `tbensensino` 
				    ON `tbalualunos`.`ialualinsen` = `tbensensino`.`pensencodig` 
				WHERE `empresa_visivel` = 1
				  AND contrato_visivel = 1
				  AND `iempemcodig` = '.$empresa.' 
				  AND ( ( (`contrestempr_status` = "Ativo") AND ((`contrestempr_data_renIII` >= (CURDATE())) OR (`contrestempr_data_renII` >= (CURDATE())) OR (`contrestempr_data_renI` >= (CURDATE()))  OR (`contrestempr_data_term` >= (CURDATE())) )) 
  				  OR ( (`contrestempr_status` = "Rescindido") AND (`contrestempr_data_rescisao` >= (CURDATE())) ) )
				   ) AS total';

		$this->db->select( $sql.', tbempempres.sempemrazao,tbempempres.sempemcnpj, tbempempres.sempemvalor, tbensensino.sensennome,  tbalualunos.*, tbcontrestempr.*, tbdemonstrativoempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbdemonstrativoempr','tbcontrestempr.iempemcodig = tbdemonstrativoempr.demonstrativoempr_empresa_id AND `tbdemonstrativoempr`.`demonstrativoempr_mes` = MONTH(NOW()) AND `tbdemonstrativoempr`.`demonstrativoempr_ano` = YEAR(NOW())','left');

		$this->db->where('contrato_visivel', 1);
		$this->db->where('empresa_visivel', 1);
		$this->db->where('iempemcodig',$empresa);
		$where = "( ( (`contrestempr_status` = 'Ativo') AND ((`contrestempr_data_renIII` >= (CURDATE())) OR (`contrestempr_data_renII` >= (CURDATE())) OR (`contrestempr_data_renI` >= (CURDATE()))  OR (`contrestempr_data_term` >= (CURDATE())) )) ";
		$this->db->where($where);
		$where = "( (`contrestempr_status` = 'Rescindido') AND (`contrestempr_data_rescisao` >= (CURDATE())) ) )";
		$this->db->or_where($where);		
		// $this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		$this->db->order_by("tbalualunos.salualnome", "asc");
		return $this->db->get('tbcontrestempr')->result();	
	}

	public function pegarDemonstrativoId($empresa)
	{	

		$sql = '(SELECT
			  GROUP_CONCAT(`tbalualunos`.`palualcodig`) 
			FROM
			  `tbcontrestempr` 
			  JOIN `tbempempres` 
			    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
			  JOIN `tbalualunos` 
			    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
			LEFT  JOIN `tbensensino` 
			    ON `tbalualunos`.`ialualinsen` = `tbensensino`.`pensencodig` 
			WHERE `empresa_visivel` = 1
			  AND contrato_visivel = 1
			  AND `iempemcodig` = '.$empresa.'			    
    		  AND  ( ((`contrestempr_status` = "Ativo") AND (DATE_FORMAT(contrestempr_data_ini, "%Y-%m")  <= DATE_FORMAT(CURDATE(), "%Y-%m"))) 
    		   OR ( ( `contrestempr_status` = "Rescindido" ) AND ( MONTH(contrestempr_data_rescisao) = MONTH(CURDATE()) AND YEAR(contrestempr_data_rescisao) = YEAR(CURDATE()) ) ) )
			   ) AS grupo,';

		$sqlempresa = ' tbempempres.pempemcodig, tbempempres.sempemrazao,tbempempres.sempemcnpj, tbempempres.sempemvalor, tbensensino.sensennome, ';

		$this->db->select( $sql . $sqlempresa . ' tbalualunos.palualcodig, tbalualunos.salualnome , tbcontrestempr.contrestempr_data_ini , tbcontrestempr.contrestempr_data_term, tbcontrestempr.contrestempr_data_renI, tbcontrestempr.contrestempr_data_renII, tbcontrestempr.contrestempr_data_renIII, tbcontrestempr.contrestempr_data_rescisao, tbcontrestempr.contrestempr_status, tbdemonstrativoempr.* ');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');
		$this->db->join('tbdemonstrativoempr','tbcontrestempr.iempemcodig = tbdemonstrativoempr.demonstrativoempr_empresa_id AND `tbdemonstrativoempr`.`demonstrativoempr_mes` = MONTH(NOW()) AND `tbdemonstrativoempr`.`demonstrativoempr_ano` = YEAR(NOW())','left');

		$this->db->where('contrato_visivel', 1);
		$this->db->where('empresa_visivel', 1);
		$this->db->where('iempemcodig',$empresa);
		// Aecisa
		// if($empresa == '325'){
		// $where = " ( (`contrestempr_status` = 'Ativo')  
  //   	OR ((`contrestempr_status` = 'Rescindido') AND (contrestempr_data_rescisao BETWEEN DATE_FORMAT(CURRENT_DATE() - INTERVAL 1 MONTH, '%Y-%m-%20') AND DATE_FORMAT(CURRENT_DATE(), '%Y-%m-%19')) )) ";
		// } else {
		// $where = " ( (`contrestempr_status` = 'Ativo')  
  //   	OR ((`contrestempr_status` = 'Rescindido') AND (contrestempr_data_rescisao BETWEEN DATE_FORMAT(CURRENT_DATE() - INTERVAL 1 MONTH, '%Y-%m-%15') AND DATE_FORMAT(CURRENT_DATE(), '%Y-%m-%15')) )) ";	
		// }

		// sql retirado 26082022
		// $where = " ( ((`contrestempr_status` = 'Ativo') AND (DATE_FORMAT(contrestempr_data_ini, '%Y-%m')  <= DATE_FORMAT(CURDATE(), '%Y-%m')))   
    	// OR ((`contrestempr_status` = 'Rescindido') AND  ( MONTH(contrestempr_data_rescisao) = (MONTH(CURDATE())) ) ) ) ";	

		// ajuste sql - estava pegando estudante ano anterior
		$where = " ( ((`contrestempr_status` = 'Ativo') AND (DATE_FORMAT(contrestempr_data_ini, '%Y-%m')  <= DATE_FORMAT(CURDATE(), '%Y-%m')))   
    	OR ((`contrestempr_status` = 'Rescindido') AND ( MONTH(contrestempr_data_rescisao) = MONTH(CURDATE()) AND YEAR(contrestempr_data_rescisao) = YEAR(CURDATE()) ) ) ) ";	
		

		$this->db->where($where);		
		$this->db->order_by("tbalualunos.salualnome", "asc");

		return $this->db->get('tbcontrestempr')->result();	
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public  function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
            if($returnId == true){
                return $this->db->insert_id($table);
            }

			return TRUE;

		}		

		return FALSE;       

    }


	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function editarDemonstrativo($mes,$ano,$empresa,$dados)
	{
		$this->db->where('demonstrativoempr_mes', $mes);
		$this->db->where('demonstrativoempr_ano', $ano);
		$this->db->where('demonstrativoempr_empresa_id', $empresa);
		$this->db->update('tbdemonstrativoempr',$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}

	public function todasCidadesAlunos()
	{
		$this->db->select("palualcodig as 'id', salualcidad as 'nome' ");
		$this->db->group_by('salualcidad');
		$this->db->order_by('salualcidad','asc');			
		return $this->db->get('tbalualunos')->result();
	}


	public function getBairroCidadeEstado($cidade)
	{
		$this->db->select("palualcodig as 'id', salualcidad as 'nome' ");
		$this->db->group_by('salualcidad');
		$this->db->order_by('salualcidad','asc');			
		return $this->db->get('tbalualunos')->result();
	}

	public function tadasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}
	public function todosCursos()
	{
		$this->db->select('*');	
		$this->db->where('curso_visivel', 1);			
		return $this->db->get('tbcurcursos')->result();
	}

	public function pegarVagas()
	{
		$this->db->select("tbestvagas.pestvacodig AS 'idVaga',
						   tbestvagas.sestvacurso AS 'idCursos',
							  CONCAT(
							    tbestvagas.pestvacodig,
							    ' - ',
							    tbestvagas.sestvaativi
							  ) AS 'label'");		
		$this->db->where("testvaexpir >= '".date('Y-m-d')."'");	
		$this->db->order_by('pestvacodig','desc');	

		return $this->db->get('tbestvagas')->result();
	}

	public function pegarAlunosVagasCidade($cidade,$cursos,$sexo)
	{
		$this->db->select("
							 CONCAT(tbalualunos.`salualnome`,'  ', tbalualunos.`salualbairr`,'  ', tbalualunos.`salualtel02` ) AS 'nome',
							 tbalualunos.`salualnome`,
							 tbalualunos.`salualemail`,
						     tbalualunos.`latitude`,
						     tbalualunos.`longitude`,
						     tbalualunos.`salualtel02`,						     						     
						     tbalualunos.`salualbairr`,
						     tbalualunos.`date_operacao`,
						     IF(tbalualunos.`ealualsexo` = 1, 'male-2.png','female-2.png') AS 'imagem'");
		//$this->db->join('tbestvagas','tbestvagas.`sestvasexo` = tbalualunos.`ealualsexo`');
		$this->db->where('tbalualunos.`latitude` IS NOT NULL');		
		$this->db->where('tbalualunos.`longitude` IS NOT NULL');		
		$this->db->where_in('tbalualunos.`salualcidad`',$cidade);		
		$this->db->where_in('tbalualunos.`ialualcurso`',$cursos);	
		$this->db->where_in('tbalualunos.`ealualsexo`',$sexo);
		//$this->db->limit(4);	
		return $this->db->get('tbalualunos')->result();
	}


	public function agenciaEmitente()
	{
		$this->db->select('*');		
		return $this->db->get('emitente')->result();			
		// return $this->db->get('tbemitente')->result(); // Utilizar sempre a tabela de EMITENTE padrão do sistema WDM-GESTOR | WDM-CONTROLLER | WDM-PDV | WDM-OUVIDORIA
	}




}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */