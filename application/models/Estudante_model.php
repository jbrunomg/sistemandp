<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudante_model extends CI_Model {

	private $tabela  = 'tbalualunos';
	private $id      = 'palualcodig';
	private $visivel = 'estudante_visivel';

	public function listar()
	{
		$this->db->select('*');
		$this->db->join('tbcurcursos','pcurcucodig = ialualcurso');	
		$this->db->where($this->visivel, 1);
		$this->db->order_by("palualcodig", "desc");	
		//$this->db->limit(15000);
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select(" DATE_FORMAT(tbalualunos.date_operacao,'%d/%m/%Y') AS datacadastro,  ");
		$this->db->select('tbcurcursos.*, tbensensino.* ,tbalualunos.* ');		
		$this->db->join('tbcurcursos','pcurcucodig = ialualcurso','left');
		$this->db->join('tbensensino','pensencodig = ialualinsen','left');	
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarContratoPorId($id)
	{
		
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$this->db->where('`ialualcodig`',$id);		
		$this->db->where('contrato_visivel', 1);		
		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get('tbcontrestempr')->result();

	}

	public function pegarRecrutamentoPorId($id)
	{
		
		$this->db->select('  `tbempempres`.`sempemrazao`,  `tbempempres`.`sempemcnpj`,  `tbensensino`.`sensennome`,  `tbalualunos`.*, tbrecrutamento.*');

		$this->db->join('tbrecrutamento','tbrecrutamento.`recrut_id` = tbrecrutaluno.`recrutalu_recrut_id`');
		$this->db->join('tbalualunos','tbrecrutaluno.`recrutalu_estud_id` = tbalualunos.`palualcodig`');
		$this->db->join('tbempempres','`tbempempres`.`pempemcodig` = `tbrecrutamento`.`recrut_empr_id`');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$this->db->where('tbalualunos.`palualcodig`',$id);		
		$this->db->where('tbrecrutaluno.`recrutalu_selec`', '1');		
		$this->db->order_by("tbrecrutamento.vaga_id", "desc");	
		return $this->db->get('tbrecrutaluno')->result();

	}



	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

    // Bruno Magnata - Tratamento CPF Duplicado
	public function checarCpfEstudantes($cpf)
	{

	$this->db->select('*');
	$this->db->where('salualcpf',$cpf);
	return $this->db->get($this->tabela)->result();

	  if($this->db->affected_rows() == '1')
	{
	  return true;
	}

	return false;

	}


	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}
	public function tadasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}
	public function todosCursos()
	{
		$this->db->select('*');	
		$this->db->where('curso_visivel', 1);	
		$this->db->order_by("scurcunome", "asc");			
		return $this->db->get('tbcurcursos')->result();
	}
	public function todosInstEnsino()
	{
		$this->db->select('*');	
		$this->db->order_by("sensennome", "asc");			
		return $this->db->get('tbensensino')->result();
	}

	public function selecionarCidadeEstudantes($termo)
	{
		$this->db->select('trim(salualcidad) AS id, trim(salualcidad) AS text');
		$this->db->group_by('salualcidad'); 		
		$this->db->like('salualcidad',$termo);			
		$this->db->where('estudante_visivel', 1);
		$this->db->limit(10);
		return $this->db->get('tbalualunos')->result_array();
	}

	public function selecionarCursos($termo)
	{
		$this->db->select('pcurcucodig AS id, scurcunome AS text'); 		
		$this->db->like('scurcunome',$termo);			
		$this->db->where('curso_visivel', 1);
		$this->db->limit(10);
		return $this->db->get('tbcurcursos')->result_array();
	}

	public function agenciaEmitente()
	{
		$this->db->select('*');		
		return $this->db->get('emitente')->result();			
		// return $this->db->get('tbemitente')->result(); // Utilizar sempre a tabela de EMITENTE padrão do sistema WDM-GESTOR | WDM-CONTROLLER | WDM-PDV | WDM-OUVIDORIA
	}



}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */