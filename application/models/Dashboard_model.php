<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	// private $tabela  = 'tbcurcursos';
	// private $id      = 'pcurcucodig';
	// private $visivel = 'curso_visivel';

public function tcePendente()
{

	$query = $this->db->query(" SELECT COUNT(*) AS total FROM tbcontrestempr WHERE ((contrestempr_data_ini + INTERVAL '10' DAY) < CURRENT_DATE ) AND contrestempr_termo_C_E = 0 ");

    return $query->result();

}

public function taPendente()
{
	$query = $this->db->query(" SELECT COUNT(*)  AS total  FROM `tbcontrestempr` WHERE (`contrestempr_data_renI` <> '0000-00-00' AND (`contrestempr_data_renI` + INTERVAL '10' DAY) < CURRENT_DATE )
    AND `contrestempr_termo_A_I` = 0 ");

    return $query->result();

}

public function contratoVencido($cron = null)
{
	$sql = ($cron) ? 'F.data_term, F.salualnome, F.sempemfanta, F.salualemail, DATEDIFF(NOW(),F.data_term) AS dias_vencido ' : 'COUNT(*)  AS total';


	$query = $this->db->query(" SELECT 

		{$sql}

	/*COUNT(*)  AS total	
	 F.data_term,
	F.salualnome,
	F.sempemfanta */
	FROM 
	( SELECT 
	CASE
	    WHEN ((`contrestempr_data_rescisao` <> '0000-00-00') AND (`contrestempr_data_rescisao` <> '1970-01-01')) THEN contrestempr_data_rescisao
	    WHEN ((`contrestempr_data_renIII` <> '0000-00-00') AND (`contrestempr_data_renIII` <> '1970-01-01')) THEN contrestempr_data_renIII
	    WHEN ((`contrestempr_data_renII` <> '0000-00-00') AND (`contrestempr_data_renII` <> '1970-01-01')) THEN contrestempr_data_renII
	    WHEN ((`contrestempr_data_renI` <> '0000-00-00') AND (`contrestempr_data_renI` <> '1970-01-01')) THEN contrestempr_data_renI
	    WHEN ((`contrestempr_data_term` <> '0000-00-00') AND (`contrestempr_data_term` <> '1970-01-01')) THEN contrestempr_data_term
	    ELSE 'False'
	END AS data_term, 
	tba.`salualnome`, tbe.`sempemfanta`, tba.`salualemail`
	FROM
	  tbcontrestempr AS tbc
	INNER JOIN `tbalualunos` AS tba ON `tba`.`palualcodig` =  `tbc`.`ialualcodig`
	INNER JOIN `tbempempres` AS tbe ON `tbe`.`pempemcodig` =  `tbc`.`iempemcodig`
	  WHERE 
	  `contrestempr_status` = 'Ativo'
	  AND `contrato_visivel` = 1) AS F	  
	  WHERE F.data_term < CURRENT_DATE  ");

    return $query->result();

    // return $this->db->get('vendas')->result_array();

}

public function contrato20Vencer($cron = null)
{
	$sql = ($cron) ? 'F.data_term, F.salualnome, F.sempemfanta, F.salualemail, DATEDIFF(NOW(),F.data_term) AS dias_vencido ' : 'COUNT(*)  AS total';

	$query = $this->db->query("SELECT 
		{$sql}

	/* COUNT(*)  AS total	
	 F.data_term,
	F.salualnome,
	F.sempemfanta */
	FROM 
	(SELECT 
	CASE
	    WHEN ((`contrestempr_data_rescisao` <> '0000-00-00') AND (`contrestempr_data_rescisao` <> '1970-01-01')) THEN contrestempr_data_rescisao
	    WHEN ((`contrestempr_data_renIII` <> '0000-00-00') AND (`contrestempr_data_renIII` <> '1970-01-01')) THEN contrestempr_data_renIII
	    WHEN ((`contrestempr_data_renII` <> '0000-00-00') AND (`contrestempr_data_renII` <> '1970-01-01')) THEN contrestempr_data_renII
	    WHEN ((`contrestempr_data_renI` <> '0000-00-00') AND (`contrestempr_data_renI` <> '1970-01-01')) THEN contrestempr_data_renI
	    WHEN ((`contrestempr_data_term` <> '0000-00-00') AND (`contrestempr_data_term` <> '1970-01-01')) THEN contrestempr_data_term
	    ELSE 'False'
	END AS data_term, 
	tba.`salualnome`, tbe.`sempemfanta`, tba.`salualemail`
	FROM
	  tbcontrestempr AS tbc
	INNER JOIN `tbalualunos` AS tba ON `tba`.`palualcodig` =  `tbc`.`ialualcodig`
	INNER JOIN `tbempempres` AS tbe ON `tbe`.`pempemcodig` =  `tbc`.`iempemcodig`
	  WHERE 
	  `contrestempr_status` = 'Ativo'
	  AND `contrato_visivel` = 1) AS F
	  
	  WHERE  F.data_term BETWEEN CURDATE() AND CURDATE() + INTERVAL /*35*/ 30 DAY ");

    return $query->result();
}

public function solicRecrut()
{
	$query = $this->db->query("    SELECT COUNT(*)  AS total  FROM `tbrecrutamento` WHERE `recrut_solicitacao_site` = 0 AND `recrutamento_visivel` = 1; ");

    return $query->result();

}

public function solicContra()
{
	$query = $this->db->query("  SELECT COUNT(*)  AS total  FROM `tbcontrestempr` WHERE `contrestempr_solicitacao_site` = 0 AND `contrato_visivel` = 1; ");

    return $query->result();

}



public function graficoEstudante()
{

$query = $this->db->query("
			SELECT 'Jan' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 1 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 1 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 1 ) AS fim
FROM tbalualunos


UNION

SELECT 'Fev' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 2 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 2 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 2 ) AS fim
FROM tbalualunos

UNION

SELECT 'Mar' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 3 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 3 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 3 ) AS fim
FROM tbalualunos

UNION

SELECT 'Abr' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 4 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 4 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 4 ) AS fim
FROM tbalualunos

UNION

SELECT 'Mai' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 5 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 5 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 5 ) AS fim
FROM tbalualunos

UNION

SELECT 'Jun' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 6 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 6 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 6 ) AS fim
FROM tbalualunos

UNION

SELECT 'Jul' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 7 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 7 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 7 ) AS fim
FROM tbalualunos

UNION

SELECT 'Ago' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 8 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 8 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 8 ) AS fim
FROM tbalualunos

UNION

SELECT 'Set' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 9 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 9 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 9 ) AS fim
FROM tbalualunos

UNION

SELECT 'Out' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 10 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 10 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 10 ) AS fim
FROM tbalualunos

UNION

SELECT 'Nov' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 11 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 11 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 11 ) AS fim
FROM tbalualunos

UNION

SELECT 'Dez' AS Ano, 
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 12 ) AS atual,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 12 ) AS meio,
( SELECT  COUNT(*) FROM tbalualunos WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 12 ) AS fim
FROM tbalualunos

       	");

       return $query->result();
	
	}


public function graficoEmpresa()
	{

       $query = $this->db->query("

SELECT 'Jan' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 1 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 1 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 1 ) AS fim
FROM tbempempres


UNION

SELECT 'Fev' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 2 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 2 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 2 ) AS fim
FROM tbempempres

UNION

SELECT 'Mar' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 3 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 3 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 3 ) AS fim
FROM tbempempres

UNION

SELECT 'Abr' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 4 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 4 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 4 ) AS fim
FROM tbempempres

UNION

SELECT 'Mai' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 5 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 5 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 5 ) AS fim
FROM tbempempres

UNION

SELECT 'Jun' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 6 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 6 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 6 ) AS fim
FROM tbempempres

UNION

SELECT 'Jul' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 7 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 7 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 7 ) AS fim
FROM tbempempres

UNION

SELECT 'Ago' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 8 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 8 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 8 ) AS fim
FROM tbempempres

UNION

SELECT 'Set' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 9 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 9 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 9 ) AS fim
FROM tbempempres

UNION

SELECT 'Out' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 10 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 10 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 10 ) AS fim
FROM tbempempres

UNION

SELECT 'Nov' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 11 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 11 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 11 ) AS fim
FROM tbempempres

UNION

SELECT 'Dez' AS Ano, 
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) AND  MONTH(date_cadastro) = 12 ) AS atual,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(date_cadastro) = 12 ) AS meio,
( SELECT  COUNT(*) FROM tbempempres WHERE YEAR(date_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(date_cadastro) = 12 ) AS fim
FROM tbempempres

       	");

       return $query->result();
	
	}


public function graficoVaga()
	{

       $query = $this->db->query("

SELECT 'Jan' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 1 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 1 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 1 ) AS fim
FROM tbestvagas


UNION

SELECT 'Fev' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 2 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 2 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 2 ) AS fim
FROM tbestvagas

UNION

SELECT 'Mar' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 3 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 3 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 3 ) AS fim
FROM tbestvagas

UNION

SELECT 'Abr' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 4 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 4 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 4 ) AS fim
FROM tbestvagas

UNION

SELECT 'Mai' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 5 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 5 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 5 ) AS fim
FROM tbestvagas

UNION

SELECT 'Jun' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 6 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 6 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 6 ) AS fim
FROM tbestvagas

UNION

SELECT 'Jul' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 7 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 7 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 7 ) AS fim
FROM tbestvagas

UNION

SELECT 'Ago' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 8 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 8 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 8 ) AS fim
FROM tbestvagas

UNION

SELECT 'Set' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 9 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 9 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 9 ) AS fim
FROM tbestvagas

UNION

SELECT 'Out' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 10 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 10 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 10 ) AS fim
FROM tbestvagas

UNION

SELECT 'Nov' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 11 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 11 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 11 ) AS fim
FROM tbestvagas

UNION

SELECT 'Dez' AS Ano, 
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) AND  MONTH(date_operacao) = 12 ) AS atual,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -1  AND  MONTH(date_operacao) = 12 ) AS meio,
( SELECT  COUNT(*) FROM tbestvagas WHERE YEAR(date_operacao) = YEAR(CURDATE()) -2  AND  MONTH(date_operacao) = 12 ) AS fim
FROM tbestvagas

       	");

       return $query->result();
	
	}	


	


}

/* End of file Curso_model.php */
/* Location: ./application/models/Curso_model.php */