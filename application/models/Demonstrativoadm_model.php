<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demonstrativoadm_model extends CI_Model {

	private $tabela  = 'tbdemonstrativoadm';
	private $id      = 'demonstrativoadm_id';
	private $visivel = 'demonstrativoadm_visivel';

	public function listar()
	{
		$this->db->select('*');
		//$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where($this->visivel, 1);
		$this->db->order_by("demonstrativoadm_id", "desc");		
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}




}

/* End of file Curso_model.php */
/* Location: ./application/models/Curso_model.php */