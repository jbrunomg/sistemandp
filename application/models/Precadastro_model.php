<?php
ini_set('memory_limit', '2048M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa
defined('BASEPATH') OR exit('No direct script access allowed');

class Precadastro_model extends CI_Model {

	private $tabela  = 'tbprecadastro';
	private $id      = 'precadastro_id';
	private $visivel = 'precadastro_visivel';

	public function listar()
	{
		$this->db->select('*');		
		$this->db->join('tbempempres', 'tbempempres.pempemcodig = tbprecadastro.precadastro_empresa_id','LEFT');
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function inserircron($dados)
	{
		$this->db->insert('tbmaladireta_enviar', $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editarcron($id,$dados)
	{
		//var_dump($dados);die();

		$valor = ['precadastro_email_enviado'=> $dados];
		
		$this->db->where('precadastro_id', $id);
		$this->db->update('tbprecadastro', $valor);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function listarEstudantes()
	{
		$this->db->select('palualcodig,salualnome');		
		$this->db->where('estudante_visivel', 1);	
		return $this->db->get('tbalualunos')->result();	
	}

	public function listarCursos()
	{
		$this->db->select('pcurcucodig,scurcunome');		
		$this->db->where('curso_visivel', 1);	
		return $this->db->get('tbcurcursos')->result();	
	}

	public function pegarEmails($cursos, $estudantes, $sexo)
	{
		//var_dump($sexo);die();

		$this->db->select('salualemaill');

		if($cursos != ''){
			$this->db->where_in('ialualcurso', $cursos);	
		}			

		if($estudantes != ''){
			$this->db->where_in('palualcodig', $estudantes);	
		}

		if($sexo != ''){			
			$this->db->where('ealualsexo', $sexo);	
		}			
		
		return $this->db->get('tbalualunos')->result();	
	}

	public function pegarEmailsCron()
	{		
		$this->db->select('*');				
		$this->db->where('precadastro_status', 'Preparado');
		$this->db->where('precadastro_visivel', 1);
		$this->db->where('precadastro_email_enviado < 11');
		$this->db->limit(400);
		return $this->db->get('tbprecadastro')->result_array();
	}

	public function selecionarEstudantes($termo)
	{
		$this->db->select('palualcodig AS id, salualnome AS text');		
		$this->db->like('salualnome',$termo);
		$this->db->or_like('salualcpf',$termo);			
		$this->db->where('estudante_visivel', 1);
		$this->db->limit(10);
		return $this->db->get('tbalualunos')->result_array();
	}

	public function todasEmpresas()
	{
		$this->db->select('*');	
		$this->db->order_by("sempemrazao", "asc");				
		return $this->db->get('tbempempres')->result();

	}
}

/* End of file Precadastromodel.php */
/* Location: ./application/models/Emails_model.php */