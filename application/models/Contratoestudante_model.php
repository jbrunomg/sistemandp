<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratoestudante_model extends CI_Model {

	private $tabela  = 'tbcontrestempr';
	private $id      = 'contrestempr_id';
	private $visivel = 'contrato_visivel';

	public function listar()
	{
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		// $this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$this->db->where($this->visivel, 1);		
		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get($this->tabela)->result();	
	}

	public function tcePendenteListar()
	{	
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		// $this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$Sql = "((contrestempr_data_ini + INTERVAL '10' DAY) < CURRENT_DATE() ) AND contrestempr_termo_C_E = 0 ";

		$this->db->where($Sql);		
		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get($this->tabela)->result();	

	}

	public function taPendenteListar()
	{	
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		// $this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$Sql = " (contrestempr_data_renI <> '0000-00-00' AND (contrestempr_data_renI + INTERVAL '10' DAY) < CURRENT_DATE() ) AND contrestempr_termo_A_I = 0 ";

		$this->db->where($Sql);		
		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get($this->tabela)->result();	

	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados,$returnId = false)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			 if($returnId == true){
                return $this->db->insert_id($this->tabela);
            }
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}

	public function todosEstudante()
	{
		$this->db->select('*');	
		$this->db->order_by("salualnome", "asc");	
		$this->db->limit(10);			
		return $this->db->get('tbalualunos')->result();
	}

	public function pegarEstudante($id)
	{
		$this->db->select('*');		
		$this->db->where('estudante_visivel',1);
		$this->db->where('palualcodig',$id);		
		return $this->db->get('tbalualunos')->result();
	}
		
	public function todasEmpresas()
	{
		$this->db->select('pempemcodig,sempemrazao,sempemfanta');	
		$this->db->where('empresa_visivel',1);
		$this->db->order_by("sempemrazao", "asc");				
		return $this->db->get('tbempempres')->result();
	}

	public function todasVagas()
	{
		$this->db->select('*');	
		$this->db->where('vaga_visivel',1);	
		$this->db->order_by("pestvacodig", "desc");				
		return $this->db->get('tbestvagas')->result();
	}

	public function agenciaEmitente()
	{
		$this->db->select('*');	
		$this->db->join('`tbuf`','`emitente`.`uf` = `tbuf`.`cd_uf`','LEFT');	

		return $this->db->get('emitente')->result();			
		// return $this->db->get('tbemitente')->result(); // Utilizar sempre a tabela de EMITENTE padrão do sistema WDM-GESTOR | WDM-CONTROLLER | WDM-PDV | WDM-OUVIDORIA
	}


	public function carregarContratoId($id)
	{
		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbcurcursos.scurcunome, tbensensino.*, tbalualunos.*, tbcontrestempr.*, tbempempres.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbcurcursos','tbalualunos.ialualcurso = tbcurcursos.pcurcucodig');

		$this->db->where($this->id,$id);
		$this->db->where($this->visivel, 1);		
		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get($this->tabela)->result();	
	}

	public function duplicidade($estudante,$empresas)
	{
		$this->db->select('*');	
		$this->db->where('tbcontrestempr.iempemcodig', $empresas);
		$this->db->where('tbcontrestempr.ialualcodig', $estudante);
		$this->db->where('tbcontrestempr.contrestempr_status', 'Ativo'); // 				
		return $this->db->get('tbcontrestempr')->result();
	}

	public function selecionarEmpresas($termo)
	{
		$this->db->select('pempemcodig AS id, sempemfanta AS text');
		$this->db->where('empresa_visivel', 1);

		$this->db->where("(sempemfanta LIKE '%".$termo."%' OR sempemrazao LIKE '%".$termo."%')", NULL, FALSE);
		// $this->db->like('( sempemfanta',$termo);
		// $this->db->or_like('sempemrazao',$termo,')');	
		$this->db->limit(10);
		return $this->db->get('tbempempres')->result_array();
	}

	public function pegarVagaPeloID($id)
	{

		$this->db->select(' CONCAT(
	    IF(recrut_bolsa_auxilio = 1, "", "Bolsa"), 
	    IF( recrut_transporte = 1, ""," + Vale Transporte"),
	    IF( recrut_refei = 1,""," + Refeição"),
	    IF( recrut_outro = 1,""," + Outros")
		  ) AS beneficios,
		  tbrecrutamento.* ');		
		$this->db->where('vaga_id',$id);	
		$this->db->where('recrutamento_visivel',1);		
		return $this->db->get('tbrecrutamento')->result();
	}


	public function solicitacaoContr()
	{

		$this->db->select('tbempempres.sempemrazao,tbempempres.sempemcnpj, tbensensino.sensennome, tbalualunos.*, tbcontrestempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		// $this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');

		$this->db->where($this->visivel, 1);
		$this->db->where('`contrestempr_solicitacao_site`', 0);

		$this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		return $this->db->get($this->tabela)->result();	
	}


}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */
