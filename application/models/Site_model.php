<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_model extends CI_Model {


	public function getConfiguracaosite()
    {
        $this->db->select('*');

        $this->db->from('configuracao_site');

        $this->db->where('site_id','1');

        return $this->db->get()->result();

    }


	public function atualizarConfiguracaosite($data)
    {
    	
        $this->db->where('site_id',1);

        $this->db->update('configuracao_site', $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;   
    }


}

/* End of file Site_model.php */
/* Location: ./application/models/Site_model.php */