<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailsempresa_model extends CI_Model {

	private $tabela  = 'tbmaladiretaempresa';
	private $id      = 'maladireta_id';
	private $visivel = 'maladireta_visivel';

	public function listar()
	{
		$this->db->select('*');		
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function listarEmpresa()
	{
		$this->db->select('pempemcodig,sempemrazao');		
		$this->db->where('empresa_visivel', 1);	
		return $this->db->get('tbempempres')->result();	
	}

	public function pegarEmails($empresas)
	{
		$this->db->select('sempememail');

		if($empresas != ''){
			$this->db->where_in('pempemcodig', $empresas);	
		}	
	
		return $this->db->get('tbempempres')->result();	
	}

}

/* End of file Emails_model.php */
/* Location: ./application/models/Emails_model.php */