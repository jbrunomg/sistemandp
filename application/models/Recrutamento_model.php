<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Recrutamento_model extends CI_Model
{

	private $tabela  = 'tbrecrutamento';

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1') {
			return TRUE;
		}

		return FALSE;
	}

	public function add($table,$data,$returnId = false){

        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1'){

            if($returnId == true){
                return $this->db->insert_id($table);
            }
            return TRUE;
        }   

        return FALSE;  

    }  

	public function listar()
	{
		$this->db->select('tbrecrutamento.recrut_id as id, tbrecrutamento.vaga_id, tbrecrutamento.recrut_status,  tbempempres.sempemfanta as empresa_nome, DATE_FORMAT(tbrecrutamento.recrut_data_cadastro, "%d/%m/%Y") as data_cadastro');
		$this->db->join('tbempempres', 'tbempempres.pempemcodig = tbrecrutamento.recrut_empr_id');
		$this->db->where('tbrecrutamento.recrutamento_visivel', '1');
		$this->db->order_by("tbrecrutamento.recrut_id", "desc");
		return $this->db->get($this->tabela)->result();
	}

	public function excluir($id, $dados)
	{
		$this->db->where('recrut_id', $id);
		$this->db->update($this->tabela, $dados);

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');
		$this->db->where('recrut_id',$id);		
		$this->db->where('recrutamento_visivel', '1');
		return $this->db->get($this->tabela)->result();
	}

	public function pegarRecrutPorId($id)
	{
		$this->db->select("*,  IF(paluvagcodig IS NULL, 'NO', 'YES') AS interesado,  DATE_FORMAT(`tbalualunos`.`date_operacao`,'%d-%m-%Y') AS `date_operacao`  ");	
		$this->db->join('tbrecrutamento', 'tbrecrutaluno.recrutalu_recrut_id = tbrecrutamento.recrut_id');
		$this->db->join('tbalualunos', 'tbrecrutaluno.recrutalu_estud_id = tbalualunos.palualcodig');
		$this->db->join('tbensensino', 'tbensensino.pensencodig = tbalualunos.ialualinsen');


		$this->db->join('tbestvagas', ' `tbestvagas`.`recrutamento_id` = `tbrecrutaluno`.`recrutalu_recrut_id`','LEFT');
		$this->db->join('tbalunovagas', '`tbalunovagas`.`festvacodig` = `tbestvagas`.`pestvacodig` AND `tbalunovagas`.`falualcodig` = tbalualunos.`palualcodig` AND  `tbalunovagas`.`falualcodig` IS NOT NULL','LEFT');	



		$this->db->where('recrutalu_recrut_id',$id);
		$this->db->order_by("paluvagcodig,recrutalu_selec,recrutalu_aprov,tbalualunos.date_operacao","desc");

		return $this->db->get('tbrecrutaluno')->result();
	}

	public function pegarRecrutFinalPorId($id)
	{
		$this->db->select('*');	
		$this->db->join('tbrecrutamento', 'tbrecrutaluno.recrutalu_recrut_id = tbrecrutamento.recrut_id');
		$this->db->join('tbalualunos', 'tbrecrutaluno.recrutalu_estud_id = tbalualunos.palualcodig');
		$this->db->join('tbensensino', 'tbensensino.pensencodig = tbalualunos.ialualinsen');
		$this->db->join('tbempempres', 'tbempempres.pempemcodig = tbrecrutamento.recrut_empr_id');	
		$this->db->where('recrutalu_recrut_id',$id);
		$this->db->where('(recrutalu_aprov','1');
		$this->db->or_where('recrutalu_selec = "1")');
		$this->db->order_by('recrutalu_aprov','desc');
		return $this->db->get('tbrecrutaluno')->result();

	}

	public function pegarRecrutSelecionadoPorId($id)
	{
		$this->db->select('*');	
		$this->db->join('tbrecrutamento', 'tbrecrutaluno.recrutalu_recrut_id = tbrecrutamento.recrut_id');
		$this->db->join('tbalualunos', 'tbrecrutaluno.recrutalu_estud_id = tbalualunos.palualcodig');
		$this->db->join('tbensensino', 'tbensensino.pensencodig = tbalualunos.ialualinsen');
		$this->db->join('tbempempres', 'tbempempres.pempemcodig = tbrecrutamento.recrut_empr_id');	
		$this->db->where('recrutalu_recrut_id',$id);
		$this->db->where('recrutalu_selec = "1"');
		$this->db->order_by('recrutalu_aprov','desc');
		return $this->db->get('tbrecrutaluno')->result();

	}
	
	public function editar($id,$dados)
	{
		$this->db->where('recrut_id', $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function  pegarEstudante($curso = null, $nivel = null, $curso2 = null, $nivel2 = null, $curso3 = null,  $nivel3 = null, $curso4 = null,  $nivel4 = null, $sexo = null, $disponib = null)
	{ 
		
		$this->db->select("'' AS 'idRecrut', tbalualunos.*, tbensensino.* ");
		$this->db->join('tbensensino', 'tbensensino.pensencodig = tbalualunos.ialualinsen');
		
		$this->db->where_in('`ealualsexo`', $sexo);

		$where_like = " ( "; 
			foreach($disponib as $key => $value) {

				if($key == 0) {
			    	$where_like .=  ' salualdispo like "%'.$value.'%" ';			        
			    } else {
			    	$where_like .=  ' OR salualdispo like "%'.$value.'%"';			        
			    }
			}
		$where_like .= ")";

		$this->db->where($where_like);


        $where_curso = " ( "; 

		if($curso  != null){
            $where_curso .= "( ialualcurso = ".$curso." AND  ialualcurso_nivel = ".$this->db->escape($nivel).")";            
        } 

        if($curso2  != null){
            $where_curso .= "OR ( ialualcurso = ".$curso2." AND  ialualcurso_nivel = ".$this->db->escape($nivel2).")";            
        } 

        if($curso3  != null){
            $where_curso .= "OR ( ialualcurso = ".$curso3." AND  ialualcurso_nivel = ".$this->db->escape($nivel3).")";            
        } 

        if($curso4  != null){
            $where_curso .= "OR ( ialualcurso = ".$curso4." AND  ialualcurso_nivel = ".$this->db->escape($nivel4).")";            
        } 

        // CURSO 02

        if($curso  != null){
            $where_curso .= "OR ( ialualcurso2 = ".$curso." AND  ialualcurso_nivel2 = ".$this->db->escape($nivel).")";            
        } 

        if($curso2  != null){
            $where_curso .= "OR ( ialualcurso2 = ".$curso2." AND  ialualcurso_nivel2 = ".$this->db->escape($nivel2).")";            
        } 

        if($curso3  != null){
            $where_curso .= "OR ( ialualcurso2 = ".$curso3." AND  ialualcurso_nivel2 = ".$this->db->escape($nivel3).")";            
        } 

        if($curso4  != null){
            $where_curso .= "OR ( ialualcurso2 = ".$curso4." AND  ialualcurso_nivel2 = ".$this->db->escape($nivel4).")";            
        }

		$where_curso .= ")";

		$this->db->where($where_curso);   


		// $this->db->where_in('`salualdispo`', $disponib);
		$this->db->order_by("tbalualunos.date_cadastro", "desc");


		return $this->db->get('`tbalualunos`')->result();
	}



	public function recrutSelecionando($idEstudante,$idRecrutament,$dados)
	{
		$this->db->where('recrutalu_estud_id', $idEstudante);
        $this->db->where('recrutalu_recrut_id', $idRecrutament);
		$this->db->update('tbrecrutaluno', $dados);

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function inserirVaga($id)
	{
		// code...

		$query = "INSERT INTO `tbestvagas` (`sestvaempresa`,`sestvaativi`,`sestvacarho`,`sestvacidad`,`sestvabairr`, `sestvauf`, `sestvasexo`, `eestvabolsa`, `eestva_valorauxilio`, `eestvavale`, `eestvarefei`, `eestvaoutros`,
				 vaga_curso, vaga_nivel,
				 vaga_curso2, vaga_nivel2,
				 vaga_curso3, vaga_nivel3,
				 vaga_curso4, vaga_nivel4,
				 sestvaserie,sestvamodul,sestvaperio,
				 observacaovaga,
				 recrutamento_id, testvaexpir )		
		SELECT `recrut_empr_id`,`recrut_atividade`,`recrut_horario`,tb_cidades.`id` ,`recrut_bairro`, `recrut_uf` ,`recrut_sexo`, `recrut_bolsa_auxilio`, `recrut_valor_auxilio`, `recrut_transporte`, `recrut_refei`, `recrut_outro`, 
				 recrut_curso, recrut_nivel,
				 recrut_curso2, recrut_nivel2,
				 recrut_curso3, recrut_nivel3,
				 recrut_curso4, recrut_nivel4,
				 recrut_grau_serie,recrut_grau_modulo,recrut_grau_periodo,
				 recrut_observacao,
				 {$id}, recrut_dataexpir     
		 FROM `tbrecrutamento`
		 LEFT JOIN `tb_cidades` ON tbrecrutamento.`recrut_cidade` = tb_cidades.`nome` AND tbrecrutamento.`recrut_uf` = tb_cidades.`uf`
		WHERE `recrut_id` = ".$id ;

		// DATE_ADD(CURDATE(), INTERVAL 15 DAY)

		// return $this->db->query($query);


		$this->db->query($query);

		if ($this->db->affected_rows() == '1'){

            if(true == true){
                return $this->db->insert_id('`tbestvagas`');
            }
            return TRUE;
        }   

        return FALSE;



	}


    public function editarVaga($idVaga, $idRecrut, $dados)
	{
		$this->db->where('pestvacodig', $idVaga);
		$this->db->where('recrutamento_id', $idRecrut);
		$this->db->update('tbestvagas', $dados);

		if ($this->db->affected_rows() == '1') {
			return true;
		}

		return false;
	}

	public function getEmitente()
	{
		$this->db->select('*');
		return $this->db->get('emitente')->result();

	}

	// Inserir os email's na base WDM-Gestor que a CRON enviara os email's a cada 15Mint
    public function inserircron($table, $dados)
    {
        $this->db->insert($table, $dados);     

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE; 
    }


    public function solicitacaoRecrut()
	{
		$this->db->select('tbrecrutamento.recrut_id as id, tbrecrutamento.vaga_id, tbrecrutamento.recrut_status,  tbempempres.sempemfanta as empresa_nome, DATE_FORMAT(tbrecrutamento.recrut_data_cadastro, "%d/%m/%Y") as data_cadastro');
		$this->db->join('tbempempres', 'tbempempres.pempemcodig = tbrecrutamento.recrut_empr_id');
		$this->db->where('tbrecrutamento.recrutamento_visivel', '1');
		$this->db->where('`recrut_solicitacao_site`', 0);
		$this->db->order_by("tbrecrutamento.recrut_id", "desc");
		return $this->db->get($this->tabela)->result();
	}


	 public function pegarRecrutAguardando()
	{
		$this->db->select(' * ');
		$this->db->join('tbempempresSSS', '`tbrecrutamento`.`recrut_empr_id` = `tbempempres`.`pempemcodig`');
		$this->db->where('tbrecrutamento.recrutamento_visivel', '1');
		$this->db->where('tbrecrutamento.`recrut_status`', 'AGUARDANDO');
		$this->db->where('DATEDIFF(NOW(),recrut_data_status) >','5');
		
		return $this->db->get($this->tabela)->result();
	}




}
