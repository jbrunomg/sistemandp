<!-- Form horizontal -->
<script>
    urlBase = '<?php echo base_url(); ?>';
</script>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Novo recrutamento</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <form id="formCadastro" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">       
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="inputEmpresa">Empresa</label>
                    <select id="empresa" name="empresa" class="form-control">
                        <option value="">Selecione</option>
                        <?php foreach ($empresa as $valor) { ?>
                            <option value="<?php echo $valor->pempemcodig; ?>"><?php echo $valor->sempemrazao; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="input">Usar endereço da empresa?</label>
                    <select id="usar" class="form-control">
                        <option value="sim">Sim</option>
                        <option value="nao">Não</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="input">Endereço estágio</label>
                    <input type="text" name="logradouro" class="form-control" id="logradouro" value="">
                </div>              
                <div class="form-group col-md-3">
                    <label for="input">Bairro</label>
                    <input type="text" name="bairro" class="form-control" id="bairro" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Cidade</label>
                    <input type="text" name="cidade" class="form-control" id="cidade" value="">
                </div>
                  <div class="form-group col-md-2">
                    <label for="input">Estado</label>
                    <input type="text" name="estado" class="form-control" id="estado" value="">
                </div>

                <div class="form-group col-md-3">
                    <label for="input">CEP</label>
                    <input type="text" class="form-control" placeholder="Cep" id="sempemcep" name="sempemcep" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Inicio</label>
                    <input type="text" class="form-control" placeholder="Inicio do estágio"  id="inicio" name="inicio" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Duração</label>
                    <input type="text" class="form-control" placeholder="Duração do Estágio"  id="duracao" name="duracao" value="">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Carga Horária</label>
                    <input type="text" class="form-control" placeholder="Horario trabalhando no estágio"  id="horario" name="horario" value="">
                </div>


                <legend class="text-bold">Benefícios:</legend>

                <div class="row">
                    <div class="col-md-2">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Bolsa</h6>
                          <label class="checkbox-inline">
                            <input type="checkbox" class="styled" name="bolsa" value="1">  
                            Checked
                          </label><br><br>
                          <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                          <input type="text"  class="form-control input-xs" name="valor_auxilio" id="valor_auxilio">
                        </div>
                    </div>
        
                  <div class="col-md-2">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Vale transporte</h6>
                      <label class="checkbox-inline">
                        <input type="checkbox" class="styled" name="vale" value="1">   
                        Checked
                      </label><br><br>
                        <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                        <input type="text"  class="form-control input-xs" name="recrut_valor_transporte" id="recrut_valor_transporte">
                    </div>
                  </div>

                  <div class="col-md-2">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Refeição</h6>
                      <label class="checkbox-inline">
                        <input type="checkbox" class="styled" name="refeicao" value="1">                 
                        Checked
                      </label><br><br>
                        <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                        <input type="text"  class="form-control input-xs" name="recrut_valor_refei" id="recrut_valor_refei">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Outros</h6>
                      <label class="checkbox-inline">
                        <input type="checkbox" class="styled" name="outros" value="1">                 
                        Checked
                      </label><br><br>
                        <h6 class="no-margin text-semibold">Informações</h6>
                        <input type="text"  class="form-control input-xs" name="recrut_valor_outro" id="recrut_valor_outro">
                    </div>
                  </div>
                </div>

   

                <legend class="text-bold">Cursos/Nível:</legend>

                <div class="row">
                  
                   <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Curso 01</h6>
                            <select id="curso" name="curso" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>
                           
                            <h6 class="no-margin text-semibold">Nível</h6>            
                            <select id="nivel" name="nivel" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 02</h6>
                            <select id="curso2" name="curso2" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>                                     
                            <select id="nivel2" name="nivel2" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 03</h6>
                            <select id="curso3" name="curso3" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>
                            <select id="nivel3" name="nivel3" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 04</h6>
                            <select id="curso4" name="curso4" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>
                            <select id="nivel4" name="nivel4" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">A partir:</label>
                        <div class="col-lg-10">
                          <div class="row">
                            <div class="col-md-3">
                              <input type="text" class="form-control" placeholder="digite ex: 3ª" name="recrut_grau_serie" id="recrut_grau_serie" >
                              <span class="help-block text-center">Série</span>
                            </div>

                            <div class="col-md-3">
                              <input type="text" class="form-control"  placeholder="digite ex: 4º" name="recrut_grau_modulo" id="recrut_grau_modulo" >
                              <span class="help-block text-center">Módulo</span>
                            </div>

                            <div class="col-md-3">
                              <input type="text" class="form-control"  placeholder="digite ex: 5º" name="recrut_grau_periodo" id="recrut_grau_periodo" >
                              <span class="help-block text-center">Período</span>
                            </div>
                          </div>
                        </div>
                    </div>


                </div>


                <legend class="text-bold">Dados da Vaga:</legend>


                <div class="form-group col-md-6">
                    <label for="input">Atividades previstas</label>
                    <input type="text" class="form-control" id="atv-previstas" name="atv-previstas">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Responsável</label>
                    <input type="text" class="form-control" name="responsavel" id="responsavel">
                </div>
                <div class="form-group col-md-3">
                    <label for="input">Horário para entrevista</label>
                    <input type="text" class="form-control" name="h-entrevista" id="h-entrevista">
                </div>


                <div class="form-group col-md-6">
                    <label for="input">Indicado pela empresa ?</label>
                    <select id="indicado" name="indicado" class="form-control">
                        <option value="0">Não | (Iremos, divulgar vaga no site)</option>
                        <option value="1">Sim | (Sem vaga no site)</option>
                    </select>
                </div>

                <div class="form-group col-md-3">
                    <label for="input">Período/Estágio</label>
                    <select id="periodo" name="periodo" class="form-control">
                        <option value="">Selecione</option>
                        <option value="Manha">Manhã</option>
                        <option value="Tarde">Tarde</option>
                        <option value="Noite">Noite</option>
                        <option value="Manha/Tarde">Manhã/Tarde</option>
                        <option value="Manha/Noite">Manhã/Noite</option>
                        <option value="Tarde/Noite">Tarde/Noite</option>
                        <option value="Integral">Integral</option>
                        <option value="EAD">EAD</option>
                    </select>
                </div>

                <div class="form-group col-md-3">
                    <label for="input">Gênero</label>
                    <select id="sexo" name="sexo" class="form-control">
                        <option value="Masculino">Masculino</option>
                        <option value="Feminino">Feminino</option>
                        <!-- <option value="Ambos">Ambos</option> -->
                        <option value="Ambos">Indiferente</option>
                        
                    </select>
                </div>
 
                <div class="form-group col-md-6">
                    <label for="input">Requisitos exigidos pela empresa</label>
                    <input type="text" class="form-control" name="requisitosExigidos" id="requisitosExigidos">
                </div>
                <div class="form-group col-md-6">
                    <label for="input">Autorizar - Divulgação (Rede Sociais)</label>
                    <select id="divulgacao" name="divulgacao" class="form-control">
                        <option value="1">Sim | (Exibir botão de compartilhamento no site)</option>
                        <option value="0">Não | (Não exibir botão de compartilhamento no site)</option>                        
                    </select>                    
                </div>


                

                <div class="form-group col-md-8">
                    <label for="input">Observação</label>
                    <input type="text" class="form-control" placeholder="Observação Recrutamento"  id="recrut_observacao" name="recrut_observacao" value="">
                </div>
                <div class="form-group col-md-4">
                    <label for="input">Data para expirar</label>
                    <input type="text" class="form-control classData" placeholder="Horario trabalhando no estágio"  id="recrut_dataexpir" name="recrut_dataexpir" value="">
                </div>

       





            </div>
            <!-- <button class="btn btn-primary" id="btn-cadastrar-novo" style="float: right;">Cadastrar</button> -->
     

            <div class="text-right">
                <button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </form>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" integrity="sha512-Rdk63VC+1UYzGSgd3u2iadi0joUrcwX0IWp2rTh6KXFoAmgOjRS99Vynz1lJPT8dLjvo6JZOqpAHJyfCEZ5KoA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $('#empresa').change(function() {
        console.log(urlBase)

        preencherDadosEmpresa();
    });

    $('#usar').change(function() {
        preencherDadosEmpresa();
    });

    function preencherDadosEmpresa() {
        if ($('#empresa').val() > 0 && $('#usar').val() == 'sim') {
            buscarDadosEmpresa();
        } else {
            $('#logradouro').val('');
            $('#bairro').val('');
            $('#sempemcep').val('');
            $('#cidade').val('');
            $('#estado').val('');
        }
    }

    function buscarDadosEmpresa() {
        $.ajax({
            type: "POST",
            url: urlBase + "recrutamento/pegarEmpresaPeloID",
            dataType: "json",
            data: {
                id: $('#empresa').val()
            },
            success: function(data) {
                $('#logradouro').val(data[0].logradouro);
                $('#bairro').val(data[0].bairro);
                $('#sempemcep').val(data[0].CEP);
                $('#cidade').val(data[0].cidade);
                $('#estado').val(data[0].uf);
            }
        });
    }
</script>