<!-- Form horizontal -->
<script>
    urlBase = '<?php echo base_url(); ?>';
</script>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Ficha de oferta de estágio</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <form id="formCadastro">

            <div class="form-row">         


                <div class="form-group col-md-12">

                    <label for="inputEmpresa">Empresa</label>

                    <select disabled id="empresa" name="empresa" class="form-control">
                        <option value="">Selecione</option>
                        <?php foreach ($empresa as $valor) { ?>
                            <option value="<?php echo $valor->pempemcodig; ?>" <?php if ($valor->pempemcodig == $data[0]->recrut_empr_id) {

                            echo 'selected';
                            } ?>><?php echo $valor->sempemrazao; ?></option>
                        <?php } ?>
                    </select>

                </div>

                <div class="form-group col-md-4">

                    <label for="input">Endereço estágio</label>

                    <input disabled type="text" name="logradouro" class="form-control" id="logradouro" value="<?php echo $data[0]->recrut_end_esta; ?>">

                </div>           

                <div class="form-group col-md-3">

                    <label for="input">Bairro</label>

                    <input disabled  type="text" name="bairro" class="form-control" id="bairro" value="<?php echo $data[0]->recrut_bairro; ?>">

                </div>

                <div class="form-group col-md-3">

                    <label for="input">Cidade</label>

                    <input disabled type="text" name="cidade" class="form-control" id="cidade" value="<?php echo $data[0]->recrut_cidade; ?>">

                </div>

                <div class="form-group col-md-2">

                    <label for="input">Estado</label>

                    <input disabled type="text" name="estado" class="form-control" id="estado" value="<?php echo $data[0]->recrut_uf; ?>">

                </div>



                <div class="form-group col-md-3">

                    <label for="input">CEP</label>

                    <input disabled type="text" class="form-control" placeholder="Cep" id="sempemcep" name="sempemcep" value="<?php echo $data[0]->recrut_cep; ?>">

                </div>

                <div class="form-group col-md-3">

                    <label for="input">Inicio</label>

                    <input disabled type="text" class="form-control" id="inicio" name="inicio" value="<?php echo $data[0]->recrut_inicio; ?>">

                </div>

                <div class="form-group col-md-3">

                    <label for="input">Duração</label>

                    <input disabled type="text" class="form-control" id="duracao" name="duracao" value="<?php echo $data[0]->recrut_duracao; ?>">

                </div>

                <div class="form-group col-md-3">

                    <label for="input">Horário</label>

                    <input disabled type="text" class="form-control" id="horario" name="horario" value="<?php echo $data[0]->recrut_horario; ?>">

                </div>





                <legend class="text-bold">Benefícios:</legend>



                <div class="row">

                    <div class="col-md-2">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Bolsa</h6>
                            <label class="checkbox-inline">
                                <input class="styled" name="bolsa" type="checkbox" value="1" <?php if($data[0]->recrut_bolsa_auxilio <> 0){ echo 'checked';}else{ echo '';} ?>>
                                Checked
                            </label><br><br>
                            <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                            <input disabled type="text"  class="form-control input-xs" name="valor_auxilio" id="valor_auxilio" value="<?php echo $data[0]->recrut_valor_auxilio; ?>" >                       

                        </div>
                    </div>                  

             
                    <div class="col-md-2">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Vale transporte</h6>
                            <label class="checkbox-inline">
                                <input class="styled" name="vale" type="checkbox" value="1"  <?php if($data[0]->recrut_transporte <> 0){ echo 'checked';}else{ echo '';} ?>>
                                Checked          
                            </label><br><br>
                            <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                            <input disabled type="text"  class="form-control input-xs" name="recrut_valor_transporte" id="recrut_valor_transporte" value="<?php echo $data[0]->recrut_valor_transporte; ?>" > 
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Refeição</h6>
                            <label class="checkbox-inline">
                                <input  class="styled" name="refeicao" type="checkbox" value="1"   <?php echo ($data[0]->recrut_refei == '1')?'checked':''; ?>>
                                Checked
                            </label><br><br>
                            <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                            <input disabled type="text"  class="form-control input-xs" name="recrut_valor_refei" id="recrut_valor_refei" value="<?php echo $data[0]->recrut_valor_refei; ?>" > 
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Outros</h6>
                            <label class="checkbox-inline">
                                <input  disabled class="styled" name="outro" type="checkbox" value="1"   <?php echo ($data[0]->recrut_outro == '1')?'checked':''; ?>>
                                Checked
                            </label><br><br>
                            <h6 class="no-margin text-semibold">Informações</h6>
                            <input disabled type="text"  class="form-control input-xs" name="recrut_valor_outro" id="recrut_valor_outro" value="<?php echo $data[0]->recrut_valor_outro; ?>" > 
                        </div>
                    </div>

                </div>  



                <legend class="text-bold">Cursos/Nivel:</legend>

                <div class="row">
                  
                   <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Curso 01</h6>
                                <select class="form-control" name="curso" id="curso">
                                    <option  disabled value="">Selecione</option>
                                    <?php foreach ($cursos as $valor) { ?>
                                          <?php $selected = ($valor->pcurcucodig == $data[0]->recrut_curso)?'SELECTED': ''; ?>
                                          <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                    <?php } ?>                                   
                                </select>

                            
                            <h6 class="no-margin text-semibold">Nível</h6>  
                                <select disabled id="nivel" name="nivel" class="form-control">
                                    <option value="" <?php if($data[0]->recrut_nivel == ''){ echo "SELECTED";}?>>Selecione</option>
                                    <option value="Medio" <?php if($data[0]->recrut_nivel == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                    <option value="Tecnico" <?php if($data[0]->recrut_nivel == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                    <option value="Superior" <?php if($data[0]->recrut_nivel == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                    <option value="Pos" <?php if($data[0]->recrut_nivel == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>
                                </select>


                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Curso 02</h6>
                                <select disabled class="form-control" name="curso2" id="curso2">
                                    <option  value="">Selecione</option>
                                    <?php foreach ($cursos as $valor) { ?>
                                          <?php $selected = ($valor->pcurcucodig == $data[0]->recrut_curso2)?'SELECTED': ''; ?>
                                          <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                    <?php } ?>                                   
                                </select>


                            <h6 class="no-margin text-semibold">Nível</h6>  
                                <select disabled id="nivel2" name="nivel2" class="form-control">
                                    <option value="" <?php if($data[0]->recrut_nivel2 == ''){ echo "SELECTED";}?>>Selecione</option>
                                    <option value="Medio" <?php if($data[0]->recrut_nivel2 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                    <option value="Tecnico" <?php if($data[0]->recrut_nivel2 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                    <option value="Superior" <?php if($data[0]->recrut_nivel2 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                    <option value="Pos" <?php if($data[0]->recrut_nivel2 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>
                                </select>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Curso 03</h6>
                                <select disabled class="form-control" name="curso3" id="curso3">
                                    <option  value="">Selecione</option>
                                    <?php foreach ($cursos as $valor) { ?>
                                          <?php $selected = ($valor->pcurcucodig == $data[0]->recrut_curso3)?'SELECTED': ''; ?>
                                          <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                    <?php } ?>                                   
                                </select>

                            <h6 class="no-margin text-semibold">Nível</h6>  
                                <select disabled id="nivel3" name="nivel3" class="form-control">
                                    <option value="" <?php if($data[0]->recrut_nivel3 == ''){ echo "SELECTED";}?>>Selecione</option>
                                    <option value="Medio" <?php if($data[0]->recrut_nivel3 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                    <option value="Tecnico" <?php if($data[0]->recrut_nivel3 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                    <option value="Superior" <?php if($data[0]->recrut_nivel3 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                    <option value="Pos" <?php if($data[0]->recrut_nivel3 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>
                                </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 04</h6>
                            <select disabled class="form-control" name="curso4" id="curso4">
                                <option  value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                      <?php $selected = ($valor->pcurcucodig == $data[0]->recrut_curso4)?'SELECTED': ''; ?>
                                      <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>                                   
                            </select>

                                      <h6 class="no-margin text-semibold">Nível</h6>  
                                <select id="nivel4" name="nivel4" class="form-control">
                                    <option value="" <?php if($data[0]->recrut_nivel4 == ''){ echo "SELECTED";}?>>Selecione</option>
                                    <option value="Medio" <?php if($data[0]->recrut_nivel4 == 'Medio'){ echo "SELECTED";}?>>Médio</option>
                                    <option value="Tecnico" <?php if($data[0]->recrut_nivel4 == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
                                    <option value="Superior" <?php if($data[0]->recrut_nivel4 == 'Superior'){ echo "SELECTED";}?>>Superior</option>
                                    <option value="Pos" <?php if($data[0]->recrut_nivel4 == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>
                                </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">A partir:</label>
                        <div class="col-lg-10">
                          <div class="row">
                            <div class="col-md-3">
                              <input disabled type="text" class="form-control" placeholder="digite ex: 3ª" name="recrut_grau_serie" id="recrut_grau_serie"  value="<?php echo $data[0]->recrut_grau_serie; ?>">
                              <span class="help-block text-center">Série</span>
                            </div>

                            <div class="col-md-3">
                              <input disabled type="text" class="form-control"  placeholder="digite ex: 4º" name="recrut_grau_modulo" id="recrut_grau_modulo" value="<?php echo $data[0]->recrut_grau_modulo; ?>" >
                              <span class="help-block text-center">Módulo</span>
                            </div>

                            <div class="col-md-3">
                              <input disabled type="text" class="form-control"  placeholder="digite ex: 5º" name="recrut_grau_periodo" id="recrut_grau_periodo" value="<?php echo $data[0]->recrut_grau_periodo; ?>">
                              <span class="help-block text-center">Período</span>
                            </div>
                          </div>
                        </div>
                    </div>



                </div>



            <legend class="text-bold">Dados da Vaga:</legend>


                <div class="form-group col-md-6">
                    <label for="input">Atividades previstas</label>
                    <input disabled type="text" class="form-control" id="atv-previstas" name="atv-previstas" value="<?php echo $data[0]->recrut_atividade; ?>">
                </div>

                <div class="form-group col-md-3">
                    <label for="input">Responsável</label>
                    <input disabled type="text" class="form-control" name="responsavel" id="responsavel" value="<?php echo $data[0]->recrut_respo; ?>">
                </div>

                <div class="form-group col-md-3">
                    <label for="input">Horário para entrevista</label>
                    <input disabled type="text" class="form-control" name="h-entrevista" id="h-entrevista" value="<?php echo $data[0]->recrut_hs_entrev; ?>">
                </div>



                <div class="form-group col-md-6">
                    <label for="input">Indicado pela empresa</label>
                    <select disabled id="indicado" name="indicado" class="form-control">
                        <option value="0" <?php if ($data[0]->recrut_indicado == '0') { echo 'SELECTED'; } ?>>Não | (Iremos, divulgar vaga no site)</option>
                        <option value="1" <?php if ($data[0]->recrut_indicado == '1') { echo 'SELECTED'; } ?>>Sim | (Sem vaga no site)</option>
                    </select>

                </div>

                <div class="form-group col-md-3">

                    <label for="input">Período/Estágio</label>
                    <select disabled id="periodo" name="periodo" class="form-control">
                        <option value="" <?php if($data[0]->recrut_periodo == ''){ echo "SELECTED";}?>>Selecione</option>
                        <option value="Manha" <?php if($data[0]->recrut_periodo == 'Manha'){ echo "SELECTED";}?>>Manhã</option>
                        <option value="Tarde" <?php if($data[0]->recrut_periodo == 'Tarde'){ echo "SELECTED";}?>>Tarde</option>
                        <option value="Noite" <?php if($data[0]->recrut_periodo == 'Noite'){ echo "SELECTED";}?>>Noite</option>
                        <option value="Manha/Tarde" <?php if($data[0]->recrut_periodo == 'Manha/Tarde'){ echo "SELECTED";}?>>Manhã/Tarde</option>
                        <option value="Manha/Noite" <?php if($data[0]->recrut_periodo == 'Manha/Noite'){ echo "SELECTED";}?>>Manhã/Noite</option>
                        <option value="Tarde/Noite" <?php if($data[0]->recrut_periodo == 'Tarde/Noite'){ echo "SELECTED";}?>>Tarde/Noite</option>
                        <option value="Integral" <?php if($data[0]->recrut_periodo == 'Integral'){ echo "SELECTED";}?>>Integral</option>
                    </select>

                </div>

                <div class="form-group col-md-3">
                    <label for="input">Gênero</label>
                    <select disabled id="sexo" name="sexo" class="form-control">
                            <option value="">Selecione</option>
                            <option value="Masculino" <?php echo ($data[0]->recrut_sexo == 'Masculino')?'selected':''; ?>>Masculino</option>
                            <option value="Feminino" <?php echo ($data[0]->recrut_sexo == 'Feminino')?'selected':''; ?>>Feminino</option>
                            <!-- <option value="Ambos" <?php echo ($data[0]->recrut_sexo == 'Ambos')?'selected':''; ?>>Feminino / Masc </option> -->
                            <option value="Ambos" <?php echo ($data[0]->recrut_sexo == 'Ambos')?'selected':''; ?>>Indiferente </option>
                            
                    </select>

                </div>     
            

                <div class="form-group col-md-6">
                    <label for="input">Requisitos exigidos pela empresa</label>
                    <input disabled type="text" class="form-control" name="requisitosExigidos" id="requisitosExigidos" value="<?php echo $data[0]->recrut_requisito; ?>">
                </div>

                <div class="form-group col-md-3">
                    <label for="input">Autorizar - Divulgação (Rede Sociais)</label>
                        <select disabled id="divulgacao" name="divulgacao" class="form-control">
                        <option value="1" <?php if ($data[0]->recrut_divul == '1') { echo 'SELECTED'; } ?>>Sim | (Exibir botão de compartilhamento no site)</option>    
                        <option value="0" <?php if ($data[0]->recrut_divul == '0') { echo 'SELECTED'; } ?>>Não | (Não exibir botão de compartilhamento no site)</option>                        
                    </select>
                    <!-- <input type="text" class="form-control" name="divulgacao" id="divulgacao" value="<?php echo $data[0]->recrut_divul; ?>"> -->
                </div>


                <div class="form-group col-md-3">
                    <label for="input">Data para expirar</label>
                    <input disabled type="text" class="form-control classData" placeholder=""  id="recrut_dataexpir" name="recrut_dataexpir" value="<?php echo date('d/m/Y', strtotime($data[0]->recrut_dataexpir)); ?>">
                    <?php echo form_error('recrut_dataexpir'); ?>
                </div>

                <div class="form-group col-md-8">
                    <label for="input">Observação</label>
                    <input disabled type="text" class="form-control" placeholder="Observação Recrutamento"  id="recrut_observacao" name="recrut_observacao" value="<?php echo $data[0]->recrut_observacao; ?>">
                </div>

                <div class="form-group col-md-4">
                    <label for="input">Status</label>
                    <select disabled id="recrut_status" name="recrut_status" class="form-control">
                        <option value="ABERTO" <?php if ($data[0]->recrut_status == 'ABERTO') { echo 'SELECTED'; } ?>>ABERTO</option>
                        <option value="CANCELADO" <?php if ($data[0]->recrut_status == 'CANCELADO') { echo 'SELECTED';  } ?>>CANCELADO</option>
                        <option value="AGUARDANDO" <?php if ($data[0]->recrut_status == 'AGUARDANDO') { echo 'SELECTED'; } ?>>AGUARDANDO</option>                                               
                    </select>

                </div>

           

            </div>








        </form>



            <br>    
   



    </div>
</div>



<!-- Column selectors -->

<div id='semScroll' class="panel panel-flat">
    <div class="panel-heading">             
    

        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>           
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
    <br>   

    <table class="table datatable-button-html5-columns">
        <thead>
            <tr>               
                <th>Estudante</th>
                <th>Bairro</th>               
                <th>Fone</th>
                <th>Inst. Ensino</th>
                <th>Cadastro</th>
                <th>Selec | Aprov | Data </th>  
                <!-- <th>Opções</th> -->
            </tr>
        </thead>
        <tbody>
            <div class="datatable-wrap">  
            <?php if (!$recrut == null) {  ?>
            <?php foreach($recrut as $valor) {
                //var_dump($valor->recrutalu_selec);die();
                $selec = $valor->recrutalu_selec == '0' ? 'Não' : 'Sim';
                $aprov = $valor->recrutalu_aprov == '0' ? 'Não' : 'Sim'.' | '. date('d/m/Y', strtotime($valor->recrutalu_data_atualizacao)); 
            ?>                

                <tr>                  

               
                    <td><span class="label bg-success"><?php echo $valor->salualnome; ?></span></td>                        
                    <td><?php echo $valor->salualbairr; ?></td>             
                    <td><?php echo $valor->salualtel02; ?></td>
                    <td><?php echo $valor->sensennome; ?></td>
                    <td><?php echo $valor->date_operacao; ?></td>
                    <td><?php echo $selec .' | '.$aprov ?></td>       

<!--                     <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php if ($valor->url) {?>
                                    <li><a target="_black" href="<?php  echo str_replace("sistema.","",base_url()).$valor->url; ?>"><i class="icon-libreoffice"></i> Baixar Curriculum</a></li>
                                    <?php } ?>                               
                                    <li><a target="_blank" href="<?php echo base_url()?>estudante/visualizarficha/<?php echo $valor->palualcodig;  ?>"><i class="icon-book"></i> Ficha Incrição</a></li>
                                    <li><a href="<?php echo base_url()?>relatorios/exportarCarta/<?php  echo  $valor->recrutalu_estud_id.'/'.$data[0]->recrut_id; ?>"><i class="icon-file-pdf"></i> Exportar Carta</a></li>
                                    <li><a href="<?php echo base_url()?>recrutamento/recrutSelecAprov/<?php  echo  $valor->recrutalu_estud_id.'/'.$data[0]->recrut_id.'/S' ?>"><i class="icon-checkmark3"></i> Selecionado</a></li>
                                    <li><a href="<?php echo base_url()?>recrutamento/recrutSelecAprov/<?php  echo  $valor->recrutalu_estud_id.'/'.$data[0]->recrut_id.'/A' ?>"><i class="icon-spell-check2"></i> Aprovado</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>  -->                       
                </tr>
                <?php } } ?>                       
        </div>
        </tbody>
    </table>
</div>

<!-- /column selectors