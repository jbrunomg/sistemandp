<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Edição de Contrato</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form  action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do contrato:</legend>

					<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

					<input  type="hidden" name="contrestempr_id" value="<?php echo $dados[0]->contrestempr_id; ?>" />
					<input  type="hidden" name="empresas" value="<?php echo $dados[0]->iempemcodig; ?>" />

		<div class="form-row">
            <div class="form-group col-md-6">
              <label class="control-label">Empresas:</label>              
              <select disabled class="select-item-color" data-placeholder="Selecione um Empresa" multiple="multiple" name="empresas" id="empresas" >                     
                  	<?php foreach ($empresas as $valor){ ?>
					<?php $selected = ($valor->pempemcodig == $dados[0]->iempemcodig)?'SELECTED': ''; ?>
                            <option value="<?php echo $valor->pempemcodig; ?>" <?php echo $selected; ?>><?php echo $valor->sempemfanta; ?></option>
					<?php } ?>
              </select>
              <?php  echo form_error('empresas'); ?>                                
            </div> 

            <div class="form-group col-md-6">
              <label class="control-label">Nome/CPF:</label>              
                <select name="estudante" id="estudanteEmails" class="js-example-basic-single js-states form-control" >
        		<?php foreach ($estudante as $valor){ ?>
				<?php $selected = ($valor->palualcodig == $dados[0]->ialualcodig)?'SELECTED': ''; ?>
                    <option value="<?php echo $valor->palualcodig; ?>" <?php echo $selected; ?>><?php echo $valor->salualnome; ?></option>
				<?php } ?>
				</select> 
			    <?php  echo form_error('empresas'); ?> 	                        
            </div> 

            <legend class="text-bold">Dados da Vaga:</legend>

            <div class="form-group col-md-8">
              <label class="control-label">Vaga:</label>              
                <select class="form-control" name="vaga" id="vaga">
                  <option value="">Selecione</option>
                  	<?php foreach ($vaga as $valor){ ?>
					<?php $selected = ($valor->pestvacodig == $dados[0]->iestvacodig)?'SELECTED': ''; ?>
                            <option value="<?php echo $valor->pestvacodig; ?>" <?php echo $selected; ?>><?php echo $valor->pestvacodig." - ".$valor->sestvaativi; ?></option>
					<?php } ?>	                       
                </select>              
            </div>

			<div class="form-group col-md-4">			
			    <label for="input">Estágio Obrigatório?</label>
			    <select id="contrestempr_estagioobg" name="contrestempr_estagioobg" class="form-control">                              
			        <option value="1" <?php if($dados[0]->contrestempr_estagioobg == '1'){ echo "SELECTED";}?>>Sim</option>
			        <option value="0" <?php if($dados[0]->contrestempr_estagioobg == '0'){ echo "SELECTED";}?>>Não</option>
			    </select>			
			</div>




            <div class="form-group col-md-8">
              <label class="control-label">Atividades previstas:</label>              
                <input type="text" class="form-control" placeholder="Atividade do contrato"  data-mask-selectonfocus="true" name="contrestempr_atividade" id="contrestempr_atividade" value="<?php echo $dados[0]->contrestempr_atividade; ?>">
              <?php echo form_error('contrestempr_atividade'); ?>                                
            </div> 

            <div class="form-group col-md-4">
              <label class="control-label">Bolsa Auxílio:</label>              
                <input type="text" class="form-control" placeholder="Bolsa Auxílio"  data-mask-selectonfocus="true" name="contrestempr_bolsa_Auxilio" id="contrestempr_bolsa_Auxilio" value="<?php echo $dados[0]->contrestempr_bolsa_Auxilio; ?>">
              <?php echo form_error('contrestempr_bolsa_Auxilio'); ?>                                
            </div>       

        

            <!-- CONCATENAR Vale-Transporte + Refeição + Outros -->
            <div class="form-group col-md-4">
              <label class="control-label">Benefícios:</label>              
                <input type="text" class="form-control" placeholder="Benefícios do contrato"  data-mask-selectonfocus="true" name="contrestempr_beneficios" id="contrestempr_beneficios" value="<?php echo $dados[0]->contrestempr_beneficios; ?>">
              <?php echo form_error('contrestempr_beneficios'); ?>                                 
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Carga Horária:</label>              
                <input type="text" class="form-control" placeholder="Carga Horária"  data-mask-selectonfocus="true" name="contrestempr_cargahoraria" id="contrestempr_cargahoraria" value="<?php echo $dados[0]->contrestempr_cargahoraria; ?>">
              <?php echo form_error('contrestempr_cargahoraria'); ?>                                
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Duração estágio:</label>              
                <input type="text" class="form-control" placeholder="Duração Estágio"  data-mask-selectonfocus="true" name="contrestempr_duracao_estagio" id="contrestempr_duracao_estagio" value="<?php echo $dados[0]->contrestempr_duracao_estagio; ?>">
              <?php echo form_error('contrestempr_duracao_estagio'); ?>                                
            </div>   

            <div class="form-group col-md-3">
              <label class="control-label">Setor estágio:</label>              
                <input type="text" class="form-control" placeholder="Setor"  data-mask-selectonfocus="true" name="contrestempr_setor" id="contrestempr_setor" value="<?php echo $dados[0]->contrestempr_setor; ?>">
              <?php echo form_error('contrestempr_setor'); ?>                                
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Orientador:</label>              
                <input type="text" class="form-control" placeholder="Orientador"  data-mask-selectonfocus="true" name="contrestempr_orientador" id="contrestempr_orientador" value="<?php echo $dados[0]->contrestempr_orientador; ?>">
              <?php echo form_error('contrestempr_orientador'); ?>                                
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Cargo:</label>              
                <input type="text" class="form-control" placeholder="Cargo"  data-mask-selectonfocus="true" name="contrestempr_cargo" id="contrestempr_cargo" value="<?php echo $dados[0]->contrestempr_cargo; ?>">
              <?php echo form_error('contrestempr_cargo'); ?>                                 
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Formação:</label>              
                <input type="text" class="form-control" placeholder="Formação"  data-mask-selectonfocus="true" name="contrestempr_formacao" id="contrestempr_formacao" value="<?php echo $dados[0]->contrestempr_formacao; ?>">
              <?php echo form_error('contrestempr_formacao'); ?>                                
            </div>



<!--             <div class="form-group col-md-4">
              <label class="control-label">Representante legal:</label>              
                <input type="text" class="form-control" placeholder="Representante legal"  data-mask-selectonfocus="true" name="contrestempr_represent_legal" id="contrestempr_represent_legal" value="<?php echo $dados[0]->contrestempr_represent_legal; ?>">
              <?php echo form_error('contrestempr_represent_legal'); ?>                                
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Representante cargo:</label>              
                <input type="text" class="form-control" placeholder="Representante cargo"  data-mask-selectonfocus="true" name="contrestempr_repr_cargo" id="contrestempr_repr_cargo" value="<?php echo $dados[0]->contrestempr_repr_cargo; ?>">
              <?php echo form_error('contrestempr_repr_cargo'); ?>                                 
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Representante CPF:</label>              
                <input type="text" class="form-control" placeholder="Representante CPF"  data-mask="999.999.999-99" data-mask-selectonfocus="true" name="contrestempr_repr_cpf" id="contrestempr_repr_cpf" value="<?php echo $dados[0]->contrestempr_repr_cpf; ?>">
              <?php echo form_error('contrestempr_repr_cpf'); ?>                                
            </div> -->


          </div>






					<legend class="text-bold">Datas Contratação:</legend>

			        <div class="row">
				        <div class="col-md-2">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Inicial</h6>
				                <br/>                
				                <input type="text" class="form-control classData" name="contrestempr_data_ini" id="contrestempr_data_ini" value="<?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_ini)); ?>">
								<?php echo form_error('contrestempr_data_ini'); ?> 
				            </div>
				        </div>

				        <div class="col-md-2">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Termino</h6>
				                <br/>               
				                <input type="text" class="form-control classData" name="contrestempr_data_term" id="contrestempr_data_term" value="<?php echo date('d-m-Y', strtotime($dados[0]->contrestempr_data_term)); ?>">
								<?php echo form_error('contrestempr_data_term'); ?> 
				            </div>
				        </div>

				        <div class="col-md-2">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Renovação</h6>
				                <br/>
				                <?php $renovacaoI = $dados[0]->contrestempr_data_renI; ?>
				                 <input type="text" class="form-control" data-mask="99-99-9999" name="contrestempr_data_renI" id="contrestempr_data_renI" value="<?php echo (($renovacaoI == '1970-01-01') or ($renovacaoI == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoI))); ?>">
								<?php echo form_error('contrestempr_data_renI'); ?>                  
				            </div>
				        </div>

				        <div class="col-md-2">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Renovação II</h6>
				              <br/>  
				              	<?php $renovacaoII = $dados[0]->contrestempr_data_renII; ?>
				                <input type="text" class="form-control" data-mask="99-99-9999" name="contrestempr_data_renII" id="contrestempr_data_renII" value="<?php echo (($renovacaoII == '1970-01-01') or ($renovacaoII == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoII))); ?>">
								<?php echo form_error('contrestempr_data_renII'); ?>                                                                      
				            </div>
				        </div>

				        <div class="col-md-2">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Renovação III</h6>
				              <br/> 
				                <?php $renovacaoIII = $dados[0]->contrestempr_data_renIII; ?> 
				                <input type="text" class="form-control" data-mask="99-99-9999" name="contrestempr_data_renIII" id="contrestempr_data_renIII" value="<?php echo (($renovacaoIII == '1970-01-01') or ($renovacaoIII == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoIII))); ?>">
								<?php echo form_error('contrestempr_data_renIII'); ?>                                                                      
				            </div>
				        </div>
			        </div>

			        <legend class="text-bold">Contrato:</legend>

			        <div class="row">

			          
			          <div class="col-md-2">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Rescisão</h6>
			              <br/>
			              	  <?php $rescisao = $dados[0]->contrestempr_data_rescisao; ?> 	                                                          
			                  <input type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_rescisao" id="contrestempr_data_rescisao" value="<?php echo   (($rescisao == '1970-01-01') or ($rescisao == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$rescisao))); ?>">
			                  <?php echo form_error('contrestempr_data_rescisao'); ?>                
			            </div>
			          </div>


			            <div class="col-md-3">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Situação Estudante:</h6><br />
				          	
				          	<select  class="form-control" name="situacao" id="situacao">
	                        	<option value="">Selecione</option>
	                            <option value="Ativo" <?php echo ($dados[0]->contrestempr_status == 'Ativo')?'selected':''; ?>>Contrato Ativo</option>
	                            <option value="Rescindido" <?php echo ($dados[0]->contrestempr_status == 'Rescindido')?'selected':''; ?>>Contrato Rescindido</option> 
	                            <option value="Inativo" <?php echo ($dados[0]->contrestempr_status == 'Inativo')?'selected':''; ?>>Contrato Inativo</option>                          
	                                                      
	                        </select>
	                        <?php echo form_error('situacao'); ?>
				    

				            </div>
				        </div>

		<!-- 	          <div class="col-md-3">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Bolsa Auxílio:</h6>
			              <br/> 
			                <input type="text" name="contrestempr_bolsa_Auxilio" class="form-control" placeholder="R$ Valor da Bolsa"  data-mask-selectonfocus="true" name="contrestempr_bolsa_Auxilio" id="contrestempr_bolsa_Auxilio" value="<?php echo $dados[0]->contrestempr_bolsa_Auxilio; ?>">
			               <?php echo form_error('contrestempr_bolsa_Auxilio'); ?>
			            </div>
			          </div>

			          <div class="col-md-3">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Duração Estágio:</h6>
			              <br/> 
			                <input type="text" name="contrestempr_duracao_estagio" class="form-control" placeholder="Duração"  data-mask-selectonfocus="true" name="contrestempr_duracao_estagio" id="contrestempr_duracao_estagio" value="<?php echo $dados[0]->contrestempr_duracao_estagio; ?>">
			               <?php echo form_error('contrestempr_duracao_estagio'); ?>
			            </div>
			          </div> -->

			          <div class="col-md-3">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Documento Entregue:</h6><br />
			          
			              <label class="checkbox-inline">
			                <input type="checkbox" class="styled" name="contrestempr_termo_C_E"  value="1" <?php if($dados[0]->contrestempr_termo_C_E <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                T.C.E
			              </label>
			            
			              <label class="checkbox-inline">
			                <input type="checkbox" class="styled" name="contrestempr_termo_A_I" value="1" <?php if($dados[0]->contrestempr_termo_A_I <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                T.A I
			              </label>
			               <br />
			              <label class="checkbox-inline">
			                <input type="checkbox" class="styled" name="contrestempr_termo_A_II"  value="1" <?php if($dados[0]->contrestempr_termo_A_II <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                T.A II
			              </label>
			            
			              <label class="checkbox-inline">
			                <input type="checkbox" class="styled" name="contrestempr_termo_A_III" value="1" <?php if($dados[0]->contrestempr_termo_A_III <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                T.A III
			              </label>

			            </div>
			          </div>


			        <div class="col-md-3">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Seguro Ativo:</h6><br />
			          
			              <label class="checkbox-inline">
			                <input type="checkbox" class="styled" name="contrestempr_seguro"  value="1" <?php if($dados[0]->contrestempr_seguro <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                Checked
			              </label>
			            
			            </div>
			        </div>

			        </div> 
	

					<legend class="text-bold">Contrato:</legend>

					<div class="form-group">
	                	
	        

	                    <div class="col-md-4">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Pagar Estorno:</h6>
				              <br />
				          
			                <div class="col-md-6">
			                <label class="checkbox-inline">
			               		<input type="checkbox" class="control-danger" name="contrestempr_estorno"  value="1" <?php if($dados[0]->contrestempr_estorno <> 0){ echo 'checked';}else{ echo '';} ?> >  
			                Estorno contrato
			                </label>
			                </div>
				              
			                <div class="col-md-6">
								<div class="row">
									<label class="control-label col-lg-4 text-right">Dias:</label>
									<div class="col-lg-8">		                                
		                                <input  type="number" placeholder="Dias" class="form-control"  name="contrestempr_estorno_dias" min="0" max="30" id="contrestempr_estorno_dias" value="<?php echo $dados[0]->contrestempr_estorno_dias; ?>">
					            <?php echo form_error('contrestempr_estorno_dias'); ?>
	                                </div>
                                </div>
							</div>				            
				     

				            </div>
				        </div>

				        <div class="col-md-4">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Observações:</h6>
				             
				                <textarea class="form-control" rows="2" id="contrestempr_obs" name="contrestempr_obs" ><?php echo $dados[0]->contrestempr_obs; ?></textarea>
				                <?php echo form_error('contrestempr_obs'); ?>
				            </div>
				        </div>

	                    <div class="col-md-4">
				            <div class="panel panel-body border-top-teal text-center">
				              <h6 class="no-margin text-semibold">Download Documento:</h6>

				               <br />				          		
				          		<button type="button" class="btn text-teal-800 border-teal btn-flat" ><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarContrato/<?php echo $dados[0]->contrestempr_id; ?>"><i class="icon-cloud-upload2 position-left"></i> T.C.E</a></button>
				          		

				          		<button type="button" class="btn text-teal-800 border-teal btn-flat" ><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarContratoAditivo/<?php echo $dados[0]->contrestempr_id; ?>"><i class="icon-cloud-upload2 position-left"></i> T.A</a></button>		
				        
				               <br />	

				            </div>
			            </div>

	                </div>

                </fieldset>
                
                <?php if(checarPermissao('aAdministrativo')){ ?>
				<div class="col-md-3">
					<a class="btn bg-teal" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Novo Cadastro <i class="icon-plus2 position-right"></i></a>
				</div>
				<?php } ?> 				

				<div class="text-right">
					<button type="submit" id='validar' class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>

	
  <script type="text/javascript">


    $('#validar').click(function(event) {
    	
    	var valor = $('#contrestempr_data_rescisao').val();

    	var situacao = $('#situacao').val();

    	if(valor !== '00-00-0000') {
    		if (situacao == 'Ativo') {
    			event.preventDefault();
    			 //$this->session->set_flashdata('erro', 'Estudante com data de rescisão não pode está na situação ATIVO!');    			
    			alert('Estudante com data de rescisão não pode está na situação ATIVO!');
    		}
    	}    	 

  });

  var renovacaoI   = document.getElementById('contrestempr_data_renI').value;
  var renovacaoII  = document.getElementById('contrestempr_data_renII').value;
  var renovacaoIII = document.getElementById('contrestempr_data_renIII').value;

  var rescisao = document.getElementById('contrestempr_data_rescisao').value;

 
  window.onload = function(){

  	if( renovacaoI == "")
    document.getElementById("contrestempr_data_renI").value = "00-00-0000";

	if( renovacaoII == "")
    document.getElementById("contrestempr_data_renII").value = "00-00-0000";

	if( renovacaoIII == "")
    document.getElementById("contrestempr_data_renIII").value = "00-00-0000";

  	if( rescisao == "")
    document.getElementById("contrestempr_data_rescisao").value = "00-00-0000";

  }

  


  </script>