
<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Contrato</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form  action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">

				<fieldset class="content-group">
					<legend class="text-bold">Dados do contrato:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />    
          
          <div class="form-row">
            <div class="form-group col-md-6">
              <label class="control-label">Empresas:</label>              
              <select class="select-item-color" data-placeholder="Selecione um Empresa" multiple="multiple" name="empresas" id="empresas" >                     
                  <?php foreach ($empresas as $valor){ ?>
                    <option value="<?php echo $valor->pempemcodig; ?>"><?php echo $valor->sempemfanta;?></option>
                  <?php } ?>
              </select>
              <?php  echo form_error('empresas'); ?>                                
            </div> 

            <div class="form-group col-md-6">
              <label class="control-label">Nome/CPF:</label>              
                <select name="estudante" id="estudanteEmails" class="js-example-basic-single js-states form-control" ></select>                        
            </div> 

            <legend class="text-bold">Dados da Vaga:</legend>

            <div class="form-group col-md-8">
              <label class="control-label">Vaga/Recrutamento:</label>              
                <select class="form-control" name="vaga" id="vaga" >
                  <option value="">Selecione</option>
                    <?php foreach ($vaga as $valor) { ?>
                        <option value="<?php echo $valor->pestvacodig; ?>"><?php echo "(".$valor->pestvacodig."-".$valor->recrutamento_id .") : ".$valor->sestvaativi; ?></option>
                  <?php } ?>                       
                </select>              
            </div>

            <div class="form-group col-md-4">              
              <label for="input">Estágio Obrigatório?</label>
              <select id="contrestempr_estagioobg" name="contrestempr_estagioobg" class="form-control">
                  <option value="1">Sim</option>
                  <option value="0">Não</option>
              </select>            
            </div>

            <div class="form-group col-md-8">
              <label class="control-label">Atividades previstas:</label>              
                <input type="text" class="form-control" placeholder="Atividade do contrato"  data-mask-selectonfocus="true" name="contrestempr_atividade" id="contrestempr_atividade" value="<?php echo set_value('contrestempr_atividade'); ?>">
              <?php echo form_error('contrestempr_atividade'); ?>                                
            </div> 

            <div class="form-group col-md-4">
              <label class="control-label">Bolsa Auxílio:</label>              
                <input type="text" class="form-control" placeholder="Bolsa Auxílio"  data-mask-selectonfocus="true" name="contrestempr_bolsa_Auxilio" id="contrestempr_bolsa_Auxilio" value="<?php echo set_value('contrestempr_bolsa_Auxilio'); ?>">
              <?php echo form_error('contrestempr_bolsa_Auxilio'); ?>                                
            </div>       

        

            <!-- CONCATENAR Vale-Transporte + Refeição + Outros -->
            <div class="form-group col-md-4">
              <label class="control-label">Benefícios:</label>              
                <input type="text" class="form-control" placeholder="Benefícios do contrato"  data-mask-selectonfocus="true" name="contrestempr_beneficios" id="contrestempr_beneficios" value="<?php echo set_value('contrestempr_beneficios'); ?>">
              <?php echo form_error('contrestempr_beneficios'); ?>                                 
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Carga Horária:</label>              
                <input type="text" class="form-control" placeholder="Carga Horária"  data-mask-selectonfocus="true" name="contrestempr_cargahoraria" id="contrestempr_cargahoraria" value="<?php echo set_value('contrestempr_cargahoraria'); ?>">
              <?php echo form_error('contrestempr_cargahoraria'); ?>                                
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Duração estágio:</label>              
                <input type="text" class="form-control" placeholder="Duração Estágio"  data-mask-selectonfocus="true" name="contrestempr_duracao_estagio" id="contrestempr_duracao_estagio" value="<?php echo set_value('contrestempr_duracao_estagio'); ?>">
              <?php echo form_error('contrestempr_duracao_estagio'); ?>                                
            </div>   

            <div class="form-group col-md-3">
              <label class="control-label">Setor estágio:</label>              
                <input type="text" class="form-control" placeholder="Setor Estagio"  data-mask-selectonfocus="true" name="contrestempr_setor" id="contrestempr_setor" value="<?php echo set_value('contrestempr_setor'); ?>">
              <?php echo form_error('contrestempr_setor'); ?>                                
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Orientador:</label>              
                <input type="text" class="form-control" placeholder="Orientador"  data-mask-selectonfocus="true" name="contrestempr_orientador" id="contrestempr_orientador" value="<?php echo set_value('contrestempr_orientador'); ?>">
              <?php echo form_error('contrestempr_orientador'); ?>                                
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Cargo:</label>              
                <input type="text" class="form-control" placeholder="Cargo"  data-mask-selectonfocus="true" name="contrestempr_cargo" id="contrestempr_cargo" value="<?php echo set_value('contrestempr_cargo'); ?>">
              <?php echo form_error('contrestempr_cargo'); ?>                                 
            </div>

            <div class="form-group col-md-3">
              <label class="control-label">Formação:</label>              
                <input type="text" class="form-control" placeholder="Formação"  data-mask-selectonfocus="true" name="contrestempr_formacao" id="contrestempr_formacao" value="<?php echo set_value('contrestempr_formacao'); ?>">
              <?php echo form_error('contrestempr_formacao'); ?>                                
            </div>




<!--             <div class="form-group col-md-4">
              <label class="control-label">Representante legal:</label>              
                <input type="text" class="form-control" placeholder="Representante legal"  data-mask-selectonfocus="true" name="contrestempr_represent_legal" id="contrestempr_represent_legal" value="<?php echo set_value('contrestempr_represent_legal'); ?>">
              <?php echo form_error('contrestempr_represent_legal'); ?>                                
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Representante cargo:</label>              
                <input type="text" class="form-control" placeholder="Representante cargo"  data-mask-selectonfocus="true" name="contrestempr_repr_cargo" id="contrestempr_repr_cargo" value="<?php echo set_value('contrestempr_repr_cargo'); ?>">
              <?php echo form_error('contrestempr_repr_cargo'); ?>                                 
            </div>

            <div class="form-group col-md-4">
              <label class="control-label">Representante CPF:</label>              
                <input type="text" class="form-control" placeholder="Representante cpf" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="contrestempr_repr_cpf" id="contrestempr_repr_cpf" value="<?php echo set_value('contrestempr_repr_cpf'); ?>">
              <?php echo form_error('contrestempr_repr_cpf'); ?>                                
            </div> -->

          </div>


				<legend class="text-bold">Datas Contratação:</legend>

        <div class="row">
          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Inicial</h6>
                <br/>
                <input type="text" class="form-control classData" name="contrestempr_data_ini" placeholder="00-00-0000" id="contrestempr_data_ini" value="<?php echo set_value('contrestempr_data_ini'); ?>" >
                <?php echo form_error('contrestempr_data_ini'); ?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Termino</h6>
                <br/>
                <input type="text" class="form-control classData" name="contrestempr_data_term" placeholder="00-00-0000" id="contrestempr_data_term" value="<?php echo set_value('contrestempr_data_term'); ?>" required>
                <?php echo form_error('contrestempr_data_term'); ?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Renovação</h6>
                <br/>
                <input type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_renI" placeholder="00-00-0000" id="contrestempr_data_renI" value="<?php echo set_value('contrestempr_data_renI'); ?>">
                <?php echo form_error('contrestempr_data_renI'); ?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Renovação II</h6>
              <br/>                                                          
                  <input type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_renII" placeholder="00-00-0000" id="contrestempr_data_renII" value="<?php echo set_value('contrestempr_data_renII'); ?>">
                  <?php echo form_error('contrestempr_data_renII'); ?>                
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Renovação III</h6>
              <br/>                                                          
                  <input type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_renIII" placeholder="00-00-0000" id="contrestempr_data_renIII" value="<?php echo  set_value('contrestempr_data_renIII'); ?>">
                  <?php echo form_error('contrestempr_data_renIII'); ?>                
            </div>
          </div>

        </div>



        <legend class="text-bold">Contrato:</legend>

        <div class="row">

          
          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Rescisão</h6>
              <br/>                                                          
                  <input type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_rescisao" placeholder="00-00-0000" id="contrestempr_data_rescisao" value="<?php echo  set_value('contrestempr_data_rescisao'); ?>">
                  <?php echo form_error('contrestempr_data_rescisao'); ?>                
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Situação:</h6>
              <br/> 
                <select class="form-control" name="situacao" id="situacao">
                  <!-- <option value="">Selecione</option> -->
                  <option value="Ativo">Contrato Ativo</option>
                  <option value="Rescindido">Contrato Rescindido</option>                  
                  <option value="Inativo">Contrato Inativo</option>
                </select>
            </div>
          </div>

     <!--      <div class="col-md-3">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Bolsa Auxílio:</h6>
              <br/> 
                <input type="text" name="contrestempr_bolsa_Auxilio" class="form-control" placeholder="R$ Valor da Bolsa"  data-mask-selectonfocus="true" name="contrestempr_bolsa_Auxilio" id="contrestempr_bolsa_Auxilio" value="<?php echo set_value('contrestempr_bolsa_Auxilio'); ?>">
               <?php echo form_error('contrestempr_bolsa_Auxilio'); ?>
            </div>
          </div> -->

 <!--          <div class="col-md-3">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Duração Estágio:</h6>
              <br/> 
                <input type="text" name="contrestempr_duracao_estagio" class="form-control" placeholder="Duração"  data-mask-selectonfocus="true" name="contrestempr_duracao_estagio" id="contrestempr_duracao_estagio" value="<?php echo set_value('contrestempr_duracao_estagio'); ?>">
               <?php echo form_error('contrestempr_duracao_estagio'); ?>
            </div>
          </div> -->

          <div class="col-md-3">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Documento Entregue:</h6><br>
          
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="contrestempr_termo_C_E" value="1">  
                T.C.E
              </label>
              
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="contrestempr_termo_A_I" value="1">  
                T.A I
              </label>
               <br />
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="contrestempr_termo_A_II" value="1">  
                T.A II
              </label>
             
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="contrestempr_termo_A_III" value="1">  
                T.A III
              </label>

            </div>
          </div>

          <div class="col-md-3">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Seguro Ativo:</h6><br />
          
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="contrestempr_seguro" value="1">  
                Checked
              </label>

            </div>
          </div>

        </div>   


        <!-- <legend class="text-bold">Contrato:</legend> -->

      

          <div class="col-md-12">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Observações:</h6>
              <br/> 
                <textarea class="form-control" rows="2" id="contrestempr_obs" name="contrestempr_obs" ><?php echo set_value('contrestempr_obs'); ?></textarea>
                <?php echo form_error('contrestempr_obs'); ?>
            </div>
          </div>
                  		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal" style="display:none;" id="btnSalvar">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>

  <script type="text/javascript">


    $('#contrestempr_atividade').blur(function(event) {    
      
       var estudante = document.getElementById('estudanteEmails').value;

       // alert(estudante);   

       var empresas  = document.getElementById('empresas').value;   
       
       $.ajax({
           url: '<?php echo base_url();?>index.php/contratoestudante/duplicidade/',
           type: 'POST',
           dataType: 'json',
           data: {estudante: estudante, empresas: empresas},
           beforeSend: function() {
                   $('#Verificando').css('display', 'block');
           }
       })
       .always(function(result) {

          $('#Verificando').css('display', 'none');

          if(result.status){
            alert('Já existe contrato desse estudante para essa Empresa!');
          }else{
            $('#btnSalvar').css('display', 'block');
            alert('Estuante Liberado para Cadastro!');
          }
       });
       
    });

    // $('#vaga').blur(function(event) {

    //   var vaga = document.getElementById('vaga').value;
    //   console.log(vaga);

    // });  

    $('#vaga').change(function() {
        preencherDadosVaga();
    });

    function preencherDadosVaga() {
        if ( $('#vaga').val() > 0 ) {
            buscarDadosVaga();
        } else {
            $('#contrestempr_atividade').val('');
            $('#contrestempr_bolsa_Auxilio').val('');
            $('#contrestempr_beneficios').val('');
            $('#contrestempr_cargahoraria').val('');
            $('#contrestempr_duracao_estagio').val('');
        }
    }

    function buscarDadosVaga() {
        $.ajax({
            type: "POST",           
            url: '<?php echo base_url();?>index.php/contratoestudante/pegarVagaPeloID/',
            dataType: "json",
            data: {
                id: $('#vaga').val()
            },
            success: function(data) {                
                $('#contrestempr_atividade').val(data[0].recrut_atividade);
                $('#contrestempr_bolsa_Auxilio').val(data[0].recrut_valor_auxilio);

                $('#contrestempr_beneficios').val(data[0].beneficios);

                $('#contrestempr_cargahoraria').val(data[0].recrut_horario);
                $('#contrestempr_duracao_estagio').val(data[0].recrut_duracao);
            }
        });
    }



    // `recrut_atividade`
    // `recrut_valor_auxilio`

    // `recrut_bolsa_auxilio`
    // `recrut_transporte`
    // `recrut_refei`
    // `recrut_outro`

    // `recrut_horario`
    // `recrut_duracao`


  window.onload = function(){
    document.getElementById("contrestempr_data_ini").value = "0000-00-00";
    document.getElementById("contrestempr_data_term").value = "0000-00-00";
    document.getElementById("contrestempr_data_renI").value = "0000-00-00";
    document.getElementById("contrestempr_data_renII").value = "0000-00-00";
    document.getElementById("contrestempr_data_renIII").value = "0000-00-00";
    document.getElementById("contrestempr_data_rescisao").value = "0000-00-00";
  }

  function myFunction() {
        var x = document.getElementById("myDate").required;
        
    }
    

  </script>


