<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Estudante Contratado</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url() ?>contratoestudante/" method="get" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Demonstrativo:</legend>

			<input  disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />


      <input  type="hidden" name="contrestempr_id" value="<?php echo $dados[0]->contrestempr_id; ?>" />

      
            <div class="form-group">
              <label class="control-label col-lg-2">Empresas:</label>
              <div class="col-lg-5">
                <select disabled class="select-item-color" data-placeholder="Selecione um Empresa" multiple="multiple" name="empresas" id="empresas" >                     
                    <?php foreach ($empresas as $valor){ ?>
                    <?php $selected = ($valor->pempemcodig == $dados[0]->iempemcodig)?'SELECTED': ''; ?>
                    <option value="<?php echo $valor->pempemcodig; ?>" <?php echo $selected; ?>><?php echo $valor->sempemfanta; ?></option>
                    <?php } ?>
                </select>
                <?php  echo form_error('empresas'); ?>
              </div>                    
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Nome/CPF:</label>
                <div class="col-lg-5">
                  <select disabled name="estudante" class="js-example-basic-single js-states form-control" id="estudanteEmails">       
              
              <?php foreach ($estudante as $valor){ ?>
              <?php $selected = ($valor->palualcodig == $dados[0]->ialualcodig)?'SELECTED': ''; ?>
                              <option value="<?php echo $valor->palualcodig; ?>" <?php echo $selected; ?>><?php echo $valor->salualnome; ?></option>
              <?php } ?>                    
                
                    </select> 
                  <?php  echo form_error('empresas'); ?>           
                </div>                    
            </div>  


            <div class="form-group">
                <label class="control-label col-lg-2">Vaga:</label>
                <div class="col-lg-5">
                    <select disabled class="form-control" name="vaga" id="vaga">
                      <option  value="">Selecione</option>
              <?php foreach ($vaga as $valor){ ?>
              <?php $selected = ($valor->pestvacodig == $dados[0]->iestvacodig)?'SELECTED': ''; ?>
                                <option value="<?php echo $valor->pestvacodig; ?>" <?php echo $selected; ?>><?php echo $valor->pestvacodig." - ".$valor->sestvaativi; ?></option>
              <?php } ?>                                   
                    </select>
                    <?php  echo form_error('vaga'); ?>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Atividades:</label>
              <div class="col-lg-5">
                <input  disabled type="text" class="form-control" placeholder="Atividade do contrato" name="contrestempr_atividade" id="contrestempr_atividade" value="<?php echo $dados[0]->contrestempr_atividade; ?>">
                <?php echo form_error('contrestempr_atividade'); ?>
              </div>                    
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Benefísios:</label>
              <div class="col-lg-5">
                <input  disabled type="text" class="form-control" placeholder="Benefísios do contrato" name="contrestempr_beneficios" id="contrestempr_beneficios" value="<?php echo $dados[0]->contrestempr_beneficios; ?>">
                <?php echo form_error('contrestempr_beneficios'); ?>
              </div>                    
            </div>

            <div class="form-group">
              <label class="control-label col-lg-2">Carga Horária:</label>
              <div class="col-lg-5">
                <input  disabled type="text" class="form-control" placeholder="Carga Horária" name="contrestempr_cargahoraria" id="contrestempr_cargahoraria" value="<?php echo $dados[0]->contrestempr_cargahoraria; ?>">
                <?php echo form_error('contrestempr_cargahoraria'); ?>
              </div>                    
            </div>

            <legend class="text-bold">Datas Contratação:</legend>

                <div class="row">
                  <div class="col-md-2">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Inicial</h6>
                          <br/>                
                          <input disabled type="text" class="form-control classData" name="contrestempr_data_ini" id="contrestempr_data_ini" value="<?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_ini)); ?>">
                  <?php echo form_error('contrestempr_data_ini'); ?> 
                      </div>
                  </div>

                  <div class="col-md-2">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Termino</h6>
                          <br/>               
                          <input disabled type="text" class="form-control classData" name="contrestempr_data_term" id="contrestempr_data_term" value="<?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_term)); ?>">
                  <?php echo form_error('contrestempr_data_term'); ?> 
                      </div>
                  </div>

                  <div class="col-md-2">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Renovação</h6>
                          <br/>
                          <?php $renovacaoI = $dados[0]->contrestempr_data_renI; ?>
                           <input disabled type="text" class="form-control classData" name="contrestempr_data_renI" id="contrestempr_data_renI" value="<?php echo (($renovacaoI == '1970-01-01') or ($renovacaoI == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoI))); ?>">
                  <?php echo form_error('contrestempr_data_renI'); ?>                  
                      </div>
                  </div>

                  <div class="col-md-2">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Renovação II</h6>
                        <br/>  
                          <?php $renovacaoII = $dados[0]->contrestempr_data_renII; ?>
                          <input disabled type="text" class="form-control classData" name="contrestempr_data_renII" id="contrestempr_data_renII" value="<?php echo (($renovacaoII == '1970-01-01') or ($renovacaoII == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoII))); ?>">
                  <?php echo form_error('contrestempr_data_renII'); ?>                                                                      
                      </div>
                  </div>

                  <div class="col-md-2">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Renovação III</h6>
                        <br/> 
                          <?php $renovacaoIII = $dados[0]->contrestempr_data_renIII; ?> 
                          <input disabled type="text" class="form-control classData" name="contrestempr_data_renIII" id="contrestempr_data_renIII" value="<?php echo (($renovacaoIII == '1970-01-01') or ($renovacaoIII == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$renovacaoIII))); ?>">
                  <?php echo form_error('contrestempr_data_renIII'); ?>                                                                      
                      </div>
                  </div>
                </div>
          
                        <legend class="text-bold">Contrato:</legend>

              <div class="row">

                
                <div class="col-md-2">
                  <div class="panel panel-body border-top-teal text-center">
                    <h6 class="no-margin text-semibold">Rescisão</h6>
                    <br/>
                        <?php $rescisao = $dados[0]->contrestempr_data_rescisao; ?>                                                             
                        <input disabled type="text" class="form-control" data-mask="99/99/9999" name="contrestempr_data_rescisao" id="contrestempr_data_rescisao" value="<?php echo   (($rescisao == '1970-01-01') or ($rescisao == '0000-00-00')) ? '' : date('d-m-Y', strtotime(str_replace('/', '-',$rescisao))); ?>">
                        <?php echo form_error('contrestempr_data_rescisao'); ?>                
                  </div>
                </div>


                <div class="col-md-3">
                  <div class="panel panel-body border-top-teal text-center">
                    <h6 class="no-margin text-semibold">Bolsa Auxílio:</h6>
                    <br/> 
                      <input disabled type="text" name="contrestempr_bolsa_Auxilio" class="form-control" placeholder="R$ Valor da Bolsa"  data-mask-selectonfocus="true" name="contrestempr_bolsa_Auxilio" id="contrestempr_bolsa_Auxilio" value="<?php echo $dados[0]->contrestempr_bolsa_Auxilio; ?>">
                     <?php echo form_error('contrestempr_bolsa_Auxilio'); ?>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="panel panel-body border-top-teal text-center">
                    <h6 class="no-margin text-semibold">Duração Estágio:</h6>
                    <br/> 
                      <input disabled type="text" name="contrestempr_duracao_estagio" class="form-control" placeholder="Duração"  data-mask-selectonfocus="true" name="contrestempr_duracao_estagio" id="contrestempr_duracao_estagio" value="<?php echo $dados[0]->contrestempr_duracao_estagio; ?>">
                     <?php echo form_error('contrestempr_duracao_estagio'); ?>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="panel panel-body border-top-teal text-center">
                    <h6 class="no-margin text-semibold">Documento Entregue:</h6>
                
                    <label class="checkbox-inline">
                      <input disabled type="checkbox" class="styled" name="contrestempr_termo_C_E"  value="1" <?php if($dados[0]->contrestempr_termo_C_E <> 0){ echo 'checked';}else{ echo '';} ?> >  
                      T.C.E
                    </label>
                  
                    <label class="checkbox-inline">
                      <input disabled type="checkbox" class="styled" name="contrestempr_termo_A_I" value="1" <?php if($dados[0]->contrestempr_termo_A_I <> 0){ echo 'checked';}else{ echo '';} ?> >  
                      T.A I
                    </label>
                     <br />
                    <label class="checkbox-inline">
                      <input disabled type="checkbox" class="styled" name="contrestempr_termo_A_II"  value="1" <?php if($dados[0]->contrestempr_termo_A_II <> 0){ echo 'checked';}else{ echo '';} ?> >  
                      T.A II
                    </label>
                  
                    <label class="checkbox-inline">
                      <input disabled type="checkbox" class="styled" name="contrestempr_termo_A_III" value="1" <?php if($dados[0]->contrestempr_termo_A_III <> 0){ echo 'checked';}else{ echo '';} ?> >  
                      T.A III
                    </label>

                  </div>
                </div>

              </div> 







            <legend class="text-bold">Contrato:</legend>


            <div class="row">

              <div class="col-md-3">
                <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Situação Estudante:</h6><br />
                    
                    <select  disabled class="form-control" name="situacao" id="situacao">
                      <option value="">Selecione</option>
                      <option value="Ativo" <?php echo ($dados[0]->contrestempr_status == 'Ativo')?'selected':''; ?>>Contrato Ativo</option>
                      <option value="Rescindido" <?php echo ($dados[0]->contrestempr_status == 'Rescindido')?'selected':''; ?>>Contrato Rescindido</option> 
                      <option value="Inativo" <?php echo ($dados[0]->contrestempr_status == 'Inativo')?'selected':''; ?>>Contrato Inativo</option>                                                          
                    </select>
                          <?php echo form_error('situacao'); ?>
                </div>
              </div>


              <div class="col-md-4">
                <div class="panel panel-body border-top-teal text-center">
                  <h6 class="no-margin text-semibold">Observações:</h6>
                 
                    <textarea disabled  class="form-control" rows="2" id="contrestempr_obs" name="contrestempr_obs" ><?php echo $dados[0]->contrestempr_obs; ?></textarea>
                    <?php echo form_error('contrestempr_obs'); ?>
                </div>
              </div>

            </div>  



		
          </fieldset>  
          <div class="text-right">
					 <button type="submit" class="btn bg-teal">Voltar <i class="icon-arrow-right14 position-right"></i></button>
				  </div>
		</form>
	</div>
</div>


<script type="text/javascript">

  var renovacaoI   = document.getElementById('contrestempr_data_renI').value;
  var renovacaoII  = document.getElementById('contrestempr_data_renII').value;
  var renovacaoIII = document.getElementById('contrestempr_data_renIII').value;

  var rescisao = document.getElementById('contrestempr_data_rescisao').value;


  window.onload = function(){

    if( renovacaoI == "")
    document.getElementById("contrestempr_data_renI").value = "00-00-0000";

  if( renovacaoII == "")
    document.getElementById("contrestempr_data_renII").value = "00-00-0000";

  if( renovacaoIII == "")
    document.getElementById("contrestempr_data_renIII").value = "00-00-0000";

    if( rescisao == "")
    document.getElementById("contrestempr_data_rescisao").value = "00-00-0000";

  }


</script>
