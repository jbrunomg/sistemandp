<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aAdministrativo')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-contrato">
		<thead>
			<tr>
				<th>Código</th>
				<th>Empresa - Cnpj</th>				
				<th>Estudante - Cpf - Seguro</th>				
				<th>Data Nascimento</th>
				<th>Inst. Ensino</th>
				<th>contrato</th>								
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>
					
					<td><?php echo $valor->contrestempr_id; ?></td> 

					<td><?php echo $valor->sempemrazao.' - '.$valor->sempemcnpj ?></td>						

					<?php if($valor->contrestempr_seguro == 1){ ?> 	
					<td><?php echo $valor->salualnome.' - '.$valor->salualcpf.'  ' ?><span class="badge bg-success"> com seguro</span></td>
					<?php } else if($valor->contrestempr_seguro == 0){ ?> 
					<td><?php echo $valor->salualnome.' - '.$valor->salualcpf.'  ' ?><span class="badge bg-danger-400"> sem seguro</span></td>
					<?php } else if($valor->contrestempr_seguro == null){ ?> 
					<td><?php echo $valor->salualnome.' - '.$valor->salualcpf.'  ' ?><span class="text-info"> Alerta</span></td>
					<?php } ?>

					
					<td><?php echo date('d/m/Y', strtotime($valor->talualniver)); ?></td> 
					
					<td><?php echo $valor->sensennome; ?></td>

					<td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">									
									<li><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarContrato/<?php echo $valor->contrestempr_id; ?>" title="Termo Contrato Estudante"><i class="icon-file-pdf"></i> Exportar T.C.E</a></li>
									<li><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarContratoAditivo/<?php echo $valor->contrestempr_id; ?>" title="Termo Aditivo"><i class="icon-file-pdf"></i> Exportar T.A</a></li>
									<li><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarDeclaracaoEstagio/<?php echo $valor->contrestempr_id; ?>" title="Declaração Estudante"><i class="icon-file-pdf"></i> Exportar D.E</a></li>
									<li><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarTermoRecisaoEstagio/<?php echo $valor->contrestempr_id; ?>" title="Termo de Recisao"><i class="icon-file-pdf"></i> Exportar T.R</a></li>
									<li><a target="_blank" href="<?php echo base_url()?>contratoestudante/carregarFichaAcompanhamentoEstagio/<?php echo $valor->contrestempr_id; ?>" title="Ficha de Acompanhamento de Estagio"><i class="icon-file-pdf"></i> Exportar F.A.E</a></li>
								</ul>
							</li>
						</ul>
					</td> 
					
														
					<td>																				
							<ul class="icons-list">
									<?php if(checarPermissao('eAdministrativo')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('dAdministrativo')){ ?>
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->contrestempr_id; ?>"><i class="icon-trash"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('vAdministrativo')){ ?>
									<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
									<?php } ?>																				
								
							</ul>																			
					</td>


				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<!-- /column selectors -->

<script type="text/javascript">
	
	$(document).ready(function(){

		 // Column selectors
	    $('.datatable-button-html5-contrato').DataTable({
	    	"ordering": false,
   			"order": [],
	    	columnDefs: [{
	            targets: [3,4], // Hide actions column
	            visible: false
	        }],
			//sort: false,
	        buttons: {            
	            dom: {
	                button: {
	                    className: 'btn btn-default'
	                }
	            },
	            buttons: [
	                {
	                    extend: 'copyHtml5',
	                    exportOptions: {
	                        columns: [ 0, ':visible' ]
	                    }
	                },
	                {
	                    extend: 'excelHtml5',
	                    exportOptions: {
	                        columns: ':visible'
	                    }
	                },
	                {
	                    extend: 'pdfHtml5',
	                    exportOptions: {
	                        columns: ':visible' // columns: [0, 1, 2, 5]
	                    }
	                },
	                {
	                    extend: 'colvis',
	                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
	                    className: 'btn-icon'
	                }
	            ]
	        }
	    });

	});	

   

</script>





