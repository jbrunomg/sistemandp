<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Instituição de Ensino</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>Instensino/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados da Instituição:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="pensencodig" value="<?php echo $dados[0]->pensencodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">CNPJ:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="sensencnpj" id="sensencnpj" value="<?php echo $dados[0]->sensencnpj; ?>">
					<?php echo form_error('sensencnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
		          <label class="control-label col-lg-2">Matricula:</label>
		          <div class="col-lg-5">
		            <input  disabled type="text" class="form-control" placeholder="Matricula"  data-mask-selectonfocus="true" name="sensenmatricula" id="sensenmatricula" value="<?php echo $dados[0]->sensenmatricula; ?>">
		          <?php echo form_error('sensenmatricula'); ?>
		          </div>                    
		        </div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Instituição:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome I. de Ensino" name="sensennome" id="sensennome" value="<?php echo $dados[0]->sensennome; ?>">
					<?php echo form_error('sensennome'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Razão Social:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Razão" name="sensenrazao" id="sensenrazao" value="<?php echo $dados[0]->sensenrazao; ?>">
					<?php echo form_error('sensenrazao'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Mantededora:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" placeholder="Nome Mantededora" name="sensenmantededora" id="sensenmantededora" value="<?php echo $dados[0]->sensenmantededora; ?>">
					<?php echo form_error('sensenmantededora'); ?>
					</div>										
				</div>


				<legend class="text-bold">Dados Comerciais:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label> 
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sensenlogra" id="sensenlogra" value="<?php echo $dados[0]->sensenlogra; ?>">
					<?php echo form_error('sensenlogra'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sensennumer" id="sensennumer" value="<?php echo $dados[0]->sensennumer; ?>">
					<?php echo form_error('sensennumer'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sensencompl" id="sensencompl" value="<?php echo $dados[0]->sensencompl; ?>">
					<?php echo form_error('sensencompl'); ?>
					</div>										
				</div>
<!--
				<div class="form-group">
                	<label class="control-label col-lg-2">UF:</label>
                	<div class="col-lg-5">
                        <select disabled  class="form-control" name="estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sensenuf)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select disabled class="form-control" name="cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('cidade'); ?>	
                    </div>				                            	                            
				</div> -->

				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="bairro" id="bairro" value="<?php echo $dados[0]->sensenbairr;  ?>">
					<?php echo form_error('sensenbairr'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sensencidad" id="sensencidad" value="<?php echo $dados[0]->sensencidad; ?>">
					<?php echo form_error('sensencidad'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Estado:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sensenuf" id="sensenuf" value="<?php echo $dados[0]->sensenuf; ?>">
					<?php echo form_error('sensenuf'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->sensencep; ?>">
					<?php echo form_error('sensencep'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone:</label>
					<div class="col-lg-5">
						<input disabled type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sensentel01" id="sensentel01" value="<?php echo $dados[0]->sensentel01; ?>">
					<?php echo form_error('sensentel01'); ?>
					</div>										
				</div>		

				<div class="form-group">
					<label class="control-label col-lg-2">Representante:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="sensenrepre" id="sensenrepre" value="<?php echo $dados[0]->sensenrepre; ?>">
					<?php echo form_error('sensenrepre'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cargo Representante:</label>
					<div class="col-lg-5">
						<input disabled type="text" name="sensencargo" class="form-control" placeholder="Cargo Responsável"  data-mask-selectonfocus="true" name="sensencargo" id="sensencargo" value="<?php echo $dados[0]->sensencargo; ?>">
					<?php echo form_error('sensencargo'); ?>
					</div>										
				</div>
						
		
            </fieldset>  
		</form>
	</div>
</div>
