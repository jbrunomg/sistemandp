<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro Instituição de Ensino</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados da vaga:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />


        <div class="form-group">
          <label class="control-label col-lg-2">CNPJ:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="sensencnpj" id="sensencnpj" value="<?php echo set_value('sensencnpj'); ?>">
          <?php echo form_error('sensencnpj'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Matricula:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Matricula"  data-mask-selectonfocus="true" name="sensenmatricula" id="sensenmatricula" value="<?php echo set_value('sensenmatricula'); ?>">
          <?php echo form_error('sensenmatricula'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Nome da Instituição:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Instituição" name="sensennome" id="sensennome" value="<?php echo set_value('sensennome'); ?>">
          <?php  echo form_error('sensennome'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Razão Social:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Razão" name="sensenrazao" id="sensenrazao" value="<?php echo set_value('sensenrazao'); ?>">
          <?php echo form_error('sensenrazao'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Mantededora:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Mantededora" name="sensenmantededora" id="sensenmantededora" value="<?php echo set_value('sensenmantededora'); ?>">
          <?php  echo form_error('sensenmantededora'); ?>
          </div>                    
        </div>


        <legend class="text-bold">Dados Comerciais:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Endereço:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Rua/Av/Trav" name="sensenlogra" id="sensenlogra" value="<?php echo set_value('sensenlogra'); ?>">
          <?php echo form_error('sensenlogra'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Numero:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="numero"   name="sensennumer" id="sensennumer" value="<?php echo set_value('sensennumer'); ?>">
          <?php echo form_error('sensennumer'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Complemento:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Complemento"  name="sensencompl" id="sensencompl" value="<?php echo set_value('sensencompl'); ?>">
          <?php echo form_error('sensencompl'); ?>
          </div>                    
        </div>
<!--
        <div class="form-group">
                  <label class="control-label col-lg-2">UF:</label>
                  <div class="col-lg-5">
                        <select disabled  class="form-control" name="estado" id="estado" >
                          <option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                              <?php $selected = ($valor->id == $dados[0]->sensenuf)?'SELECTED': ''; ?>
                                  <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('estado'); ?>
                    </div>                                  
                </div>                              
        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
                        <select disabled class="form-control" name="cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                              <?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
                                  <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
                            <?php } ?>
                        </select>
                    <?php echo form_error('cidade'); ?> 
                    </div>                                                                  
        </div> -->

        <div class="form-group">
          <label class="control-label col-lg-2">Bairro:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Bairro" name="sensenbairr" id="sensenbairr" value="<?php echo set_value('sensenbairr');  ?>">
          <?php echo form_error('sensenbairr'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cidade" name="sensencidad" id="sensencidad" value="<?php echo set_value('sensencidad'); ?>">
          <?php echo form_error('sensencidad'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Estado:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Uf"  name="sensenuf" id="sensenuf" value="<?php echo set_value('sensenuf'); ?>">
          <?php echo form_error('sensenuf'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">CEP:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cep"  name="sensencep" id="sensencep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo set_value('sensencep'); ?>">
          <?php echo form_error('sensencep'); ?>
          </div>                    
        </div>

        <legend class="text-bold">Dados Contato:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Telefone :</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)9999-9999" data-mask="(99)9999-9999" data-mask-selectonfocus="true" name="sensentel01" id="sensentel01" value="<?php echo set_value('sensentel01'); ?>">
          <?php echo form_error('sensentel01'); ?>
          </div>                    
        </div>
      
        <div class="form-group">
          <label class="control-label col-lg-2">Representante:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="sensenrepre" id="sensenrepre" value="<?php echo set_value('sensenrepre'); ?>">
          <?php echo form_error('sensenrepre'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Cargo Representante:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cargo Responsável"  data-mask-selectonfocus="true" name="sensencargo" id="sensencargo" value="<?php echo set_value('sensencargo'); ?>">
          <?php echo form_error('sensencargo'); ?>
          </div>                    
        </div>

                     		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>


  <script type="text/javascript">

  $('#sensencnpj').blur(function(event) {
  
       var documento = $(this).val();

       if(documento.length == 18){

        $.ajax({
           url: '<?php echo base_url();?>empresa/wscnpj/',
           type: 'POST',
           dataType: 'json',
           data: {documento: documento},
           beforeSend: function() {
                 $('#carregando').css('display', 'block');
           }
            
       })
       .always(function(result) {

          $('#carregando').css('display', 'none');

          if(result.status == 'OK'){

            var cep = result.cep.replace('.','');
            var telefone = result.telefone.replace(' ','');

            // console.log(result);
             $('#sensenrazao').val(result.nome);
             $('#sensentel01').val(telefone);
            // $('#sempememail').val(result.email);
             $('#sensencep').val(cep);
             $('#sensenlogra').val(result.logradouro);
             $('#sensennumer').val(result.numero);
             $('#sensenbairr').val(result.bairro);
             $('#sensenuf').val(result.uf);
             $('#sensencidad').val(result.municipio);
             // $('#sempemcidad').html('<option value="'+result.municipio+'">'+result.municipio+'</option>');


          }else{
            //alert(result.message);
          }
        
       });

     }
     
  });




</script>
