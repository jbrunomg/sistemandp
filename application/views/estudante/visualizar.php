<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Estudante</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>estudante/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Estudante:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="palualcodig" value="<?php echo $dados[0]->palualcodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Estudante:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Estudante" name="salualnome" id="salualnome" value="<?php echo $dados[0]->salualnome; ?>">
					<?php echo form_error('salualnome'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">CPF:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Cpf" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="salualcpf" id="salualcpf" value="<?php echo $dados[0]->salualcpf; ?>">
					<?php echo form_error('salualcpf'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">RG:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Rg"  data-mask-selectonfocus="true" name="salualrg" id="salualrg" value="<?php echo $dados[0]->salualrg; ?>">
					<?php echo form_error('salualrg'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Data de Nascimento:</label>
					<div class="col-lg-5">																						
						<input type="text" class="form-control classData" name="talualniver" id="talualniver" value="<?php echo date('d/m/Y', strtotime($dados[0]->talualniver)); ?>">
					<?php echo form_error('testvaexpir'); ?>
					</div>										
				</div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Sexo:</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="ealualsexo" id="ealualsexo">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualsexo == '0')?'selected':''; ?>>Masculino</option>
                            <option value="1" <?php echo ($dados[0]->ealualsexo == '1')?'selected':''; ?>>Feminino</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualsexo'); ?>
                    </div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Habilitação:</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="ealualhabil" id="ealualhabil">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualhabil == '0')?'selected':''; ?>>Não</option>
                            <option value="1" <?php echo ($dados[0]->ealualhabil == '1')?'selected':''; ?>>Sim</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualsexo'); ?>
                    </div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Veiculo Próprio:</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="ealualvepro" id="ealualvepro">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualvepro == '0')?'selected':''; ?>>Não</option>
                            <option value="1" <?php echo ($dados[0]->ealualvepro == '1')?'selected':''; ?>>Sim</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualsexo'); ?>
                    </div>
                </div>
		

				<legend class="text-bold">Dados Logradouro:</legend>
				
				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="saluallogra" id="saluallogra" value="<?php echo $dados[0]->saluallogra;  ?>">
					<?php echo form_error('saluallogra'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualnumer" id="salualnumer" value="<?php echo $dados[0]->salualnumer;  ?>">
					<?php echo form_error('salualnumer'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualcompl" id="salualcompl" value="<?php echo $dados[0]->salualcompl;  ?>">
					<?php echo form_error('salualcompl'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualbairr" id="salualbairr" value="<?php echo $dados[0]->salualbairr;  ?>">
					<?php echo form_error('salualbairr'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualcidad" id="salualcidad" value="<?php echo $dados[0]->salualcidad; ?>">
					<?php echo form_error('salualcidad'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Estado:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualestad" id="salualestad" value="<?php echo $dados[0]->salualestad; ?>">
					<?php echo form_error('salualestad'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="salualcep" id="salualcep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->salualcep; ?>">
					<?php echo form_error('salualcep'); ?>
					</div>										
				</div>


				<legend class="text-bold">Dados Contato:</legend>

							<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input  disabled type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)9999-9999" data-mask-selectonfocus="true" name="salualtel01" id="salualtel01" value="<?php echo $dados[0]->salualtel01; ?>">
					<?php echo form_error('salualtel01'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular:</label>
					<div class="col-lg-5">
						<input  disabled type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel02" id="salualtel02" value="<?php echo $dados[0]->salualtel02; ?>">
					<?php echo form_error('salualtel02'); ?>
					</div>										
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input  disabled type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel03" id="salualtel03" value="<?php echo $dados[0]->salualtel03; ?>">
					<?php echo form_error('salualtel03'); ?>
					</div>										
				</div>			

				<div class="form-group">
					<label class="control-label col-lg-2">Email:</label>
					<div class="col-lg-5">
						<input disabled type="email" name="email"  placeholder="seu@email.com" class="form-control" name="salualemail" id="salualemail" value="<?php echo $dados[0]->salualemail; ?>">
					<?php echo form_error('salualemail'); ?>
					</div>										 
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Naturalidade:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Naturalidade"  data-mask-selectonfocus="true" name="salualnatur" id="salualnatur" value="<?php echo $dados[0]->salualnatur; ?>">
					<?php echo form_error('salualnatur'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Estado Civil:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Estado Civil"  data-mask-selectonfocus="true" name="salualestci" id="salualestci" value="<?php echo $dados[0]->salualestci; ?>">
					<?php echo form_error('salualestci'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Filiação:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome Pai:</label>
					<div class="col-lg-5">
						<input disabled type="text"  class="form-control" placeholder="Pai Responsável"  data-mask-selectonfocus="true" name="salualpai" id="salualpai" value="<?php echo $dados[0]->salualpai; ?>">
					<?php echo form_error('salualpai'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome Mãe:</label>
					<div class="col-lg-5">
						<input disabled type="text"  class="form-control" placeholder="Mãe Responsável"  data-mask-selectonfocus="true" name="salualmae" id="salualmae" value="<?php echo $dados[0]->salualmae; ?>">
					<?php echo form_error('salualmae'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Acadêmico:</legend>

	
				<div class="form-group">
                	<label class="control-label col-lg-2">Instintuição de Ensino:</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="instensino" id="instensino">
                       		<option  value="">Selecione</option>
                            <?php foreach ($ensino as $valor) { ?>
                            	  <?php $selected = ($valor->pensencodig == $dados[0]->ialualinsen)?'SELECTED': ''; ?>
		                              <option  value="<?php echo $valor->pensencodig; ?>" <?php  echo $selected; ?>><?php echo $valor->sensennome; ?></option>
		                        <?php } ?>
                           
                        </select>
                    </div>
                </div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Curso :</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="curso" id="curso">
                       		<option  value="">Selecione</option>
                            <?php foreach ($curso as $valor) { ?>
                            	  <?php $selected = ($valor->pcurcucodig == $dados[0]->ialualcurso )?'SELECTED': ''; ?>
		                              <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
		                        <?php } ?>
                           
                        </select>
                    </div>
                </div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Periodo:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Periodo"  data-mask-selectonfocus="true" name="salualperio" id="salualperio" value="<?php echo $dados[0]->salualperio; ?>">
					<?php echo form_error('salualperio'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Turno:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Turno"  data-mask-selectonfocus="true" name="salualturno" id="salualturno" value="<?php echo $dados[0]->salualturno; ?>">
					<?php echo form_error('salualturno'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Disponível:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Turno"  data-mask-selectonfocus="true" name="salualdispo" id="salualdispo" value="<?php echo $dados[0]->salualdispo; ?>">
					<?php echo form_error('salualdispo'); ?>
					</div>										
				</div>
		
				<legend class="text-bold">Conhecimento Infomática:</legend>

 				<div class="row">
				   	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Windows</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualwindo == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Linux</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealuallinux == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Word</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualword == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Excel</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualexel == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Power Point</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualpower == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>		
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Internet</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualinter == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>


		    	</div>	

		    		<div class="form-group">
						<label class="control-label col-lg-2">Outros:</label>
					
						<div class="col-lg-5">
							<input disabled type="text" class="form-control" placeholder="Outros"  data-mask-selectonfocus="true" name="salualoutro" id="salualoutro" value="<?php echo $dados[0]->salualoutro; ?>">
						<?php echo form_error('salualoutro'); ?>
						</div>										
					</div>




				<legend class="text-bold">Conhecimento Idiomas:</legend>
				<div class="row">
				   	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Inglês</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualingle == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Espanhol:</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualespan == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Francês:</h6>
							<label class="checkbox-inline">
								<input disabled type="checkbox" class="styled"  <?php echo ($dados[0]->ealualfranc == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>	

				</div>   

					<div class="form-group">
					  <label class="control-label col-lg-2">Outros:</label>
					  <textarea disabled class="form-control" rows="2" id="salualoutro"><?php echo $dados[0]->salualoutro; ?></textarea>
					  <?php echo form_error('salualoutro'); ?>
					</div>
			

				<legend class="text-bold">Informações Profissionais:</legend>

					<div class="form-group">
					  <label class="control-label col-lg-2">Experiências:</label>
					  <textarea disabled class="form-control" rows="5" id="salualexper"><?php echo $dados[0]->salualexper; ?></textarea>
					  <?php echo form_error('salualexper'); ?>
					</div>	

					<div class="form-group">
					  <label disabled class="control-label col-lg-2">Observações:</label>
					  <textarea class="form-control" rows="4" id="salualobser"><?php echo $dados[0]->salualobser; ?></textarea>
					  <?php echo form_error('salualobser'); ?>
					</div>								
		
            </fieldset>  
		</form>
	</div>
</div>
