<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Estudante</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Estudante:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="palualcodig" value="<?php echo $dados[0]->palualcodig; ?>" />

				
				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Estudante:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Estudante" name="salualnome" id="salualnome" value="<?php echo $dados[0]->salualnome; ?>">
					<?php echo form_error('salualnome'); ?>
					</div>										
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">CPF:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Cpf" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="salualcpf" id="salualcpf" value="<?php echo $dados[0]->salualcpf; ?>">
					<?php echo form_error('salualcpf'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">RG:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Rg"  data-mask-selectonfocus="true" name="salualrg" id="salualrg" value="<?php echo $dados[0]->salualrg; ?>">
					<?php echo form_error('salualrg'); ?>
					</div>										
				</div>

				<div class="form-group">
		          <label class="control-label col-lg-2">Orgõ Exped:</label>
		          <div class="col-lg-5">
		            <input  type="text" class="form-control" placeholder="orgão expedição"  data-mask-selectonfocus="true" name="salualrg_org_expd" id="salualrg_org_expd" value="<?php echo $dados[0]->salualrg_org_expd; ?>">
		          <?php echo form_error('salualrg_org_expd'); ?>
		          </div>                    
		        </div>

				<div class="form-group">
					<label class="control-label col-lg-2">Data de Nascimento:</label>
					<div class="col-lg-5">																						
						<input type="text" class="form-control classData" name="talualniver" id="talualniver" value="<?php echo date('d/m/Y', strtotime($dados[0]->talualniver)); ?>">
					<?php echo form_error('talualniver'); ?>
					</div>										
				</div>

				<div class="form-group">
		          <label class="control-label col-lg-2">Naturalidade:</label>
		          <div class="col-lg-5">
		            <input  type="text" class="form-control" placeholder="Naturalidade"  data-mask-selectonfocus="true" name="salualnatur" id="salualnatur" value="<?php echo $dados[0]->salualnatur; ?>">
		          <?php echo form_error('salualnatur'); ?>
		          </div>                    
		        </div>

		        <div class="form-group">
                	<label class="control-label col-lg-2">Estado Civil:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="salualestci" id="salualestci">
                	    <option value="">Estado Civil</option>
	                    <option value="Solteiro" <?php echo ($dados[0]->salualestci == 'Solteiro')?'selected':''; ?>>Solteiro(a)</option>
	                    <option value="Casado" <?php echo ($dados[0]->salualestci == 'Casado')?'selected':''; ?>>Casado(a)</option>
	                    <option value="Divorciado" <?php echo ($dados[0]->salualestci == 'Divorciado')?'selected':''; ?>>Divorciado(a)</option>
	                    <option value="Viuvo" <?php echo ($dados[0]->salualestci == 'Viuvo')?'selected':''; ?>>Viúvo(a)</option>
	                    <option value="Separado" <?php echo ($dados[0]->salualestci == 'Separado')?'selected':''; ?>>Separado(a)</option>                         
                                                      
                        </select>
                        <?php echo form_error('salualestci'); ?>
                    </div>
                </div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Sexo:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="ealualsexo" id="ealualsexo">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualsexo == '0')?'selected':''; ?>>Masculino</option>
                            <option value="1" <?php echo ($dados[0]->ealualsexo == '1')?'selected':''; ?>>Feminino</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualsexo'); ?>
                    </div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Habilitação:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="ealualhabil" id="ealualhabil">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualhabil == '0')?'selected':''; ?>>Sim</option>
                            <option value="1" <?php echo ($dados[0]->ealualhabil == '1')?'selected':''; ?>>Não</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualhabil'); ?>
                    </div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Veiculo Próprio:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="ealualvepro" id="ealualvepro">
                        	<option value="">Selecione</option>
                            <option value="0" <?php echo ($dados[0]->ealualvepro == '0')?'selected':''; ?>>Sim</option>
                            <option value="1" <?php echo ($dados[0]->ealualvepro == '1')?'selected':''; ?>>Não</option>                          
                                                      
                        </select>
                        <?php echo form_error('ealualvepro'); ?>
                    </div>
                </div>


                <legend class="text-bold">Dados Filiação:</legend>

		        <div class="form-group">
		          <label class="control-label col-lg-2">Nome Pai:</label>
		          <div class="col-lg-5">
		            <input  type="text"  class="form-control" placeholder="Pai Responsável"  data-mask-selectonfocus="true" name="salualpai" id="salualpai" value="<?php echo $dados[0]->salualpai; ?>">
		          <?php echo form_error('salualpai'); ?>
		          </div>                    
		        </div>
		        
		        <div class="form-group">
		          <label class="control-label col-lg-2">Profissão do Pai:</label>
		          <div class="col-lg-5">
		            <input  type="text"  class="form-control" placeholder="Profissão do Pai"  data-mask-selectonfocus="true" name="salualpprof" id="salualpprof" value="<?php echo $dados[0]->salualpprof; ?>">
		          <?php echo form_error('salualpprof'); ?>
		          </div>                    
		        </div>

		        <div class="form-group">
		          <label class="control-label col-lg-2">Nome Mãe:</label>
		          <div class="col-lg-5">
		            <input  type="text"  class="form-control" placeholder="Mãe Responsável"  data-mask-selectonfocus="true" name="salualmae" id="salualmae" value="<?php echo $dados[0]->salualmae; ?>">
		          <?php echo form_error('salualmae'); ?>
		          </div>                    
		        </div>  

		        <div class="form-group">
		          <label class="control-label col-lg-2">Profissão da Mãe:</label>
		          <div class="col-lg-5">
		            <input  type="text"  class="form-control" placeholder="Profissão da Mãe"  data-mask-selectonfocus="true" name="salualmprof" id="salualmprof" value="<?php echo $dados[0]->salualmprof; ?>">
		          <?php echo form_error('salualmprof'); ?>
		          </div>                    
		        </div>       
		

				<legend class="text-bold">Dados Logradouro:</legend>
				
				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualcep" id="salualcep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->salualcep; ?>">
					<?php echo form_error('salualcep'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="saluallogra" id="saluallogra" value="<?php echo $dados[0]->saluallogra;  ?>">
					<?php echo form_error('saluallogra'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualnumer" id="salualnumer" value="<?php echo $dados[0]->salualnumer;  ?>">
					<?php echo form_error('salualnumer'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualcompl" id="salualcompl" value="<?php echo $dados[0]->salualcompl;  ?>">
					<?php echo form_error('salualcompl'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualbairr" id="salualbairr" value="<?php echo $dados[0]->salualbairr;  ?>">
					<?php echo form_error('salualbairr'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualcidad" id="salualcidad" value="<?php echo $dados[0]->salualcidad; ?>">
					<?php echo form_error('salualcidad'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Estado:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="salualestad" id="salualestad" value="<?php echo $dados[0]->salualestad; ?>">
					<?php echo form_error('salualestad'); ?>
					</div>										
				</div>	


				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)9999-9999" data-mask-selectonfocus="true" name="salualtel01" id="salualtel01" value="<?php echo $dados[0]->salualtel01; ?>">
					<?php echo form_error('salualtel01'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular:</label>
					<div class="col-lg-5">
						<input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel02" id="salualtel02" value="<?php echo $dados[0]->salualtel02; ?>">
					<?php echo form_error('salualtel02'); ?>
					</div>										
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel03" id="salualtel03" value="<?php echo $dados[0]->salualtel03; ?>">
					<?php echo form_error('salualtel03'); ?>
					</div>										
				</div>				

				<div class="form-group">
					<label class="control-label col-lg-2">Email:</label>
					<div class="col-lg-5">
						<input  type="email" placeholder="seu@email.com" class="form-control" name="salualemail" id="salualemail" value="<?php echo $dados[0]->salualemail; ?>">
					<?php echo form_error('salualemail'); ?>
					</div>										 
				</div>	

		

				<legend class="text-bold">Dados Acadêmico:</legend>

				<div class="form-group">
                	<label class="control-label col-lg-2">Curso :</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="pcurcucodig" id="pcurcucodig">
                       		<option  value="">Selecione</option>
                            <?php foreach ($curso as $valor) { ?>
                            	  <?php $selected = ($valor->pcurcucodig == $dados[0]->ialualcurso )?'SELECTED': ''; ?>
		                              <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
		                        <?php } ?>                           
                        </select>
                    </div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Nº Matricula:</label>
                	<div class="col-lg-5">
						<input  type="text" placeholder="Matricula" class="form-control" name="salualmatricula" id="salualmatricula" value="<?php echo $dados[0]->salualmatricula; ?>">
					<?php echo form_error('salualmatricula'); ?>
					</div>
                </div>

                <div class="form-group">
                	<label class="control-label col-lg-2">Nivel:</label>	                
	                <div class="col-lg-5">
	                	<select class="form-control" id="ialualcurso_nivel" name="ialualcurso_nivel">
		                    <option value="" <?php if($dados[0]->ialualcurso_nivel == ''){ echo "SELECTED";}?>>Selecione</option>
		                    <option value="Medio" <?php if($dados[0]->ialualcurso_nivel == 'Medio'){ echo "SELECTED";}?>>Médio</option>
		                    <option value="Tecnico" <?php if($dados[0]->ialualcurso_nivel == 'Tecnico'){ echo "SELECTED";}?>>Técnico</option>
		                    <option value="Superior" <?php if($dados[0]->ialualcurso_nivel == 'Superior'){ echo "SELECTED";}?>>Superior</option>
		                    <option value="Pos" <?php if($dados[0]->ialualcurso_nivel == 'Pos'){ echo "SELECTED";}?>>Pós Graduação</option>
		                </select>		     
		            </div>    
	            </div>	          
				

				<div class="form-group">
                	<label class="control-label col-lg-2">Instintuição de Ensino:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="pensencodig" id="pensencodig">
                       		<option  value="">Selecione</option>
                            <?php foreach ($ensino as $valor) { ?>
                            	  <?php $selected = ($valor->pensencodig == $dados[0]->ialualinsen)?'SELECTED': ''; ?>
		                              <option  value="<?php echo $valor->pensencodig; ?>" <?php  echo $selected; ?>><?php echo $valor->sensennome; ?></option>
		                        <?php } ?>
                           
                        </select>
                    </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-lg-2">Período / Série:</label>
                    <div class="col-lg-5">
	                  <select class="form-control" id="salualperio" name="salualperio">
	                  	  <option value="" <?php if($dados[0]->salualperio == ''){ echo "SELECTED";}?>>Selecione</option>
	                      <option value="1º" <?php if($dados[0]->salualperio == '1º'){ echo "SELECTED";}?>>1º</option>
	                      <option value="2º" <?php if($dados[0]->salualperio == '2º'){ echo "SELECTED";}?>>2º</option>
	                      <option value="3º" <?php if($dados[0]->salualperio == '3º'){ echo "SELECTED";}?>>3º</option>
	                      <option value="4º" <?php if($dados[0]->salualperio == '4º'){ echo "SELECTED";}?>>4º</option>
	                      <option value="5º" <?php if($dados[0]->salualperio == '5º'){ echo "SELECTED";}?>>5º</option>
	                      <option value="6º" <?php if($dados[0]->salualperio == '6º'){ echo "SELECTED";}?>>6º</option>
	                      <option value="7º" <?php if($dados[0]->salualperio == '7º'){ echo "SELECTED";}?>>7º</option>
	                      <option value="8º" <?php if($dados[0]->salualperio == '8º'){ echo "SELECTED";}?>>8º</option>
	                      <option value="9º" <?php if($dados[0]->salualperio == '9º'){ echo "SELECTED";}?>>9º</option>
	                      <option value="10º" <?php if($dados[0]->salualperio == '10º'){ echo "SELECTED";}?>>10º</option>
	                      <option value="11º" <?php if($dados[0]->salualperio == '11º'){ echo "SELECTED";}?>>11º</option>
	                      <option value="12º" <?php if($dados[0]->salualperio == '12º'){ echo "SELECTED";}?>>12º</option>
	                      <option value="13º" <?php if($dados[0]->salualperio == '13º'){ echo "SELECTED";}?>>13º</option>
	                      <option value="14º" <?php if($dados[0]->salualperio == '14º'){ echo "SELECTED";}?>>14º</option>               
	                  </select>
                    </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-lg-2">Turno:</label>
                  <div class="col-lg-5">
	                  <select class="form-control" id="salualturno" name="salualturno">
	                  	<option value="" <?php if($dados[0]->salualturno == ''){ echo "SELECTED";}?>>Selecione</option>
	                    <option value="Manha" <?php if($dados[0]->salualturno == 'Manha'){ echo "SELECTED";}?>>Manhã</option>
	                    <option value="Tarde" <?php if($dados[0]->salualturno == 'Tarde'){ echo "SELECTED";}?>>Tarde</option>
	                    <option value="Noite" <?php if($dados[0]->salualturno == 'Noite'){ echo "SELECTED";}?>>Noite</option>
	                    <option value="Manha/Tarde" <?php if($dados[0]->salualturno == 'Manha/Tarde'){ echo "SELECTED";}?>>Manhã/Tarde</option>
	                    <option value="Manha/Noite" <?php if($dados[0]->salualturno == 'Manha/Noite'){ echo "SELECTED";}?>>Manhã/Noite</option>
	                    <option value="Tarde/Noite" <?php if($dados[0]->salualturno == 'Tarde/Noite'){ echo "SELECTED";}?>>Tarde/Noite</option>
	                    <option value="Integral" <?php if($dados[0]->salualturno == 'Integral'){ echo "SELECTED";}?>>Integral</option>
	                  </select>
                  </div>
                </div>


                <div class="form-group">
					<label class="control-label col-lg-2">Data Término:</label>
					<div class="col-lg-5">													
						<input type="text" class="form-control classData" name="salualtermino" id="salualtermino" value="<?php echo date('d/m/Y', strtotime($dados[0]->salualtermino)); ?>">
					<?php echo form_error('salualtermino'); ?>
					</div>										
				</div>
		

				<div class="form-group">
	                <label class="control-label col-lg-2">Disponibilidade:</label>
	                <div class="col-lg-5">
		                <select class="form-control" id="salualdispo" name="salualdispo">
		                  <option value="Manha" <?php if($dados[0]->salualdispo == 'Manha'){ echo "SELECTED";}?>>Manhã</option>
		                  <option value="Tarde" <?php if($dados[0]->salualdispo == 'Tarde'){ echo "SELECTED";}?>>Tarde</option>
		                  <option value="Noite" <?php if($dados[0]->salualdispo == 'Noite'){ echo "SELECTED";}?>>Noite</option>
		                  <option value="Manha/Tarde" <?php if($dados[0]->salualdispo == 'Manha/Tarde'){ echo "SELECTED";}?>>Manhã/Tarde</option>
		                  <option value="Manha/Noite" <?php if($dados[0]->salualdispo == 'Manha/Noite'){ echo "SELECTED";}?>>Manhã/Noite</option>
		                  <option value="Tarde/Noite" <?php if($dados[0]->salualdispo == 'Tarde/Noite'){ echo "SELECTED";}?>>Tarde/Noite</option>
		                  <option value="Integral" <?php if($dados[0]->salualdispo == 'Integral'){ echo "SELECTED";}?>>Integral</option>
		                </select>
		            </div>    
	            </div>
			
				<legend class="text-bold">Conhecimento Infomática:</legend>

 				<div class="row">
				   	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Windows</h6>
							<label class="checkbox-inline">

								<input class="styled" name="ealualwindo" type="checkbox" value="1" <?php if($dados[0]->ealualwindo <> 0){ echo 'checked';}else{ echo '';} ?>>
								Checked
							</label>
						
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Linux</h6>
							<label class="checkbox-inline">
								<input class="styled" name="ealuallinux" type="checkbox" value="1"  <?php if($dados[0]->ealuallinux <> 0){ echo 'checked';}else{ echo '';} ?>>
								Checked							
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Word</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualword" type="checkbox" value="1"   <?php echo ($dados[0]->ealualword == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Excel</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualexel" type="checkbox" value="1"   <?php echo ($dados[0]->ealualexel == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Power Point</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualpower" type="checkbox" value="1"   <?php echo ($dados[0]->ealualpower == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>		
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Internet</h6>
							<label class="checkbox-inline">
								<input class="styled" name="ealualinter" type="checkbox" value="1"   <?php echo ($dados[0]->ealualinter == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>


		    	</div>	

		    		<div class="form-group">
						<label class="control-label col-lg-2">Outros:</label>
					
						<div class="col-lg-5">
							<input  type="text" class="form-control" placeholder="Outros"  data-mask-selectonfocus="true" name="salualoutro" id="salualoutro" value="<?php echo $dados[0]->salualoutro; ?>">
						<?php echo form_error('salualoutro'); ?>
						</div>										
					</div>




				<legend class="text-bold">Conhecimento Idiomas:</legend>
				<div class="row">
				   	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Inglês</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualingle" type="checkbox" value="1"  <?php echo ($dados[0]->ealualingle == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Espanhol:</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualespan" type="checkbox" value="1"  <?php echo ($dados[0]->ealualespan == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Francês:</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualfranc" type="checkbox" value="1"  <?php echo ($dados[0]->ealualfranc == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>	
			    	<div class="col-md-2">
			    		<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Alemão:</h6>
							<label class="checkbox-inline">
								<input  class="styled" name="ealualalema" type="checkbox" value="1"  <?php echo ($dados[0]->ealualalema == '1')?'checked':''; ?>>
								Checked
							</label>
						</div>
			    	</div>	

				</div>   

					<div class="form-group">
					  <label class="control-label col-lg-2">Outros:</label>
					  <textarea  class="form-control" rows="2" id="salualoutro"  name="salualoutro"  ><?php echo $dados[0]->salualoutro; ?></textarea>
					  <?php echo form_error('salualoutro'); ?>
					</div>
			

				<legend class="text-bold">Informações Profissionais:</legend>

					<div class="form-group">
					  <label class="control-label col-lg-2">Experiências:</label>
					  <textarea  class="form-control" rows="5" id="salualexper" name="salualexper"  ><?php echo $dados[0]->salualexper; ?></textarea>
					  <?php echo form_error('salualexper'); ?>
					</div>	

					<div class="form-group">
					  <label  class="control-label col-lg-2">Observações:</label>
					  <textarea class="form-control" rows="4" id="salualobser" name="salualobser"  ><?php echo $dados[0]->salualobser; ?></textarea>
					  <?php echo form_error('salualobser'); ?>
					</div>		


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
