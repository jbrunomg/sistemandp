  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<style type="text/css">
  .my_text
  {
      font-family:    Arial;
      font-weight:    bold;
  }
</style>
<!-- Form horizontal -->
<div class="panel panel-flat">
  <div class="panel-heading">
    <div class="heading-elements">
      <ul class="icons-list">
         <!--  <li><a data-action="collapse"></a></li>
         <li><a data-action="reload"></a></li>
         <li><a data-action="close"></a></li> -->
          </ul>
      </div>
  </div>
  <div class="panel-body"> 
  
   <!-- <div class="my_text" style="" width="1000px" id='Ficha' > -->

    <div class="container">

      <table>        
        <tr>  
            <td width="200px"><img src="<?php echo $emitente[0]->url_logo; ?>" alt="Nudep"></td>
            <td width="700px" style="font-size: 30px; padding-left: 200px"> <b><u>FICHA DE INSCRIÇÃO</u></b></td>
            <td width="100px"> 
              <div style="width: 80px;height: 100px;margin: 20px 40px 0 0; border: solid 1px #000;"><div>
            </td>                                
        </tr> 
      </table>
      
      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 500px;font-size: 19px;"><b>Data de Inscrição: </b> <?php echo (isset($dados[0]->datacadastro))?$dados[0]->datacadastro:'______/______/______';  ?></td>
            <!-- <td style="font-size: 19px; padding-left: 150px"><b>Inscrição N.º: </b><?php echo (isset($dados[0]->palualcodig))?$dados[0]->palualcodig:' _______________________________________'; ?></td> -->
        </tr> 
      </table>

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 800px;font-size: 19px;"><b>Nome: </b><?php echo (isset($dados[0]->salualnome))?$dados[0]->salualnome:'_____________________________________________________________________________________________________________________'; ?></td>
            <td style="font-size: 19px;"><b>Dt. Nasc: </b><?php echo (isset($dados[0]->talualniver))?date('d/m/Y', strtotime($dados[0]->talualniver)):'______/______/______'; ?></td>
        </tr> 
      </table>  

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>Endereço: </b><?php echo (isset($dados[0]->saluallogra))?$dados[0]->saluallogra.','.$dados[0]->salualnumer.' '.$dados[0]->salualcompl:'__________________________________________________________________________________________________________________________________________________'; ?></td>           
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 400px;font-size: 19px;"><b>Bairro: </b><?php echo (isset($dados[0]->salualbairr))?$dados[0]->salualbairr:' _______________________________________________________'; ?></td>
            <td style="width: 300px;font-size: 19px;"><b>Cidade/UF: </b><?php echo (isset($dados[0]->salualcidad))?$dados[0]->salualcidad."/".$dados[0]->salualestad:'___________________________________'; ?></td>
            <td style="width: 300px;font-size: 19px;"><b>CEP: </b><?php echo (isset($dados[0]->salualcep))?$dados[0]->cep:'________________________________________'; ?></td>
            
        </tr> 
      </table>

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 250px;font-size: 19px;"><b>Fone: </b><?php echo (isset($dados[0]->salualtel01))?$dados[0]->salualtel01: '_______________________';  ?></td>
            <td style="width: 250px;font-size: 19px;"><b>Cel.: </b><?php echo (((isset($dados[0]->salualtel02))?$dados[0]->salualtel02:'') .'<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. ((isset($dados[0]->salualtel03))?$dados[0]->salualtel03:  ' _________________________________'));  ?></td>
            <td style="width: 250px;font-size: 19px;"><b>RG: </b><?php echo (isset($dados[0]->salualrg))?$dados[0]->salualrg: ' __________________________________';  ?></td>
            <td style="width: 250px;font-size: 19px;"><b>Órgão Exp.: </b><?php echo (isset($dados[0]->salualrg_org_expd))?$dados[0]->salualrg_org_expd: ' _____________ ';  ?></td>            
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 400px;font-size: 19px;"><b>CPF: </b><?php echo (isset($dados[0]->salualcpf))?$dados[0]->cpf: ' _____________._____________._____________-_____________';  ?></td>
            <td style="width: 600px;font-size: 19px;"><b>E-mail: </b><?php echo (isset($dados[0]->salualemail))?$dados[0]->salualemail: '_____________________________________________________________________________________';  ?></td>            
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 350px;font-size: 19px;"><b>C. Habilitação: (&nbsp;<?php  if ($dados[0]->ealualhabil == '0'){ echo 'X';} ?>&nbsp;)  SIM  &nbsp; (&nbsp;<?php  if ($dados[0]->ealualhabil == '1'){echo 'X';} ?>&nbsp;)  NÃO </b> </td>
            <td style="width: 350px;font-size: 19px;"><b>Veículo Própio: (&nbsp;<?php echo ($dados[0]->ealualvepro == '0')?'X':''; ?>&nbsp;)  SIM  &nbsp; (&nbsp;<?php echo ($dados[0]->ealualvepro == '1')?'X':''; ?>&nbsp;)  NÃO </b> </td>
            <td style="width: 300px;font-size: 19px;"><b>Sexo: (&nbsp;<?php echo ($dados[0]->ealualsexo == 0)?'X':''; ?>&nbsp;)  Masc &nbsp;   (&nbsp;<?php echo ($dados[0]->ealualsexo == 1)?'X':''; ?>&nbsp;) Fem.  </b> </td>
            
        </tr> 
      </table>

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"><b>Estado Civil: </b><?php echo (isset($dados[0]->salualestci))?$dados[0]->salualestci: '_________________________________________________________________________________';  ?></td>            
            <td style="width: 400px;font-size: 19px;"><b>Naturalidade: </b><?php echo (isset($dados[0]->salualnatur))?$dados[0]->salualnatur: ' ______________________________________________';  ?></td>
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"><b>Curso 01: </b><?php echo (isset($dados[0]->scurcunome))?$dados[0]->scurcunome: '_______________________________________________________________________________________';  ?></td>
            <td style="width: 600px;font-size: 19px;"><b>Nivel 01: </b><?php echo (isset($dados[0]->ialualcurso_nivel))?$dados[0]->ialualcurso_nivel: '_________________________________________________________________________';  ?></td>                        
            <td style="width: 400px;font-size: 19px;"><b>Período/Série 01: </b><?php echo (isset($dados[0]->salualperio))?$dados[0]->salualperio: ' _____________________________________________';  ?></td>
        </tr> 
      </table> 
      
      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"><b>Ins. Ensino 01: </b><?php echo (isset($dados[0]->sensennome))?$dados[0]->sensennome: '__________________________________________________________________________________';  ?></td>            
            <td style="width: 400px;font-size: 19px;"><b>Turno 01: </b><?php echo (isset($dados[0]->salualturno))?$dados[0]->salualturno: ' _____________________________________________________';  ?></td>
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>
        <?php foreach ($curso as $c) { 
             if($dados[0]->ialualcurso2 == $c->pcurcucodig){  ?>  
            <td style="width: 600px;font-size: 19px;"><b>Curso 023: </b><?php  echo  $c->scurcunome; } ?></td> 
        <?php } ?>    

   
            <td style="width: 600px;font-size: 19px;"><b>Nivel  02: </b><?php echo (isset($dados[0]->ialualcurso_nivel2))?$dados[0]->ialualcurso_nivel2: '_________________________________________________________________________';  ?></td>                        
            <td style="width: 400px;font-size: 19px;"><b>Período/Série  02: </b><?php echo (isset($dados[0]->salualperio2))?$dados[0]->salualperio2: ' _____________________________________________';  ?></td>
        </tr> 
      </table> 
      
      <table style="margin-top: 5px;">                
        <tr>             
          <?php foreach ($ensino as $e) { 
             if($dados[0]->ialualinsen2 == $e->pensencodig){  ?>  
              <td style="width: 600px;font-size: 19px;"><b>Ins. Ensino  02: </b><?php echo $e->sensennome; } ?></td>            
          <?php } ?> 
            <td style="width: 400px;font-size: 19px;"><b>Turno  02: </b><?php echo (isset($dados[0]->salualturno2))?$dados[0]->salualturno2: ' _____________________________________________________';  ?></td>
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>Horário Disponível para estágio: </b> <?php echo (isset($dados[0]->salualdispo))?$dados[0]->salualdispo: '(&nbsp;&nbsp;)  MANHÃ &nbsp; (&nbsp;&nbsp;)  TARDE &nbsp; (&nbsp;&nbsp;)  NOITE';  ?> </td>
        </tr> 
      </table>

      <br>
      <h5><b><u>FILIAÇÃO(opcional)</u></b></h5>

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"><b>Pai: </b><?php echo (isset($dados[0]->salualpai))?$dados[0]->salualpai: '__________________________________________________________________________________________';  ?></td>            
            <td style="width: 400px;font-size: 19px;"><b>Profissão: </b><?php echo (isset($dados[0]->salualpprof))?$dados[0]->salualpprof: ' ________________________________________________';  ?></td>
        </tr> 
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"><b>Mãe: </b><?php echo (isset($dados[0]->salualmae))?$dados[0]->salualmae: '_________________________________________________________________________________________';  ?></td>            
            <td style="width: 400px;font-size: 19px;"><b>Profissão: </b><?php echo (isset($dados[0]->salualmprof))?$dados[0]->salualmprof: ' ________________________________________________';  ?></td>
        </tr> 
      </table> 

      <br>
      <h5><b><u>CONHECIMENTO EM INFORMÁTICA MARQUE UM (X)</u></b></h5>
      
      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>(&nbsp;<?php echo ($dados[0]->ealualwindo)?'X':''; ?>&nbsp;)  WINDOWS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualword)?'X':''; ?>&nbsp;)  WORD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualexel)?'X':''; ?>&nbsp;)  EXCEL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualpower)?'X':''; ?>&nbsp;)  POWERPOINT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualinter)?'X':''; ?>&nbsp;)  INTERNET &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealuallinux)?'X':''; ?>&nbsp;)  LINUX</b> </td>
        </tr> 
      </table>  

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>OUTROS: </b> ________________________________________________________________________________________________________________________</td>           
        </tr> 
      </table>  

      <br>
      <h5><b><u>IDIOMAS MARQUE UM (X)</u></b></h5>
      
      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>(&nbsp;<?php echo ($dados[0]->ealualingle)?'X':''; ?>&nbsp;)  INGLÊS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualespan)?'X':''; ?>&nbsp;)  ESPANHOL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->ealualfranc)?'X':''; ?>&nbsp;)  FRANCÊS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;)  ALEMÃO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (&nbsp;<?php echo ($dados[0]->salualoutro)?'X':''; ?>&nbsp;)  OUTROS:
            </b> _________________________________________</td>
        </tr> 
      </table>    

      <br>
      <h5><b><u>EXPERIÊNCIAS PROFISSIONAIS.</u></b></h5> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b></b> ________________________________________________________________________________________________________________________</td>            
        </tr>         
      </table> 

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b></b> ________________________________________________________________________________________________________________________</td>           
        </tr>         
      </table>

      <table style="margin-top: 5px;">                
        <tr>  
            <td style="width: 1000px;font-size: 19px;"><b>OBSERVAÇÕES: </b> ________________________________________________________________________________________________________________________</td>            
        </tr>         
      </table>

      <br>

      <table class="table table-bordered">
        <h4>Historico estágio / Recrutamento</h4>
        <thead>
          <tr>
            <th style="font-size: 12px;" >Tipo</th>      
            <th style="font-size: 12px;" >Empresa</th>      
            <th style="font-size: 12px;" >Dt Inicial</th>
          <!--   <th style="font-size: 12px;" >Dt Termino</th>
            <th style="font-size: 12px;" >Dt Renovação</th>
            <th style="font-size: 12px;" >Dt Renovação II</th>
            <th style="font-size: 12px;" >Renovação III</th> -->
            <th style="font-size: 12px;" >Situação</th>
          </tr>
        </thead>
        <tbody>

        <?php foreach ($contratos as $valor){ ?>  
        <tr>  
          <td style="font-size: 15px;">Contrato</td>    
          <td style="width: 600px;font-size: 15px;"><?php echo $valor->sempemrazao; ?></td> 
          <td style="font-size: 15px;"><?php echo date('d/m/Y', strtotime($valor->contrestempr_data_ini)); ?></td> 
        <!--  <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renII; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renI; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renII; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renIII; ?></td> --> 
          <td style="font-size: 15px;"><?php echo $valor->contrestempr_status; ?></td>        
        </tr>
      
        <?php } ?>

        <?php foreach ($recrutamento as $valor){ ?>  
        <tr>  
          <td style="font-size: 15px;">Recrutamento</td>    
          <td style="width: 600px;font-size: 15px;"><?php echo $valor->sempemrazao; ?></td> 
          <td style="font-size: 15px;"><?php echo date('d/m/Y', strtotime($valor->recrut_data_cadastro)); ?></td> 
        <!--  <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renII; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renI; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renII; ?></td> 
          <td style="width: 1000px;font-size: 19px;"><?php echo $valor->contrestempr_data_renIII; ?></td> --> 
          <td style="font-size: 15px;"><?php echo $valor->recrut_status; ?></td>        
        </tr>
      
        <?php } ?>

        </tbody>

      </table>

      <br><br>

      <table style="margin-top: 10px;">                
        <tr>  
            <td style="width: 600px;font-size: 19px;"></td>            
            <td style="width: 400px;font-size: 19px;"><b></b> _________________________________________________________</td>
        </tr> 
      </table>

      <table style="margin-top: 0px;">                
        <tr>  
            <td style="width: 700px;font-size: 19px;"></td>            
            <td style="width: 300px;font-size: 19px;"><b></b> ASSINATURA DO ESTUDANTE</td>
        </tr> 
      </table>

  </div>