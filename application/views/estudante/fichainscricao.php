<!-- Form horizontal -->
<div class="panel panel-flat">
  <div class="panel-heading">
    <div class="col-md-3 col-sm-6 row">
      <a class="btn bg-teal btn-block" id='imprimir' href="javascript;">Imprimir <i class="icon-plus2 position-right"></i></a>
    </div>
    <div class="heading-elements">
      <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
            <li><a data-action="reload"></a></li>
            <li><a data-action="close"></a></li>
          </ul>
      </div>
  </div>
 
  <div class="panel-body"> 



    <!-- Início do corpo da FICHA INSCRIÇÃO -->
        <table border=1 width='100%' height='800' class="col-md-6" id='Ficha' >
          <tbody>
            <!-- Razão social -->
            <tr>                                      
              <td colspan=5>
                <center> <?php //echo $emitente[0]->nome; ?> </center>
              </td>
            </tr>
            <tr>
              <td colspan=5>                                      
                <table width='100%'>
                  <tr>  
                      <td  ROWSPAN=3 style="width: 10%"><img src="<?php echo base_url() ?>public/assets/images/logo.png" alt="Nudep"></td>                                            
                  </tr>
                  <tr>                                                                                      
                    <td colspan=3> CNPJ: <?php //echo $emitente[0]->cnpj; ?> </td>
                    <td colspan=3> E-mail: <?php //echo $emitente[0]->email; ?> </td>
                    <!-- <td colspan=3> Data de Inscrição: <?php echo date('d/m/Y', strtotime($dados[0]->date_operacao)) ?></td> -->
                    <td colspan=3> Data de Inscrição: <?php echo $dados[0]->datacadastro ?></td> 
                  </tr>
                  <tr>
                    <td colspan=6> Rua João Fernandes Vieira, 574 sala 602 - Boa Vista - Recife-PE CEP: 50.050-200 </td>
                    <td colspan=4> Telefone: (81) 3221-4740 </td>
                  </tr>
                  <tr>
                    <td colspan=10>
                      <center>
                        <table border=1>
                          <tr>
                            <td> Inscrição N.º <?php echo $dados[0]->palualcodig ?> </td>
                          </tr>
                        </table>
                      </center>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan=4> Nome: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualnome;  ?></span> </td>
              <td> Dt. Nasc: <?php echo date('d/m/Y', strtotime($dados[0]->talualniver)) ?></td>
            </tr>
            <tr>
              <td colspan=6> Endereço: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->saluallogra ?></span></td>
              
            </tr>
            <tr>
              <td colspan=2> Bairro: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualbairr ?></span></td>
              <td colspan=2> Cidade/UF: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualcidad."/".$dados[0]->salualestad ?></span></td>
              <td> CEP: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualcep ?></span></td>                                      
            </tr>
            <tr>
              <td colspan=2> Fone: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualtel01 ?></span> </td>
              <td colspan=2> Cel: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualtel02 ?></span></td>
              <td> CPF: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualcpf ?></span></td>
            </tr>
            <tr>
              <td colspan=2> E-mail: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualemail ?></span></td>
              <td colspan=2> Rg: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualrg ?></span></td>
              <td> Órgão Exp: <span style="color:#00050a;font-weight:bold"></span></td>                
            </tr>
            <tr>
              <td colspan=2> Habilitação: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualhabil == '1') ? 'Sim' : 'N&atilde;o'?></span></td>
              <td colspan=2> Veículo Própio: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualvepro == '1') ? 'Sim' : 'N&atilde;o'?></span></td>
              <td> Sexo: <span style="color:#00050a;font-weight:bold"><?php echo ($dados[0]->ealualsexo == '0') ? 'Masculino' : 'Feminino' ?></span></td>                
            </tr>
            <tr>
              <td colspan=3> Estado civil: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualestci ?></span> </td>
              <td colspan=3> Naturalidade: <?php echo $dados[0]->salualnatur ?></td>
            </tr>
            <tr>
              <td colspan=6> Observação: <span style="color:#00050a;font-weight:bold"><?php //echo $result->equi_obs ?></span></td>                                      
            </tr>
          </tbody>
          <tr>
            <td colspan=5>
              <table border=1 width=100%>
                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td colspan=3> Curso: <span style="color:#00050a;font-weight:bold"> <?php  echo $dados[0]->scurcunome ?> </span></td>
                        <td colspan=3> Período/Série: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualperio ?> </span></td>
                      </tr>
                      <tr>                                                
                        <td colspan=3> Inst. ensino: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->sensennome ?> </span></td>
                        <td colspan=3> Turno: <span style="color:#00050a;font-weight:bold"> <?php echo $dados[0]->salualturno ?> </span></td>
                      </tr>
                      <tr>
                        <td colspan=6> Horário Disponível para estágio: <span style="color:#00050a;font-weight:bold"><?php echo $dados[0]->salualdispo ?></span></td>                                      
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td colspan=6> Filiação(Opcional): </td>
                      </tr>
                      <tr>
                        <td colspan=3> Pai:  <span style="color:#00050a;font-weight:bold"><?php  echo $dados[0]->salualpai  ?></span></td>
                        <td colspan=3> Profissão: <span style="color:#00050a;font-weight:bold"><?php  echo $dados[0]->salualpprof  ?></span></td>
                      </tr>
                      <tr>
                        <td colspan=3> Mãe: <span style="color:#00050a;font-weight:bold"><?php  echo $dados[0]->salualmae  ?></span></td>
                        <td colspan=3> Profissão: <span style="color:#00050a;font-weight:bold"><?php  echo $dados[0]->salualmprof  ?></span></td>
                      </tr>                                     
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td colspan=12> Conhecimento em Informática: </td>
                      </tr>
                      <tr>
                        <td colspan=2> Windows: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualwindo == '1') ? 'SIM' : ''; ?></span></td>
                        <td colspan=2> Word: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualword == '1') ? 'SIM' : ''; ?></span></td>
                        <td colspan=2> Excel:  <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualexel == '1') ? 'SIM' : ''; ?></span></td>
                        <td colspan=2> PowerPoint: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualpower == '1') ? 'SIM' : ''; ?></span></td>
                        <td colspan=2> Internet: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualinter == '1') ? 'SIM' : ''; ?></span></td>
                        <td colspan=2> Digitação:<span style="color:#00050a;font-weight:bold"> </td>
                      </tr> 
                      <tr>
                        <td colspan=12> Outros: </td>
                      </tr>                                      
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td colspan=8> Idiomas: </td>
                      </tr>
                      <tr>
                        <td colspan=2> Inglês: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualingle == '1') ? 'Ingl&ecirc;s&nbsp;&nbsp;' : ''; ?></span></td>
                        <td colspan=2> Espanhol: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualespan == '1') ? 'Espanhol&nbsp;&nbsp;' : ''; ?></span></td>
                        <td colspan=2> Francês: <span style="color:#00050a;font-weight:bold"><?=($dados[0]->ealualfranc == '1') ? 'Franc&ecirc;s' : ''; ?></span></td>
                        <td colspan=2> Alemão: </td>                                               
                      </tr> 
                      <tr>
                        <td colspan=8> Outros: </td>
                      </tr>                                      
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td colspan=8> Experiências Profissionais: </td>
                      </tr>
                      <tr>
                        <td colspan=8> <span style="color:#00050a;font-weight:bold"><?=(!empty($object[0]->salualexper)) ? nl2br(utf8_encode($object[0]->salualexper)) : 'N&atilde;o Informada'; ?></span></td>                                                                                               
                      </tr> 
                      <tr>
                        <td colspan=8> Observações:<span style="color:#00050a;font-weight:bold"><?php  echo $dados[0]->salualobser  ?></span> </td>
                      </tr>                                      
                    </table>
                  </td>
                </tr>       

                <tr>
                  <td>
                    <table width='100%'>
                      <tr>
                        <td> Assinatura do Estudante: <?php //echo $result->nome ?> </td>                                                
                      </tr>
                    </table>
                  </td>
                </tr>                                       
              </table>
            </td>
          </tr>
        </table>



    
  </div> 

</div>



 <script type="text/javascript">
      $(document).ready(function(){
          $("#imprimir").click(function(){         
              PrintElem('#Ficha');
          })

          function PrintElem(elem)
          {
              Popup($(elem).html());
          }

          function Popup(data)
          {
              var mywindow = window.open('', 'Estudante', 'height=600,width=800');
              mywindow.document.write('<html><head><title></title>');


            //mywindow.document.write("<link rel='stylesheet' href='utils/js/bootstrap/dist/css/bootstrap.min.css' />");
            //mywindow.document.write("<link rel='stylesheet' href='utils/js/bootstrap/dist/css/bootstrap-responsive.min.css' />");
            //mywindow.document.write("<link rel='stylesheet' href='utils/js/bootstrap/dist/css/matrix-style.css' />");
            //mywindow.document.write("<link rel='stylesheet' href='utils/js/bootstrap/dist/css/matrix-media.css' />");


              mywindow.document.write("</head><body >");
              mywindow.document.write("<div  align='center' class='form-group'>");
              mywindow.document.write("<h3><label>FICHA DE INSCRIÇÃO:</label></h3>");                         
              mywindow.document.write("</div>");
             // console.log(data);
              mywindow.document.write(data);
              
              mywindow.document.write("</body></html>");

              // myWindow.document.close();
              // myWindow.focus();
              mywindow.print();
              mywindow.close();
              
              return true;
          }

      });
  </script>
