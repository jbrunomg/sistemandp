<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Estudante</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do Aluno:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />


          <div class="form-group">
          <label class="control-label col-lg-2">Nome da Estudante:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Estudante" name="salualnome" id="salualnome" value="<?php echo set_value('salualnome'); ?>" required>
          <?php echo form_error('salualnome'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">CPF:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cpf" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="salualcpf" id="salualcpf" value="<?php echo set_value('salualcpf'); ?>" required>
          <?php echo form_error('salualcpf'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">RG:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Rg"  data-mask-selectonfocus="true" name="salualrg" id="salualrg" value="<?php echo set_value('salualrg'); ?>" required>
          <?php echo form_error('salualrg'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Orgõ Exped:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="orgão expedição"  data-mask-selectonfocus="true" name="salualrg_org_expd" id="salualrg_org_expd" value="<?php echo set_value('salualrg_org_expd'); ?>" required>
          <?php echo form_error('salualrg_org_expd'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Naturalidade:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Naturalidade"  data-mask-selectonfocus="true" name="salualnatur" id="salualnatur" value="<?php echo set_value('salualnatur'); ?>" required>
          <?php echo form_error('salualnatur'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Estado Civil:</label>
          <div class="col-lg-5">
                 <select class="form-control" id="salualestci" name="salualestci">
                    <option value="">Estado Civil</option>
                    <option value="Solteiro">Solteiro(a)</option>
                    <option value="Casado">Casado(a)</option>
                    <option value="Divorciado">Divorciado(a)</option>
                    <option value="Viuvo">Viúvo(a)</option>
                    <option value="Separado">Separado(a)</option>                         
                                              
                </select>
                <?php echo form_error('salualestci'); ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Data de Nascimento:</label>
          <div class="col-lg-5">                                            
            <input type="text" class="form-control classData" name="talualniver" id="talualniver" value="<?php echo set_value('talualniver'); ?>">
            <?php echo form_error('talualniver'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Sexo:</label>
          <div class="col-lg-5">
                <select  class="form-control" name="ealualsexo" id="ealualsexo">
                  <option value="">Selecione</option>
                    <option value="0" >Masculino</option>
                    <option value="1" >Feminino</option>                          
                                              
                </select>
                <?php echo form_error('ealualsexo'); ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Habilitação:</label>
          <div class="col-lg-5">
                <select  class="form-control" name="ealualhabil" id="ealualhabil">
                  <option value="">Selecione</option>
                    <option value="0" >Sim</option>
                    <option value="1" >Não</option>                          
                                              
                </select>
                <?php echo form_error('ealualhabil'); ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Veiculo Próprio:</label>
          <div class="col-lg-5">
                <select  class="form-control" name="ealualvepro" id="ealualvepro">
                  <option value="">Selecione</option>
                    <option value="0" >Sim</option>
                    <option value="1" >Não</option>                          
                                              
                </select>
                <?php echo form_error('ealualvepro'); ?>
            </div>
        </div>

        <legend class="text-bold">Dados Filiação:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Nome Pai:</label>
          <div class="col-lg-5">
            <input  type="text"  class="form-control" placeholder="Pai Responsável"  data-mask-selectonfocus="true" name="salualpai" id="salualpai" value="<?php echo set_value('salualpai'); ?>">
          <?php echo form_error('salualpai'); ?>
          </div>                    
        </div>
        
        <div class="form-group">
          <label class="control-label col-lg-2">Profissão do Pai:</label>
          <div class="col-lg-5">
            <input  type="text"  class="form-control" placeholder="Profissão do Pai"  data-mask-selectonfocus="true" name="salualpprof" id="salualpprof" value="<?php echo set_value('salualpprof'); ?>">
          <?php echo form_error('salualpprof'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Nome Mãe:</label>
          <div class="col-lg-5">
            <input  type="text"  class="form-control" placeholder="Mãe Responsável"  data-mask-selectonfocus="true" name="salualmae" id="salualmae" value="<?php echo set_value('salualmae'); ?>">
          <?php echo form_error('salualmae'); ?>
          </div>                    
        </div>  

        <div class="form-group">
          <label class="control-label col-lg-2">Profissão da Mãe:</label>
          <div class="col-lg-5">
            <input  type="text"  class="form-control" placeholder="Profissão da Mãe"  data-mask-selectonfocus="true" name="salualmprof" id="salualmprof" value="<?php echo set_value('salualmprof'); ?>">
          <?php echo form_error('salualmprof'); ?>
          </div>                    
        </div>       
    

        <legend class="text-bold">Dados Logradouro:</legend>
        
        <div class="form-group">
          <label class="control-label col-lg-2">CEP:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualcep" id="salualcep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo set_value('salualcep'); ?>">
          <?php echo form_error('salualcep'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Endereço:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="saluallogra" id="saluallogra" value="<?php echo set_value('saluallogra');  ?>" required>
          <?php echo form_error('saluallogra'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Numero:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualnumer" id="salualnumer" value="<?php echo set_value('salualnumer');  ?>" required>
          <?php echo form_error('salualnumer'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Complemento:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualcompl" id="salualcompl" value="<?php echo set_value('salualcompl');  ?>" required>
          <?php echo form_error('salualcompl'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Bairro:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualbairr" id="salualbairr" value="<?php echo set_value('salualbairr');  ?>" required>
          <?php echo form_error('salualbairr'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualcidad" id="salualcidad" value="<?php echo set_value('salualcidad'); ?>" required> 
          <?php echo form_error('salualcidad'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Estado:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" name="salualestad" id="salualestad" value="<?php echo set_value('salualestad'); ?>" required>
          <?php echo form_error('salualestad'); ?>
          </div>                    
        </div>




        <legend class="text-bold">Dados Contato:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Fixo:</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)9999-9999" data-mask="(99)9999-9999" data-mask-selectonfocus="true" name="salualtel01" id="salualtel01" value="<?php echo set_value('salualtel01'); ?>">
          <?php echo form_error('salualtel01'); ?>
          </div>                    
        </div>
        
        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Celular:</label>
          <div class="col-lg-5">
            <input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel02" id="salualtel02" value="<?php echo set_value('salualtel02'); ?>" required>
          <?php echo form_error('salualtel02'); ?>
          </div>                    
        </div> 

        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Celular II:</label>
          <div class="col-lg-5">
            <input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="salualtel03" id="salualtel03" value="<?php echo set_value('salualtel03'); ?>">
          <?php echo form_error('salualtel03'); ?>
          </div>                    
        </div>       

        <div class="form-group">
          <label class="control-label col-lg-2">Email:</label>
          <div class="col-lg-5">
            <input  type="email"  placeholder="seu@email.com" class="form-control" name="salualemail" id="salualemail" value="<?php echo set_value('salualemail'); ?>" required>
          <?php echo form_error('salualemail'); ?>
          </div>                     
        </div>  



        <legend class="text-bold">Dados Acadêmico:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Curso:</label>
          <div class="col-lg-5">
                <select class="form-control" name="pcurcucodig" id="pcurcucodig" required>
                  <option value="">Selecione</option>
                    <?php foreach ($curso as $valor) { ?>
                        <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                  <?php } ?>
                   
                </select>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Nº Matricula:</label>
          <div class="col-lg-5">
            <input  type="text"  placeholder="Matricula" class="form-control" name="salualmatricula" id="salualmatricula" value="<?php echo set_value('salualmatricula'); ?>" required>
          <?php echo form_error('salualmatricula'); ?>
          </div>                     
        </div>  

        <div class="form-group">
          <label class="control-label col-lg-2">Nivel:</label>
          <div class="col-lg-5">
            <select class="form-control" name="ialualcurso_nivel" id="ialualcurso_nivel" required>
              <option value="">Selecione</option>
              <option value="Medio">Médio</option>
              <option value="Tecnico">Técnico</option>                  
              <option value="Superior">Superior</option>
              <option value="Pos">Pós Graduação</option>
            </select> 
                <?php echo form_error('ialualcurso_nivel'); ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Instintuição de Ensino:</label>
          <div class="col-lg-5">
                <select  class="form-control" name="pensencodig" id="pensencodig" required>
                  <option  value="">Selecione</option>
                    <?php foreach ($ensino as $valor) { ?>
                      <option value="<?php echo $valor->pensencodig; ?>"><?php echo $valor->sensennome; ?></option>
                    <?php } ?>
                   
                </select>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Período/Série:</label>
          <div class="col-lg-5">
              <select class="form-control" id="salualperio" name="salualperio" required>
                  <option value="">Selecione</option>
                  <option value="1º">1º</option>
                  <option value="2º">2º</option>
                  <option value="3º">3º</option>
                  <option value="4º">4º</option>
                  <option value="5º">5º</option>
                  <option value="6º">6º</option>
                  <option value="7º">7º</option>
                  <option value="8º">8º</option>
                  <option value="9º">9º</option>
                  <option value="10º">10º</option>
                  <option value="11º">11º</option>
                  <option value="12º">12º</option>
                  <option value="13º">13º</option>
                  <option value="14º">14º</option>
              </select> 
              <?php echo form_error('salualperio'); ?>
            </div>
        </div> 

        <div class="form-group">
          <label class="control-label col-lg-2">Turno:</label>
          <div class="col-lg-5">
            <select class="form-control" id="salualturno" name="salualturno" required>
              <option value="">Selecione</option>
              <option value="Manha">Manhã</option>
              <option value="Tarde">Tarde</option>
              <option value="Noite">Noite</option>
              <option value="Manha/Tarde">Manhã/Tarde</option>
              <option value="Manha/Noite">Manhã/Noite</option>
              <option value="Tarde/Noite">Tarde/Noite</option>
              <option value="Integral">Integral</option>
            </select>
              <?php echo form_error('salualturno'); ?>
          </div>    
        </div>


        <div class="form-group">
          <label class="control-label col-lg-2">Data Término:</label>
          <div class="col-lg-5">                                            
            <input type="text" class="form-control classData" name="salualtermino" id="salualtermino" value="<?php echo set_value('salualtermino'); ?>">
            <?php echo form_error('salualtermino'); ?>
          </div>                    
        </div>
     

        <div class="form-group">
          <label class="control-label col-lg-2">Disponibilidade:</label>
          <div class="col-lg-5">
            <select class="form-control" id="salualdispo" name="salualdispo" required>
              <option value="">Selecione</option>
              <option value="Manha">Manhã</option>
              <option value="Tarde">Tarde</option>
              <option value="Noite">Noite</option>
              <option value="Manha/Tarde">Manhã/Tarde</option>
              <option value="Manha/Noite">Manhã/Noite</option>
              <option value="Tarde/Noite">Tarde/Noite</option>
              <option value="Integral">Integral</option>
            </select>
              <?php echo form_error('salualdispo'); ?>
          </div>    
        </div>

        <legend class="text-bold">Conhecimento Infomática:</legend>

        <div class="row">
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
                <h6 class="no-margin text-semibold">Windows</h6>
                <label class="checkbox-inline">
                  <input type="checkbox" class="styled" name="ealualwindo" value="1">  
                  Checked
                </label>
              </div>
            </div>

            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Linux</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealuallinux" value="1">   
                Checked
              </label>
            </div>
            </div>

            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Word</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualword" value="1">                 
                Checked
              </label>
            </div>
            </div>  

            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Excel</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualexel" value="1">                
                Checked
              </label>
            </div>
            </div>
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Power Point</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualpower" value="1">                
                Checked
              </label>
            </div>
            </div>    
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Internet</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualinter" value="1">                  
                Checked
              </label>
            </div>
            </div>


          </div>  

            <div class="form-group">
            <label class="control-label col-lg-2">Outros:</label>
          
            <div class="col-lg-5">
              <input  type="text" class="form-control" placeholder="Outros"  data-mask-selectonfocus="true" name="salualoutro" id="salualoutro" value="<?php echo set_value('salualoutro'); ?>">
            <?php echo form_error('salualoutro'); ?>
            </div>                    
          </div>




        <legend class="text-bold">Conhecimento Idiomas:</legend>
        <div class="row">
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Inglês</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualingle" value="1">                
                Checked
              </label>
            </div>
            </div>
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Espanhol:</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualespan" value="1">
                Checked
              </label>
            </div>
            </div>
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Francês:</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualfranc" value="1">
                Checked
              </label>
            </div>
            </div>
            <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Alemão:</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="ealualalema" value="1">
                Checked
              </label>
            </div>
            </div>               

        </div>   

          <div class="form-group">
            <label class="control-label col-lg-2">Outros:</label>
            <textarea  class="form-control" rows="2" id="salualoutro" name="salualoutro" ><?php echo set_value('salualoutro'); ?></textarea>
            <?php echo form_error('salualoutro'); ?>
          </div>
      

        <legend class="text-bold">Informações Profissionais:</legend>

          <div class="form-group">
            <label class="control-label col-lg-2">Experiências:</label>
            <textarea  class="form-control" rows="5" id="salualexper" name="salualexper" ><?php echo set_value('salualexper'); ?></textarea>
            <?php echo form_error('salualexper'); ?>
          </div>  

          <div class="form-group">
            <label  class="control-label col-lg-2">Observações:</label>
            <textarea class="form-control" rows="4" id="salualobser" name="salualobser" ><?php echo set_value('salualobser'); ?></textarea>
            <?php echo form_error('salualobser'); ?>
          </div>                
    


					
         
               		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
