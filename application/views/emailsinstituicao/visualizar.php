<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Visualizar Mala Direta</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">					

        <fieldset class="content-group">
          <legend class="text-bold">Dados de envio:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Assunto:</label>
          <div class="col-lg-10">
            <input  type="text" class="form-control" placeholder="Assunto" name="assunto" id="assunto" value="<?php echo $dados[0]->maladireta__assunto; ?>" disabled>
          <?php  echo form_error('assunto'); ?>
          </div>                    
        </div>    

        <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Descrição:</h5>             
            </div>

            <div class="panel-body">
              
                <?php echo $dados[0]->maladireta_mensagem; ?>

          </div>
			</form>
		</div>
	</div>