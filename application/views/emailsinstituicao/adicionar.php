<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Mala Direta</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Filtros:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

          <div class="form-group">
            <label class="control-label col-lg-2">Instituição Ensino:</label>
            <div class="col-lg-5">
            <select class="select-item-color" data-placeholder="Selecione um Instituição" multiple="multiple" name="instituicao[]">
                   
                <?php foreach ($instituicao as $valor){ ?>
                  <option value="<?php echo $valor->pensencodig; ?>"><?php echo $valor->sensennome;?></option>
                <?php } ?>

            </select>
            <?php  echo form_error('instituicao'); ?>
            </div>                    
          </div>  
          						
				</fieldset>

        <fieldset class="content-group">
          <legend class="text-bold">Dados de envio:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Assunto:</label>
          <div class="col-lg-10">
            <input  type="text" class="form-control" placeholder="Assunto" name="assunto" id="assunto" value="<?php echo set_value('assunto'); ?>">
          <?php  echo form_error('assunto'); ?>
          </div>                    
        </div>    

        <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Descrição:</h5>             
            </div>

            <div class="panel-body">
              <div class="summernoteEmails">
               
              </div>
            </div>
          </div>


        <input type="hidden" id="descricao" name="descricao">


				<div class="text-right">
					<button type="submit" id="enviarMalaDireta" class="btn bg-teal">Enviar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>