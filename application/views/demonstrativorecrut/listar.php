
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aDemonstrativoRecrut')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
	
				<th>Mês / Ano</th>					
				<!-- <th>Estudantes Inscritos</th>
		        <th>Estudantes Encaminhados</th>
		        <th>Estudantes Selecionados</th>
		        <th>Vagas</th> -->
		        
								
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>     								
					
					<td><?php echo $valor->demonstrativorecrut_mes.'/'.$valor->demonstrativorecrut_ano; ?></td>
					
					<!-- <td><?php echo $valor->demonstrativorecrut_estud_inscr; ?></td>
			        <td><?php echo $valor->demonstrativorecrut_estud_encamin; ?></td>
			        <td><?php echo $valor->demonstrativorecrut_estud_selec; ?></td>
			        <td><?php echo $valor->demonstrativorecrut_vagas; ?></td> -->			       
																		
					<td>																				
							<ul class="icons-list">
									<?php if(checarPermissao('eDemonstrativoRecrut')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->demonstrativorecrut_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('dDemonstrativoRecrut')){ ?>
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->demonstrativorecrut_id; ?>"><i class="icon-trash"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('vDemonstrativoRecrut')){ ?>
									<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->demonstrativorecrut_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
									<?php } ?>																				
								
							</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
