<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Demonstrativo</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Demonstrativo:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="demonstrativorecrut_id" value="<?php echo $dados[0]->demonstrativorecrut_id; ?>" />

				
			<div class="col-md-12">
				<div class="panel panel-body border-top-teal">
					<div class="text-center">
						<h6 class="no-margin text-semibold">Mês/Ano:</h6>                
					</div>              
					<div class="text-center">
						<input  type="Month" class="form-control" placeholder="mes_ano" name="mes_ano" id="mes_ano" value="<?php echo $dados[0]->demonstrativorecrut_ano.'-'.$dados[0]->demonstrativorecrut_mes; ?>">
						<?php echo form_error('mes_ano'); ?>                
					</div>           
				</div>
			</div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estudantes Inscritos</h6>
                <p></p>                
              </div>

              <input type="number" name= 'demonstrativorecrut_estud_inscr' value="<?php echo $dados[0]->demonstrativorecrut_estud_inscr; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estudantes Encaminhados</h6>
                <p></p>                
              </div>

              <input type="number" name= 'demonstrativorecrut_estud_encamin' value="<?php echo $dados[0]->demonstrativorecrut_estud_encamin; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estudantes Selecionados</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativorecrut_estud_selec' value="<?php echo $dados[0]->demonstrativorecrut_estud_selec; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Vagas Preenchidas</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativorecrut_vagas_pree' value="<?php echo $dados[0]->demonstrativorecrut_vagas_pree; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Vagas Canceladas</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativorecrut_vagas_canc' value="<?php echo $dados[0]->demonstrativorecrut_vagas_canc; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Vagas Suspensas</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativorecrut_vagas_susp' value="<?php echo $dados[0]->demonstrativorecrut_vagas_susp; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
