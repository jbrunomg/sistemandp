<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Patrimônio</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados Patrimoniais:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

          <div class="form-group">
            <label class="control-label col-lg-2">Nº Documento:<span class="text-danger">*</span></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="" name="Documento" id="documento" value="<?php echo set_value('documento'); ?>">
            <?php echo form_error('documento'); ?>
            </div>                    
          </div>

          <div class="form-group">
            <label class="control-label col-lg-2">Descrição:<span class="text-danger">*</span></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="" name="descricao" id="descricao" value="<?php echo set_value('descricao'); ?>">
            <?php echo form_error('descricao'); ?>
            </div>                    
          </div>              

          <div class="form-group">
            <label class="control-label col-lg-2">Data de Aquisição:</label>
            <div class="col-lg-5">                                            
              <input type="text" class="form-control classData" name="dataAquisicao" id="dataAquisicao" value="<?php echo set_value('dataAquisicao'); ?>">
            <?php echo form_error('dataAquisicao'); ?>
            </div>                    
          </div>

          <div class="form-group">
            <label class="control-label col-lg-2">Estado:</label>
            <div class="col-lg-5">
                <select class="form-control" name="conservacao" id="conservacao">
                    <option value="0">Novo</option>
                    <option value="1">Semi-Novo</option>
                    <option value="2">Usado</option>
                </select>
              </div>
          </div>
          
 
          <div class="form-group">
            <label class="control-label col-lg-2">Valor:<span class="text-danger">*</span></label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="R$ valor" name="valor" id="valor" value="<?php echo set_value('valor'); ?>">
            <?php echo form_error('valor'); ?>
            </div>                    
          </div>    
          <div class="form-group">
            <label class="control-label col-lg-2">Fornecedor:</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="" name="etiqueta" id="etiqueta" value="<?php echo set_value('etiqueta'); ?>">
            <?php echo form_error('etiqueta'); ?>
            </div>                    
          </div>                
          <div class="form-group">
            <label class="control-label col-lg-2">Situação:</label>
            <div class="col-lg-5">
                <select class="form-control" name="tipo" id="tipo">                    
                    <option value="0">Débito</option>
                    <option value="1">Quitado</option>
                </select>
              </div>
          </div>  					
               		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
