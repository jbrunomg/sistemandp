<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Patrimônio</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Patrimônio:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input type="hidden" class="form-control" name="patrimonio_id" id="patrimonio_id" value="<?php echo $dados[0]->patrimonio_id; ?>">

				<div class="form-group">
					<label class="control-label col-lg-2">Nº Documento:<span class="text-danger">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Documento" name="documento" id="documento" value="<?php echo $dados[0]->patrimonio_documento; ?>">						
					     <?php echo form_error('documento'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Descrição:<span class="text-danger">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="Descrição" name="descricao" id="descricao" value="<?php echo $dados[0]->patrimonio_nome; ?>">						
					     <?php echo form_error('descricao'); ?>
					</div>
				</div>							
				<div class="form-group">
					<label class="control-label col-lg-2">Data de Aquisição:</label>
					<div class="col-lg-5">																						
						<input type="text" class="form-control daterange-single" name="dataAquisicao" id="dataAquisicao" value="<?php echo date('d/m/Y',strtotime($dados[0]->patrimonio_aquisicao)); ?>">
					<?php echo form_error('dataAquisicao'); ?>
					</div>									
				</div>
				<div class="form-group">
	            	<label class="control-label col-lg-2">Estado:</label>
	            	<div class="col-lg-5">
	                    <select class="form-control" name="conservacao" id="conservacao">
	                      <option value="0" <?php echo ($dados[0]->patrimonio_estado_conservacao == 0)? 'SELECTED':''; ?>>Novo</option>
	                      <option value="1" <?php echo ($dados[0]->patrimonio_estado_conservacao == 1)? 'SELECTED':''; ?>>Semi-Novo</option>
	                      <option value="2" <?php echo ($dados[0]->patrimonio_estado_conservacao == 2)? 'SELECTED':''; ?>>Usado</option>
	                    </select>
	                </div>
	            </div>
				
		
				<div class="form-group">
					<label class="control-label col-lg-2">Valor:<span class="text-danger">*</span></label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="" name="valor" id="valor" value="<?php echo $dados[0]->patrimonio_valor; ?>">
					<?php echo form_error('valor'); ?>
					</div>										
				</div>		
				<div class="form-group">
					<label class="control-label col-lg-2">Fornecedor:</label>
					<div class="col-lg-5">
						<input type="text" class="form-control" placeholder="" name="etiqueta" id="etiqueta" value="<?php echo $dados[0]->patrimonio_etiqueta; ?>">
					<?php echo form_error('etiqueta'); ?>
					</div>										
				</div>								
				<div class="form-group">
	            	<label class="control-label col-lg-2">Situação:</label>
	            	<div class="col-lg-5">
	                    <select class="form-control" name="tipo" id="tipo">
	                        <option value="0" <?php echo ($dados[0]->patrimonio_tipo == 0)? 'SELECTED':''; ?>>Débito</option>
	                        <option value="1" <?php echo ($dados[0]->patrimonio_tipo == 1)? 'SELECTED':''; ?>>Quitado</option>				                           
	                    </select>
	                </div>
	            </div>				


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
