<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aCurso')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th>Nº Doc.</th>
				<th>Descrição</th>
				<th>Data de Aquisição</th>
				<th>Tipo</th>
				<th>Estado</th>
				<th>Valor</th>
				
				<th>Opções</th>

			</tr>
		</thead>

		<tbody>
			<?php foreach ($dados as $valor) {
			if ($valor->patrimonio_estado_conservacao == 0) {
			  	$conservacao = 'Novo';
			  };
			if ($valor->patrimonio_estado_conservacao == 1) {
			  	$conservacao = 'Semi-Novo';
			  };
			if ($valor->patrimonio_estado_conservacao == 2) {
			  	$conservacao = 'Usado';
			  };    

			if ($valor->patrimonio_tipo == 0) {
				$tipo = 'Débito';
			} else {
				$tipo = 'Quitado';
			};			

            ?>
			

				<tr>

					<td><?php echo $valor->patrimonio_documento; ?></td>
					<td><?php echo $valor->patrimonio_nome; ?></td>
					<td><?php echo date('d/m/Y',strtotime($valor->patrimonio_aquisicao)); ?></td>
					<td><?php echo $tipo; ?></td>
					<td><?php echo $conservacao; ?></td>										
					<td><?php echo $valor->patrimonio_valor; ?></td>										
					<td>																				
							<ul class="icons-list">
								<?php if(checarPermissao('ePatrimonio')) {?>
									<li class="text-primary-600"><a href="<?php echo base_url()?>patrimonio/editar/<?php echo $valor->patrimonio_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dPatrimonio')) {?>
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="patrimonio/excluir" registro="<?php echo $valor->patrimonio_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vPatrimonio')) {?>
									<li class="text-teal-600"><a href="<?php echo base_url()?>patrimonio/visualizar/<?php echo $valor->patrimonio_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>																					
								<?php } ?>
							</ul>																			
					</td>						
				</tr>
			<?php } ?>
			
		</tbody>
	</table>
</div>
<!-- /column selectors -->