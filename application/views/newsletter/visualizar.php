<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Newsletter</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>newsletter/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Curso:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="newsid" value="<?php echo $dados[0]->newsid; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">E-mail:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="E-mail" name="newsemail" id="newsemail" value="<?php echo $dados[0]->newsemail; ?>">
					<?php echo form_error('newsemail'); ?>
					</div>										
				</div>			
								
		
            </fieldset>  
		</form>
	</div>
</div>
