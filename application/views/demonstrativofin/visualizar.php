<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Demonstrativo</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Demonstrativo:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="demonstrativofin_id" value="<?php echo $dados[0]->demonstrativofin_id; ?>" />

				
			<div class="col-md-12">
				<div class="panel panel-body border-top-teal">
					<div class="text-center">
						<h6 class="no-margin text-semibold">Mês/Ano:</h6>                
					</div>              
					<div class="text-center">
						<input  disabled type="Month" class="form-control" placeholder="mes_ano" name="mes_ano" id="mes_ano" value="<?php echo $dados[0]->demonstrativofin_ano.'-'.$dados[0]->demonstrativofin_mes; ?>">
						<?php echo form_error('mes_ano'); ?>                
					</div>           
				</div>
			</div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Faturamento</h6>
                <p></p>                
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input disabled type="text" name= 'demonstrativofin_faturamento' value="<?php echo $dados[0]->demonstrativofin_faturamento; ?>" class="dinheiro form-control" placeholder="Valor">                
              </div>              
           
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Crédito</h6>
                <p></p>                
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input disabled type="text" name='demonstrativofin_credito' value="<?php echo $dados[0]->demonstrativofin_credito; ?>" class="dinheiro form-control" placeholder="Valor">                
              </div>              
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Débito / Despesa Mês</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input disabled type="text" name='demonstrativofin_debito' value="<?php echo $dados[0]->demonstrativofin_debito; ?>" class="dinheiro form-control" placeholder="Valor">                
              </div>              
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Lucro/Prejuizo</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input disabled type="text" name='demonstrativofin_lucro_prejuizo' value="<?php echo $dados[0]->demonstrativofin_lucro_prejuizo; ?>" class="dinheiro form-control" placeholder="Valor">                
              </div>              
              
            </div>
          </div>
        </fieldset>

        <fieldset class="content-group">
          <legend class="text-bold">CAIXA - CONTA CORRENTE:</legend>  


<!-- CAIXA C/Corrente -->

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Descontos/Serviços</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input disabled type="text" name='demonstrativofin_cc_desc_serv' value="<?php echo $dados[0]->demonstrativofin_cc_desc_serv; ?>" class="dinheiro form-control" placeholder="Valor">                
              </div>            

            </div>
          </div>
        </fieldset>

  <!-- CAIXA C/Poupança -->
      <fieldset class="content-group">
      <legend class="text-bold">CAIXA - CONTA POUPANÇA:</legend> 
        
        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Saldo Inicial</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_saldo_inicial' value="<?php echo $dados[0]->demonstrativofin_cp_saldo_inicial; ?>"  class="dinheiro form-control" placeholder="Valor">                
            </div>           

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Renda/Crédito Juros</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_renda_credito' value="<?php echo $dados[0]->demonstrativofin_cp_renda_credito; ?>"  class="dinheiro form-control" placeholder="Valor">                
            </div>            

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Descontos(Deb IRRF) E Taxas</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_desc_taxas' value="<?php echo $dados[0]->demonstrativofin_cp_desc_taxas; ?>" class="dinheiro form-control" placeholder="Valor">                
            </div>             

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Movimentação Mensal(Débito)</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_mov_debito' value="<?php echo $dados[0]->demonstrativofin_cp_mov_debito; ?>" class="dinheiro form-control" placeholder="Valor">                
            </div>           

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Movimentação Mensal(Crédito)</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_mov_credito' value="<?php echo $dados[0]->demonstrativofin_cp_mov_credito; ?>" class="dinheiro form-control" placeholder="Valor">                
            </div>          

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Saldo Final</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input disabled type="text" name='demonstrativofin_cp_saldo_final' value="<?php echo $dados[0]->demonstrativofin_cp_saldo_final; ?>" class="dinheiro form-control" placeholder="Valor">                
            </div>             

          </div>
        </div>

      </fieldset>        


      </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
