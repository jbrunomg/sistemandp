<!-- Form horizontal -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h5 class="panel-title">Cadastro de Demonstrativo</h5>
      <div class="heading-elements">
        <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
              </ul>
          </div>
    </div>

    <div class="panel-body">
      <form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
        <fieldset class="content-group">
          <legend class="text-bold">Atividades:</legend>

          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

          <div class="col-md-12">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Mês/Ano:</h6>                
              </div>              
              <div class="text-center">
                <input  type="Month" class="form-control" placeholder="mes_ano" name="mes_ano" id="mes_ano" required value="<?php echo set_value('mes_ano'); ?>">
                  <?php echo form_error('mes_ano'); ?>                
              </div>           
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Faturamento</h6>
                <p></p>                
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" name= 'demonstrativofin_faturamento' class="dinheiro form-control" placeholder="Valor">                
              </div>              
           
            </div>

          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Crédito</h6>
                <p></p>                
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="demonstrativofin_credito" name='demonstrativofin_credito' class="dinheiro form-control" placeholder="Valor">                
              </div>              
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Débito / Despesa Mês</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="demonstrativofin_debito" name='demonstrativofin_debito' class="dinheiro form-control" placeholder="Valor" onblur="myFunction()">                
              </div>              
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Lucro/Prejuizo</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="demonstrativofin_lucro_prejuizo" name='demonstrativofin_lucro_prejuizo' class="dinheiro form-control" placeholder="Valor" readonly>                
              </div>              
              
            </div>
          </div>
        </fieldset>

        <fieldset class="content-group">
          <legend class="text-bold">CAIXA - CONTA CORRENTE:</legend>  


<!-- CAIXA C/Corrente -->

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Descontos/Serviços</h6>
                <p></p>
              </div>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" name='demonstrativofin_cc_desc_serv' class="dinheiro form-control" placeholder="Valor">                
              </div>            

            </div>
          </div>
        </fieldset>

       

<!-- CAIXA C/Poupança -->
      <fieldset class="content-group">
      <legend class="text-bold">CAIXA - CONTA POUPANÇA:</legend> 
        
        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Saldo Inicial</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id="demonstrativofin_cp_saldo_inicial" name='demonstrativofin_cp_saldo_inicial' class="dinheiro form-control" placeholder="Valor">                
            </div>           

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Renda/Crédito Juros</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id="demonstrativofin_cp_renda_credito" name='demonstrativofin_cp_renda_credito' class="dinheiro form-control" placeholder="Valor">                
            </div>            

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Descontos(Deb IRRF) E Taxas</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id="demonstrativofin_cp_desc_taxas" name='demonstrativofin_cp_desc_taxas' class="dinheiro form-control" placeholder="Valor">                
            </div>             

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Movimentação Mensal(Débito)</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id="demonstrativofin_cp_mov_debito" name='demonstrativofin_cp_mov_debito' class="dinheiro form-control" placeholder="Valor">                
            </div>           

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Movimentação Mensal(Crédito)</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id='demonstrativofin_cp_mov_credito' name='demonstrativofin_cp_mov_credito' class="dinheiro form-control" placeholder="Valor" onblur="myAutoSoma()">                
            </div>          

          </div>
        </div>

        <div class="col-md-4">
          <div class="panel panel-body border-top-teal">
            <div class="text-center">
              <h6 class="no-margin text-semibold">Saldo Final</h6>
              <p></p>
            </div>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" name='demonstrativofin_cp_saldo_final' id='demonstrativofin_cp_saldo_final' class="dinheiro form-control" placeholder="Valor" readonly>                
            </div>             

          </div>
        </div>

      </fieldset>  



        </fieldset>
        <div class="text-right">
          <button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
        </div>
      </form>
    </div>
  </div>


<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>     
<script type="text/javascript">
jQuery(document).ready(function($) {

//$(".money").maskMoney();

$('.dinheiro').mask('#.##0,00', {reverse: true});

});

function myFunction() {
    var str = $("#demonstrativofin_credito").val();    
    var est_cobrados = moedaParaNumero(str); 
    
    var str =  $("#demonstrativofin_debito").val();    
    var est_ativo = moedaParaNumero(str); 
    
    var rescindido = (parseFloat(est_cobrados) - parseFloat(est_ativo)); // 20/02/19 retirado (parseInt -> parseFloat) 

    var rescindido = numeroParaMoeda(rescindido);    

    $("#demonstrativofin_lucro_prejuizo").val(rescindido);
    
};

function myAutoSoma() {  

    var B31 = $("#demonstrativofin_cp_saldo_inicial").val(); //B31
    var B31 = moedaParaNumero(B31); 
    // var B31 = B31.replace(".","");
    // var B31 = B31.replace(",","");

    var B32 = $("#demonstrativofin_cp_renda_credito").val(); //B32
    var B32 = moedaParaNumero(B32); 
    // var B32 = B32.replace(".","");
    // var B32 = B32.replace(",","");

    var B33 = $("#demonstrativofin_cp_desc_taxas").val(); //B33
    var B33 = moedaParaNumero(B33); 
    // var B33 = B33.replace(".","");
    // var B33 = B33.replace(",","");

    var B34 = $("#demonstrativofin_cp_mov_debito").val(); //B34
    var B34 = moedaParaNumero(B34); 
    // var B34 = B34.replace(".","");
    // var B34 = B34.replace(",","");

    var B35 = $("#demonstrativofin_cp_mov_credito").val(); //B35
    var B35 = moedaParaNumero(B35); 
    // var B35 = B35.replace(".","");
    // var B35 = B35.replace(",","");
    
    var saldo_final = (parseInt(B31)+parseInt(B32)-parseInt(B33)-parseInt(B34)+parseInt(B35));

    var saldo_final = numeroParaMoeda(saldo_final);  
    
    $("#demonstrativofin_cp_saldo_final").val(saldo_final);
};


function moedaParaNumero(valor)
{
    return isNaN(valor) == false ? parseFloat(valor) :   parseFloat(valor.replace("R$","").replace(".","").replace(",","."));
}

function numeroParaMoeda(n, c, d, t)
{
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

</script>
