
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aDemonstrativoFin')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
	
				<th>Mês / Ano</th>					
			<!-- 	<th>Lucro/Prejuizo</th>
		        <th>Descontos/Serviços</th>
		        <th>Saldo Inicial</th>
		        <th>Saldo Final</th> -->
		        
								
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>     								
					
					<td><?php echo $valor->demonstrativofin_mes.'/'.$valor->demonstrativofin_ano; ?></td>
					
					<!-- <td><?php echo $valor->demonstrativofin_lucro_prejuizo; ?></td>
			        <td><?php echo $valor->demonstrativofin_cc_desc_serv; ?></td>
			        <td><?php echo $valor->demonstrativofin_cp_saldo_inicial; ?></td>
			        <td><?php echo $valor->demonstrativofin_cp_saldo_final; ?></td> -->
			        
																		
					<td>																				
							<ul class="icons-list">
									<?php if(checarPermissao('eDemonstrativoFin')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->demonstrativofin_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('dDemonstrativoFin')){ ?>
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->demonstrativofin_id; ?>"><i class="icon-trash"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('vDemonstrativoFin')){ ?>
									<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->demonstrativofin_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
									<?php } ?>																				
								
							</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
