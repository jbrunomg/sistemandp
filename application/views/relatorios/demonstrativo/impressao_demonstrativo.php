<?php 
// var_dump($estudante);die();

$mes = date('m'); // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0,0,0,$mes,'01',$ano)); // Mágica, plim!

// $lastmes = date('m', strtotime('-1 months'));
$lastmes = date('m', strtotime('first day of -1 months'));


setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

?>
<img src="<?php echo $emitente[0]->url_logo; ?>">
<table class="table">	
	<tbody>		
		<tr>	
	<!-- 		<td style="font-size:12px;">
				<?php 
				    echo $emitente->emitente_nome."<br>";
					echo "Fone  : ".$emitente->emitente_telefone."<br>";
					echo "E-mail: ".$emitente->emitente_email."<br>";								
				?>
			</td>	 -->

			<!-- <td class="text-right"><img src="<?php echo $emitente[0]->url_logo ?>" width="100px" heigth="100px"></td> -->
			
		</tr>		
	</tbody>
</table>
<hr style="margin-top:-10px">
<br/>
<?php echo 'Recife, '. utf8_encode(strftime(' %d, de %B de %Y')) ?>
<br/>
<br/>
<h5 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h5>		
	<br/>
	<br/>	
	<div class="panel-body">		
		Discriminamos, através deste, o valor de <code><?php echo 'R$ '. number_format((end($estudante)->SomaTotal - end($estorno)->estornototal), 2, ".", ""); ?></code> ( <?php echo extenso((end($estudante)->SomaTotal - end($estorno)->estornototal)) ?> ) recorrente à Empresa: <code><?php echo $contrato[0]->sempemrazao; ?></code>  referente à taxa alusiva aos estagiários relacionados abaixo:
		
	</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th style="padding: 5px;fonte-size:12" class="text-center">Código</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Estagiário</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Periodo Cobrança</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Valor</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Início Contrato</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Término Contrato</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Vinculo/Dias</th>	
		</tr>
	</thead>
		<tbody>

			<?php
				$contador = 1;
				$cobrado = 0;
				$ativo = 0; 
				$totalServico = 0;
				$contratoVencido = '';

			foreach ($contrato as $valor) { 

				// Contrato pós o mês atual não entra no demonstrativo - 30052022
				// if ($estudante[$valor->palualcodig]->dataInicio > date('Y-m-d') ) { 
				if (date('Y-m', strtotime($estudante[$valor->palualcodig]->dataInicio))  > date('Y-m') ) {
		    		continue;
				}	

		
				if ($valor->contrestempr_status == 'Ativo') {						
				$ativo++;							
				} else {
				$cobrado++;						
				}

				$totalServico = $totalServico + $estudante[$valor->palualcodig]->valorTotal;

				?>

				<?php if ($valor->palualcodig = $estudante[$valor->palualcodig]->palualcodig ) { ?>
				<tr>							
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $contador++; ?></td> <!-- Código -->
					<?php if ($valor->contrestempr_status == 'Rescindido') { ?>
					<td style="padding: 5px;fonte-size:12" class="text-center"><code><?php echo mb_strtoupper($valor->salualnome); ?></code></td> <!-- Estagiário -->
					<?php } else { ?>
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo mb_strtoupper($valor->salualnome); ?></td> <!-- Estagiário -->
					<?php } ?>
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo date('d/m/y', strtotime($estudante[$valor->palualcodig]->dataInicio)).' a '.date('d/m/y', strtotime($estudante[$valor->palualcodig]->datafim)); ?></td> <!-- Periodo contrato rescindido-->
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo 'R$ '. number_format($estudante[$valor->palualcodig]->valorTotal, 2, ".", ""); ?></td>  <!-- Valor -->							
					


					<td><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->contrestempr_data_ini )); ?></td>

					<?php if ($estudante[$valor->palualcodig]->termino < date('Y-m-d') ) {
						  if ($valor->contrestempr_status  <> 'Rescindido') { # Lista de Aluno com contrato vencido e não recindindo.					  			
						$contratoVencido = $contratoVencido.'<br />'.mb_strtoupper($valor->salualnome).' - '.date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino ));  			
						  		}		
					?>				

					<?php //$contratoVencido = $contratoVencido.'<br />'.mb_strtoupper($valor->salualnome).' - '.date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?>
					    <td style="padding: 5px;fonte-size:12" class="text-center"><code><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></code></td> <!-- Término para contrato (Vencido) -->
					<?php } else { ?> 
						<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></td> <!-- Término contrato -->
					<?php } ?> 

					<?php 

						$dataInicio = $estudante[$valor->palualcodig]->contrestempr_data_ini;
						$dataFim = $estudante[$valor->palualcodig]->datafim;

						// Criar objetos DateTime a partir das strings de data
						$inicio = new DateTime($dataInicio);
						$fim = new DateTime($dataFim);

						// Calcular a diferença entre as datas
						$intervalo = $inicio->diff($fim);

						// Acessar o resultado em dias
						$intervaloEmDias = $intervalo->days + 1;
												
     				?>

     				<td><?php echo $intervaloEmDias.' Dias' ?></td>													


				</tr>
					
				<?php } ?>

			<?php } ?>	
				

		</tbody>	
</table>

	<div class="panel-body">
		<?php 
		$totalEstorno = 0;
		if (count($estorno) >= 1) {  ?>	

			<label>Estorno:</label><br />
			<?php				
			foreach ($estorno as $valor) {

			$totalEstorno = $totalEstorno + $valor->estorno;

			?>				
			<div class="form-control-static">
				<?php echo $valor->salualnome; ?> -  Estorno <span><code><?php echo 'R$ '. number_format($valor->estorno, 2, ".", ""); ?></span></code>, Foi cobrado mês: <?php echo $lastmes; ?> rescindido no dia: <?php echo date('d/m/Y', strtotime($valor->contrestempr_data_rescisao ));  ?> .<br />	     
			</div>
			<?php } ?>
		<?php } ?>  

		<?php if ($contratoVencido <> '') { ?>
		<br /><label>Estudante com contrato vencido:</label><br />
		<div class="form-control-static">
			<?php echo $contratoVencido ?>	     
	    </div>	
		<?php } ?>	

	</div>



	<?php if ($contrato[0]->demonstrativoempr_obs <> '') {  ?>
		<label  class="control-label col-lg-2">Observação:</label><br/>
        <?php echo $contrato[0]->demonstrativoempr_obs; ?>
        <br/>
        <br/>
	<?php  } ?>	

	<span><code>IMPORTANTE:</code> 
		Qualquer tipo de alteração que venha ser feita no contrato de algum estagiário,
		<?php echo mb_strtoupper(CLIENTE) ?> precisa ser sinalizado de imediato, principalmente quando for um desligamento.<br/>
		Existem procedimentos que devem ser feitos, evitando vínculo empregatício.  </span>
	<br/>
	<br/>
	<span>Valor Total: <?php echo 'R$'. number_format($totalServico , 2, ",", "."); ?></span><br />
	<span>Estorno: <?php echo 'R$'. number_format($totalEstorno , 2, ",", "."); ?></span><br />
	<?php if (CLIENTE == 'nudep') { ?>	
	<!-- <span>Nota Fiscal: <?php echo  $contrato[0]->demonstrativoempr_notafiscal; ?></span><br /> -->
	<?php } ?>

	<?php 
	$dia = $contrato[0]->sempemvenccontrato;
	$mes = date('m')+1;
	$ano = date('Y');
  	$data = mktime(0, 0, 0, $mes, $dia, $ano);
  	$dia_semana = date("w", $data);
    
  	// domingo = 0;
  	// sábado = 6;
  
 	// verifica domingo + 1 dia	
    if ($dia_semana == 0) {
  		$dia = $dia + 01;
  		}
  	// verifica sábado + 2 dias	
  	if ($dia_semana == 6) {
  		$dia = $dia + 02;	
  		}	

  	$mesRelatorio = date('m', strtotime($estudante[$valor->palualcodig]->dataInicio));
  	$mes = ($mesRelatorio <> date('m') ) ? $mesRelatorio + 1 : $mes ;	
	?>

	

	<?php if (date('m') <> '12') { ?>
		<span>Vencimento: <?php echo  $dia.'/'.$mes.'/'.$ano; ?></span>
	<?php } else {?>
		<span>Vencimento: <?php echo  $dia.'/01/'.date('y'); ?></span>
	<?php } ?>
	<br/>
	<br/>
	<br/>
	<div class="text-center">
	<?php if (CLIENTE == 'nudep') { ?>	
	<span><i><u>Paula Nascimento</u></i></span><br/>
	<span><i>Analista / Depto. Financeiro</i></span><br/>
	<?php } else { ?>	
	<span><i> Depto. Financeiro</i></span><br/>
	<?php } ?>
	<span><i><?php echo mb_strtoupper(CLIENTE) ?> LTDA</i></span>
	</div>

	<?php   $Totalextenso =  extenso($totalServico - $totalEstorno); ?>

	<script type="text/javascript">

	var valor = '<?php echo  "R$". number_format($totalServico - $totalEstorno, 2, ",", ".");  ?>';
	var valorExtenso =  '<?php echo  $Totalextenso;  ?>';

	$("#totalServico").text(valor);
	$("#totalExtenso").text(valorExtenso);


	</script>



