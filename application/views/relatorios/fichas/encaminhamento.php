<?php

$mes = date('m');      // Mês desejado, pode ser por ser obtido por POST, GET, etc.
// $ano = date('Y'); // Ano atual
// $ultimo_dia = date('t', mktime(0, 0, 0, $mes, '01', $ano)); // Mágica, plim!
// $mesExtenso = date('m',  );

// switch ($mesExtenso){
 
// case 1: $mesExtenso = "JANEIRO"; break;
// case 2: $mesExtenso = "FEVEREIRO"; break;
// case 3: $mesExtenso = "MARÇO"; break;
// case 4: $mesExtenso = "ABRIL"; break;
// case 5: $mesExtenso = "MAIO"; break;
// case 6: $mesExtenso = "JUNHO"; break;
// case 7: $mesExtenso = "JULHO"; break;
// case 8: $mesExtenso = "AGOSTO"; break;
// case 9: $mesExtenso = "SETEMBRO"; break;
// case 10: $mesExtenso = "OUTUBRO"; break;
// case 11: $mesExtenso = "NOVEMBRO"; break;
// case 12: $mesExtenso = "DEZEMBRO"; break;
 
// }

switch ($mes){
 
case 1: $mes = "JANEIRO"; break;
case 2: $mes = "FEVEREIRO"; break;
case 3: $mes = "MARÇO"; break;
case 4: $mes = "ABRIL"; break;
case 5: $mes = "MAIO"; break;
case 6: $mes = "JUNHO"; break;
case 7: $mes = "JULHO"; break;
case 8: $mes = "AGOSTO"; break;
case 9: $mes = "SETEMBRO"; break;
case 10: $mes = "OUTUBRO"; break;
case 11: $mes = "NOVEMBRO"; break;
case 12: $mes = "DEZEMBRO"; break;
 
}

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        #titulo {
            text-decoration: underline;
            text-align: center;
            margin-bottom: 50px;
        }

        #logo {
            margin-left: 100px;
        }

        #data {
            margin-bottom: 50px;
        }

        .txtMeio {
            text-align: center;
        }

        .c3 {
            text-decoration: underline;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid;
            text-align: left;
            padding: 8px;
        }
    </style>
</head>

<body>
    <div id="logo">
        <img alt="" src="<?php echo $emitente[0]->url_logo; ?>">
    </div>
    <p id="titulo">
        <b> ENCAMINHAMENTO DE ESTAGIÁRIO</b>
    </p>
    <p id="data">
        Recife, <?php echo  date('d'); ?> de <?php echo $mes; ?> de <?php echo  date('Y'); ?>
        <!-- Recife, 19 de março de 2021. -->
    </p>
    <p> <b> Empresa: </b> <?php echo strtoupper($dados[0]->sempemrazao) ?></p> 
    <p> <b> Endereço: </b><?php echo strtoupper($dados[0]->recrut_end_esta) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<span><b>Bairro:</b> <?php echo $dados[0]->recrut_bairro; ?></span></p>
    <p> <b> Cidade/UF: </b><?php echo $dados[0]->recrut_cidade.'/'.$dados[0]->recrut_uf ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><b>Fone:</b> <?php echo $dados[0]->sempemtel01; ?></span></p>
    <p> <b> Responsável: </b><?php echo strtoupper($dados[0]->recrut_respo) ?></p>
    <p> <b> Ponto de Referência: </b>Próximo atendimento a Celpe na Caxangá.</p>
    <p> <b> Entrevista: </b><?php echo strtoupper($dados[0]->recrut_hs_entrev) ?></p>
    <p></p>
    <p>Apresentamos o candidato <span class="c3"> <?php echo strtoupper($recrut[0]->salualnome) ?></span> curso <span class="c3"> <?php echo mb_strtoupper($recrut[0]->scurcunome) ?> </span> para participar do processo seletivo da vaga de estágio aí existente. Caso a estudante atenda aos requisitos estabelecidos, preencha os dados abaixo a fim de que possamos efetivar sua contratação.
    </p>


    <p class="txtMeio c3"><b> <?php echo strtoupper($emitente[0]->representada) ?> </b></p>
    <p class="txtMeio"><b> <?php echo strtoupper($emitente[0]->nome) ?> </b></p>

    <p>_________________________________________________________________________________________</p>
    <p>_________________________________________________________________________________________</p>
    <p></p>

    <?php 
    $bolsa = ($dados[0]->recrut_bolsa_auxilio == '1') ? 'Bolsa Auxilio | ' : '' ;
    $trans = ($dados[0]->recrut_transporte == '1') ? 'Vale Transporte | ' : '' ;
    $refei = ($dados[0]->recrut_refei == '1') ? 'Vale Refeição | ' : '' ;
    $outro = ($dados[0]->recrut_outro == '1') ? 'Outro | ' : '' ;
    

    ?>

    <p class="c3"><b> PERFIL DA VAGA:</b></p>
    <p></p>

    <table>
        <tr>
            <th>Atividades: </th>
            <th><?php echo strtoupper($dados[0]->recrut_atividade) ?></th>
        </tr>
        <tr>
            <th>Bolsa Auxílio: </th>
            <th><?php echo strtoupper($dados[0]->recrut_valor_auxilio) ?></th>
        </tr>
        <tr>
            <th>Benefícios: </th>
            <th><?php echo strtoupper($bolsa.$trans.$refei.$outro) ?></th>
        </tr>
        <tr>
            <th>Horário: </th>
            <th><?php echo strtoupper($dados[0]->recrut_horario) ?></th>
        </tr>
    </table>
    <p></p>
    <p>Observações: Levar esta carta no dia da entrevista e um currículo. (A empresa só inicia o processo de seleção, se o candidato apresentar o currículo impresso com FOTO na hora da entrevista).
    </p>

    <p><b>Atenção! Esta carta de encaminhamento não é considerada como vínculo do TCE (Termo de Compromisso de Estágio).
        </b></p>
        <p></p>


        <p class="txtMeio">_________________________________</p>
        <p class="txtMeio"> <b> Assinatura da empresa </b></p>
</body>

</html>