<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        #logo {
            margin-left: 100px;
            margin-bottom: 50px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid;
            text-align: center;
            padding: 5px;
        }

        .paraesquerda{
            text-align: left !important;
        }
    </style>
</head>

<body>
    <div id="logo">
        <img alt="" src="<?php echo $emitente[0]->url_logo; ?>">
    </div>
    <table>
        <tr>
            <td colspan="100%">FICHA DE OFERTA DE ESTÁGIO</td>
        </tr>
        <tr>
            <td colspan="40%">EMPRESA</td>
            <td colspan="20%">PÚBLICA</td>
            <td colspan="20%">PRIVADA</td>
            <td colspan="20%">MISTA</td>
        </tr>
        <tr>
            <td colspan="40%"><?php echo strtoupper($dados[0]->sempemrazao) ?></td>
            <td colspan="20%"></td>
            <td colspan="20%">X</td>
            <td colspan="20%"></td>
        </tr>
        <tr>
            <td colspan="40%">ENDEREÇO</td>
            <td colspan="20%">BAIRRO</td>
            <td colspan="20%">CIDADE</td>
            <td colspan="20%">CEP</td>
        </tr>
        <tr>
            <td colspan="40%"><?php echo strtoupper($dados[0]->sempemlogra).','.strtoupper($dados[0]->sempemnumer).' '.strtoupper($dados[0]->sempemcompl) ?></td>
            <td colspan="20%"><?php echo $dados[0]->sempembairr; ?></td>
            <td colspan="20%"><?php echo $dados[0]->sempemcidad; ?></td>
            <td colspan="20%"><?php echo $dados[0]->sempemcep; ?></td>
        </tr>
        <tr>
            <td colspan="20%">FONE</td>
            <td colspan="20%">FAX</td>
            <td colspan="40%">E-MAIL</td>
            <td colspan="20%">RESPONSÁVEL</td>
        </tr>
        <tr>
            <td colspan="20%"><?php echo $dados[0]->sempemtel01; ?> </td>
            <td colspan="20%"></td>
            <td colspan="40%"><?php echo strtoupper($dados[0]->sempememail) ?></td>
            <td colspan="20%"><?php echo strtoupper($dados[0]->sempemrespo) ?></td>
        </tr>
        <tr>
            <td colspan="100%">CARACTERÍSTICAS DA VAGA</td>
        </tr>
        <tr>
            <td colspan="40%">LOCAL ESTÁGIO</td>
            <td colspan="20%">BAIRRO</td>
            <td colspan="20%">CIDADE</td>
            <td colspan="20%">CEP</td>
        </tr>
        <tr>
            <td colspan="40%"><?php echo strtoupper($dados[0]->recrut_end_esta) ?> </td>
            <td colspan="20%"><?php echo strtoupper($dados[0]->recrut_bairro) ?></td>
            <td colspan="20%"><?php echo strtoupper($dados[0]->recrut_cidade) ?></td>
            <td colspan="20%"><?php echo strtoupper($dados[0]->recrut_cep) ?></td>
        </tr>
        <tr>
            <td colspan="33%">INICIO</td>
            <td colspan="34%">DURAÇÃO</td>
            <td colspan="33%">HORÁRIO</td>
        </tr>
        <tr>
            <td colspan="33%"><?php echo strtoupper($dados[0]->recrut_inicio) ?></td>
            <td colspan="34%"><?php echo strtoupper($dados[0]->recrut_duracao) ?></td>
            <td colspan="33%"><?php echo strtoupper($dados[0]->recrut_horario) ?></td>
        </tr>
        <tr>
            <td colspan="100%">BENEFÍCIOS</td>
        </tr>
        <tr>
            <td colspan="25%">BOLSA-AUXILIO</td>
            <td colspan="25%">TRANSPORTE</td>
            <td colspan="25%">REFEIÇÃO</td>
            <td colspan="25%">OUTROS</td>
        </tr>
        <?php 
        $bolsa = ($dados[0]->recrut_bolsa_auxilio == '1') ? 'S | ' : 'N';
        $trans = ($dados[0]->recrut_transporte == '1') ? 'S' : 'N';
        $refei = ($dados[0]->recrut_refei == '1') ? 'S' : 'N';
        $outro = ($dados[0]->recrut_outro == '1') ? 'S' : 'N';   

        ?>
        <tr>
            <td colspan="25%"><?php echo $bolsa.strtoupper($dados[0]->recrut_valor_auxilio) ?></td>
            <td colspan="25%"><?php echo strtoupper($trans) ?></td>
            <td colspan="25%"><?php echo strtoupper($refei) ?></td>
            <td colspan="25%"><?php echo strtoupper($outro) ?></td>
        </tr>
        <tr>
            <td colspan="100%">ATIVIDADES PREVISTAS</td>
        </tr>
        <tr>
            <td colspan="100%"><?php echo strtoupper($dados[0]->recrut_atividade) ?></td>
        </tr>
        <tr>
            <td colspan="50%">RESPONSÁVEL</td>
            <td colspan="50%"><?php echo strtoupper($dados[0]->recrut_respo) ?></td>
        </tr>
        <tr>
            <td colspan="50%"></td>
            <td colspan="50%">Currículo: <?php echo $dados[0]->sempememail ?></td>
        </tr>
        <tr>
            <td colspan="100%">PERFIL DO ESTUDANTE SOLICITADO</td>
        </tr>
        <tr>
            <td colspan="100%">CURSO: 
            <?php foreach ($cursos as $valor) { ?>
                  <?php echo ($valor->pcurcucodig == $cursosSelecionados[0])? $valor->scurcunome : ''; ?>
            <?php } ?> 
            </td>
        </tr>
        <tr>
            <td colspan="30%">NÍVEL</td>
            <td colspan="10%"></td>
            <td colspan="30%">SEXO</td>
            <td colspan="30%">INDICADO PELA EMPRESA</td>
        </tr>
        <tr>
            <td colspan="15%">MÉDIO</td>
            <td colspan="15%">SUPERIOR</td>
            <td colspan="10%">PERÍODO</td>
            <td colspan="15%">FEM.</td>
            <td colspan="15%">MASC.</td>
            <td colspan="15%">SIM</td>
            <td colspan="15%">NÃO</td>
        </tr>
       <tr>
            <td colspan="15%"><?php echo $dados[0]->recrut_nivel == 'Medio' ? 'X' : '' ; ?></td>
            <td colspan="15%"><?php echo $dados[0]->recrut_nivel == 'Superior' ? 'X' : '' ; ?></td>
            <td colspan="10%"><?php echo $dados[0]->recrut_periodo; ?></td>
            <td colspan="15%"><?php echo $dados[0]->recrut_sexo == 'Feminino' ? 'X' : '' ; ?></td>
            <td colspan="15%"><?php echo $dados[0]->recrut_sexo == 'Masculino' ? 'X' : '' ; ?></td>
            <td colspan="15%"><?php echo $dados[0]->recrut_indicado == '1' ? 'X' : '' ; ?></td>
            <td colspan="15%"><?php echo $dados[0]->recrut_indicado == '0' ? 'X' : '' ; ?></td>
        </tr>
         <tr>
            <td class="paraesquerda" colspan="100%">REQUISITOS EXIGIDOS PELA EMPRESA: <?php echo strtoupper($dados[0]->recrut_requisito) ?></td>
        </tr>
        <tr>
            <td class="paraesquerda" colspan="100%">DIVULGAÇÃO: <?php echo strtoupper($dados[0]->recrut_divul) ?></td>
        </tr>
        <tr>
            <td class="paraesquerda" colspan="50%">DATA: <?php echo date('d-m-Y') ?> </td>
            <td class="paraesquerda" colspan="50%">ASS. EMITENTE:</td>
        </tr>
    </table>
    <table style="margin-top: 200px;">
        <tr>
            <td style="color:black; background-color:#d99594;" colspan="25%">Data</td>
            <td style="color:black; background-color:#d99594;" colspan="25%">Encaminhados</td>
            <td style="color:black; background-color:#d99594;" colspan="25%">Contato</td>
            <td style="color:black; background-color:#d99594;" colspan="25%">Selec | Aprov</td>
        </tr>
        
            <?php foreach ($recrut as $v) {
            $selec = $v->recrutalu_selec == '0' ? 'Não' : 'Sim';
            $aprov = $v->recrutalu_aprov == '0' ? 'Não' : 'Sim';   
            
            ?>
            <tr>
                <td colspan="25%"><?php echo date('d/m/Y', strtotime($v->recrutalu_data_atualizacao)); ?></td>
                <td colspan="25%"><?php echo $v->salualnome; ?></td>
                <td colspan="25%"><?php echo $v->salualtel01.' | '.$v->salualtel02; ?></td>
                <td colspan="25%"><?php echo $selec .' | '.$aprov ?></td> 
            </tr>    
            <?php } ?>      
        </table>
</body>

</html>