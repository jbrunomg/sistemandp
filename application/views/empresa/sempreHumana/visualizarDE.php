<html>

<head>
    <title>DECLARAÇÃO ESTÁGIO</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=cNlK_-Ccawu4vzi2WSZBow');

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c5 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify;
            height: 12pt
        }

        .c0 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c1 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center;
            height: 12pt
        }

        .c12 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c6 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c3 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c7 {
            background-color: #ffffff;
            max-width: 453.6pt;
            padding: 70.8pt 56.6pt 70.8pt 85pt
        }

        .c11 {
            font-size: 16pt;
            font-family: "Verdana";
            font-weight: 400
        }

        .c8 {
            font-size: 48pt;
            font-family: "Corsiva"
        }

        .c4 {
            font-weight: 700
        }

        .c10 {
            height: 12pt
        }

        .c2 {
            vertical-align: baseline
        }

        .c9 {
            font-size: 8pt
        }

        .title {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 36pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 18pt;
            color: #666666;
            font-size: 24pt;
            padding-bottom: 4pt;
            font-family: "Georgia";
            line-height: 1.0;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 24pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 18pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 12pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 11pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 10pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        .cyxyy {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            font-size: 45pt;
            font-family: "Corsiva";
            font-style: normal
        }
        .c31 {
            margin-left: -56.7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }
    </style>
</head>

<!-- `contrestempr_data_renIII`
`contrestempr_data_renII`
`contrestempr_data_renI`
`contrestempr_data_term`
 -->
<?php 

$dataFinal = '';

if ($dados[0]->contrestempr_data_renIII <> '0000-00-00') {
    // code...
    $dataFinal = $dados[0]->contrestempr_data_renIII;
}elseif ($dados[0]->contrestempr_data_renII <> '0000-00-00') {
    // code...
    $dataFinal = $dados[0]->contrestempr_data_renII;
}elseif ($dados[0]->contrestempr_data_renI <> '0000-00-00') {
    // code...
    $dataFinal = $dados[0]->contrestempr_data_renI;
}elseif ($dados[0]->contrestempr_data_term <> '0000-00-00') {
    // code...
    $dataFinal = $dados[0]->contrestempr_data_term;
}





?>
<body class="c7">
    <p class="c31"><img alt="" src="<?php echo $emitente[0]->url_logo; ?>" style="width: 131.80px; height: 69.40px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""><i><span class="c14 cyxyy">Sempre Humana</i></span></p>
    <p class="c3"><span class="c2 c11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c4 c2">DESENVOLVIMENTO HUMANO E TECNOLÓGICO</span></p>
    <p class="c3 c10"><span class="c0"></span></p>
    <p class="c3"><span class="c0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c3 c10"><span class="c0"></span></p>
    <p class="c3 c10"><span class="c0"></span></p>
    <p class="c1"><span class="c0"></span></p>
    <p class="c1"><span class="c0"></span></p>
    <p class="c12"><span class="c0">DECLARAÇÃO DE ESTÁGIO</span></p>
    <p class="c1"><span class="c0"></span></p>
    <p class="c1"><span class="c0"></span></p>
    <p class="c1"><span class="c0"></span></p>
    <p class="c6"><span class="c2">Declaramos para os devidos fins que se fizerem necessários que &nbsp;<?php echo strtoupper($dados[0]->salualnome) ?></span><span class="c4 c2">,</span><span class="c2">&nbsp;graduando no curso de <?php echo mb_strtoupper($dados[0]->scurcunome) ?>da <?php echo strtoupper($dados[0]->sensennome) ?></span><span class="c2 c4">, </span><span class="c2">matricula nº.______________</span><span class="c2 c9">;</span><span class="c0">&nbsp;realizou o estágio, na Empresa <?php echo strtoupper($dados[0]->sempemrazao) ?> no período de <?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_ini)); ?> a <?php echo date('d/m/Y', strtotime($dataFinal)); ?>, fazendo uma carga horária de <?php echo mb_strtoupper($dados[0]->contrestempr_cargahoraria) ?> ; desempenhando seu estágio na sua área de formação acadêmica, no Setor de &nbsp;________ nas atividades relacionadas a <?php echo mb_strtoupper($dados[0]->contrestempr_atividade) ?>; sob a orientação do Sr. __________________ que ocupa o cargo de____________; &nbsp;fundamentado nos termos da Lei nº. 11.788 / 2008; conforme regulamento do Programa de Estágio Supervisionado.</span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c6"><span class="c0">Recife, ___ de &nbsp;_____ de 2021.</span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c5"><span class="c0"></span></p>
    <p class="c6"><span class="c0"><?php echo strtoupper($emitente[0]->representada) ?> </span></p>
    <p class="c6"><span class="c0"><?php echo strtoupper($emitente[0]->cargo) ?> da Central de Estágio &amp; Empregos &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></p>
    <p class="c3 c10"><span class="c0"></span></p>
</body>

</html>