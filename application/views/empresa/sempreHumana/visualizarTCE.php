<?php

$mes = date('m');      // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0, 0, 0, $mes, '01', $ano)); // Mágica, plim!
$mesExtenso = date('m', strtotime($dados[0]->contrestempr_data_ini));

switch ($mesExtenso){
 
case 1: $mesExtenso = "JANEIRO"; break;
case 2: $mesExtenso = "FEVEREIRO"; break;
case 3: $mesExtenso = "MARÇO"; break;
case 4: $mesExtenso = "ABRIL"; break;
case 5: $mesExtenso = "MAIO"; break;
case 6: $mesExtenso = "JUNHO"; break;
case 7: $mesExtenso = "JULHO"; break;
case 8: $mesExtenso = "AGOSTO"; break;
case 9: $mesExtenso = "SETEMBRO"; break;
case 10: $mesExtenso = "OUTUBRO"; break;
case 11: $mesExtenso = "NOVEMBRO"; break;
case 12: $mesExtenso = "DEZEMBRO"; break;
 
}

switch ($mes){
 
case 1: $mes = "JANEIRO"; break;
case 2: $mes = "FEVEREIRO"; break;
case 3: $mes = "MARÇO"; break;
case 4: $mes = "ABRIL"; break;
case 5: $mes = "MAIO"; break;
case 6: $mes = "JUNHO"; break;
case 7: $mes = "JULHO"; break;
case 8: $mes = "AGOSTO"; break;
case 9: $mes = "SETEMBRO"; break;
case 10: $mes = "OUTUBRO"; break;
case 11: $mes = "NOVEMBRO"; break;
case 12: $mes = "DEZEMBRO"; break;
 
}

?>
<html>

<head>
    <title>TERMO DE COMPROMISSO DE ESTÁGIO</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=cwkuN-YbXleWjD6k_pVPXg');

        .lst-kix_list_4-1>li {
            counter-increment: lst-ctn-kix_list_4-1
        }

        ol.lst-kix_list_3-1 {
            list-style-type: none
        }

        ol.lst-kix_list_3-2 {
            list-style-type: none
        }

        .lst-kix_list_3-1>li {
            counter-increment: lst-ctn-kix_list_3-1
        }

        ol.lst-kix_list_3-3 {
            list-style-type: none
        }

        ol.lst-kix_list_3-4.start {
            counter-reset: lst-ctn-kix_list_3-4 0
        }

        .lst-kix_list_5-1>li {
            counter-increment: lst-ctn-kix_list_5-1
        }

        ol.lst-kix_list_3-4 {
            list-style-type: none
        }

        .lst-kix_list_2-1>li {
            counter-increment: lst-ctn-kix_list_2-1
        }

        ol.lst-kix_list_3-0 {
            list-style-type: none
        }

        .lst-kix_list_1-1>li {
            counter-increment: lst-ctn-kix_list_1-1
        }

        ol.lst-kix_list_2-6.start {
            counter-reset: lst-ctn-kix_list_2-6 0
        }

        .lst-kix_list_3-0>li:before {
            content: ""counter(lst-ctn-kix_list_3-0, decimal) ". "
        }

        ol.lst-kix_list_3-1.start {
            counter-reset: lst-ctn-kix_list_3-1 0
        }

        .lst-kix_list_3-1>li:before {
            content: ""counter(lst-ctn-kix_list_3-1, lower-latin) ". "
        }

        .lst-kix_list_3-2>li:before {
            content: ""counter(lst-ctn-kix_list_3-2, lower-roman) ". "
        }

        ol.lst-kix_list_1-8.start {
            counter-reset: lst-ctn-kix_list_1-8 0
        }

        .lst-kix_list_4-0>li {
            counter-increment: lst-ctn-kix_list_4-0
        }

        .lst-kix_list_5-0>li {
            counter-increment: lst-ctn-kix_list_5-0
        }

        ol.lst-kix_list_2-3.start {
            counter-reset: lst-ctn-kix_list_2-3 0
        }

        .lst-kix_list_3-5>li:before {
            content: ""counter(lst-ctn-kix_list_3-5, lower-roman) ". "
        }

        .lst-kix_list_3-4>li:before {
            content: ""counter(lst-ctn-kix_list_3-4, lower-latin) ". "
        }

        ol.lst-kix_list_1-5.start {
            counter-reset: lst-ctn-kix_list_1-5 0
        }

        .lst-kix_list_3-3>li:before {
            content: ""counter(lst-ctn-kix_list_3-3, decimal) ". "
        }

        ol.lst-kix_list_3-5 {
            list-style-type: none
        }

        ol.lst-kix_list_3-6 {
            list-style-type: none
        }

        ol.lst-kix_list_3-7 {
            list-style-type: none
        }

        ol.lst-kix_list_3-8 {
            list-style-type: none
        }

        .lst-kix_list_3-8>li:before {
            content: ""counter(lst-ctn-kix_list_3-8, lower-roman) ". "
        }

        .lst-kix_list_2-0>li {
            counter-increment: lst-ctn-kix_list_2-0
        }

        ol.lst-kix_list_5-3.start {
            counter-reset: lst-ctn-kix_list_5-3 0
        }

        .lst-kix_list_2-3>li {
            counter-increment: lst-ctn-kix_list_2-3
        }

        .lst-kix_list_3-6>li:before {
            content: ""counter(lst-ctn-kix_list_3-6, decimal) ". "
        }

        .lst-kix_list_4-3>li {
            counter-increment: lst-ctn-kix_list_4-3
        }

        .lst-kix_list_3-7>li:before {
            content: ""counter(lst-ctn-kix_list_3-7, lower-latin) ". "
        }

        ol.lst-kix_list_4-5.start {
            counter-reset: lst-ctn-kix_list_4-5 0
        }

        ol.lst-kix_list_5-0.start {
            counter-reset: lst-ctn-kix_list_5-0 0
        }

        .lst-kix_list_1-2>li {
            counter-increment: lst-ctn-kix_list_1-2
        }

        ol.lst-kix_list_3-7.start {
            counter-reset: lst-ctn-kix_list_3-7 0
        }

        .lst-kix_list_5-2>li {
            counter-increment: lst-ctn-kix_list_5-2
        }

        ol.lst-kix_list_4-2.start {
            counter-reset: lst-ctn-kix_list_4-2 0
        }

        .lst-kix_list_3-2>li {
            counter-increment: lst-ctn-kix_list_3-2
        }

        ol.lst-kix_list_2-2 {
            list-style-type: none
        }

        ol.lst-kix_list_2-3 {
            list-style-type: none
        }

        .lst-kix_list_5-0>li:before {
            content: ""counter(lst-ctn-kix_list_5-0, decimal) ". "
        }

        ol.lst-kix_list_2-4 {
            list-style-type: none
        }

        ol.lst-kix_list_2-5 {
            list-style-type: none
        }

        .lst-kix_list_5-4>li {
            counter-increment: lst-ctn-kix_list_5-4
        }

        .lst-kix_list_1-4>li {
            counter-increment: lst-ctn-kix_list_1-4
        }

        .lst-kix_list_4-4>li {
            counter-increment: lst-ctn-kix_list_4-4
        }

        ol.lst-kix_list_2-0 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6.start {
            counter-reset: lst-ctn-kix_list_1-6 0
        }

        ol.lst-kix_list_2-1 {
            list-style-type: none
        }

        .lst-kix_list_4-8>li:before {
            content: ""counter(lst-ctn-kix_list_4-8, lower-roman) ". "
        }

        .lst-kix_list_5-3>li:before {
            content: ""counter(lst-ctn-kix_list_5-3, decimal) ". "
        }

        .lst-kix_list_4-7>li:before {
            content: ""counter(lst-ctn-kix_list_4-7, lower-latin) ". "
        }

        .lst-kix_list_5-2>li:before {
            content: ""counter(lst-ctn-kix_list_5-2, lower-roman) ". "
        }

        .lst-kix_list_5-1>li:before {
            content: ""counter(lst-ctn-kix_list_5-1, lower-latin) ". "
        }

        .lst-kix_list_5-7>li:before {
            content: ""counter(lst-ctn-kix_list_5-7, lower-latin) ". "
        }

        ol.lst-kix_list_5-6.start {
            counter-reset: lst-ctn-kix_list_5-6 0
        }

        .lst-kix_list_5-6>li:before {
            content: ""counter(lst-ctn-kix_list_5-6, decimal) ". "
        }

        .lst-kix_list_5-8>li:before {
            content: ""counter(lst-ctn-kix_list_5-8, lower-roman) ". "
        }

        ol.lst-kix_list_4-1.start {
            counter-reset: lst-ctn-kix_list_4-1 0
        }

        ol.lst-kix_list_4-8.start {
            counter-reset: lst-ctn-kix_list_4-8 0
        }

        ol.lst-kix_list_3-3.start {
            counter-reset: lst-ctn-kix_list_3-3 0
        }

        .lst-kix_list_5-4>li:before {
            content: ""counter(lst-ctn-kix_list_5-4, lower-latin) ". "
        }

        .lst-kix_list_5-5>li:before {
            content: ""counter(lst-ctn-kix_list_5-5, lower-roman) ". "
        }

        ol.lst-kix_list_2-6 {
            list-style-type: none
        }

        ol.lst-kix_list_2-7 {
            list-style-type: none
        }

        ol.lst-kix_list_2-8 {
            list-style-type: none
        }

        ol.lst-kix_list_1-0.start {
            counter-reset: lst-ctn-kix_list_1-0 0
        }

        .lst-kix_list_3-0>li {
            counter-increment: lst-ctn-kix_list_3-0
        }

        .lst-kix_list_3-3>li {
            counter-increment: lst-ctn-kix_list_3-3
        }

        ol.lst-kix_list_4-0.start {
            counter-reset: lst-ctn-kix_list_4-0 0
        }

        .lst-kix_list_3-6>li {
            counter-increment: lst-ctn-kix_list_3-6
        }

        .lst-kix_list_2-5>li {
            counter-increment: lst-ctn-kix_list_2-5
        }

        .lst-kix_list_2-8>li {
            counter-increment: lst-ctn-kix_list_2-8
        }

        ol.lst-kix_list_3-2.start {
            counter-reset: lst-ctn-kix_list_3-2 0
        }

        ol.lst-kix_list_5-5.start {
            counter-reset: lst-ctn-kix_list_5-5 0
        }

        .lst-kix_list_2-2>li {
            counter-increment: lst-ctn-kix_list_2-2
        }

        ol.lst-kix_list_2-4.start {
            counter-reset: lst-ctn-kix_list_2-4 0
        }

        ol.lst-kix_list_4-7.start {
            counter-reset: lst-ctn-kix_list_4-7 0
        }

        ol.lst-kix_list_1-3 {
            list-style-type: none
        }

        ol.lst-kix_list_5-0 {
            list-style-type: none
        }

        ol.lst-kix_list_1-4 {
            list-style-type: none
        }

        .lst-kix_list_2-6>li:before {
            content: ""counter(lst-ctn-kix_list_2-6, decimal) ". "
        }

        .lst-kix_list_2-7>li:before {
            content: ""counter(lst-ctn-kix_list_2-7, lower-latin) ". "
        }

        .lst-kix_list_2-7>li {
            counter-increment: lst-ctn-kix_list_2-7
        }

        .lst-kix_list_3-7>li {
            counter-increment: lst-ctn-kix_list_3-7
        }

        ol.lst-kix_list_5-1 {
            list-style-type: none
        }

        ol.lst-kix_list_1-5 {
            list-style-type: none
        }

        ol.lst-kix_list_5-2 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6 {
            list-style-type: none
        }

        ol.lst-kix_list_1-0 {
            list-style-type: none
        }

        .lst-kix_list_2-4>li:before {
            content: ""counter(lst-ctn-kix_list_2-4, lower-latin) ". "
        }

        .lst-kix_list_2-5>li:before {
            content: ""counter(lst-ctn-kix_list_2-5, lower-roman) ". "
        }

        .lst-kix_list_2-8>li:before {
            content: ""counter(lst-ctn-kix_list_2-8, lower-roman) ". "
        }

        ol.lst-kix_list_1-1 {
            list-style-type: none
        }

        ol.lst-kix_list_1-2 {
            list-style-type: none
        }

        ol.lst-kix_list_5-4.start {
            counter-reset: lst-ctn-kix_list_5-4 0
        }

        ol.lst-kix_list_4-6.start {
            counter-reset: lst-ctn-kix_list_4-6 0
        }

        ol.lst-kix_list_5-1.start {
            counter-reset: lst-ctn-kix_list_5-1 0
        }

        ol.lst-kix_list_3-0.start {
            counter-reset: lst-ctn-kix_list_3-0 0
        }

        ol.lst-kix_list_5-7 {
            list-style-type: none
        }

        ol.lst-kix_list_5-8 {
            list-style-type: none
        }

        .lst-kix_list_5-7>li {
            counter-increment: lst-ctn-kix_list_5-7
        }

        ol.lst-kix_list_4-3.start {
            counter-reset: lst-ctn-kix_list_4-3 0
        }

        ol.lst-kix_list_5-3 {
            list-style-type: none
        }

        ol.lst-kix_list_1-7 {
            list-style-type: none
        }

        .lst-kix_list_4-7>li {
            counter-increment: lst-ctn-kix_list_4-7
        }

        ol.lst-kix_list_5-4 {
            list-style-type: none
        }

        .lst-kix_list_1-7>li {
            counter-increment: lst-ctn-kix_list_1-7
        }

        ol.lst-kix_list_1-8 {
            list-style-type: none
        }

        ol.lst-kix_list_3-8.start {
            counter-reset: lst-ctn-kix_list_3-8 0
        }

        ol.lst-kix_list_5-5 {
            list-style-type: none
        }

        ol.lst-kix_list_5-6 {
            list-style-type: none
        }

        ol.lst-kix_list_2-5.start {
            counter-reset: lst-ctn-kix_list_2-5 0
        }

        .lst-kix_list_5-8>li {
            counter-increment: lst-ctn-kix_list_5-8
        }

        .lst-kix_list_4-0>li:before {
            content: ""counter(lst-ctn-kix_list_4-0, decimal) ". "
        }

        .lst-kix_list_2-6>li {
            counter-increment: lst-ctn-kix_list_2-6
        }

        .lst-kix_list_3-8>li {
            counter-increment: lst-ctn-kix_list_3-8
        }

        .lst-kix_list_4-1>li:before {
            content: ""counter(lst-ctn-kix_list_4-1, lower-latin) ". "
        }

        .lst-kix_list_4-6>li {
            counter-increment: lst-ctn-kix_list_4-6
        }

        ol.lst-kix_list_1-7.start {
            counter-reset: lst-ctn-kix_list_1-7 0
        }

        .lst-kix_list_4-4>li:before {
            content: ""counter(lst-ctn-kix_list_4-4, lower-latin) ". "
        }

        ol.lst-kix_list_2-2.start {
            counter-reset: lst-ctn-kix_list_2-2 0
        }

        .lst-kix_list_1-5>li {
            counter-increment: lst-ctn-kix_list_1-5
        }

        .lst-kix_list_4-3>li:before {
            content: ""counter(lst-ctn-kix_list_4-3, decimal) ". "
        }

        .lst-kix_list_4-5>li:before {
            content: ""counter(lst-ctn-kix_list_4-5, lower-roman) ". "
        }

        .lst-kix_list_4-2>li:before {
            content: ""counter(lst-ctn-kix_list_4-2, lower-roman) ". "
        }

        .lst-kix_list_4-6>li:before {
            content: ""counter(lst-ctn-kix_list_4-6, decimal) ". "
        }

        ol.lst-kix_list_5-7.start {
            counter-reset: lst-ctn-kix_list_5-7 0
        }

        .lst-kix_list_1-8>li {
            counter-increment: lst-ctn-kix_list_1-8
        }

        ol.lst-kix_list_1-4.start {
            counter-reset: lst-ctn-kix_list_1-4 0
        }

        .lst-kix_list_5-5>li {
            counter-increment: lst-ctn-kix_list_5-5
        }

        .lst-kix_list_3-5>li {
            counter-increment: lst-ctn-kix_list_3-5
        }

        ol.lst-kix_list_1-1.start {
            counter-reset: lst-ctn-kix_list_1-1 7
        }

        ol.lst-kix_list_4-0 {
            list-style-type: none
        }

        .lst-kix_list_3-4>li {
            counter-increment: lst-ctn-kix_list_3-4
        }

        ol.lst-kix_list_4-1 {
            list-style-type: none
        }

        ol.lst-kix_list_4-4.start {
            counter-reset: lst-ctn-kix_list_4-4 0
        }

        ol.lst-kix_list_4-2 {
            list-style-type: none
        }

        ol.lst-kix_list_4-3 {
            list-style-type: none
        }

        .lst-kix_list_2-4>li {
            counter-increment: lst-ctn-kix_list_2-4
        }

        ol.lst-kix_list_3-6.start {
            counter-reset: lst-ctn-kix_list_3-6 0
        }

        .lst-kix_list_5-3>li {
            counter-increment: lst-ctn-kix_list_5-3
        }

        ol.lst-kix_list_1-3.start {
            counter-reset: lst-ctn-kix_list_1-3 0
        }

        ol.lst-kix_list_2-8.start {
            counter-reset: lst-ctn-kix_list_2-8 0
        }

        ol.lst-kix_list_1-2.start {
            counter-reset: lst-ctn-kix_list_1-2 0
        }

        ol.lst-kix_list_4-8 {
            list-style-type: none
        }

        .lst-kix_list_1-0>li:before {
            content: ""counter(lst-ctn-kix_list_1-0, decimal) ". "
        }

        ol.lst-kix_list_4-4 {
            list-style-type: none
        }

        ol.lst-kix_list_4-5 {
            list-style-type: none
        }

        .lst-kix_list_1-1>li:before {
            content: ""counter(lst-ctn-kix_list_1-1, decimal) " "
        }

        .lst-kix_list_1-2>li:before {
            content: ""counter(lst-ctn-kix_list_1-2, lower-roman) ". "
        }

        ol.lst-kix_list_2-0.start {
            counter-reset: lst-ctn-kix_list_2-0 1
        }

        ol.lst-kix_list_4-6 {
            list-style-type: none
        }

        ol.lst-kix_list_4-7 {
            list-style-type: none
        }

        .lst-kix_list_1-3>li:before {
            content: ""counter(lst-ctn-kix_list_1-3, decimal) ". "
        }

        .lst-kix_list_1-4>li:before {
            content: ""counter(lst-ctn-kix_list_1-4, lower-latin) ". "
        }

        ol.lst-kix_list_3-5.start {
            counter-reset: lst-ctn-kix_list_3-5 0
        }

        .lst-kix_list_1-0>li {
            counter-increment: lst-ctn-kix_list_1-0
        }

        .lst-kix_list_4-8>li {
            counter-increment: lst-ctn-kix_list_4-8
        }

        .lst-kix_list_1-6>li {
            counter-increment: lst-ctn-kix_list_1-6
        }

        .lst-kix_list_1-7>li:before {
            content: ""counter(lst-ctn-kix_list_1-7, lower-latin) ". "
        }

        ol.lst-kix_list_5-8.start {
            counter-reset: lst-ctn-kix_list_5-8 0
        }

        ol.lst-kix_list_2-7.start {
            counter-reset: lst-ctn-kix_list_2-7 0
        }

        .lst-kix_list_1-3>li {
            counter-increment: lst-ctn-kix_list_1-3
        }

        .lst-kix_list_1-5>li:before {
            content: ""counter(lst-ctn-kix_list_1-5, lower-roman) ". "
        }

        .lst-kix_list_1-6>li:before {
            content: ""counter(lst-ctn-kix_list_1-6, decimal) ". "
        }

        .lst-kix_list_5-6>li {
            counter-increment: lst-ctn-kix_list_5-6
        }

        .lst-kix_list_2-0>li:before {
            content: ""counter(lst-ctn-kix_list_2-0, decimal) " "
        }

        .lst-kix_list_2-1>li:before {
            content: ""counter(lst-ctn-kix_list_2-1, lower-latin) ". "
        }

        ol.lst-kix_list_2-1.start {
            counter-reset: lst-ctn-kix_list_2-1 0
        }

        .lst-kix_list_4-5>li {
            counter-increment: lst-ctn-kix_list_4-5
        }

        .lst-kix_list_1-8>li:before {
            content: ""counter(lst-ctn-kix_list_1-8, lower-roman) ". "
        }

        .lst-kix_list_2-2>li:before {
            content: ""counter(lst-ctn-kix_list_2-2, lower-roman) ". "
        }

        .lst-kix_list_2-3>li:before {
            content: ""counter(lst-ctn-kix_list_2-3, decimal) ". "
        }

        .lst-kix_list_4-2>li {
            counter-increment: lst-ctn-kix_list_4-2
        }

        ol.lst-kix_list_5-2.start {
            counter-reset: lst-ctn-kix_list_5-2 0
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c10 {
            margin-left: 5pt;
            padding-top: 0pt;
            text-indent: -4.6pt;
            padding-bottom: 1pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c7 {
            -webkit-text-decoration-skip: none;
            color: #000000;
            font-weight: 400;
            text-decoration: underline;
            vertical-align: baseline;
            text-decoration-skip-ink: none;
            font-size: 8pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c28 {
            margin-left: 0pt;
            padding-top: 0pt;
            list-style-position: inside;
            text-indent: 45pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c13 {
            margin-left: -56.7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center;
            height: 10pt
        }

        .c8 {
            margin-left: 29.2pt;
            padding-top: 0pt;
            text-indent: -22.1pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c1 {
            margin-left: 21.3pt;
            padding-top: 0pt;
            text-indent: -21.3pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c17 {
            margin-left: 21.3pt;
            padding-top: 0pt;
            text-indent: -14.2pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c16 {
            margin-left: 14.2pt;
            padding-top: 0pt;
            text-indent: -7.1pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c12 {
            margin-left: -0.1pt;
            padding-top: 0pt;
            text-indent: -3.8pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c0 {
            margin-left: 7.1pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify;
            height: 10pt
        }

        .c34 {
            margin-left: 15.1pt;
            padding-top: 0pt;
            text-indent: -8pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c3 {
            margin-left: 14.2pt;
            padding-top: 0pt;
            text-indent: -14.2pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c20 {
            margin-left: 18pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify;
            height: 10pt
        }

        .c35 {
            margin-left: 18pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c15 {
            margin-left: 7.1pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c31 {
            margin-left: -56.7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c2 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 8pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c18 {
            margin-left: 18pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c26 {
            margin-left: 35.5pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c5 {
            margin-left: 7.1pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c36 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c32 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            font-size: 10pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c21 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            font-size: 45pt;
            font-family: "Corsiva";
            font-style: normal
        }

        .c39 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c19 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            font-size: 7pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c23 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            font-size: 10pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c22 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c25 {
            font-weight: 400;
            text-decoration: none;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c29 {
            -webkit-text-decoration-skip: none;
            text-decoration: underline;
            text-decoration-skip-ink: none
        }

        .c6 {
            background-color: #ffffff;
            max-width: 567pt;
            padding: 0pt 21.2pt 14.2pt 7.1pt
        }

        .c4 {
            vertical-align: baseline;
            font-size: 8pt;
            font-weight: 700
        }

        .c24 {
            padding: 0;
            margin: 0
        }

        .c9 {
            vertical-align: baseline;
            font-size: 8pt
        }

        .c41 {
            font-weight: 700;
        }

        .c30 {
            text-indent: -21.2pt
        }

        .c27 {
            height: 10pt
        }

        .c11 {
            color: #ff0000
        }

        .c37 {
            margin-left: 28.4pt
        }

        .c14 {
            vertical-align: baseline
        }

        .c38 {
            text-indent: 7.1pt
        }

        .c33 {
            text-indent: -14.2pt
        }

        .title {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 36pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 18pt;
            color: #666666;
            font-size: 24pt;
            padding-bottom: 4pt;
            font-family: "Georgia";
            line-height: 1.0;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 10pt;
            font-family: "Times New Roman"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 10pt;
            font-family: "Times New Roman"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 24pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 18pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 12pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 11pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 10pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c6">
    <p class="c13"><span class="c2"></span></p>
    <p class="c31"><img alt="" src="<?php echo $emitente[0]->url_logo; ?>" style="width: 131.80px; height: 69.40px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title="">
        <!-- <i><span class="c14 c21">Sempre Humana</i></span> -->
    </p>
    <!-- <p class="c22"><span class="c4">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c14 c41">DESENVOLVIMENTO HUMANO E TECNOLÓGICO. </span></p> -->
    <p class="c13"><span class="c2"></span></p>
    <p class="c13"><span class="c2"></span></p>
    <p class="c31"><span class="c23 c14">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c14 c32">TERMO ACORDO DE COOPERAÇÃO &nbsp;- TERMO COMPROMISSO DE ESTÁGIO SUPERVISIONADO</span><span class="c23 c14">.</span></p>
    <p class="c39"><span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instrumentos jurídicos de que tratam o art. 5º &nbsp;do ditames da Lei &nbsp; Federal nº. 11.788./2008.</span></p>
    <p class="c22 c27"><span class="c2"></span></p>
    <?php if ($dados[0]->contrestempr_data_ini <= date('Y-m-d')) {  ?>
        <p class="c15"><span class="c2">Recife, <?php echo  date('d', strtotime($dados[0]->contrestempr_data_ini)); ?> de <?php echo  $mesExtenso; ?> de <?php echo  date('Y', strtotime($dados[0]->contrestempr_data_ini)); ?>  </span></p> 
    <?php  } else { ?>
        <p class="c15"><span class="c2">Recife, <?php echo  date('d'); ?> de <?php echo  $mes; ?> de <?php echo  date('Y'); ?> </span></p> 
    <?php  } ?>
        
    <p class="c22"><span class="c2">&nbsp; &nbsp;</span></p>
    <p class="c22"><span class="c2">&nbsp; &nbsp; ACORDO DE COOPERAÇÃO que celebram entre si as nomeadas a seguir, convencionando as cláusulas e condições que se seguem</span></p>
    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>    
    <p class="c15" style="margin-top:2px;"><span class="c29 c4"><b>INSTITUIÇÃO DE ENSINO </b></span></p>
    <p class="c15 c27"><span class="c2"></span></p>
    <p class="c15"><span class="c2">Razão Social: <?php echo $dados[0]->sensenrazao ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<?php  ($dados[0]->sensenmantededora <> '') ? 'Mantenedora:'.$dados[0]->sensenmantededora : '' ;  ?></span></p>
    <!-- <p class="c15"><span class="c2">Instituição de Ensino: <?php echo strtoupper($dados[0]->sensennome) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></p> -->
    <p class="c15"><span class="c2">Endereço: <?php echo $dados[0]->sensenlogra ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Bairro: <?php echo $dados[0]->sensenbairr ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cidade: <?php echo $dados[0]->sensencidad ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  CEP: <?php echo $dados[0]->sensencep; ?> &nbsp; &nbsp; &nbsp; &nbsp; UF: <?php echo strtoupper($dados[0]->sensenuf) ?> </span></p>
    <p class="c15"><span class="c2">CNPJ: <?php echo $dados[0]->sensencnpj ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fone: <?php echo $dados[0]->sensentel01; ?> </span></p>
    <p class="c10"><span class="c2">&nbsp;Representante legal: <?php echo $dados[0]->sensenrepre ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cargo: <?php echo $dados[0]->sensencargo ?></span></p>
    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>
    <p class="c15"><span class="c4 c29"> <b> UNIDADE CONCEDENTE DE ESTÁGIO</b></span></p>
    <p class="c36"><span class="c2">&nbsp; &nbsp;</span></p>
    <p class="c15"><span class="c2">Razão Social: <?php echo $dados[0]->sempemrazao ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CNPJ: <?php echo $dados[0]->sempemcnpj; ?></span></p>
    <p class="c15"><span class="c2">Endereço: <?php echo $dados[0]->sempemlogra.','.$dados[0]->sempemnumer.' '.$dados[0]->sempemcompl ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Bairro: <?php echo $dados[0]->sempembairr; ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Cidade: <?php echo $dados[0]->sempemcidad; ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  UF: <?php echo strtoupper($dados[0]->sempemuf) ?></span></p>
    <p class="c15"><span class="c2">Fone: <?php echo $dados[0]->sempemtel01; ?> &nbsp;</span></p>
    <p class="c15"><span class="c2">Representante legal: <?php echo $dados[0]->sempemrepre ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;CPF do representante: <?php echo $dados[0]->sempemrecpf ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cargo: <?php echo $dados[0]->sempemrecar ?>&nbsp;</span></p>
    <p class="c15"><span class="c2">Ramo de atividade:<?php echo $dados[0]->sempemativi ?> </span></p>
    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>
    <p class="c15" style="margin-top:2px;"><span class="c29 c4"> <b>ESTAGIÁRIO. </b> </span></p>
    <p class="c15 c27"><span class="c2"></span></p>
    <p class="c15"><span class="c9">Nome:<?php echo $dados[0]->salualnome ?>  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Data de Nas . <?php echo date('d/m/Y', strtotime($dados[0]->talualniver)); ?></span></p>
    <p class="c15"><span class="c2">Filiação:  <?php echo $dados[0]->salualpai.'   -  '.$dados[0]->salualmae ?>  </span></p>
    
    <p class="c15"><span class="c2">Endereço: <?php echo $dados[0]->saluallogra.' '.$dados[0]->salualnumer.' '.$dados[0]->salualcompl ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bairro: <?php echo $dados[0]->salualbairr ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cidade: <?php echo $dados[0]->salualcidad ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;UF:<?php echo strtoupper($dados[0]->salualestad) ?></span></p>
    
    <p class="c15"><span class="c2">RG: &nbsp;<?php echo $dados[0]->salualrg; ?> - <?php echo $dados[0]->salualrg_org_expd; ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;CPF: <?php echo $dados[0]->salualcpf ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c15" style="margin-top:2px;"><span class="c2">Curso: &nbsp; <?php echo $dados[0]->scurcunome ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nº. de Matrícula Escolar: <?php echo $dados[0]->salualmatricula ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Prev. Término de Curso: <?php echo date('d/m/Y', strtotime($dados[0]->salualtermino)); ?> &nbsp; &nbsp;</span></p>
       
    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>
    <p class="c15"><span class="c2">&nbsp; &nbsp;</span></p>
    <p class="c15"><span class="c4"> <b> Cláusula 1ª</b> </span></p>
    <p class="c15 c40"><span class="c2">Este documento tem por objetivo formalizar as condições para a realização de estágio de estudante junto à Unidade Concedente, o qual, obrigatório ou não, deve ser de interesse curricular e pedagogicamente útil, entendido o Estágio como um ato educativo escolar supervisionado, desenvolvido no ambiente de trabalho, proporcionando ao educando atividades de aprendizagem social, profissional e cultural que propicia “a promoção da integração ao mercado de trabalho” e “a formação para o trabalho” de acordo com a Constituição Federal vigente (Arts. 203, item III e 214, item IV e 227), a Lei de Estágio nº 11788/08 e a LDB Lei nº 9.394/96. &nbsp;</span></p>

    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>
    <!-- <p class="c15" style="margin-top:2px;"><span class="c4"><b> AGENTE DE INTEGRAÇÃO </b></span></p> -->
    <p class="c15"><span class="c4 c29"> <b> AGENTE DE INTEGRAÇÃO</b></span></p>
    <p class="c36"><span class="c2">&nbsp; &nbsp;</span></p>
    <p class="c15 c33"><span class="c2">Nome: <?php echo $emitente[0]->nome ?> &nbsp;</span></p>
    <p class="c15"><span class="c2">Endereço: <?php echo $emitente[0]->rua.' '.$emitente[0]->numero ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bairro: <?php echo $emitente[0]->bairro ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cidade: <?php echo $emitente[0]->cidade ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; UF: <?php echo strtoupper($emitente[0]->ds_uf_sigla) ?></span></p>
    <p class="c15" style="margin-top:2px;"><span class="c2">CEP:  <?php echo $emitente[0]->cep ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fone: <?php echo $emitente[0]->telefone ?></span></p>
    <p class="c15"><span class="c2">CNPJ: <?php echo $emitente[0]->cnpj ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c22"><span class="c2">&nbsp; &nbsp; Representada por: <?php echo $emitente[0]->representada ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cargo: <?php echo $emitente[0]->cargo ?></span></p>
    
    <p class="c22 c38"><span class="c4"> <b> _______________________________________________________________________________________________________________________________________</b></span></p>
    <p class="c15 c27"><span class="c2"></span></p>
    <p class="c15"><span class="c4"> <b> Cláusula 2ª </b></span></p>
    <p class="c5"><span class="c2">Para a realização de cada Estágio, em decorrência do presente Acordo, será necessário celebrar um Termo de Compromisso de Estágio entre o Estudante e a Unidade Concedente, com interveniência obrigatória da Instituição de Ensino, conforme trata o inciso II do caput do Art 3º da Lei 11.788/2008, e a concessão de bolsa de complementação educacional e demais benefícios para o estagiário o qual não caracterizará vínculo empregatício, conforme Art.12 parágrafo 1º da atual Legislação em vigor. </span></p>
    <p class="c0"><span class="c2"></span></p>
    <p class="c5"><span class="c2">Parágrafo 1º - O Termo de Compromisso de Estudante, com base no presente acordo, terá como função básica, em relação a cada estagiário, estabelecer uma relação jurídica especial existente entre o estudante, e a Unidade Concedente, bem como fundamentado no regulamento do curso a determinação de requisitos e normas internas da Unidade Concedente, e cumprimento integral das obrigações das partes celebrantes e dos intervenientes.</span></p>
    <p class="c0"><span class="c2"></span></p>
    <p class="c5"><span class="c9">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span class="c4"> <b> Plano de Atividades de Estágio: ( <?php echo ($dados[0]->contrestempr_estagioobg == 0) ? 'X' : '' ; ?>) estágio não obrigatório ( <?php echo ($dados[0]->contrestempr_estagioobg == 1) ? 'X' : '' ; ?> ) estágio obrigatório. </b> </span></p>
    <p class="c0"><span class="c2"></span></p>

    <?php 
    $setorAtiv = explode('/', $dados[0]->contrestempr_atividade);

    ?>

    <ol class="c24 lst-kix_list_1-0 start" start="1">
        <li class="c28"><span class="c2">1. Carga horária diária: <?php echo $dados[0]->contrestempr_cargahoraria;  ?> <!-- hs/por dia &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Das: ____as____ e das ____as ____ &nbsp;De segunda a sexta-feira &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Carga horária semanal:  hs/ semanal</span></li> -->
        <li class="c28"><span class="c2">2. Bolsa de Complementação Educacional: <?php echo 'R$ '.$dados[0]->contrestempr_bolsa_Auxilio; ?> p/ mês </span></li>
        <li class="c28"><span class="c9">3. Benefícios:</span><span class="c9"><?php echo $dados[0]->contrestempr_beneficios; ?></span></li> <!-- Vale transporte – R$ _____ </span><span class="c9 c11">&nbsp;</span><span class="c2">- Vale refeição -_________ </span></li> -->
        <li class="c28"><span class="c9">4. Duração do contrato: </span><span class="c9"><?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_ini)); ?> a <?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_term)); ?></span></li> <!-- <span class="c9 c11">: ___</span><span class="c2">&nbsp;meses &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; De: &nbsp;____/ ____ /20___ até ____ / ____ / 20____</span></li> -->
        <li class="c28"><span class="c2">5. Setor do estágio: <?php echo $dados[0]->contrestempr_setor; ?></span></li>
        <li class="c28"><span class="c9">6. Local do Estágio: <?php echo $dados[0]->sempemrazao ?> &nbsp;End: <?php echo $dados[0]->sempemlogra.','.$dados[0]->sempemnumer.' '.$dados[0]->sempemcompl ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></li>
        <li class="c28"><span class="c9">7. Orientador:<?php echo $dados[0]->contrestempr_orientador; ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cargo: <?php echo $dados[0]->contrestempr_cargo; ?> &nbsp; &nbsp; &nbsp; Formação: <?php echo $dados[0]->contrestempr_formacao; ?> </span></li>
        <li class="c28"><span class="c9">8. Seguradora de Acidentes Pessoais- nº da</span><span class="c9">&nbsp;</span><span class="c4">Apólice: 0929.000136775.0982.004195551 – Porto Seguro SEGURADORA - &nbsp; </span></li>
        <li class="c28"><span class="c9">válido durante a vigência deste contrato. Cujos valores segurados são: para acidentes pessoais: R$ 12.376,50 e DMH é R$ 500,00</span></li>
        <li class="c28"><span class="c9">9. Principais atividades a serem desenvolvidas no estágio: <?php echo  $dados[0]->contrestempr_atividade;  ?> </span></li>
    </ol>
    <p class="c15 c27"><span class="c2"></span></p>

    <p class="c5"><span class="c4"> <b> Cláusula 3ª </b></span><span class="c9">De acordo com o artigo 7º da &nbsp;Lei nº 11.788/2008, e no Termo de Ajuste de Conduta no 111.2015, caberá a </span><span class="c4">INSTITUIÇÃO DE ENSINO:</span></p>
    <p class="c18"><span class="c2">1. Manter a Unidade Concedente ao Estágio informada a respeito da situação escolar do aluno, quanto à matrícula, frequência, abandono ou interrupção do curso; </span></p>
    <p class="c18"><span class="c2">2. Repassar à Unidade Concedente ao Estágio informações administrativas, quanto a mudanças na evolução da escola e dos cursos; </span></p>
    <p class="c18"><span class="c2">3. Fornecer ao aluno declaração, comprovante de matrícula e frequência escolar; </span></p>
    <p class="c18"><span class="c2">4. Orientar e avaliar o estágio e seu Plano de Atividades, a fim de garantir a sua legalidade como complemento da aprendizagem; </span></p>
    <p class="c18"><span class="c2">5. Exigir do estudante, a cada seis meses, o Relatório de Atividades, preenchido pelos estagiários com visto dos supervisores da UCE, para a devida análise; </span></p>
    <p class="c18"><span class="c2">6. Informar à Unidade Concedente ao Estágio, no início do período letivo, o calendário de provas escolares, no sentido de que a jornada de estágio, durante esse período, &nbsp; seja reduzida pela metade, consoante o parágrafo 2º do art. 10 da Lei de Estágio; </span></p>
    <p class="c18"><span class="c2">7. Celebrar o presente Termo, considerando as condições de adequação do estágio à proposta pedagógica do curso, à etapa e modalidade da formação escolar do estudante &nbsp; &nbsp; e horário, como também ao calendário escolar; </span></p>
    <p class="c18"><span class="c2">8. Manter o estágio, obrigatório ou não, no projeto pedagógico do curso; </span></p>
    <p class="c18"><span class="c2">9. Realizar visitas de supervisão de estágio, no local de realização do estágio e sem aviso prévio. Caso sejam identificados desvios de finalidade do estágio, a Instituição de Ensino comunicará imediatamente ao Ministério do Trabalho e Emprego e ao Ministério Público do Trabalho.</span></p>
    <p class="c0"><span class="c2"></span></p>
    <p class="c5"><span class="c4"> <b> Cláusula 4º. </b></span><span class="c9">No decorrer do estágio, diante do instrumento de compromisso, caberá ao </span><span class="c4">ESTAGIÁRIO</span><span class="c2">:</span></p>
    <!-- <p class="c0"><span class="c2"></span></p> -->
    <p class="c5"><span class="c2">1. Conhecer e cumprir, com o máximo empenho e interesse, todo o Plano de Atividades estabelecido para seu estágio; </span></p>
    <p class="c5"><span class="c2">2. Observar &nbsp;e obedecer às normas internas da Unidade Concedente; </span></p>
    <p class="c5"><span class="c2">3. Providenciar, quando convocado, o preenchimento e devolução dos Relatórios de Atividades e/ou outros documentos do Programa do Estágio; </span></p>
    <p class="c5"><span class="c2">4. Entregar obrigatoriamente com a máxima urgência a todas partes envolvidas pela formalização do estágio, uma via do presente instrumento, devidamente assinada e &nbsp; &nbsp; &nbsp;carimbada pelas partes (Sempre Humana, Instituição de Ensino, Unidade Concedente, Estudante) e visto do Professor Orientador responsável; </span></p>
    <p class="c5"><span class="c2">5.Comunicar a Sempre Humana e Instituição de Ensino, imediatamente, para as devidas providências, qualquer ato praticado pela Unidade Concedente que lhe cause constrangimento, bem como qualquer ordem ou conduta atentatórias às Cláusulas ora acordadas; </span></p>
    <p class="c5"><span class="c2">6. Informar, de imediato e por escrito à Unidade Concedente e a Sempre Humana, qualquer fato que interrompa, suspenda ou cancele sua matrícula na Instituição de Ensino. </span></p>
    <p class="c5"><span class="c2">7. Manter atualizado o número do celular e o endereço de e-mail, a fim de que a Instituição de Ensino faça as convocações necessárias para a operacionalização do Programa de Estágio. </span></p>
    <p class="c5"><span class="c2">8. Poderá o estagiário inscrever-se e contribuir como segurado facultativo do Regime Geral de Previdência Social.</span></p>
    <p class="c5"><span class="c2">9. Atualizar sempre seus dados cadastrais no sistema da Sempre Humana</span></p>
    <p class="c0"><span class="c2"></span></p>
    <p class="c5"><span class="c4"> <b> Cláusula 5ª</b></span><span class="c2">.</span></p>
    <p class="c5"><span class="c4">Compete a </span><span class="c4">UNIDADE CONCEDENTE</span><span class="c2">, de acordo com os instrumentos jurídicos da Lei 11.788/2008 &nbsp;em vigor:</span></p>
    <p class="c5"><span class="c2">1. Proporcionar ao estagiário o aprendizado de competências próprias da atividade profissional e à contextualização curricular, objetivando o desenvolvimento do educando para a vida cidadã e para o trabalho, ofertando instalações que propiciem ao educando atividades de aprendizagem social, profissional e cultural; </span></p>
    <p class="c5"><span class="c2">2. Encaminhar para a Instituição de Ensino o relatório individual de atividade, assinado pelo Supervisor com periodicidade máxima de 6 (seis) meses com vista obrigatória do estagiário; </span></p>
    <p class="c5"><span class="c2">3. Pagar aos estagiários o valor da Bolsa-Auxílio e do auxílio transporte; </span></p>
    <p class="c5"><span class="c2">4. Respeitar, na íntegra, todas as Cláusulas ora pactuadas, sendo vedado qualquer ato que desvirtue o presente estágio; </span></p>
    <p class="c5"><span class="c2">5 .Não substituir mão-de-obra, assim entendida como ato de demitir funcionário, visando a contratação de estagiários; </span></p>
    <p class="c5"><span class="c2">6. Conceder ao estagiário, sempre que o estágio tenha a duração igual ou superior a um ano, um recesso remunerado de trinta dias, a ser gozado preferencialmente durante as férias escolares do estágio, ou um descanso remunerado proporcional, nos casos de o estágio ter duração inferior a um ano, cujo descanso pode ser corrido ou fracionados, conforme acordo escrito entre as partes. </span></p>
    <p class="c5"><span class="c2">7. Reduzir, pelo menos pela metade, a jornada de estágio, durante o período de provas escolares conforme a Lei de Estágio; </span></p>
    <p class="c5"><span class="c2">8. Entregar ao estagiário, por ocasião do seu desligamento, um Termo de Realização do Estágio, contendo a indicação resumida das atividades desenvolvidas, dos períodos e &nbsp; &nbsp; da avaliação do desempenho; </span></p>
    <p class="c5"><span class="c2">9. Manter o número máximo de estagiários, de nível médio, em relação ao quadro de pessoal dos seus empregados, conforme as proporções indicadas no Art. 17 da Lei de &nbsp; &nbsp;Estágio; </span></p>
    <p class="c5"><span class="c2">10.Garantir ao estagiário os direitos previstos na legislação relacionada à saúde e segurança o trabalho, conforme o art. 14 da Lei de Estágio; </span></p>
    <p class="c5"><span class="c2">11.Formalizar as oportunidades de estágio, em conjunto com as instituições de ensino, adequando suas condições com as exigências da legislação de estágio; </span></p>
    <p class="c5"><span class="c2">12.Assinar os documentos do estágio, providenciados pelo Agente de Integração encaminhado a Instituição de Ensino; </span></p>
    <p class="c5"><span class="c2">13.Permitir que o estudante inicie o estágio, somente quando o Termo de Compromisso estiver devidamente assinado por todas as partes envolvidas.</span></p>
    <p class="c0"><span class="c2"></span></p>
    <p class="c5"><span class="c4"> <b> Cláusula 6º</b></span><span class="c2">.</span></p>
    <p class="c5"><span class="c9">Compete </span><span class="c4">A SEMPRE HUMANA</span><span class="c2">:</span></p>
    <p class="c5"><span class="c2">1. Averiguar idade cronológica do estudante, onde somente poderão realizar estágio supervisionado, o aluno que tiver no mínimo 16 anos &nbsp; na data de início do estágio. </span></p>
    <p class="c5"><span class="c2">2. Oferecer ao estudante um acompanhamento administrativo da sua prática na organização, através de uma ficha de avaliação de desempenho. </span></p>
    <p class="c5"><span class="c2">3. Contribuir como Agente de Integração, com a Instituição de Ensino, aluno, e a Unidade Concedente, a realização do Programa de Estágio Supervisionado. </span></p>
    <p class="c5"><span class="c2">4. Suspender o contrato de estágio, caso a Unidade Concedente não devolva a via do Termo de Compromisso assinado pelas partes cabíveis no prazo de 15 dias da data de &nbsp; &nbsp; &nbsp; emissão ao Agente de Integração</span></p>
    <p class="c3"><span class="c23 c14">&nbsp; &nbsp; </span></p>
    <p class="c5"><span class="c4"> <b> Cláusula 7ª. </b></span></p>
    <p class="c5"><span class="c2">Este Termo de Compromisso poderá ser prorrogado através da emissão de um Termo Aditivo ou ser rescindido a qualquer tempo, unilateralmente, mediante comunicação escrita, por qualquer uma das partes. </span></p>
    <p class="c3"><span class="c23 c14">&nbsp; &nbsp; </span></p>
    <p class="c5"><span class="c4"> <b> Cláusula 8ª</b></span><span class="c2">. O estágio mediante o Art. 12º no parágrafo 1º. da Lei 11.788, não cria vínculo empregatício de qualquer natureza, ressalvando o que dispuser a legislação previdenciária. </span></p>
    <p class="c3"><span class="c2">&nbsp; &nbsp; &nbsp; </span></p>
    <p class="c5"><span class="c4"> <b>Cláusula 9ª </b></span><span class="c9">&nbsp;– </span><span class="c4">DA ASSINATURA ELETRÔNICA</span><span class="c2">&nbsp;</span></p>
    <p class="c5"><span class="c2">Em se tratando desta alternativa as partes envolvidas reconhecem a autenticidade, veracidade, integridade, validade e eficácia do presente Contrato e seus termos, conforme disposto no art. 219 do Código Civil, bem como das assinaturas apostas, em formato físico ou eletrônico, devendo, no caso de utilização de assinatura eletrônica, que esta seja realizada através de qualquer plataforma reconhecida pela ICP-Brasil, a teor do disposto no § 2º, do art. 10, da Medida Provisória nº 2.200-2, de 24 de agosto de 2001, podendo, ainda ser formalizada através da plataforma D4Sign ou por qualquer outra, desde que reconhecida e validade pelas Partes. </span></p>
    <p class="c3"><span class="c2">&nbsp; &nbsp; &nbsp;</span></p>
    <p class="c5"><span class="c4"> <b>Cláusula 10ª</b></span><span class="c9">&nbsp;- </span><span class="c4">DO USO DE DADOS</span><span class="c2">&nbsp;</span></p>
    <p class="c5"><span class="c2">1. É vedado divulgar e compartilhar com terceiros quaisquer dados recebidos e contidos neste presente documento, salvo Termo de Consentimento prévio por escrito tenha &nbsp; sido obtido pelas partes envolvidas, usando-o estritamente mediante objetivo proposto; </span></p>
    <p class="c5"><span class="c2">2. Não terceirizar/subcontratar o processamento dos dados pessoais recebidos, nem transferir o processamento ou tratamento para qualquer outra empresa ou terceiro, inclusive no exterior, sem o termo de consentimento prévio das partes, cujos dados estão sendo transmitidos para terceiro; </span></p>
    <p class="c5"><span class="c2">3. Impossibilitar modificação de qualquer finalidade ou propósito para o qual foi autorizada a transmissão, uso e/ou processamento de dados contratuais, assim como não combinar dados de diferentes indivíduos; &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c5"><span class="c2">4. Responsabilizar-se integralmente, pelo descumprimento de qualquer condição legal ou contratual com relação a tratamento de dados, sendo certo que na hipótese de violação, poderá a PARTE adimplente rescindir o presente instrumento. </span></p>
    <p class="c3"><span class="c2">&nbsp; &nbsp; &nbsp; </span></p>
    <p class="c5"><span class="c4"> <b>Cláusula 11ª</b> </span></p>
    <p class="c5"><span class="c2">E, por estarem justas e concordes as partes assinam o presente Acordo de Cooperação e Termo de Compromisso de Estágio em 04 (quatro) vias de igual teor, na presença &nbsp; de testemunhas. </span></p>
    <p class="c3"><span class="c14 c23">&nbsp; &nbsp; &nbsp;</span></p>
    
    <?php if ($dados[0]->contrestempr_data_ini <= date('Y-m-d')) {  ?>
        <p class="c15"><span class="c2">Recife, <?php echo  date('d', strtotime($dados[0]->contrestempr_data_ini)); ?> de <?php echo  $mesExtenso; ?> de <?php echo  date('Y', strtotime($dados[0]->contrestempr_data_ini)); ?>  </span></p> 
    <?php  } else { ?>
        <p class="c15"><span class="c2">Recife, <?php echo  date('d'); ?> de <?php echo  $mes; ?> de <?php echo  date('Y'); ?> </span></p> 
    <?php  } ?>
    <p class="c15 c27"><span class="c2"></span></p>
    <p class="c15 c27"><span class="c2"></span></p>
    <p class="c15"><span class="c2">__________________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;____________________________________</span></p>
    <p class="c22" style="margin-top: 2px;"><span class="c2">&nbsp; &nbsp;Instituição de Ensino (carimbo e assinatura)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unidade Concedente (carimbo e assinatura)</span></p>
    <p class="c15 c38"><span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></p>
    <p class="c15"><span class="c2">___________________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ____________________________________ &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c22" style="margin-top: 2px;"><span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;Estagiário&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agente de Integração</span></p>
    <div>
        <p class="c22"><span class="c14 c19">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    </div>
</body>

</html>