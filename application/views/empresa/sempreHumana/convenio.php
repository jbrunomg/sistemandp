<?php

$mes = date('m');      // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y');      // Ano atual
$ultimo_dia = date('t', mktime(0, 0, 0, $mes, '01', $ano)); // Mágica, plim!
$mesExtenso = date('m', strtotime($dados[0]->contrestempr_data_ini));

switch ($mesExtenso){
 
case 1: $mesExtenso = "JANEIRO"; break;
case 2: $mesExtenso = "FEVEREIRO"; break;
case 3: $mesExtenso = "MARÇO"; break;
case 4: $mesExtenso = "ABRIL"; break;
case 5: $mesExtenso = "MAIO"; break;
case 6: $mesExtenso = "JUNHO"; break;
case 7: $mesExtenso = "JULHO"; break;
case 8: $mesExtenso = "AGOSTO"; break;
case 9: $mesExtenso = "SETEMBRO"; break;
case 10: $mesExtenso = "OUTUBRO"; break;
case 11: $mesExtenso = "NOVEMBRO"; break;
case 12: $mesExtenso = "DEZEMBRO"; break;
 
}

switch ($mes){
 
case 1: $mes = "JANEIRO"; break;
case 2: $mes = "FEVEREIRO"; break;
case 3: $mes = "MARÇO"; break;
case 4: $mes = "ABRIL"; break;
case 5: $mes = "MAIO"; break;
case 6: $mes = "JUNHO"; break;
case 7: $mes = "JULHO"; break;
case 8: $mes = "AGOSTO"; break;
case 9: $mes = "SETEMBRO"; break;
case 10: $mes = "OUTUBRO"; break;
case 11: $mes = "NOVEMBRO"; break;
case 12: $mes = "DEZEMBRO"; break;
 
}

?>

<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=cwkuN-YbXleWjD6k_pVPXg');

        ol.lst-kix_list_1-3 {
            list-style-type: none
        }

        ol.lst-kix_list_1-4 {
            list-style-type: none
        }

        .lst-kix_list_2-6>li:before {
            content: ""counter(lst-ctn-kix_list_2-6, decimal) ". "
        }

        .lst-kix_list_2-7>li:before {
            content: ""counter(lst-ctn-kix_list_2-7, lower-latin) ". "
        }

        .lst-kix_list_2-7>li {
            counter-increment: lst-ctn-kix_list_2-7
        }

        ol.lst-kix_list_1-5 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6 {
            list-style-type: none
        }

        .lst-kix_list_2-1>li {
            counter-increment: lst-ctn-kix_list_2-1
        }

        ol.lst-kix_list_1-0 {
            list-style-type: none
        }

        .lst-kix_list_2-4>li:before {
            content: ""counter(lst-ctn-kix_list_2-4, lower-latin) ". "
        }

        .lst-kix_list_2-5>li:before {
            content: ""counter(lst-ctn-kix_list_2-5, lower-roman) ". "
        }

        .lst-kix_list_2-8>li:before {
            content: ""counter(lst-ctn-kix_list_2-8, lower-roman) ". "
        }

        ol.lst-kix_list_1-1 {
            list-style-type: none
        }

        ol.lst-kix_list_1-2 {
            list-style-type: none
        }

        .lst-kix_list_1-1>li {
            counter-increment: lst-ctn-kix_list_1-1
        }

        ol.lst-kix_list_2-6.start {
            counter-reset: lst-ctn-kix_list_2-6 0
        }

        ol.lst-kix_list_1-8.start {
            counter-reset: lst-ctn-kix_list_1-8 0
        }

        ol.lst-kix_list_2-3.start {
            counter-reset: lst-ctn-kix_list_2-3 0
        }

        ol.lst-kix_list_1-5.start {
            counter-reset: lst-ctn-kix_list_1-5 0
        }

        ol.lst-kix_list_1-7 {
            list-style-type: none
        }

        .lst-kix_list_1-7>li {
            counter-increment: lst-ctn-kix_list_1-7
        }

        ol.lst-kix_list_1-8 {
            list-style-type: none
        }

        ol.lst-kix_list_2-5.start {
            counter-reset: lst-ctn-kix_list_2-5 0
        }

        .lst-kix_list_2-0>li {
            counter-increment: lst-ctn-kix_list_2-0
        }

        .lst-kix_list_2-3>li {
            counter-increment: lst-ctn-kix_list_2-3
        }

        .lst-kix_list_2-6>li {
            counter-increment: lst-ctn-kix_list_2-6
        }

        ol.lst-kix_list_1-7.start {
            counter-reset: lst-ctn-kix_list_1-7 0
        }

        .lst-kix_list_1-2>li {
            counter-increment: lst-ctn-kix_list_1-2
        }

        ol.lst-kix_list_2-2.start {
            counter-reset: lst-ctn-kix_list_2-2 0
        }

        .lst-kix_list_1-5>li {
            counter-increment: lst-ctn-kix_list_1-5
        }

        .lst-kix_list_1-8>li {
            counter-increment: lst-ctn-kix_list_1-8
        }

        ol.lst-kix_list_1-4.start {
            counter-reset: lst-ctn-kix_list_1-4 0
        }

        ol.lst-kix_list_1-1.start {
            counter-reset: lst-ctn-kix_list_1-1 0
        }

        ol.lst-kix_list_2-2 {
            list-style-type: none
        }

        ol.lst-kix_list_2-3 {
            list-style-type: none
        }

        ol.lst-kix_list_2-4 {
            list-style-type: none
        }

        ol.lst-kix_list_2-5 {
            list-style-type: none
        }

        .lst-kix_list_1-4>li {
            counter-increment: lst-ctn-kix_list_1-4
        }

        ol.lst-kix_list_2-0 {
            list-style-type: none
        }

        .lst-kix_list_2-4>li {
            counter-increment: lst-ctn-kix_list_2-4
        }

        ol.lst-kix_list_1-6.start {
            counter-reset: lst-ctn-kix_list_1-6 0
        }

        ol.lst-kix_list_2-1 {
            list-style-type: none
        }

        ol.lst-kix_list_1-3.start {
            counter-reset: lst-ctn-kix_list_1-3 0
        }

        ol.lst-kix_list_2-8.start {
            counter-reset: lst-ctn-kix_list_2-8 0
        }

        ol.lst-kix_list_1-2.start {
            counter-reset: lst-ctn-kix_list_1-2 0
        }

        .lst-kix_list_1-0>li:before {
            content: ""counter(lst-ctn-kix_list_1-0, decimal) ". "
        }

        ol.lst-kix_list_2-6 {
            list-style-type: none
        }

        .lst-kix_list_1-1>li:before {
            content: ""counter(lst-ctn-kix_list_1-1, lower-latin) ". "
        }

        .lst-kix_list_1-2>li:before {
            content: ""counter(lst-ctn-kix_list_1-2, lower-roman) ". "
        }

        ol.lst-kix_list_2-0.start {
            counter-reset: lst-ctn-kix_list_2-0 0
        }

        ol.lst-kix_list_2-7 {
            list-style-type: none
        }

        ol.lst-kix_list_2-8 {
            list-style-type: none
        }

        .lst-kix_list_1-3>li:before {
            content: ""counter(lst-ctn-kix_list_1-3, decimal) ". "
        }

        .lst-kix_list_1-4>li:before {
            content: ""counter(lst-ctn-kix_list_1-4, lower-latin) ". "
        }

        ol.lst-kix_list_1-0.start {
            counter-reset: lst-ctn-kix_list_1-0 0
        }

        .lst-kix_list_1-0>li {
            counter-increment: lst-ctn-kix_list_1-0
        }

        .lst-kix_list_1-6>li {
            counter-increment: lst-ctn-kix_list_1-6
        }

        .lst-kix_list_1-7>li:before {
            content: ""counter(lst-ctn-kix_list_1-7, lower-latin) ". "
        }

        ol.lst-kix_list_2-7.start {
            counter-reset: lst-ctn-kix_list_2-7 0
        }

        .lst-kix_list_1-3>li {
            counter-increment: lst-ctn-kix_list_1-3
        }

        .lst-kix_list_1-5>li:before {
            content: ""counter(lst-ctn-kix_list_1-5, lower-roman) ". "
        }

        .lst-kix_list_1-6>li:before {
            content: ""counter(lst-ctn-kix_list_1-6, decimal) ". "
        }

        li.li-bullet-0:before {
            margin-left: -18pt;
            white-space: nowrap;
            display: inline-block;
            min-width: 18pt
        }

        .lst-kix_list_2-0>li:before {
            content: ""counter(lst-ctn-kix_list_2-0, decimal) ". "
        }

        .lst-kix_list_2-1>li:before {
            content: ""counter(lst-ctn-kix_list_2-1, lower-latin) ". "
        }

        ol.lst-kix_list_2-1.start {
            counter-reset: lst-ctn-kix_list_2-1 0
        }

        .lst-kix_list_2-5>li {
            counter-increment: lst-ctn-kix_list_2-5
        }

        .lst-kix_list_2-8>li {
            counter-increment: lst-ctn-kix_list_2-8
        }

        .lst-kix_list_1-8>li:before {
            content: ""counter(lst-ctn-kix_list_1-8, lower-roman) ". "
        }

        .lst-kix_list_2-2>li:before {
            content: ""counter(lst-ctn-kix_list_2-2, lower-roman) ". "
        }

        .lst-kix_list_2-3>li:before {
            content: ""counter(lst-ctn-kix_list_2-3, decimal) ". "
        }

        .lst-kix_list_2-2>li {
            counter-increment: lst-ctn-kix_list_2-2
        }

        ol.lst-kix_list_2-4.start {
            counter-reset: lst-ctn-kix_list_2-4 0
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c5 {
            margin-left: 36pt;
            padding-top: 0pt;
            padding-left: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c16 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c2 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c15 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c1 {
            margin-left: -56.7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c23 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c6 {
            padding-top: 14pt;
            text-indent: 30pt;
            padding-bottom: 14pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c18 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 48pt;
            font-family: "Corsiva";
            font-style: normal
        }

        .c4 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c22 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 8pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c8 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c7 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c0 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c11 {
            vertical-align: baseline;
            font-size: 8pt;
            font-weight: 700
        }

        .c19 {
            vertical-align: baseline;
            font-size: 9pt;
            font-weight: 700
        }

        .c17 {
            background-color: #ffffff;
            max-width: 540pt;
            padding: 7.1pt 36pt 18pt 36pt
        }

        .c9 {
            vertical-align: baseline;
            font-size: 10pt;
            font-weight: 700
        }

        .c20 {
            margin-left: 36pt;
            text-indent: -27pt
        }

        .c14 {
            padding: 0;
            margin: 0
        }

        .c25 {
            margin-left: 36pt;
            padding-left: 0pt
        }

        .c12 {
            margin-left: 36pt;
            text-indent: -18pt
        }

        .c3 {
            height: 12pt
        }

        .c24 {
            margin-left: -9pt;
            margin-top: 5px;
        }

        .c21 {
            margin-right: -13.1pt
        }

        .c13 {
            page-break-after: avoid
        }

        .c10 {
            margin-left: 18pt
        }

        .title {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 36pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 18pt;
            color: #666666;
            font-size: 24pt;
            padding-bottom: 4pt;
            font-family: "Georgia";
            line-height: 1.0;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 24pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 18pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 12pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 11pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 10pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

    </style>
</head>

<body class="c17">
    <p class="c1"><img alt="" src="<?php echo $emitente[0]->url_logo; ?>" style="width: 255.80px; height: 96.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></p>
    
    <p class="c0 c3"><span class="c2"></span></p>
    <p class="c3 c8"><span class="c16"></span></p>
    <p class="c8"><span class="c16"> <b> Termo de Convênio </b> </span></p>
    <p class="c8 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c19"> <b> Convênio nº. <?php echo $dados[0]->pempemcodig; ?> </b></span><span class="c4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c7 c3"><span class="c4"></span></p>
    
    <p class="c7"><span class="c4">Termo de Convênio em <?php echo  date('d'); ?> de <?php echo  $mes; ?> de <?php echo  date('Y'); ?>, que entre si celebram a </span><span class="c2"><b><?php echo $emitente[0]->nome; ?></b></span><span class="c4">, sociedade de direito privado, constituída legalmente e &nbsp;inscrita no CNPJ sob nº. </span><span class="c2"><b><?php echo $emitente[0]->cnpj; ?></b></span><span class="c4">&nbsp;- 00 com endereço na </span><span class="c2"><b><?php echo $emitente[0]->rua.','.$emitente[0]->numero; ?> – <?php echo $emitente[0]->bairro;  ?> - <?php echo $emitente[0]->cidade; ?></b></span><span class="c4">&nbsp; / <?php echo $emitente[0]->uf; ?>, CEP. <?php echo $emitente[0]->cep; ?>,</span><span class="c22">&nbsp;</span><span class="c4">&nbsp;neste ato representado por, </span><span class="c2"><b><?php echo $emitente[0]->representada ?></b></span><span class="c4">, brasileira, casada, inscrito no RG sob nº. </span><span class="c2"><b>1.421.550-SSP-PE e CPF/MF sob nº. 387.013.834-34</b></span><span class="c4">; cargo de Gestora da Central de Estágios, doravante denominada interveniente; </span><span class="c2"><b>AGENTE DE INTEGRAÇÃO</b></span><span class="c4">, e por outro lado a Empresa - <?php echo $dados[0]->sempemfanta; ?>. com CNPJ nº .<?php echo $dados[0]->sempemcnpj; ?>, representada legalmente pelo(a) <?php echo $dados[0]->sempemrepre; ?>, &nbsp;Registro nº &nbsp;________– SDS -PE e C.P.F. nº ________________ com o cargo de <?php echo $dados[0]->sempemrecar; ?>; &nbsp;localizada na &nbsp;<?php echo $dados[0]->sempemlogra.','.$dados[0]->sempemnumer; ?> - &nbsp;Bairro: <?php echo $dados[0]->sempembairr; ?> , cidade – <?php echo $dados[0]->sempemcidad; ?> – <?php echo $dados[0]->sempemuf; ?>; que cumprirão as Cláusulas e condições, conforme Programa de Estágio Supervisionado.</span></p>
    <p class="c7 c3"><span class="c4"></span></p>
    <p class="c7 c3 c21"><span class="c4"></span></p>
    <p class="c7 c3 c13"><span class="c2"></span></p>
    <p class="c7 c13"><span class="c2"><b>Cláusula primeira – </b></span><span class="c4">Este convênio tem a finalidade de proporcionar ao estudante a realização de estágio, oferecido pela unidade concedente, com intervenção da instituição de ensino em conseguir seus objetivos educacionais. Visa ainda estabelecer e manter parceria entre as partes, no sentido de promover a integração do estudante ao mercado de trabalho, através do estágio curricular, obrigatório ou não obrigatório, permitindo o pleno cumprimento da nova Lei de nº. 11.788 de 25/09/2008, relacionada às atividades práticas compatíveis a formação do aluno, sendo uma forma de complementação educacional e profissionalização, devendo ser planejado, executado, acompanhado e avaliado conforme as exigências da Legislação em vigor e normas escolares; impossibilitando qualquer vínculo de caráter empregatício.</span><span class="c2">&nbsp; &nbsp;</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c7 c3"><span class="c4"></span></p>
    <p class="c7 c13"><span class="c2"><b>Cláusula segunda – Obrigações do Agente de Integração</span><span class="c4">:</b> </span><span class="c2"><b><?php echo $emitente[0]->nome ?></b></span></p>
    <p class="c6"><span class="c4">Conforme o Art. 5o&nbsp;da Lei nº. 11.788/2008; as Instituições de Ensino e as Unidades &nbsp;Concedentes de estágio podem, a seu critério, recorrer a serviços de agentes de integração públicos e privados, mediante condições acordadas em instrumento jurídico apropriado.&nbsp;</span></p>
    <ol class="c14 lst-kix_list_1-0 start" start="1">
        <li class="c5 li-bullet-0"><span class="c4">1.  Atuar como agente mediador na operacionalização do Programa de Estágio, promovendo a interação das Instituições de Ensino junto às pessoas jurídicas, de direito público e privado denominados Unidades Concedentes, para os procedimentos de caráter legal, técnico e administrativo, de acordo com o processo de estágio conforme o que preceitua o Artigo 5º da Lei nº.11.788/2008</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">2.  Obter dados da Instituição de Ensino sobre as condições e requisitos mínimos necessários à realização dos estágios de forma a repassar tais informações as Unidades Concedentes.</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">3.  Conforme condições definidas pela Instituição de Ensino, realizar os ajustes de estágio, com as disponibilidades das Unidades Concedente, analisadas através das fichas de oferta, onde constarão todos os dados inerentes ao estágio, observando sua compatibilidade com o Contexto Básico da Profissão a que seu curso se refere.</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">4.  Averiguar nas ofertas de estágios o pronto cumprimento do Artigo 9º Item III da Lei nº. 11.788 quanto à formação do orientador da Unidade Concedente adequada ao curso do estagiário. &nbsp; &nbsp; </span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">5.  Cadastrar, recrutar, e encaminhar os estudantes que atendem o perfil das ofertas de estágios oferecidas pelas Unidades Concedentes.</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">6.  Adotar as providências cabíveis quanto à assinatura do instrumento jurídico, o Acordo de Cooperação e Termo de Compromisso de Estágio, devidamente assinados pelo estudante, com a interveniência obrigatória da Instituição de Ensino, Unidade Concedente, e a mediadora Sempre Humana; conforme parágrafo 1º do Artigo 5º da Lei nº. 11.788/2008. </span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">7.  Estabelecer um Seguro Contra Acidentes Pessoais para o estagiário em vigência de contrato, administrando mensalmente a devida apólice, acatando o respectivo custo.</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">8.  Adquirir contínuas informações inerentes à regularidade da matrícula do estudante, ativo, na Instituição de Ensino.</span></li>
        <li class="c5 li-bullet-0" style="margin-top: 5px;"><span class="c4">9.  Acompanhar as atividades de estágio, através de relatório periódico administrativo, preenchido pelo estagiário, e pelo orientador local de estágio; proporcionando a Instituição de Ensino respostas de interesse curricular.</span></li>
    </ol>   
    <p class="c7 c3"><span class="c4"></span></p>
    <p class="c7"><span class="c19"><b>Cláusula Terceira – Obrigações da Unidade Concedente de Estágio.</b></span></p>
    <p class="c3 c7"><span class="c4"></span></p>
    <ol class="c14 lst-kix_list_2-0 start" start="1">
        <li class="c5 li-bullet-0"><span class="c4">1.  Proporcionar oportunidades de estágios, oferecendo instalações adequadas; sedimentando na prática, a teoria adquirida na Escola; combinando em conjunto com a Sempre Humana suas condições oferecidas com as exigências das Instituições de Ensino.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">2.  Manter o Agente de Integração sempre informado dos estudantes em processo seletivo e eventuais mudanças como suspensão, cancelamento ou conclusão da fase seletiva.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">3.  Assinar o Acordo de Cooperação - Termo de Compromisso de Estágio, zelando pelo seu cumprimento;</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">4.  Acompanhar, avaliar e orientar tecnicamente o desempenho do estagiário através da indicação de um funcionário com formação ou experiência profissional na área de conhecimento desenvolvida no curso do estagiário.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">5.  Participar do trabalho de supervisão administrativa por parte do Agente de Integração, e facilitar a supervisão didática ocasionada pela Instituição de Ensino.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">6.  Reduzir a carga horária de estágio pelo menos a metade, na época de avaliação escolar.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">7.  Propiciar férias remuneradas compatíveis ao tempo de permanência na Unidade Concedente. </span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">8.  Avaliar de forma global o estagiário e em sua fase de desligamento, entregar termo de realização do estágio com indicação resumida das atividades desenvolvidas dos períodos e da avaliação de desempenho para o estudante e a Instituição de Ensino, no cumprimento das exigências de caráter curricular.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">9.  Efetuar o pagamento da Bolsa de Complementação Escolar e Vale Transporte de seus estagiários ativos, mantendo o Agente de Integração bem informado de qualquer alteração para as necessárias providências.</span></li>
        <li class="c5 li-bullet-0"style="margin-top: 5px;"><span class="c4">10.  Informar ao Agente de Integração as eventuais rescisões ou renovações do Termo de Compromisso, para serem tomadas as medidas cabíveis;</span></li>
    </ol>
    <p class="c7 c3 c10"><span class="c4"></span></p>
    <p class="c7 c3 c10"><span class="c4"></span></p>
    <p class="c7 c3 c10"><span class="c4"></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c13"><span class="c2"><b>Cláusula Quarta – Acordo de Contribuição</b></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c19"><b>Parágrafo único:</b> </span><span class="c4">A Unidade Concedente a estágio, participará de uma contribuição no valor de R$ <?php echo $dados[0]->sempemvalor.'  ('.extenso($dados[0]->sempemvalor) .')' ?> (sessenta reais ) à Sempre Humana por estudante/mês, que estiverem em treinamento como estagiário, para fins de custeio das despesas administrativas.</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c13"><span class="c2"><b>Cláusula Quinta – Quanto aos Compromissos</b></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c19"><b>Parágrafo único:</b> </span><span class="c4">A Unidade Concedente a estágio deverá assumir na íntegra, seus compromissos nos termos deste convênio, na falta do cumprimento, implicarão na suspensão do estágio, sujeitando a Empresa à Legislação Trabalhista e Previdência vigente. Este convênio faz ainda atender as &nbsp;exigências de Decreto n.º 914 de 06/09/93 referente aos Portadores de Deficiência Física.</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c13"><span class="c2"><b>Cláusula Sexta – Validade do Convênio</b></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c19"><b>Parágrafo único:</b></span><span class="c4">&nbsp;O presente convênio entrará em vigor a partir da data de sua assinatura e devolução; terá vigência por prazo indeterminado. Qualquer rasura que este documento apresentar, perderá seu valor jurídico. </span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c13"><span class="c2"><b>Cláusula Sétima – Foro</b></span></p>
    <p class="c0"><span class="c4">Em comum acordo das partes convenentes, serão resolvidos os casos omissos deste Convênio; e elegem desde já, o Foro da Comarca de Recife para decidir qualquer questão que se derivar deste Convênio e que não seja solucionada amigavelmente.</span></p>
    <p class="c0"><span class="c4">E por estarem acordes, emite-se o presente Convênio em 02(duas) vias de igual teor,os representantes legais e testemunhas assinam depois de lido e na concordância deste.</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Recife, <?php echo  date('d'); ?> de <?php echo  $mes; ?> de <?php echo  date('Y'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;TESTEMUNHA:</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________________________</span></p>
    <p class="c0"><span class="c4">_______________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome:</span></p>
    <p class="c0"><span class="c4">(Ass. Resp. Legal Empresa e carimbo)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RG: ______________________________</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c0"><span class="c4">_______________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome:</span></p>
    <p class="c0"><span class="c4">(Ass. Resp. Legal Empresa e carimbo)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RG: ______________________________</span></p>
    <p class="c0"><span class="c4">&nbsp; Sempre Humana</span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0 c3"><span class="c4"></span></p>
    <p class="c0"><span class="c4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <div>
        <p class="c0 c3"><span class="c23"></span></p>
    </div>
</body>

</html>