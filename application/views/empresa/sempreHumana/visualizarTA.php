<html>

<head>
    <title>TERMO DE COMPROMISSO DE ESTÁGIO</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        @import url('https://themes.googleusercontent.com/fonts/css?kit=cwkuN-YbXleWjD6k_pVPXg');

        ol.lst-kix_list_1-3 {
            list-style-type: none
        }


        .c31 {
            margin-left: -56.7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        ol.lst-kix_list_1-4 {
            list-style-type: none
        }

        ol.lst-kix_list_1-5 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6 {
            list-style-type: none
        }

        ol.lst-kix_list_1-0 {
            list-style-type: none
        }

        .lst-kix_list_1-4>li {
            counter-increment: lst-ctn-kix_list_1-4
        }

        ol.lst-kix_list_1-1 {
            list-style-type: none
        }

        ol.lst-kix_list_1-2 {
            list-style-type: none
        }

        ol.lst-kix_list_1-6.start {
            counter-reset: lst-ctn-kix_list_1-6 0
        }

        .lst-kix_list_1-1>li {
            counter-increment: lst-ctn-kix_list_1-1
        }

        ol.lst-kix_list_1-3.start {
            counter-reset: lst-ctn-kix_list_1-3 0
        }

        ol.lst-kix_list_1-2.start {
            counter-reset: lst-ctn-kix_list_1-2 0
        }

        ol.lst-kix_list_1-8.start {
            counter-reset: lst-ctn-kix_list_1-8 0
        }

        .lst-kix_list_1-0>li:before {
            content: ""counter(lst-ctn-kix_list_1-0, lower-latin) ") "
        }

        ol.lst-kix_list_1-5.start {
            counter-reset: lst-ctn-kix_list_1-5 0
        }

        ol.lst-kix_list_1-7 {
            list-style-type: none
        }

        .lst-kix_list_1-1>li:before {
            content: ""counter(lst-ctn-kix_list_1-1, lower-latin) ". "
        }

        .lst-kix_list_1-2>li:before {
            content: ""counter(lst-ctn-kix_list_1-2, lower-roman) ". "
        }

        .lst-kix_list_1-7>li {
            counter-increment: lst-ctn-kix_list_1-7
        }

        ol.lst-kix_list_1-8 {
            list-style-type: none
        }

        .lst-kix_list_1-3>li:before {
            content: ""counter(lst-ctn-kix_list_1-3, decimal) ". "
        }

        .lst-kix_list_1-4>li:before {
            content: ""counter(lst-ctn-kix_list_1-4, lower-latin) ". "
        }

        ol.lst-kix_list_1-0.start {
            counter-reset: lst-ctn-kix_list_1-0 0
        }

        .lst-kix_list_1-0>li {
            counter-increment: lst-ctn-kix_list_1-0
        }

        .lst-kix_list_1-6>li {
            counter-increment: lst-ctn-kix_list_1-6
        }

        .lst-kix_list_1-7>li:before {
            content: ""counter(lst-ctn-kix_list_1-7, lower-latin) ". "
        }

        .lst-kix_list_1-3>li {
            counter-increment: lst-ctn-kix_list_1-3
        }

        .lst-kix_list_1-5>li:before {
            content: ""counter(lst-ctn-kix_list_1-5, lower-roman) ". "
        }

        .lst-kix_list_1-6>li:before {
            content: ""counter(lst-ctn-kix_list_1-6, decimal) ". "
        }

        li.li-bullet-0:before {
            margin-left: -7.1pt;
            white-space: nowrap;
            display: inline-block;
            min-width: 7.1pt
        }

        ol.lst-kix_list_1-7.start {
            counter-reset: lst-ctn-kix_list_1-7 0
        }

        .lst-kix_list_1-2>li {
            counter-increment: lst-ctn-kix_list_1-2
        }

        .lst-kix_list_1-5>li {
            counter-increment: lst-ctn-kix_list_1-5
        }

        .lst-kix_list_1-8>li {
            counter-increment: lst-ctn-kix_list_1-8
        }

        ol.lst-kix_list_1-4.start {
            counter-reset: lst-ctn-kix_list_1-4 0
        }

        .lst-kix_list_1-8>li:before {
            content: ""counter(lst-ctn-kix_list_1-8, lower-roman) ". "
        }

        ol.lst-kix_list_1-1.start {
            counter-reset: lst-ctn-kix_list_1-1 0
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c7 {
            -webkit-text-decoration-skip: none;
            color: #000000;
            font-weight: 400;
            text-decoration: underline;
            vertical-align: baseline;
            text-decoration-skip-ink: none;
            font-size: 8pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c28 {
            margin-left: 27pt;
            padding-top: 0pt;
            text-indent: -27pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c27 {
            margin-left: 10.9pt;
            padding-top: 0pt;
            padding-left: -10.9pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c15 {
            margin-left: 14.2pt;
            padding-top: 0pt;
            text-indent: -27pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c23 {
            margin-left: -12;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: justify
        }

        .c0 {
            margin-left: 7.1pt;
            padding-top: 0pt;
            text-indent: -14.2pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c18 {
            margin-left: -11.1pt;
            padding-top: 0pt;
            text-indent: -12.9pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c10 {
            margin-left: 7.1pt;
            padding-top: 0pt;
            text-indent: -19.1pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c2 {
            margin-left: -9pt;
            padding-top: 0pt;
            text-indent: -9pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c17 {
            margin-left: -9pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c6 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c14 {
            padding-top: 0pt;
            text-indent: -14.2pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c32 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 4pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c1 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c3 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 8pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c4 {
            margin-left: -9pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .cyxyy {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            font-size: 45pt;
            font-family: "Corsiva";
            font-style: normal
        }

        .c21 {
            margin-left: -9pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: justify
        }

        .c24 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c31 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c29 {
            color: #ff0000;
            font-weight: 400;
            text-decoration: none;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c33 {
            vertical-align: baseline;
            font-size: 48pt;
            font-family: "Corsiva";
            font-weight: 400
        }

        .c9 {
            vertical-align: baseline;
            font-size: 8pt;
            font-weight: 700
        }

        .c11 {
            -webkit-text-decoration-skip: none;
            text-decoration: underline;
            text-decoration-skip-ink: none
        }

        .c8 {
            background-color: #ffffff;
            max-width: 537pt;
            padding: 18pt 28.3pt 9pt 30pt
        }

        .c25 {
            list-style-position: inside;
            text-indent: 45pt
        }

        .c22 {
            vertical-align: baseline;
            font-size: 9pt
        }

        .c19 {
            padding: 0;
            margin: 0
        }

        .c12 {
            vertical-align: baseline;
            font-size: 10pt
        }

        .c30 {
            margin-left: -11.1pt;
            text-indent: -1.8pt
        }

        .c13 {
            vertical-align: baseline;
            font-size: 8pt
        }

        .c26 {
            margin-left: -24pt;
            text-indent: 2.1pt
        }

        .c34 {
            margin-left: -7.1pt
        }

        .c5 {
            height: 12pt
        }

        .c20 {
            font-weight: 700
        }

        .c16 {
            text-indent: -16.1pt
        }

        .title {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 36pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 18pt;
            color: #666666;
            font-size: 24pt;
            padding-bottom: 4pt;
            font-family: "Georgia";
            line-height: 1.0;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 12pt;
            font-family: "Times New Roman"
        }

        h1 {
            padding-top: 24pt;
            color: #000000;
            font-weight: 700;
            font-size: 24pt;
            padding-bottom: 6pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-weight: 700;
            font-size: 18pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 14pt;
            color: #000000;
            font-weight: 700;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 12pt;
            color: #000000;
            font-weight: 700;
            font-size: 12pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 11pt;
            color: #000000;
            font-weight: 700;
            font-size: 11pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 10pt;
            color: #000000;
            font-weight: 700;
            font-size: 10pt;
            padding-bottom: 2pt;
            font-family: "Times New Roman";
            line-height: 1.0;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c8">

    <p class="c31"><img alt="" src="<?php echo $emitente[0]->url_logo; ?>" style="width: 131.80px; height: 69.40px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""><i><span class="c14 cyxyy">Sempre Humana</i></span></p>
    <p class="c22"><span class="c4">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c14 c41">DESENVOLVIMENTO HUMANO E TECNOLÓGICO. </span></p>
    <p class="c4 c5"><span class="c22"><br></span></p>
    <p class="c4"><span class="c29 c22">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; </span></p>
    <p class="c17"><span class="c22 c20"> <b>TERMO ADITIVO </b></span></p>
    <p class="c31"><span class="c3">Instrumentos jurídicos de que tratam o art. 5º &nbsp;do ditames da Lei &nbsp; Federal nº. 11.788./ 2008.</span></p>
    <p class="c17 c5"><span class="c1"></span></p>
    <p class="c4"><span class="c3">Recife, ____ &nbsp;de _________ &nbsp;de 20____. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c24 c34"><span class="c13">Celebram entre as partes, ______________________</span><span class="c9">e a <b> <?php echo strtoupper($emitente[0]->nome) ?> </b></span><span class="c3">, com a interveniência da Instituição de Ensino, conforme cláusulas do Termo de Compromisso citadas e assinadas em __ /__/ 20____, e último Termo Aditivo assinado em __/__/20___,</span></p>
    <p class="c24 c34"><span class="c3">de acordo com a legislação 11788/ de 25 de setembro de 2008 </span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c4" style="margin-top: 2px;"><span class="c9 c11"> <b> INSTITUIÇÃO DE ENSINO </b> </span></p>
    <p class="c4 c5"><span class="c7"></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Instituição de Ensino: <?php echo strtoupper($dados[0]->sensennome) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Endereço: <?php echo mb_strtoupper($dados[0]->sensenlogra) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bairro: <?php echo mb_strtoupper($dados[0]->sensenbairr) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cidade: <?php echo strtoupper($dados[0]->sensencidad) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; UF: <?php echo strtoupper($dados[0]->sensenuf) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; CNPJ: <?php echo $dados[0]->sempemcnpj ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Representante legal: <?php echo mb_strtoupper($dados[0]->sensenrepre) ?></span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c4" style="margin-top: 2px;"><span class="c9 c11"> <b> UNIDADE CONCEDENTE DE ESTÁGIO </b></span></p>
    <p class="c4 c5"><span class="c7"></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Razão Social: <?php echo strtoupper($dados[0]->sempemrazao) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CNPJ: <?php echo $dados[0]->sempemcnpj; ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Fone:<?php echo $dados[0]->sempemtel01; ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Endereço: <?php echo strtoupper($dados[0]->sempemlogra).','.strtoupper($dados[0]->sempemnumer).' '.strtoupper($dados[0]->sempemcompl) ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bairro : <?php echo $dados[0]->sempembairr; ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cidade: <?php echo $dados[0]->sempemcidad; ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CEP: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;UF: <?php echo strtoupper($dados[0]->sempemuf) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Representante legal: <?php echo strtoupper($dados[0]->sempemrespo) ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Rg. nº. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CPF nº.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Cargo: <?php echo strtoupper($dados[0]->sempemcargo) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Ramo de atividade: </span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c4" style="margin-top: 2px;"><span class="c9 c11"> <b> AGENTE DE INTEGRAÇÃO - ASSESPRO </b></span></p>
    <p class="c4 c5"><span class="c7"></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Nome: <?php echo strtoupper($emitente[0]->nome) ?>  &nbsp;</span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Endereço: <?php echo mb_strtoupper($emitente[0]->rua).' '.strtoupper($emitente[0]->numero) ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bairro: <?php echo strtoupper($emitente[0]->bairro) ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cidade: <?php echo strtoupper($emitente[0]->cidade) ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; UF: <?php echo strtoupper($emitente[0]->uf) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; CEP: <?php echo strtoupper($emitente[0]->cep) ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fone: <?php echo strtoupper($emitente[0]->telefone) ?></span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; CNPJ: <?php echo strtoupper($emitente[0]->cnpj) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c24 c26"><span class="c13">&nbsp; &nbsp; &nbsp; Representada por: Ana Elizabeth Carvalho Pereira &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cargo: Diretora</span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c21" style="margin-top: 2px;"><span class="c9"> <b> Clausula 1ª </b></span></p>
    <p class="c2 c26"><span class="c13">&nbsp; &nbsp; &nbsp;&nbsp;A Cláusula 2ª do Termo de Compromisso celebrado no dia __/ __ /20__, e ultimo Termo Aditivo assinado em ___/___/___, terão alterações &nbsp;sobre o seguinte dado: </span></p>
    <p class="c2 c26"><span class="c13">&nbsp; &nbsp; &nbsp;&nbsp;a Unidade Concedente a estágio renovará o contrato de estágio de caráter</span><span class="c9">&nbsp;____obrigatório</span><span class="c3">&nbsp;por mais __ meses, reiniciando as &nbsp;atividades em &nbsp;__/__/20__ </span> </p>
    <p class="c2 c26"><span class="c13">&nbsp; &nbsp; &nbsp;&nbsp;e finalizando em __/ __/ 20__, referente ao aluno,_____________________ do Curso de __________, &nbsp;desta Instituição de Ensino, matrícula nº. ___________; que terá  </span></p>
    <p class="c2 c26"><span class="c13">&nbsp; &nbsp; &nbsp;&nbsp;vigência a partir de ___/ __ /20__ &nbsp;de acordo com a legislação vigente do Programa de Estágio Supervisionado.</span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c21" style="margin-top: 2px;"><span class="c9"> <b> Cláusula 2ª – DA ASSINATURA ELETRÔNICA</b></span></p>
    <p class="c23" ><span class="c13">Em se tratando desta alternativa as partes envolvidas reconhecem a autenticidade, integridade, validade e eficácia do presente Contrato e seus termos, conforme disposto no art. 219 do Código Civil, bem como das assinaturas apostas, em formato físico ou eletrônico, devendo, no caso de utilização de assinatura eletrônica, que esta seja realizada através de qualquer plataforma reconhecida pela ICP-Brasil, a teor do disposto no § 2º, do art. 10, da Medida Provisória nº 2.200-2, de 24 de agosto de 2001, podendo, </span><span class="c29 c13">ainda ser formalizada através da plataforma D4Sign ou por qualquer outra, desde que reconhecida e validade pelas Partes.</span></p>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c21" style="margin-top: 2px;"><span class="c9"> <b>Cláusula 3ª - DO USO DE DADOS </b></span></p>
    <ol class="c19 lst-kix_list_1-0 start" start="1">
        <li class="c27 li-bullet-0" style="margin-left: 2;"><span class="c3">a) É vedado divulgar e compartilhar com terceiros quaisquer dados recebidos e contidos neste presente documento, salvo Termo de Consentimento prévio por escrito tenha sido obtido pelas partes envolvidas, usando-o estritamente mediante objetivo proposto;</span></li>
        <li class="c27 li-bullet-0" style="margin-left: 2;"><span class="c3">b) Não terceirizar/subcontratar o processamento dos dados pessoais recebidos, nem transferir o processamento ou tratamento para qualquer outra empresa ou terceiro, &nbsp; &nbsp; &nbsp; inclusive no exterior, sem o termo de consentimento prévio das partes envolvidas, cujos dados estão sendo transmitidos para terceiro;</span></li>
        <li class="c27 li-bullet-0" style="margin-left: 2;"><span class="c3">c) Impossibilitar modificação de qualquer finalidade ou propósito para o qual foi autorizado uso e/ou processamento de dados contratuais, assim como &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;não combinar dados de diferentes indivíduos;</span></li>
        <li class="c27 li-bullet-0" style="margin-left: 2;"><span class="c3">d) Responsabilizar-se integralmente, pelo descumprimento de qualquer condição legal ou contratual com relação a tratamento de dados, sendo certo que na hipótese de violação, poderá a PARTE adimplente rescindir o presente instrumento.</span></li>
        <li class="c27 li-bullet-0" style="margin-left: 2;"><span class="c3">3) permitir a retificação de dados e informações contratuais na forma da lei. </span></li>
    </ol>
    <p class="c4"><span class="c3">__________________________________________________________________________________________________________________________________________</span></p>
    <p class="c21" style="margin-top: 2px;"><span class="c9"> <b>Cláusula 2ª </b> </span></p>
    <p class="c4"><span class="c3">Continuam inalteradas todas as demais cláusulas do Termo de Compromisso, do qual este documento, o segundo Termo Aditivo, passa a valer após sua assinatura, com vigência até &nbsp;30/ 09 / 2021.</span></p>
    <p class="c4"><span class="c3">Assim todas as partes estarem justas e concordes, na presença de testemunhas, assinam o presente Termo Aditivo em 4 (quatro) vias de igual teor. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c4"><span class="c3">Recife, ____ &nbsp;de _________ &nbsp; de &nbsp;20__.</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c4"><span class="c3">Instituição de Ensino &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Unidade Concedente &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c4"><span class="c3">____________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; __________________________</span></p>
    <p class="c4"><span class="c3">Ass. e carimbo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; Ass. e carimbo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c4"><span class="c3">Estagiário &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Agente de Integração</span></p>
    <p class="c4"><span class="c3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></p>
    <p class="c4"><span class="c3">____________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ____________________________</span></p>
    <p class="c4"><span class="c3">Assinatura&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; Ass. e carimbo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
    <p class="c4 c5"><span class="c3"></span></p>
    <p class="c5 c24"><span class="c3"></span></p>
</body>

</html>