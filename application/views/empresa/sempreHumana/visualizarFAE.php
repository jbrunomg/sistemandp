<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Cabeçalho Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.CabealhoChar
	{mso-style-name:"Cabeçalho Char";
	mso-style-link:Cabeçalho;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:27.0pt 19.3pt 0cm 36.0pt;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=PT-BR>

<div class=WordSection1>

<p class=MsoHeader align=center style='text-align:center'><img width=590
height=124 src="<?php echo $emitente[0]->url_logo; ?>"  style="width: 131.80px; height: 69.40px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" align=left hspace=12><span style='font-size:9.0pt'></span></p>

<p class=MsoNormal style='line-height:115%'><b>                                                                                                       </b></p>

<p class=MsoNormal align=center style='text-align:center;line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal align=center style='text-align:center;line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal align=center style='text-align:center;line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal align=center style='text-align:center;line-height:115%'><b>FICHA
DE ACOMPANHAMENTO DAS ATIVIDADES PRÁTICAS CURRICULARES</b></p>

<p class=MsoNormal style='line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal style='line-height:115%'><b>Identificação do estagiário </b></p>

<p class=MsoNormal style='line-height:115%'>Empresa <?php echo $dados[0]->sempemrazao; ?></p>

<p class=MsoNormal style='line-height:115%'>Estagiário: <?php echo $dados[0]->salualnome; ?>
Curso: <?php echo $dados[0]->scurcunome; ?></p>

<p class=MsoNormal style='line-height:115%'>Inst. Ensino <?php echo $dados[0]->sensennome; ?><br>
Tipo de Estágio: (    ) obrigatório (    ) não obrigatório   </p>

<p class=MsoNormal style='line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal style='line-height:115%'><b>Orientador, </b></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><b>Sabendo que o
estágio é uma oportunidade que o estudante tem em desenvolver paralelamente a
teoria/prática, gostaríamos de sensibilizar a importância desta avaliação de
desempenho de forma a contribuir com o programa de estágio supervisionado,
possibilitando junto ao estagiário uma reflexão sobre seu aproveitamento dos
conhecimentos oferecidos pela Empresa, proporcionando um melhor preparo ao
futuro profissional.</b></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal style='line-height:115%'><b>Quais as principais atividades
desenvolvidas pelo estagiário na Empresa?</b></p>

<p class=MsoNormal style='line-height:115%'>__________________________________________________________________________________________________________________________________________________________________________________</p>

<p class=MsoNormal style='line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal style='line-height:115%'><b>Quais as dificuldades observadas
no decorrer do estágio?  </b></p>

<p class=MsoNormal style='line-height:115%'><b>__________________________________________________________________________________________________________________________________________________________________________________</b></p>

<p class=MsoNormal style='margin-left:18.0pt;text-indent:-18.0pt;line-height:
115%'><b>      </b></p>

<p class=MsoNormal style='line-height:115%'><b>Que sugestão você daria ao estagiário de modo a contribuir para
uma boa formação profissional?</b></p>

<p class=MsoNormal style='line-height:115%'><b>_________________________________________________________________________________________
</b></p>

<p class=MsoNormal style='line-height:115%'><b>_________________________________________________________________________________________</b></p>

<p class=MsoNormal style='line-height:115%'><b> </b></p>

<p class=MsoNormal style='line-height:115%'><b>Quais as contribuições
oferecidas pela empresa ao estagiário no investimento da formação profissional?  
</b></p>

<p class=MsoNormal style='line-height:115%'><b>_________________________________________________________________________________________
</b></p>

<p class=MsoNormal style='line-height:115%'><b>_________________________________________________________________________________________</b></p>

<p class=MsoNormal style='line-height:115%'><b> </b></p>

<p class=MsoNormal style='line-height:115%'><b>Como você avalia o
estagiário.  Utilize os conceitos: Fraco (F ) Regular (R ) Bom (B ) Ótimo (O ) </b></p>

<p class=MsoNormal style='line-height:115%'><b> -   Conhecimentos teóricos que
possibilitam a realização das atividades com qualidade (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Facilidade em assimilar e
utilizar novas informações (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Interesse em aprender e de se
empenhar nas atividades (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Iniciativa: saber como agir e
dar soluções nas demais situações (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Pontualidade: cumprir horário
e prazo na conclusão das atividades estabelecidas (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Assiduidade: comparecer
regularmente a empresa (    )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Apresentação pessoal: ter uma
apresentação pessoal compatível ao ambiente de trabalho (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Relacionamento: facilidade de
se integrar com a equipe profissional (     )</b></p>

<p class=MsoNormal style='line-height:115%'><b> – Disciplina: respeitar as
normas da empresa (     )</b></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:-14.25pt;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
115%'><b>          </b></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:-14.25pt;margin-bottom:
0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:
115%'><b>         Sugestões / Comentários adicionais:</b></p>

<p class=MsoNormal style='line-height:115%'> _________________________________________________________________________________________</p>

<p class=MsoNormal style='line-height:115%'><b>&nbsp;</b></p>

<p class=MsoNormal style='line-height:115%'><b>Data: ______/______/______.                             
Orientador(a) _____________________________________</b></p>

</div>

</body>

</html>
