<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<title>RESCISÃO DE TERMO DE COMPROMISSO DE ESTÁGIO</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Monotype Corsiva";
	panose-1:3 1 1 1 1 2 1 1 1 1;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.Estilo1, li.Estilo1, div.Estilo1
	{mso-style-name:Estilo1;
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Calibri","sans-serif";
	color:#333399;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:21.3pt 35.35pt 35.45pt 3.0cm;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=PT-BR>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

<p class=MsoNormal><span style='position:relative;z-index:251657728'><span
style='position:absolute;left:23px;top:-11px;width:139px;height:83px'><img
width=139 height=83 src="<?php echo $emitente[0]->url_logo; ?>"></span></span><b><span
style='font-size:30.0pt;font-family:"Monotype Corsiva"'>           <a
name="_Hlk83824615">Sempre Humana </a></span></b><b><span style='font-size:
8.0pt'></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><span
style='font-size:16.0pt;font-family:"Verdana","sans-serif"'>                 </span><b>DESENVOLVIMENTO
HUMANO E TECNOLÓGICO</b></p>

<p class=MsoNormal align=center style='text-align:center'><b>            </b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-family:"Arial","sans-serif"'>RESCISÃO  DE TERMO DE COMPROMISSO DE
ESTÁGIO</span></b></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>Recife,
___  de _____ de 20__.  </span></p>

<p class=MsoNormal style='text-align:justify;text-indent:35.4pt;line-height:
150%'><span style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>Comunicamos
que em ___ / _____ / 20___ fica rescindido o Termo de Compromisso de Estágio de
nº _____, iniciado na data de __/_____ / 202__, entre a empresa,_________________,
e o estagiário______________, realizando suas atividades no setor de _______________________________,
sob a orientação do supervisor,______________________ regularmente matriculado
no curso de _____________ com a <b>matrícula de nº.___________</b></span><span
style='font-size:8.0pt;line-height:150%'> </span><span style='font-size:11.0pt;
line-height:150%;font-family:"Arial","sans-serif"'>   com interveniência da IES,
________________________ .</span></p>

<p class=MsoNormal style='text-align:justify;text-indent:35.4pt;line-height:
150%'><span style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>Informamos
que o referido estágio foi concluído na supracitada data pelo seguinte motivo:</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>(   )
O estagiário foi efetivado</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>(  
) Por iniciativa da empresa</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>(  
) Situação irregular de matrícula do estudante</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>(   )
Por iniciativa do estudante</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>(   )
Carga horária concluída na realização do estágio.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-indent:35.4pt;line-height:
150%'><span style='font-size:11.0pt;line-height:150%;font-family:"Arial","sans-serif"'>E
por estarem de inteiro e comum acordo com as condições e dizeres deste
cancelamento, as partes assinam-na em 03 vias de igual teor, cabendo a 1ª a
Unidade Concedente, a 2ª ao (à) estagiário(a) e a 3ª a Instituição de Ensino. </span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-family:"Arial","sans-serif"'>  
   </span></p>

<p class=MsoNormal style='text-align:justify;text-indent:24.0pt'><span
style='font-family:"Arial","sans-serif"'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><b><span style='font-size:10.0pt;
font-family:"Arial","sans-serif"'>            INSTITUIÇÃO DE ENSINO                                     
UNIDADE CONCEDENTE</span></b></p>

<p class=MsoNormal style='text-align:justify'><b><span style='font-size:10.0pt;
font-family:"Arial","sans-serif"'>             (ciência e assinatura)                                               
(ciência e assinatura)</span></b></p>

<p class=MsoNormal style='text-align:justify'><b><span style='font-size:10.0pt;
font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:252.0pt;text-align:justify;text-indent:
86.05pt'>&nbsp;</p>

<p class=MsoNormal style='margin-left:252.0pt;text-align:justify;text-indent:
86.05pt'><span style='font-family:"Arial","sans-serif"'>                           
</span></p>

<p class=MsoNormal style='margin-left:30.2pt;text-align:justify;text-indent:
-70.2pt'><b><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>ESTAGIÁRIO(A)                              
            AGENTE DE INTEGRAÇÃO- SEMPRE HUMANA  </span></b></p>

<p class=MsoNormal style='margin-left:106.2pt;text-align:justify;text-indent:
-70.2pt'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif"'>  </span></b></p>

</div>

</body>

</html>
