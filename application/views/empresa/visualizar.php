<?php 

$mes = date('m'); // Mês desejado, pode ser por ser obtido por POST, GET, etc.

$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0,0,0,$mes,'01',$ano)); // Mágica, plim!


$lastmes = date('m', strtotime('-1 months'));
$lastultimo_dia = date('t', mktime(0,0,0,$lastmes,'01',$ano)); // Mágica, plim!



?>
<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Empresa</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>vaga/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Empresa:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="pempemcodig" value="<?php echo $dados[0]->pempemcodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">CNPJ:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="sempemcnpj" id="sempemcnpj" value="<?php echo $dados[0]->sempemcnpj; ?>">
					<?php echo form_error('sempemcnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Empresa:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Empresa" name="sempemfanta" id="sempemfanta" value="<?php echo $dados[0]->sempemfanta; ?>">
					<?php echo form_error('sempemfanta'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Razão Social:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Razão" name="sempemrazao" id="sempemrazao" value="<?php echo $dados[0]->sempemrazao; ?>">
					<?php echo form_error('sempemrazao'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Mantededora:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" placeholder="Nome Mantededora" name="sempemmantededora" id="sempemmantededora" value="<?php echo $dados[0]->sempemmantededora; ?>">
					<?php echo form_error('sempemmantededora'); ?>
					</div>										
				</div>
				


				<legend class="text-bold">Dados Comerciais:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sempemlogra" id="sempemlogra" value="<?php echo $dados[0]->sempemlogra; ?>">
					<?php echo form_error('sempemlogra'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sempemnumer" id="sempemnumer" value="<?php echo $dados[0]->sempemnumer; ?>">
					<?php echo form_error('sempemnumer'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sempemcompl" id="sempemcompl" value="<?php echo $dados[0]->sempemcompl; ?>">
					<?php echo form_error('sempemcompl'); ?>
					</div>										
				</div>
<!--
				<div class="form-group">
                	<label class="control-label col-lg-2">UF:</label>
                	<div class="col-lg-5">
                        <select disabled  class="form-control" name="estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sempemuf)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select disabled class="form-control" name="cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('cidade'); ?>	
                    </div>				                            	                            
				</div> -->

				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="bairro" id="bairro" value="<?php echo $dados[0]->sempembairr;  ?>">
					<?php echo form_error('sempembairr'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sempemcidad" id="sempemcidad" value="<?php echo $dados[0]->sempemcidad; ?>">
					<?php echo form_error('sempemcidad'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Estado:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="sempemuf" id="sempemuf" value="<?php echo $dados[0]->sempemuf; ?>">
					<?php echo form_error('sempemuf'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->sempemcep; ?>">
					<?php echo form_error('sempemcep'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Principal:</label>
					<div class="col-lg-5">
						<input disabled type="tel" name="telefone" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="telefone" id="telefone" value="<?php echo $dados[0]->sempemtel01; ?>">
					<?php echo form_error('sempemtel01'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Secundario:</label>
					<div class="col-lg-5">
						<input disabled type="tel" name="telefone" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="telefone2" id="telefone2" value="<?php echo $dados[0]->sempemtel02; ?>">
					<?php echo form_error('sempemtel02'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Fax:</label>
					<div class="col-lg-5">
						<input disabled type="tel" name="telefone" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="telefone3" id="telefone3" value="<?php echo $dados[0]->sempemtel03; ?>">
					<?php echo form_error('sempemtel03'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Mala Direta:</label>
					<div class="col-lg-5">
						<input disabled type="email" name="email"  placeholder="seu@email.com" class="form-control" name="sempememail" id="sempememail" value="<?php echo $dados[0]->sempememail; ?>">
					<?php echo form_error('sempememail'); ?>
					</div>										 
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Responsável:</label>
					<div class="col-lg-5">
						<input disabled type="text" name="sempemrespo" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="sempemrespo" id="sempemrespo" value="<?php echo $dados[0]->sempemrespo; ?>">
					<?php echo form_error('sempemrespo'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cargo Responsável:</label>
					<div class="col-lg-5">
						<input disabled type="text" name="sempemcargo" class="form-control" placeholder="Cargo Responsável"  data-mask-selectonfocus="true" name="sempemcargo" id="sempemcargo" value="<?php echo $dados[0]->sempemcargo; ?>">
					<?php echo form_error('sempemcargo'); ?>
					</div>										
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Representante:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Representante"  data-mask-selectonfocus="true" name="representante" id="representante" value="<?php echo $dados[0]->sempemrepre; ?>">
					<?php echo form_error('sempemrepre'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Cargo do Representante:</label>
					<div class="col-lg-5">
						<input disabled type="text"  class="form-control" placeholder="Cargo do Representante"  data-mask-selectonfocus="true" name="sempemrecar" id="sempemrecar" value="<?php echo $dados[0]->sempemrecar; ?>">
					<?php echo form_error('sempemrecar'); ?>
					</div>										
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-2">CPF do Representante:</label>
					<div class="col-lg-5">
						<input disabled type="text"  class="form-control" placeholder="CPF do Representante"  data-mask="999.999.999-99" data-mask-selectonfocus="true" name="sempemrecpf" id="sempemrecpf" value="<?php echo $dados[0]->sempemrecpf; ?>">
					<?php echo form_error('sempemrecpf'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Ramo de Atividade:</label>
					<div class="col-lg-5">
						<input disabled type="text"  class="form-control" placeholder="Ramo de Atividade"  data-mask-selectonfocus="true" name="ramoatividade" id="ramoatividade" value="<?php echo $dados[0]->sempemativi; ?>">
					<?php echo form_error('sempemativi'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Financeiro:</legend>

				<div class="form-group">
		          <label class="control-label col-lg-2">Cód Sacado:</label>
		          <div class="col-lg-5">
		            <input disabled type="text" placeholder="Código do sacado" class="form-control"  name="sempemcodsacado" id="sempemcodsacado" value="<?php echo $dados[0]->sempemcodsacado; ?>">
		          <?php echo form_error('sempemcodsacado'); ?>
		          </div>                     
		        </div>

				<div class="form-group">
		          <label class="control-label col-lg-2">Vecimento do Contrato:</label>
		          <div class="col-lg-5">
		            <input  disabled type="number" maxlength="2" placeholder="Vecimento do contrato" class="form-control"  name="sempemvenccontrato" id="sempemvenccontrato" value="<?php echo $dados[0]->sempemvenccontrato; ?>">
		          <?php echo form_error('sempemvenccontrato'); ?>
		          </div>                     
		        </div>
				
				<div class="form-group">
		          <label class="control-label col-lg-2">Valor UND Contrato:</label>
		          <div class="col-lg-5">
		            <input  disabled type="text" placeholder="R$ und contrato" class="form-control"  name="sempemvalor" id="sempemvalor" value="<?php echo $dados[0]->sempemvalor; ?>">
		          <?php echo form_error('sempemvalor'); ?>
		          </div>                     
		        </div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Gestor:</label>
					<div class="col-lg-5">
						<input disabled type="email" name="email"  placeholder="seu@email.com" class="form-control" name="sempememailgestor" id="sempememailgestor" value="<?php echo $dados[0]->sempememailgestor; ?>">
					<?php echo form_error('sempememailgestor'); ?>
					</div>										 
				</div>
								
		
            </fieldset>  
		</form>
	</div>
</div>


<!-- Column selectors -->
<div id='semScroll' class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aAdministrativo')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/visualizardemonstrativo/<?php echo $dados[0]->pempemcodig; ?>">Visualizar Demonstrativo <i class="icon-printer position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>	
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th>Código</th>
				<th>Estudante</th>
				<th>Cpf</th>
				<th>Data Nascimento</th>
				<th>Inst. Ensino</th>

				<th>Inicial</th>
				<th>Termino</th>
				<th>Renovação</th>
				<th>Renovação II</th>
				<th>Renovação III</th>
				<th>Rescisão</th>
				<th>Situação</th>
				<th>Opções</th>

			</tr>
		</thead>
		<tbody>
			<div class="datatable-wrap">
			<?php foreach ($contrato as $valor) { 

				$renovacaoI   = $valor->contrestempr_data_renI;
				$renovacaoII  = $valor->contrestempr_data_renII;
				$renovacaoIII = $valor->contrestempr_data_renIII;
				$rescisao = $valor->contrestempr_data_rescisao;

				if (($valor->contrestempr_status == 'Ativo') or 
					(($valor->contrestempr_status == 'Rescindido') and (substr($rescisao,0,7) == $ano.'-'.$mes )) ) {
			

				?>
				
				<tr>
					
					<td><?php echo $valor->contrestempr_id; ?></td>
					<td><?php echo $valor->salualnome; ?></td>
					<td><?php echo $valor->salualcpf; ?></td>
					<td><?php echo date('d/m/Y', strtotime($valor->talualniver)); ?></td>	<!--Calcular data aniversário -->
					<td><?php echo $valor->sensennome; ?></td>

					<td><?php echo date('d/m/Y', strtotime($valor->contrestempr_data_ini)); ?></td>					
					<td><?php echo date('d/m/Y', strtotime($valor->contrestempr_data_term)); ?></td>

					<?php if(($renovacaoI <> '0000-00-00') and ($renovacaoI <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoI))); ?></td>	
					<?php } else { ?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($renovacaoII <> '0000-00-00') and ($renovacaoII <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoII))); ?></td>	
					<?php } else { ?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($renovacaoIII <> '0000-00-00') and ($renovacaoIII <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoIII))); ?></td>	
					<?php }  else {?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($rescisao <> '0000-00-00') and ($rescisao <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$rescisao))); ?></td>	
					<?php }  else {?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>
					

					<?php if($valor->contrestempr_status == 'Ativo'){ ?> <!--Ativo -->	
					<td><span class="label label-success"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

					<?php if($valor->contrestempr_status == 'Rescindido'){ ?> <!--Inativo -->
					<td><span class="label label-warning"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

					<?php if($valor->contrestempr_status == 'Inativo'){ ?> <!--Inativo -->
					<td><span class="label label-danger"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

				    <td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?php echo base_url()?>contratoestudante/editar/<?php echo $valor->contrestempr_id; ?>"><i class="icon-pencil7"></i> Editar</a></li>
									<li><a href="<?php echo base_url()?>contratoestudante/carregarContrato/<?php echo $valor->contrestempr_id; ?>"><i class="icon-file-excel"></i> Exportar T.C.E</a></li>
									<li><a href="<?php echo base_url()?>contratoestudante/carregarContratoAditivo/<?php echo $valor->contrestempr_id; ?>"><i class="icon-file-word"></i> Exportar T.A</a></li>
								</ul>
							</li>
						</ul>
					</td> 
														
<!--  					<td>																				
						<ul class="icons-list">
								<?php if(checarPermissao('eAdministrativo')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?>contratoestudante/editar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>								
							
								<?php if(checarPermissao('vAdministrativo')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>																				
							
						</ul>																			
					</td> -->		

				</tr>
				<?php } ?>
			<?php } ?>
		</div>
		</tbody>
	</table>
</div>
<!-- /column selectors -->


<!-- Column selectors  INATIVO -->
<div id='semScroll' class="panel panel-flat">
	<div class="panel-heading">	
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>	
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th>Código</th>
				<th>Estudante</th>
				<th>Cpf</th>
				<th>Data Nascimento</th>
				<th>Inst. Ensino</th>

				<th>Inicial</th>
				<th>Termino</th>
				<th>Renovação</th>
				<th>Renovação II</th>
				<th>Renovação III</th>
				<th>Rescisão</th>
				<th>Situação</th>
				<th>Opções</th>

			</tr>
		</thead>
		<tbody>
			<div class="datatable-wrap">
			<?php foreach ($contrato as $valor) { 

				$renovacaoI   = $valor->contrestempr_data_renI;
				$renovacaoII  = $valor->contrestempr_data_renII;
				$renovacaoIII = $valor->contrestempr_data_renIII;
				$rescisao     = $valor->contrestempr_data_rescisao;				

				if (($valor->contrestempr_status == 'Inativo') or 
					(($valor->contrestempr_status == 'Rescindido') and (substr($rescisao,0,7) <> $ano.'-'.$mes )) ) {
			
				?>
				
				<tr>
					
					<td><?php echo $valor->contrestempr_id; ?></td>
					<td><?php echo $valor->salualnome; ?></td>
					<td><?php echo $valor->salualcpf; ?></td>
					<td><?php echo date('d/m/Y', strtotime($valor->talualniver)); ?></td>	<!--Calcular data aniversário -->
					<td><?php echo $valor->sensennome; ?></td>

					<td><?php echo date('d/m/Y', strtotime($valor->contrestempr_data_ini)); ?></td>					
					<td><?php echo date('d/m/Y', strtotime($valor->contrestempr_data_term)); ?></td>

					<?php if(($renovacaoI <> '0000-00-00') and ($renovacaoI <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoI))); ?></td>	
					<?php } else { ?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($renovacaoII <> '0000-00-00') and ($renovacaoII <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoII))); ?></td>	
					<?php } else { ?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($renovacaoIII <> '0000-00-00') and ($renovacaoIII <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$renovacaoIII))); ?></td>	
					<?php }  else {?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>

					<?php if(($rescisao <> '0000-00-00') and ($rescisao <> '1970-01-01')) {?>
					<td><?php echo date('d/m/Y', strtotime(str_replace('/', '-',$rescisao))); ?></td>	
					<?php }  else {?>
					<td><?php echo '0000-00-00'; ?></td>	
					<?php } ?>
					

					<?php if($valor->contrestempr_status == 'Ativo'){ ?> <!--Ativo -->	
					<td><span class="label label-success"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

					<?php if($valor->contrestempr_status == 'Rescindido'){ ?> <!--Inativo -->
					<td><span class="label label-warning"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

					<?php if($valor->contrestempr_status == 'Inativo'){ ?> <!--Inativo -->
					<td><span class="label label-danger"><?php echo $valor->contrestempr_status; ?></span></td>

					<?php } ?>

				    <td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?php echo base_url()?>contratoestudante/editar/<?php echo $valor->contrestempr_id; ?>"><i class="icon-pencil7"></i> Editar</a></li>
								</ul>
							</li>
						</ul>
					</td> 
														
<!--  					<td>																				
						<ul class="icons-list">
								<?php if(checarPermissao('eAdministrativo')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?>contratoestudante/editar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>								
							
								<?php if(checarPermissao('vAdministrativo')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->contrestempr_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>																				
							
						</ul>																			
					</td> -->		

				</tr>
				<?php } ?>
			<?php } ?>
		</div>
		</tbody>
	</table>
</div>
<!-- /column selectors INATIVO -->

