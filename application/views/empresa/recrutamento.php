<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"><?php echo strtoupper($this->uri->segment(2)); ?></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/recrutar" method="post" enctype="multipart/form-data">
		

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

<!--           <div class="form-group">
            <label class="control-label col-lg-1">Vagas:</label>
            <div class="col-lg-11">
                  <select  class="form-control" name="vagas" id="vagas" >                    
                      <?php foreach ($vagas as $valor) { ?>
                            <?php $selected = ((!empty($this->session->userdata('cursosRecutamento')) and $valor->idCursos == $this->session->userdata('cursosRecutamento')))?'SELECTED':''; ?>
                            <option value="<?php echo $valor->idCursos; ?>" <?php echo $selected ?>><?php echo $valor->label; ?></option>
                      <?php } ?>
                  </select>
                  <?php echo form_error('vagas'); ?>
              </div>                                  
          </div> -->

<!--           <div class="form-group">
            <label class="control-label col-lg-1">Cidades:</label>
            <div class="col-lg-11">
                  <select  class="form-control" name="cidades" id="cidades" >
                      <?php foreach ($cidades as $valor) { ?>                    
                            <?php $selected = ((!empty($this->session->userdata('cidadeRecutamento')) and $valor->nome == $this->session->userdata('cidadeRecutamento')))?'SELECTED':''; ?>
                            <option value="<?php echo $valor->nome; ?>" <?php echo $selected ?>><?php echo $valor->nome; ?></option>
                      <?php } ?>
                  </select>
                  <?php echo form_error('cidades'); ?>
              </div>                                  
          </div> -->


        
        <div class="row">  

          <div class="col-md-12">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Vaga</h6>
                <br/>
                <select class="form-control" name="vagas" id="vagas" >
                  <option value="">Selecione</option>
                  <?php foreach ($vagas as $valor) { ?>
                      <option value="<?php echo $valor->idCursos; ?>"><?php echo $valor->label; ?></option>
                  <?php } ?> 
                </select> 
                <?php echo form_error('vagas'); ?>       
            </div>
          </div>

        </div>  
         

        <legend class="text-bold">Filtros:</legend>

        <div class="row">
          
          <div class="col-md-4">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Cidade</h6>
                <br/> 
                <select name="cidade[]" id="estudanteCidade" class="js-example-basic-single js-states form-control" ></select>           
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Curso</h6>
                <br/> 
                <select name="curso[]" id="cursos" class="js-example-basic-single js-states form-control" ></select>           
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Sexo</h6>
                <br/>
                  <select class="form-control" name="sexo" id="sexo">
                    <option value="0,1">Selecione</option>
                      <option value="0">Masculino</option>
                      <option value="1">Feminino</option>
                      <option value="0,1">Ambos</option>
                  </select>
            </div>
          </div>

        </div>  

				
				<div class="text-right">
					 <button type="submit"  class="btn bg-teal">Listar <i class="icon-arrow-right14 position-right" id="mapsLista"></i></button>
				</div>
			</form>
		</div>
	</div>



<?php if(isset($mapa)) { ?>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLBvyEbPEw3MZtVL1kO6zPgz81LvClYac&sensor=true_OR_false"></script>
  

  <!-- Basic map -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h5 class="panel-title">Mapa candidatos</h5>
      <div class="heading-elements">
        <ul class="icons-list">
          <li><a data-action="collapse"></a></li>
        </ul>
      </div>
    </div>

    <div class="panel-body">             
      <div class="map-container map-basic"></div>
    </div>
  </div>
  <!-- /basic map -->


  <!-- Column selectors -->
<div class="panel panel-flat">
  <div class="panel-heading">
    <div class="heading-elements">
      <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
          </ul>
      </div>
  </div>

  <br>
  <table class="table datatable-button-html5-columns">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Email</th>
        <th>Bairro</th>    
        <th>Telefone</th> 
        <th>Atualização</th>  
        <th>Opções</th>

      </tr>
    </thead>

    <tbody>
      <?php foreach ($mapa as $valor) { ?>
        <tr>
        
          <td><?php echo $valor->salualnome; ?></td>
          <td><?php echo $valor->salualemail; ?></td>
          <td><?php echo $valor->salualbairr; ?></td>                   
          <td><?php echo $valor->salualtel01; ?></td> 
          <td><?php echo $valor->date_operacao; ?></td>
          <td>                                        
              <ul class="icons-list">
                
                  <li class="text-primary-600"><a href="" data-popup="tooltip" title="Recrutar"><i class="icon-check"></i></a></li>
                                                                   
              </ul>                                     
          </td>           
        </tr>
      <?php } ?>
      
    </tbody>
  </table>
</div>

<!-- /column selectors -->






<script type="text/javascript">
      
     //Aqui coloca as cordenadas da empresa para o mapa fica com ela no centro
     var empresa = new google.maps.LatLng(-8.0733936, -34.927259);
     //Json dos alunos que será incluido no mapa
     var stringAlunos = '<?php echo json_encode($mapa);?>';

     var alunos = JSON.parse(stringAlunos);

     // variável que indica as coordenadas do marcador
     var empresasMapa = new google.maps.LatLng(40.642851,-8.747596);

     function initialize() {

           var mapOptions = {
              center: empresa, // variável com as coordenadas Lat e Lng
              zoom: 11,
              mapTypeId: google.maps.MapTypeId.roadmap
           };

           var map = new google.maps.Map($('.map-basic')[0],mapOptions);
       
          //variável que define as opções do marcador
          $(alunos).each(function(index,aluno){     

              var  nome      = aluno.nome;
              var  latitude  = aluno.latitude;
              var  longitude = aluno.longitude;
              var  imagem    = aluno.imagem;

              var myLatlng = new google.maps.LatLng(aluno.latitude, aluno.longitude);
              new google.maps.Marker({

              position: myLatlng, // variável com as coordenadas Lat e Lng
              map: map,
              title: nome,
              icon: base_url+'public/assets/images/icons/'+imagem // define a nova imagem do marcador

            });
      
          });      
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

<?php } ?>