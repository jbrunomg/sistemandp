<?php

if ($dados[0]->contrestempr_data_renIII !== '0000-00-00') {
	$date_terminal = $dados[0]->contrestempr_data_renIII;

}else if ($dados[0]->contrestempr_data_renII !== '0000-00-00') {
	$date_terminal = $dados[0]->contrestempr_data_renII;

}else if ($dados[0]->contrestempr_data_renI !== '0000-00-00') {
	$date_terminal = $dados[0]->contrestempr_data_renI;

}else {
	$date_terminal = $dados[0]->contrestempr_data_term;

}
setlocale(LC_TIME,'pt_BR.utf-8');
?>

<html>

<head>
	<title>DECLARAÇÃO ESTÁGIO</title>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<link rel="stylesheet" href="<?php echo base_url('/public/assets/css/contrato_nudep/visualizarDE.css') ?>">
	<script src="https://kit.fontawesome.com/01758f5f36.js" crossorigin="anonymous"></script>
</head>

<!-- `contrestempr_data_renIII`
`contrestempr_data_renII`
`contrestempr_data_renI`
`contrestempr_data_term`
 -->

<body>

	<section id="container_geral">
		<header class="container-geral-header">
			<div class="container-header-logo">
				<img src="<?php echo $emitente[0]->url_logo; ?>" alt="logo">
				<div>Recife,<span><?php echo strftime('%d de %B de %Y', time()) ?></span>.</div>
			</div>

			<br>
			<br>
			<br>

			<div class="container-header-title">
				<strong>CONCLUSÃO DE TERMO DE COMPROMISSO DE ESTÁGIO </strong>
			</div>
		</header>

		<br>
		<br>

		<main class="container-geral-main">
			<p>Declaramos para os devidos fins, que a aluna/estagiária <strong><?php echo $dados[0]->salualnome . ' de CPF: ' . $dados[0]->salualcpf?></strong>, estudante do curso de <strong><?php echo $dados[0]->scurcunome ?></strong>, da instituição de ensino <strong><?php echo $dados[0]->sensenrazao ?></strong>, concluiu seu contrato de estágio na empresa <strong><?php echo $dados[0]->sempemrazao ?> - CNPJ: <?php echo $dados[0]->sempemcnpj ?></strong>, com a interveniência do <strong>Núcleo de Desenvolvimento Profissional Ltda – NUDEP</strong> o mesmo cumpriu integralmente o que foi estabelecido no Termo de Compromisso de Estágio (Lei de estágio nº 11.788, de 25 de setembro de 2008) firmado entre as partes no período de <span><?php echo date('d/m/Y', strtotime($dados[0]->contrestempr_data_ini)) ?></span> a <span><?php echo date('d/m/Y', strtotime($date_terminal)) ?></span>.
			</p>
			<p>
				<strong>Atividades realizadas: </strong>
				<span><?php echo $dados[0]->contrestempr_atividade ?></span>
			</p>

			<br>

			<p>
				Conforme o art. 13, da Lei 11.788/08, caso <strong>o estágio tenha duração igual ou superior a 1 (um) ano, o estagiário terá assegurado um período de recesso remunerado de 30 (trinta) dias, ou proporcionalmente ao tempo estagiado.</strong>
			</p>

			<br>

			<p>
				Orientação: “Qual o prazo para o pagamento da rescisão de estagiário? A legislação atual sobre estágio adota os mesmos prazos da CLT para pagamento e rescisão de contrato, ou seja, o pagamento da rescisão deve ocorrer em até 10 dias corridos.” Contudo, orientamos que a empresa tendo o encerramento programado e o valor para pagamento esteja na programação, pode realizar o pagamento de imediato.
			</p>
		</main>

		<br>
		<br>

		<footer class="container-geral-footer">
			<div>
				<ul>
					<img src="<?php echo base_url('public/assets/images/') ?>assinatura.png" style="width: 100px;" alt="assinatura">
				</ul>
				<ul>Assinaturas:</ul>
				<ul>Empresa</ul>
				<ul>Estagiária</ul>
			</div>
			<div class="container-footer-duvida">
				<p>Dúvidas estarei à disposição</p>
			</div>

			<br>
			<br>
			<br>

			<div class="container-footer-atendimento">
				<span>Canais de atendimento: </span>
				<span><i class="fa-brands fa-whatsapp" ></i> (81) 98793-1943 / (81) 99871-0516 </span>
				<span><i class="fa-solid fa-earth-americas"></i> www.nuped.com.br </span>
				<span><i class="fa-regular fa-envelope"></i> nudep@nudep.com.br</span>
			</div>
		</footer>
		<br>
	</section>
<br>
</body>

</html>
