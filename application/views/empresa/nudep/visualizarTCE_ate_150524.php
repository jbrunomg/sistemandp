<?php

$mes = date('m');      // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0,0,0,$mes,'01',$ano)); // Mágica, plim!
$mesExtenso = date('m', strtotime($dados[0]->contrestempr_data_ini));

switch ($mesExtenso){

	case 1: $mesExtenso = "JANEIRO"; break;
	case 2: $mesExtenso = "FEVEREIRO"; break;
	case 3: $mesExtenso = "MARÇO"; break;
	case 4: $mesExtenso = "ABRIL"; break;
	case 5: $mesExtenso = "MAIO"; break;
	case 6: $mesExtenso = "JUNHO"; break;
	case 7: $mesExtenso = "JULHO"; break;
	case 8: $mesExtenso = "AGOSTO"; break;
	case 9: $mesExtenso = "SETEMBRO"; break;
	case 10: $mesExtenso = "OUTUBRO"; break;
	case 11: $mesExtenso = "NOVEMBRO"; break;
	case 12: $mesExtenso = "DEZEMBRO"; break;

}

switch ($mes){

	case 1: $mes = "JANEIRO"; break;
	case 2: $mes = "FEVEREIRO"; break;
	case 3: $mes = "MARÇO"; break;
	case 4: $mes = "ABRIL"; break;
	case 5: $mes = "MAIO"; break;
	case 6: $mes = "JUNHO"; break;
	case 7: $mes = "JULHO"; break;
	case 8: $mes = "AGOSTO"; break;
	case 9: $mes = "SETEMBRO"; break;
	case 10: $mes = "OUTUBRO"; break;
	case 11: $mes = "NOVEMBRO"; break;
	case 12: $mes = "DEZEMBRO"; break;

}

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/png" sizes="32x32" href="imagens/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="imagens/favicon/favicon-16x16.png">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo base_url('/public/assets/css/contrato_nudep/index.css') ?>">

	<!-- CSS -->
	<link rel="stylesheet" href="css/estilo.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<!--	<link href="https://fonts.googleapis.com/css2?family=Albert+Sans:wght@200&family=Open+Sans:wght@500&display=swap" rel="stylesheet">-->
	<script src="https://kit.fontawesome.com/01758f5f36.js" crossorigin="anonymous"></script>
	<title>Termo de Compromisso de Estágio</title>
</head>
<body>
<div id="container_geral">
	<!--Título-->
	<div style="display: flex;">
		<img src="<?php echo base_url('public/assets/images/') ?>logoNudep.png" width="210px" style="float: left;">
		<div style="float: right; padding-left: 29px;">
			<h3 style="margin-left: 65px; padding-top: 50px; width: 283px; border: none; border-bottom: 2px solid #000; outline: none;">
				<strong> TERMO DE COMPROMISSO DE ESTÁGIO</strong>
			</h3>
			<h4>(Instrumentos jurídicos de que trata o art. 3º, item II da Lei 11.788 /08).</h4>
		</div>
	</div>
	<!--Instituição de Ensino-->
	<div style="padding-left: 32px; padding-right: 32px;">
		<?php if ($dados[0]->contrestempr_data_ini <= date('Y-m-d')) {  ?>
			<div style="padding-left: 3px;">
				No dia
				<label for="dia"><?php echo  date('d', strtotime($dados[0]->contrestempr_data_ini)); ?> </label> <!--PHP-->
				de
				<label for="mes"><?php echo  $mesExtenso; ?></label> <!--PHP-->
				de
				<label for="ano"><?php echo  date('Y', strtotime($dados[0]->contrestempr_data_ini)); ?></label> <!--PHP-->
				, na cidade de Recife, PE.
			</div>
		<?php  } else { ?>
			<div style="padding-left: 3px;">
				No dia
				<label for="dia"><?php echo  date('d'); ?> </label> <!--PHP-->
				de
				<label for="mes"><?php echo  $mes; ?></label> <!--PHP-->
				de
				<label for="ano"> <?php echo  date('Y'); ?></label> <!--PHP-->
				, na cidade de Recife, PE.
			</div>
		<?php  } ?>

		<div style="padding-left: 3px;">Neste ato, as partes a seguir nomeadas:</div> <br>

		<div style="padding-left: 3px; text-align: center; margin-bottom: 3px;"><strong>INSTITUIÇÃO DE ENSINO</strong></div>
		<div class="div_categorias">
			<div class="intercalarDiv">
				<?php if($dados[0]->sensenmantededora <> '') { ?>
					<label>Mantenedora:</label>
					<label style="padding-left: 10px;"><?php echo $dados[0]->sensenmantededora; ?></label> <!--PHP-->
					<br>
				<?php }?>
				<label>Inst. de Ensino:</label>
				<label style="padding-left: 04px;"><?php echo strtoupper($dados[0]->sensennome) ?></label> <!--PHP-->
				<div style="display: grid; grid-template-columns: 100px 234px 50px 192px 45px 100px;">
					<div>Endereço:</div>
					<div><?php echo mb_strtoupper($dados[0]->sensenlogra) ?></div> <!--PHP-->
					<div class="divValores">Bairro:</div>
					<div><?php echo mb_strtoupper($dados[0]->sensenbairr) ?></div> <!--PHP-->
					<div class="divValores">CEP:</div>
					<div><?php echo $dados[0]->sempemcep; ?></div> <!--PHP-->
				</div>
				<label>Cidade:</label>
				<label style="padding-left: 51px; width: 477px; display: inline-block;"><?php echo strtoupper($dados[0]->sensencidad) ?></label>  <!--PHP-->
				<label>UF:</label>
				<label style="padding-left: 17px; width: 200px; display: inline-block;"><?php echo strtoupper($dados[0]->sensenuf) ?></label> <!--PHP-->
				<br>
				<label>CNPJ/MF:</label>
				<label style="padding-left: 40px; width: 477px; display: inline-block;"><?php echo $dados[0]->sempemcnpj; ?></label> <!--PHP-->
				<label>Fone:</label>
				<label style="padding-left: 03px; width: 202px; display: inline-block;"><?php echo $dados[0]->sempemtel01; ?></label> <!--PHP-->
				<div style="display: grid; grid-template-columns: 100px 234px 53px 435px;">
					<div>Representado por:</div>
					<div><?php echo strtoupper($dados[0]->sempemrespo) ?></div> <!--PHP-->
					<div class="divValores">Cargo:</div>
					<div><?php echo strtoupper($dados[0]->sempemcargo) ?></div> <!--PHP-->
				</div>
			</div>
		</div>
	</div>
	<!--Unidade Concedente-->
	<div style="padding-left: 32px; padding-right: 32px;">
		<div style="padding-left: 3px; text-align: center; margin-bottom: 3px;"><strong>UNIDADE CONCEDENTE</strong></div>
		<div class="div_categorias">
			<div class="intercalarDiv">
				<label>Razão Social:</label>
				<label style="padding-left: 18px;"><?php echo strtoupper($dados[0]->sempemrazao) ?></label> <!--PHP-->
				<div style="display: grid; grid-template-columns: 100px 234px 50px 192px 45px 100px;">
					<div>Endereço:</div>
					<div><?php echo strtoupper($dados[0]->sempemlogra).','.strtoupper($dados[0]->sempemnumer).' '.strtoupper($dados[0]->sempemcompl) ?></div> <!--PHP-->
					<div class="divValores">Bairro:</div>
					<div><?php echo $dados[0]->sempembairr; ?></div> <!--PHP-->
					<div class="divValores">CEP:</div>
					<div><?php echo $dados[0]->sempemcep; ?></div> <!--PHP-->
				</div>
				<label>Cidade:</label>
				<label style="padding-left: 51px; width: 475px; display: inline-block;"><?php echo $dados[0]->sempemcidad; ?></label>  <!--PHP-->
				<label>UF:</label>
				<label style="padding-left: 17px; width: 200px; display: inline-block;"><?php echo strtoupper($dados[0]->sempemuf) ?></label> <!--PHP-->
				<label>CNPJ/MF:</label>
				<label style="padding-left: 40px; width: 475px; display: inline-block;"><?php echo $dados[0]->sempemcnpj; ?></label> <!--PHP-->
				<label>Fone:</label>
				<label style="padding-left: 03px; width: 202px; display: inline-block;"><?php echo $dados[0]->sempemtel01; ?></label> <!--PHP-->
				<div style="display: grid; grid-template-columns: 100px 475px 53px 200px;">
					<div>Representado por:</div>
					<div><?php echo strtoupper($dados[0]->sempemrespo) ?></div> <!--PHP-->
					<div class="divValores">Cargo:</div>
					<div><?php echo strtoupper($dados[0]->sempemcargo) ?></div> <!--PHP-->
				</div>
			</div>
		</div>
	</div>
	<!--Clausulas Concedente-->
	<div style="padding-left: 32px; padding-right: 32px;">
		<div style="padding-left: 3px; margin-bottom: 5px;">celebram entre si este Termo de Compromisso de Estágio, conveniando as cláusulas e condições seguintes:</div>
		<div class="div_categorias">
			<div style="margin-bottom: 6px;">
				<label>
					<strong>CLAUSULA 1ª:</strong>
					O Termo de Compromisso de Estágio tem por objetivo estabelecer condições básicas para a realização de estágio de estudantes da INSTITUIÇÃO DE ENSINO junto à UNIDADE CONCEDENTE, o qual poderá ser de interesse curricular obrigatório ou não.
				</label>
			</div>
			<br>
			<div style="margin-bottom: 6px;">
				<label>
					<strong>CLAUSULA 2ª:</strong>
					Em vista do presente documento firma-se um Termo de Compromisso de Estágio - TCE, entre o estudante e a UNIDADE CONCEDENTE com a interveniência e a assinatura da INSTITUIÇÃO DE ENSINO, no item II do Art. 3º da Lei 11.788/08 e nos artigos que
					couber que constituirá documento obrigatório para comprovação da inexistência do vínculo empregatício.
				</label>
			</div>
			<br>
			<div style="margin-bottom: 6px;">
				<label>
					<strong>CLAUSULA 3ª:</strong>
					As condições básicas para a realização de estágio de estudante constarão no Termo de Compromisso de Estágio - TCE, cujo conteúdo será de conhecimento do estagiário. <br>
					A UNIDADE CONCEDENTE, com interveniência e assinatura da INSTITUIÇÃO DE ENSINO e de outro lado, o:
				</label>
			</div>
			<br>
			<div class="div_categorias">
				<div class="intercalarDiv">
					<div style="display: grid; grid-template-columns: 100px 234px 85px 192px;">
						<div>Estagiária:</div>
						<div><?php echo strtoupper($dados[0]->salualnome) ?></div> <!--PHP-->
						<div class="divValores">Dt. De Nasc:</div>
						<div><?php echo date('d/m/Y', strtotime($dados[0]->talualniver)); ?></div> <!--PHP-->
					</div>
					<div style="display: grid; grid-template-columns: 100px 234px 50px 192px 45px 100px;">
						<div>Endereço:</div>
						<div><?php echo mb_strtoupper($dados[0]->saluallogra).' '.strtoupper($dados[0]->salualnumer)  /*strtoupper($dados[0]->salualcompl)*/ ?></div> <!--PHP-->
						<div class="divValores">Bairro:</div>
						<div><?php echo mb_strtoupper($dados[0]->salualbairr) ?></div> <!--PHP-->
						<div class="divValores">CEP:</div>
						<div><?php echo $dados[0]->salualcep; ?></div> <!--PHP-->
					</div>
					<div style="display: grid; grid-template-columns: 100px 234px 29px 213px 45px 150px;">
						<div>Cidade:</div>
						<div><?php echo strtoupper($dados[0]->salualcidad) ?></div> <!--PHP-->
						<div class="divValores">UF:</div>
						<div><?php echo strtoupper($dados[0]->salualestad) ?></div> <!--PHP-->
						<div class="divValores">Fone:</div>
						<div><?php echo $dados[0]->salualtel01.' / '.$dados[0]->salualtel02.' / '.$dados[0]->salualtel03 ?></div> <!--PHP-->
					</div>
					<div style="display: grid; grid-template-columns: 100px 234px 59px 183px 45px 100px;">
						<div>Curso:</div>
						<div><?php echo mb_strtoupper($dados[0]->scurcunome) ?></div> <!--PHP-->
						<div class="divValores">Período:</div>
						<div><?php echo $dados[0]->salualperio; ?></div> <!--PHP-->
						<div class="divValores">Nível:</div>
						<div><?php echo mb_strtoupper($dados[0]->ialualcurso_nivel) ?></div> <!--PHP-->
					</div>
					<div style="display: grid; grid-template-columns: 100px 234px 39px 203px 45px 100px;">
						<div>CPF:</div>
						<div><?php echo strtoupper($dados[0]->salualcpf) ?></div> <!--PHP-->
						<div class="divValores">R.G.:</div>
						<div><?php echo $dados[0]->salualrg; ?><?php echo $dados[0]->salualrg_org_expd; ?></div> <!--PHP-->
						<div class="divValores">E-mail:</div>
						<div><?php echo $dados[0]->salualemail; ?></div> <!--PHP-->
					</div>
				</div>
			</div>
			<div style="margin-bottom: 6px;">
				<label>
					<strong>CLAUSULA 4ª - </strong>
					O Termo de Compromisso de Estágio - TCE, por finalidade estabelecer a relação jurídica entre o estagiário e a UNIDADE CONCEDENTE, que caracterizará a não existência do vínculo empregatício.
				</label>
			</div>
			<div style="margin-bottom: 6px;">
				<label>
					<strong>CLAUSULA 5ª - </strong>
					Na vigência do presente termo, o estagiário estará assegurado nos casos de morte acidental, invalidez permanente total ou parcial por acidente no estágio, de acordo com
					apólice nº <strong>500390</strong> sob a responsabilidade do grupo <strong>Sul América Seguros de Vida e Previdência S/A</strong>, valor de R$ 5.000,00 (cinco mil reais).
				</label>
				<div>&nbsp;</div>
				<div style="padding-left: 30px; display: grid; grid-template-columns: 30px 760px">
					<div><strong>➢</strong></div>
					<div>
						Art. 14. Aplica-se ao estagiário a legislação relacionada à saúde e segurança no trabalho, sendo sua implementação de responsabilidade da parte concedente do estágio.
					</div>
				</div>
			</div>
			<br>
			<div>
				<strong>CLAUSULA 6ª - </strong>
				Ficam acordadas entre as partes as condições básicas para a realização do estágio:
				<div style="display: grid; grid-template-columns: 30px 790px;">
					<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
					<div>
						O Termo de Compromisso de Estágio - TCE terá vigência de <strong>03/08/2023<!--PHP--> a 30/12/2023<!--PHP--></strong> podendo ser rescindido a qualquer momento, unilateralmente, mediante comunicação
						por escrito ou ser prorrogado através da emissão de um Termo Aditivo. Nos casos de prorrogação, o Termo Aditivo deverá ser assinado até o último dia de vigência do Termo e Compromisso de Estágio
						- TCE. O não comparecimento do estagiário na data prevista, a UNIDADE CONCEDENTE responsabilizar-se-á pelas consequências oriundas do não cumprimento desta exigência jurídica.
					</div>
				</div>


				<div style="display: grid; grid-template-columns: 678px 150px;">
					<div style="margin-top: 100px;">
						Canais de atendimento:
						<label><i class="fa-brands fa-whatsapp"></i> (81) 98793-1943 / (81) 99871-0516</label>
						<label class="divValores"><i class="fa-solid fa-earth-americas"></i> www.nudep.com.br</label>
						<label><i class="fa-regular fa-envelope"></i> nudep@nudep.com.br</label>
					</div>
					<div class="divValores">
						<img src="<?php echo base_url('public/assets/images/') ?>assinatura.png" style="width: 150px;">
					</div>
				</div>

			</div>
		</div>
	</div>
	<!--Quebra de Página-->
	<!--Logo-->
	<div style="display: flex;">
		<img src="<?php echo base_url('public/assets/images/') ?>logoNudep.png" width="210px" style="float: left;">
	</div>
	<!--Clausulas Concedente pag2-->
	<div style="padding-left: 32px; padding-right: 32px;">
		<div style="display: grid; grid-template-columns: 30px 790px; margin-bottom: 10px;">
			<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
			<div>
				A duração do estágio, da mesma parte Concedente, não poderá exceder 2 (dois) anos, exceto quando se tratar de estagiário portador de deficiência, conforme Cap. IV, art. 11, Lei 11.788/08;
			</div>
		</div>
		<br>
		<div style="display: grid; grid-template-columns: 30px 790px;">
			<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
			<div>
				Conforme o art. 13, da Lei 11.788/08, caso o estágio tenha duração igual ou superior a 1 (um) ano, o estagiário terá assegurado um período de recesso remunerado de 30 (trinta) dias, ou proporcionalmente ao tempo estagiado.
			</div>
		</div>
		<br>
		<div style="display: grid; grid-template-columns: 30px 790px;">
			<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
			<div>
				A carga horária total do estágio será 20h semanais (14h às 18h) de segunda-feira a sexta-feira. <strong>§1º - </strong> Obrigatório um intervalo de 15 (quinze) minutos quando a duração ultrapassar 4 (quatro) horas. Mesmo que o estágio não gere vínculo empregatício, pode se interpretar aplicável a disposição.
			</div>
		</div>
		<br>
		<div style="display: grid; grid-template-columns: 30px 790px;">
			<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
			<div>
				Por ocasião de verificações de aprendizagem periódicas ou finais, através de avaliações, feitas pela instituição de ensino, a carga horária do estágio será reduzida pelo menos à metade da sua carga horária diária total;
			</div>
		</div>
		<br>
		<div style="display: grid; grid-template-columns: 30px 790px;">
			<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
			<div>
				As principais atividades a serem desenvolvidas pelos estagiários estarão de acordo com o contexto básico do curso, conforme relação abaixo, podendo ser ampliadas, reduzidas, alteradas ou substituídas:
			</div>
		</div>
		<br>
		<div style="display: grid; grid-template-columns: 83px 737px; margin-top: 5px;">
			<div>
				<strong>ATIVIDADES:</strong>
			</div>
			<div>
				<?php echo $dados[0]->contrestempr_atividade; ?> <!--PHP-->
			</div>
		</div>
		<div style="display: grid; grid-template-columns: 108px 712px; margin-top: 5px;">
			<div>
				<strong>REMUNERAÇÃO:</strong>
			</div>
			<div>
				<?php echo 'R$ '.$dados[0]->contrestempr_bolsa_Auxilio; ?><?php echo $dados[0]->contrestempr_beneficios; ?> <!--PHP-->
			</div>
		</div>
		<div style="display: grid; grid-template-columns: 108px 712px; margin-top: 5px;">
			<div>
				<strong>HORÁRIO:</strong>
			</div>
			<div>
				<?php echo $dados[0]->contrestempr_cargahoraria; ?><br> <!--PHP-->
				§ 1º – Obrigatório um intervalo de 15 (quinze) minutos quando a duração ultrapassar 4 (quatro) horas. Mesmo que o   estágio não gere vínculo empregatício, pode se interpretar aplicável a disposição. “(De acordo com o disposto na Lei do Estágio, não há nenhuma proibição quanto à possibilidade de atuação aos sábados, domingos ou feriados nacionais. Dessa maneira, estando o jovem dentro da jornada permitida regimentalmente, trabalhar nesses dias é legalmente possível).”
			</div>
		</div>
		<div style="display: grid; grid-template-columns: 108px 712px; margin-top: 5px;">
			<div>
				<strong>BENEFICIOS:</strong>
			</div>
			<div>
				Treinar na unidade sem custo, mediante apresentação de identificação e contrato de estágio ativo.
			</div>
		</div>

		<div style="margin-bottom: 6px; margin-top: 5px;">
			<strong>CLAUSULA 7ª -</strong> Durante o estágio caberá a <strong>UNIDADE CONCEDENTE:</strong>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Oferecer instalações que tenham condições de proporcionar ao estagiário o desenvolvimento de atividades de aprendizagem social, profissional e cultural, sempre relacionadas com atividades previstas no seu curso.
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Possibilitar condições que permitam à INSTITUIÇÃO DE ENSINO e ou NUDEP, realização de acompanhamento, supervisão e avaliação do estágio.
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Indicar um funcionário de seu quadro de pessoal com formação ou experiência profissional na área de conhecimento do curso do estagiário.
				</div>
			</div>
		</div>
		<div style="margin-bottom: 6px; margin-top: 5px;">
			<strong>CLAUSULA 8ª -</strong> Durante o estágio caberá a <strong>INSTITUIÇÃO DE ENSINO:</strong>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Indicar professor orientador da área a ser desenvolvida no estágio, como responsável pelo acompanhamento e avaliação das atividades do estagiário;
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Receber e examinar periodicamente (a cada seis meses) o relatório de estágio encaminhado pelo NUDEP através do estagiário;
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Comunicar à Unidade Concedente de estágio, as datas de realização de avaliações escolares ou acadêmicas.
				</div>
			</div>
		</div>
		<div style="margin-top: 5px;">
			<strong>CLAUSULA 9ª -</strong> ESTAGIÁRIO:
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Devolver ao NUDEP, dentro do prazo de 10 (dez) dias úteis uma via do TCE devidamente assinado pelas partes envolvidas no estágio.
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Desempenhar com interesse as atividades do estágio;
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Comprovar vinculo ativo com a instituição de ensino, apresentando-nos declarações de matrícula semestral.
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Respeitar as normas internas da UNIDADE CONCEDENTE;
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Entregar ao NUDEP, relatórios sobre atividades realizadas durante o estágio;
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Não posso ser vinculado a outro estágio que a soma das duas cargas-horárias ultrapasse 6 (seis) horas diárias e 30h semanais. Sendo de minha responsabilidade optar por uma das vagas antes de assinar esse contrato.
				</div>
			</div>
			<div style="display: grid; grid-template-columns: 30px 790px;">
				<div><img src="<?php echo base_url('public/assets/images/') ?>icone.png" alt=""></div>
				<div>
					Ao decidir desligar-se do estágio, deve comunicar por escrito à Unidade Concedente e ao NUDEP.
				</div>
			</div>
			<br><br><br><br>
			<div style="display: grid; grid-template-columns: 678px 150px;">
				<div style="margin-top: 100px;">
					Canais de atendimento:
					<label><i class="fa-brands fa-whatsapp"></i> (81) 98793-1943 / (81) 99871-0516</label>
					<label class="divValores"><i class="fa-solid fa-earth-americas"></i> www.nudep.com.br</label>
					<label><i class="fa-regular fa-envelope"></i> nudep@nudep.com.br</label>
				</div>
				<div class="divValores">
					<img src="<?php echo base_url('public/assets/images/') ?>assinatura.png" style="width: 150px;">
				</div>
			</div>

		</div>





	</div>
	<!--Quebra de Página-->
	<!--Logo-->
	<div style="display: flex;">
		<img src="<?php echo base_url('public/assets/images/') ?>logoNudep.png" width="210px" style="float: left;">
	</div>
	<div style="padding-left: 32px; padding-right: 32px;">
		<div style="margin-bottom: 5px;">
			<label>
				<strong>CLAUSULA 10ª-</strong>
				O estágio poderá ser rescindido automaticamente nos casos em que haja conclusão, abandono ou trancamento do curso, bem como o não cumprimento do convencionado Termo de Compromisso de Estágio - TCE.
			</label>
		</div>
		<div style="margin-bottom: 5px;">
			<label>
				<strong>CLAUSULA 11ª-</strong>
				As partes declaram expresso CONSENTIMENTO que todos os dados sensíveis das estagiárias e estagiários pela <strong>NUDEP</strong> coletados, armazenados, tratados e compartilhados, são os estritamente necessários
				ao cumprimento do contrato, nos termos do Art. 7°, inc. V da LGPD, os dados necessários para cumprimento de obrigações legais, nos termos do Art. 7°, inc II da LGPD, bem como os dados, se necessários para proteção ao crédito,
				conforme autorizado pelo Art. 7°, inc. V da LGPD, devendo, se ocorrer qualquer dúvida ou situação que venha a exigir uma atuação por parte da <strong>NUDEP</strong>, o contato deverá ocorrer diretamente com a <strong>Representante legal para administração de convênios e contratos da NUDEP</strong>
				para a solução e processamento da proteção dos dados pessoais.
			</label>
		</div>
		<div style="margin-bottom: 5px;">
			<label>
				<strong>CLAUSULA 12ª-</strong>
				A INSTITUIÇÃO DE ENSINO, a UNIDADE CONCEDENTE e o ESTAGIÁRIO, signatários deste termo elegem de comum acordo como órgão de apoio o Núcleo de Desenvolvimento Profissional Ltda - NUDEP, empresa privada, sob CNPJ n° 06.195.488/0001-36, a quem comunicarão a interrupção, a conclusão ou as eventuais
				modificações do conveniado no presente termo.
			</label>
		</div>
		<div style="margin-bottom: 5px;">
			<label>
				E, por estarem de inteiro e comum acordo com as condições e dizeres do Termo de Compromisso de Estágio - TCE, as partes assinam as vias de igual teor.
			</label>
		</div>
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>



		<div style="display: grid; grid-template-columns: 520px 300px;" >
			<div style="margin-top: 30px;">
				Assinatura digital:
				<div style="padding-left: 30px; display: grid; grid-template-columns: 30px 760px">
					<div><strong>➢</strong></div>
					<div>
						Estagiário
					</div>
				</div>
				<div style="padding-left: 30px; display: grid; grid-template-columns: 30px 760px">
					<div><strong>➢</strong></div>
					<div>
						Unidade concedente
					</div>
				</div>
				<div style="padding-left: 30px; display: grid; grid-template-columns: 30px 760px">
					<div><strong>➢</strong></div>
					<div>
						Instituição de Ensino
					</div>
				</div>
			</div>
			<div class="divValores">
				<img src="<?php echo base_url('public/assets/images/') ?>assinatura.png" style="width: 150px; padding-left: 65px;">
				<p>Agente de Integração - Representante legal</p>
			</div>
		</div>
		<br><br><br>
		<div style="display: grid; grid-template-columns: 678px 150px;">
			<div>
				Canais de atendimento:
				<label><i class="fa-brands fa-whatsapp"></i> (81) 98793-1943 / (81) 99871-0516</label>
				<label class="divValores"><i class="fa-solid fa-earth-americas"></i> www.nudep.com.br</label>
				<label><i class="fa-regular fa-envelope"></i> nudep@nudep.com.br</label>
			</div>
		</div>
	</div>
</div>
 </body>
</html>
