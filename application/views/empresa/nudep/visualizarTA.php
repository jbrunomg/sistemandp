<?php
if($dados[0]->contrestempr_termo_A_III) {
	$date_terminal = $dados[0]->contrestempr_termo_A_III;

}
else if($dados[0]->$dados[0]->contrestempr_termo_A_II) {
	$date_terminal = $dados[0]->contrestempr_termo_A_II;

} else if($dados[0]->contrestempr_termo_A_I) {
	$date_terminal = $dados[0]->contrestempr_termo_A_I;

} else {
	$date_terminal = $dados[0]->contrestempr_data_term;

}
setlocale(LC_TIME,'pt_BR.utf-8');
?>

<!DOCTYPE html>
<html lang="pt">
<head>
	<title>TERMO DE COMPROMISSO DE ESTÁGIO</title>

	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var token = '<?php echo $this->security->get_csrf_hash(); ?>';
	</script>

	<!-- css -->
	<link rel="stylesheet" href="<?php echo base_url('/public/assets/css/contrato_nudep/visualizarTA.css') ?>">
	<!-- external imports -->
	<script src="https://kit.fontawesome.com/01758f5f36.js" crossorigin="anonymous"></script>
</head>
<body>

<section  id="container_geral">
	<header class="container-geral-header">
		<img src="<?php echo $emitente[0]->url_logo; ?>" alt="logo">
		<div class="div-title-main">
			<strong>
				<u class="title-main-text">TERMO ADITIVO -  Renovação de estágio</u>
			</strong>
		</div>
		<div class="container-geral-header-date">
			<p>Recife,<span><?php echo strftime('%d de %B de %Y', time()) ?></span>.</p>
		</div>
	</header>

	<main class="container-geral-main">

		<p class="container-content-text-main">Termo Aditivo e Termo de Compromisso de Estágio, instrumentos jurídicos de que tratam o parágrafo único do art. 7º e inciso II do caput do art. 3º, da Lei 11.788 /08, de 25/09/2008, firmado entre as partes: a Instituição de Ensino: <strong><?php echo $dados[0]->sensennome; ?></strong>, a Unidade Concedente de Estágio: <strong><?php echo $dados[0]->sempemrazao; ?></strong> e a estudante <strong><?php echo $dados[0]->salualnome . ' de CPF: ' . $dados[0]->salualcpf?></strong>, já devidamente qualificados no contrato que ora se adita. Pelo presente Termo Aditivo ficam conveniadas as cláusulas e condições seguintes:</p>
		<br>
		<div class="container-content-clausulas">
			<strong>CLÁUSULA 1ª -</strong>
			<span>As partes declaram expresso CONSENTIMENTO que todos os dados sensíveis das estagiárias e estagiários pela NUDEP coletados, armazenados, tratados e compartilhados, são os estritamente necessários ao cumprimento do contrato, nos termos do Art. 7º, inc. V da LGPD, os dados necessários para cumprimento de obrigações legais, nos termos do Art. 7º, inc. II da LGPD, bem como os dados, se necessários para proteção ao crédito, conforme autorizado pelo Art. 7º, inc. V da LGPD, devendo, se ocorrer qualquer dúvida ou situação que venha a exigir uma atuação por parte da NUDEP, o contato deverá ocorrer diretamente com a Representante legal para administração de convênios e contratos da NUDEP para a solução e processamento da proteção dos dados pessoais.</span>
			<ul>
				<li>A duração do estágio, na mesma parte Concedente, não poderá exceder 2 (dois) anos, exceto quando se tratar de estagiário portador de deficiência, conforme Cap. IV, art. 11, Lei 11.788/08;</li><br>
				<li>Conforme o art. 13, da Lei 11.788/08, caso o estágio tenha duração igual ou superior a 1 (um) ano, o estagiário terá assegurado um período de recesso remunerado de 30 (trinta) dias, ou proporcionalmente ao tempo estagiado;</li><br>
				<li>Por ocasião de verificações de aprendizagem periódicas ou finais, através de avaliações, feitas pela instituição de ensino, a carga horária do estágio será reduzida pelo menos à metade.</li>
			</ul>
		</div>
		<br>

		<div class="container-content-clausulas">
			<strong>CLÁUSULA 2ª - </strong>
			<span>O Termo de Compromisso de Estágio - TCE será prorrogado por um período de 06 ( seis),meses de <strong>
					<span><?php echo date('d/m/Y',strtotime($dados[0]->contrestempr_data_ini)) ?></span>
					a
					<span><?php echo date('d/m/Y', strtotime($date_terminal)); ?></span>
				</strong></span>
		</div>

		<br><br>

		<div class="container-content-clausulas">
			<strong>CLÁUSULA 3ª - </strong>
			<span>Permanecem inalteradas todas as demais cláusulas existentes no Termo de Compromisso de Estágio, já mencionado e aqui ratificado.</span>
		</div>

		<br><br>

		<div class="container-content-clausulas">
			<strong>CLÁUSULA 4ª - </strong>
			<span>E, por estarem de inteiro e comum acordo com as condições e dizeres do Termo de Compromisso de Estágio – TCE, as partes assinam as vias de igual teor.</span>
		</div>

	</main>

	<br />
	<br />
	<br />

	<footer class="container-geral-footer">
		<div class="container-contant-footer-assinatura">
			<img src="<?php echo base_url('public/assets/images/') ?>assinatura.png" style="width: 150px;" alt="assinatura">
			<strong>Agente de integração</strong>
			<strong>NUDEP LTDA</strong>
		</div>
		<div>
			<strong>Assinatura digital:</strong>
			<ul>
				<div>
					<strong>➢</strong>
					<span>Estagiária</span>
				</div>
				<div>
					<strong>➢</strong>
					<span>Unidade concedente</span>
				</div>
				<div>
					<strong>➢</strong>
					<span>Instituição de Ensino</span>
				</div>
			</ul>
		</div>
		<br />
		<br />
		<br />
		<div class="container-content-footer-atendimento">
			<span>Canais de atendimento: </span>
			<span><i class="fa-brands fa-whatsapp" ></i> (81) 98793-1943 / (81) 99871-0516 </span>
			<span><i class="fa-solid fa-earth-americas"></i> www.nuped.com.br </span>
			<span><i class="fa-regular fa-envelope"></i> nudep@nudep.com.br</span>
		</div>
	</footer>
	<br>
</section>
<br><br>
</body>
</html>
