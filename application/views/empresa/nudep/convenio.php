<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>pdf-html</title>
<meta name="generator" content="BCL easyConverter SDK 5.0.252">
<meta name="author" content="Unigape">
<meta name="title" content="CONVÊNIO UNIDADE CONCEDENTE – NUDEP">
<style type="text/css">

body {margin-top: 0px;margin-left: 20px;}

#page_1 {position:relative; overflow: hidden;margin: 0px 0px 0px 20px;padding: 0px;border: none;width: 737px;}

#page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:218px;height:118px;}
#page_1 #p1dimg1 #p1img1 {width:218px;height:118px;}



.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: bold 19px 'Century Gothic';text-decoration: underline;line-height: 23px;}
.ft1{font: 13px 'Arial';line-height: 16px;}
.ft2{font: 1px 'Arial';line-height: 1px;}
.ft3{font: bold 13px 'Arial';line-height: 16px;}
.ft4{font: 1px 'Arial';line-height: 7px;}
.ft5{font: 11px 'Arial';line-height: 14px;}
.ft6{font: 11px 'Verdana';color: #002060;line-height: 13px;}
.ft7{font: 11px 'Verdana';text-decoration: underline;color: #002060;line-height: 13px;}
.ft8{font: 13px 'Arial';margin-left: 5px;line-height: 16px;}
.ft9{font: 13px 'Arial';margin-left: 4px;line-height: 16px;}
.ft10{font: 13px 'Arial';margin-left: 6px;line-height: 16px;}
.ft11{font: bold 13px 'Arial';line-height: 15px;}
.ft12{font: 13px 'Arial';margin-left: 3px;line-height: 16px;}
.ft13{font: 13px 'Arial';margin-left: 8px;line-height: 16px;}
.ft14{font: 13px 'Arial';margin-left: 7px;line-height: 16px;}
.ft15{font: 13px 'Arial';line-height: 15px;}
.ft16{font: 13px 'Arial';margin-left: 6px;line-height: 15px;}
.ft17{font: 13px 'Arial';margin-left: 4px;line-height: 15px;}
.ft18{font: 12px 'Arial';line-height: 13px;}
.ft19{font: bold 12px 'Arial';line-height: 15px;}
.ft20{font: 9px 'Arial';line-height: 12px;}
.ft21{font: 9px 'Verdana';text-decoration: underline;color: #002060;line-height: 13px;}
.ft22{font: 9px 'Verdana';color: #002060;line-height: 13px;}

.p0{text-align: left;padding-left: 162px;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;margin-top: 31px;margin-bottom: 0px;}
.p3{text-align: left;margin-top: 14px;margin-bottom: 0px;}
.p4{text-align: justify;padding-right: 41px;margin-top: 15px;margin-bottom: 0px;}
.p5{text-align: left;margin-top: 11px;margin-bottom: 0px;}
.p6{text-align: justify;padding-right: 41px;margin-top: 0px;margin-bottom: 0px;}
.p7{text-align: justify;padding-right: 41px;margin-top: 12px;margin-bottom: 0px;}
.p8{text-align: justify;padding-right: 42px;margin-top: 14px;margin-bottom: 0px;}
.p9{text-align: justify;padding-right: 41px;margin-top: 13px;margin-bottom: 0px;}
.p10{text-align: left;margin-top: 15px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 98px;margin-top: 31px;margin-bottom: 0px;}
.p12{text-align: left;padding-left: 139px;margin-top: 0px;margin-bottom: 0px;}
.p13{text-align: left;padding-left: 148px;margin-top: 0px;margin-bottom: 0px;}
.p15{text-align: left;padding-right: 42px;margin-top: 15px;margin-bottom: 0px;}
.p16{text-align: left;padding-right: 42px;margin-top: 13px;margin-bottom: 0px;}
.p17{text-align: left;padding-right: 41px;margin-top: 14px;margin-bottom: 0px;}
.p18{text-align: left;padding-right: 41px;margin-top: 0px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 193px;margin-top: 44px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 19px;padding-right: 76px;margin-top: 15px;margin-bottom: 0px;}
.p21{text-align: left;padding-left: 19px;margin-top: 13px;margin-bottom: 0px;}
.p22{text-align: left;padding-left: 19px;padding-right: 42px;margin-top: 1px;margin-bottom: 0px;}
.p23{text-align: left;padding-left: 19px;margin-top: 14px;margin-bottom: 0px;}
.p24{text-align: left;padding-left: 19px;margin-top: 15px;margin-bottom: 0px;}
.p25{text-align: left;padding-left: 19px;padding-right: 42px;margin-top: 14px;margin-bottom: 0px;}
.p26{text-align: left;padding-left: 98px;margin-top: 40px;margin-bottom: 0px;}
.p27{text-align: left;padding-right: 41px;margin-top: 117px;margin-bottom: 0px;}
.p28{text-align: left;padding-right: 41px;margin-top: 3px;margin-bottom: 0px;}
.p29{text-align: left;padding-right: 76px;margin-top: 14px;margin-bottom: 0px;}
.p30{text-align: justify;padding-right: 41px;margin-top: 14px;margin-bottom: 0px;}
.p31{text-align: justify;padding-right: 42px;margin-top: 13px;margin-bottom: 0px;}
.p32{text-align: left;margin-top: 13px;margin-bottom: 0px;}
.p33{text-align: left;padding-left: 59px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p34{text-align: left;padding-left: 41px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p35{text-align: left;padding-left: 300px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p36{text-align: left;padding-left: 221px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p37{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p38{text-align: center;padding-right: 16px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p39{text-align: center;padding-left: 43px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p40{text-align: center;padding-right: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p41{text-align: center;padding-left: 40px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p42{text-align: left;padding-left: 79px;margin-top: 35px;margin-bottom: 0px;}
.p43{text-align: left;padding-left: 129px;padding-right: 153px;margin-top: 0px;margin-bottom: 0px;text-indent: -8px;}

.td0{padding: 0px;margin: 0px;width: 326px;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;width: 180px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 31px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
.td4{padding: 0px;margin: 0px;width: 355px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;width: 599px;vertical-align: bottom;}

.tr0{height: 18px;}
.tr1{height: 35px;}
.tr2{height: 32px;}
.tr3{height: 31px;}
.tr4{height: 38px;}
.tr5{height: 7px;}
.tr6{height: 39px;}
.tr8{height: 13px;}
.tr9{height: 46px;}
.tr10{height: 29px;}

.t0{width: 537px;margin-left: 9px;margin-top: 45px;font: bold 13px 'Arial';}
.t1{width: 599px;margin-left: 37px;margin-top: 62px;font: 11px 'Arial';}
.ini{margin-top: 60px;}
</style>
</head>

<body>
<div id="page_1">
<div id="p1dimg1" style="margin-left:70px;">
<img src="http://localhost/sistemandp/public/assets/images/LogoSempreHumana.png" id="p1img1"></div>


<p class="p0 ft0">CONVÊNIO UNIDADE CONCEDENTE – NUDEP</p>
<table cellpadding="0" cellspacing="0" class="ini">
<tbody><tr>
	<td class="tr0 td0"><p class="p1 ft1">Recife, de 20.</p></td>
	<td class="tr0 td1"><p class="p1 ft2">&nbsp;</p></td>
	<td class="tr0 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr1 td0"><p class="p1 ft3">Empresa: <?php echo $dados[0]->sempemrazao; ?></p></td>
	<td class="tr1 td1"><p class="p1 ft2">&nbsp;</p></td>
	<td class="tr1 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr2 td0"><p class="p1 ft3">Ramo de atuação: <?php echo $dados[0]->sempemativi; ?></p></td>
	<td class="tr2 td1"><p class="p1 ft2">&nbsp;</p></td>
	<td class="tr2 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr3 td0"><p class="p1 ft3">Endereço: <?php echo $dados[0]->sempemlogra; ?></p></td>
	<td class="tr3 td1"><p class="p1 ft3">Bairro: <?php echo $dados[0]->sempembairr;  ?></p></td>
	<td class="tr3 td2"><p class="p1 ft3">CEP: <?php echo $dados[0]->sempemcep; ?></p></td>
</tr>
<tr>
	<td class="tr3 td0"><p class="p1 ft3">Cidade: <?php echo $dados[0]->sempemcidad; ?></p></td>
	<td rowspan="2" class="tr4 td1"><p class="p1 ft3">Fone: <?php echo $dados[0]->sempemtel01; ?></p></td>
	<td class="tr3 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr5 td0"><p class="p1 ft4">&nbsp;</p></td>
	<td class="tr5 td2"><p class="p1 ft4">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr6 td0"><p class="p1 ft3">CNPJ/MF: <?php echo $dados[0]->sempemcnpj; ?></p></td>
	<td class="tr6 td1"><p class="p1 ft2">&nbsp;</p></td>
	<td class="tr6 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
<tr>
	<td class="tr6 td0"><p class="p1 ft3">Representada por: <?php echo $dados[0]->sempemrepre; ?></p></td>
	<td class="tr6 td1"><p class="p1 ft3">Cargo: <?php echo $dados[0]->sempemrecar; ?><span class="ft1">Sócio</span></p></td>
	<td class="tr6 td2"><p class="p1 ft2">&nbsp;</p></td>
</tr>
</tbody></table>
<p class="p2 ft3">Contato financeiro: <nobr>E-mail:</nobr></p>
<p class="p3 ft3">Fone: (81) Responsável:</p>
<p class="p4 ft1">E o <span class="ft3">NUDEP - Núcleo de Desenvolvimento Profissional Ltda</span>., empresa de direito privado, com sede à Rua João Fernandes Vieira, 574 Sala 602 - Boa Vista - Recife - PE, CEP <nobr>50050-200,</nobr> fone (81) <nobr>3221-4740</nobr> - Inscrita no CNPJ nº <nobr>06.195.488/0001-36,</nobr> representada por <span class="ft3">Iracilda Portella e Andréa Portella Ferreira Gomes Ugarte, </span>Sócias – Diretoras. <span class="ft3">Representante legal para administração de convênios e contratos: Paula Patrícia do Nascimento Correia Cargo: Analista do setor financeiro </span>celebram este convênio, entre si, estipulando as cláusulas e condições seguintes:</p>
<p class="p5 ft3">CLÁUSULA 1ª</p>
<p class="p6 ft1">Este convênio tem como objetivo a cooperação mútua, visando o desenvolvimento de ações conjuntas capazes de possibilitar a plena operacionalização <span class="ft3">da Lei 11.788 de 25/09/2008, </span>que permite a realização de estágios de estudantes, de interesse curricular, obrigatório ou não obrigatório, capazes de complementar o processo de ensino- aprendizagem;</p>
<p class="p7 ft1"><span class="ft3">Art- 1º </span>- Fica o <span class="ft3">NUDEP, </span>no seu papel de interveniente, autorizado a representar formalmente, a <span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO</span>, junto às <span class="ft5">INSTITUIÇÕES DE ENSINO</span>, para executar os procedimentos de caráter legal, técnico, burocrático e administrativo, necessários à realização de estágios.</p>
<p class="p8 ft1"><span class="ft3">Art- 2º - </span>O estágio representa a oportunidade que a <span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO </span>oferece aos estudantes para que eles obtenham em suas dependências a prática dos ensinamentos adquiridos na <span class="ft5">INSTITUIÇÃO DE ENSINO</span>.</p>
<p class="p9 ft1"><span class="ft3">Art- 3º - </span>Essa oportunidade é representada pelo conjunto de fatores técnicos, instrumentais, humanos, físicos e culturais que são colocados à sua disposição para complementação da educação.</p>
<p class="p10 ft3" style="margin-top: 180px;">COMPETE AO NUDEP:</p>
<p class="p3 ft1"><span class="ft3">CLÁUSULA 2ª: </span>Para cumprir o estabelecido na cláusula 1º caberá ao <span class="ft3">NUDEP</span>:</p>
<p class="p8 ft1"><span class="ft1">a)</span><span class="ft8">- celebrar convênios específicos com as </span><span class="ft5">INSTITUIÇÕES DE ENSINO</span>, constando às condições exigidas pelas mesmas, a fim de definir e caracterizar os estágios de seus alunos;</p>
<p class="p8 ft1"><span class="ft1">b)</span><span class="ft9">- comunicar à </span><span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO</span>, as condições citadas na alínea “A”;</p>
<p class="p8 ft1"><span class="ft1">c)</span><span class="ft9">- obter da </span><span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO </span>o perfil das vagas de estágio a serem disponibilizadas, identificando os respectivos cursos;</p>
<p class="p8 ft1"><span class="ft1">d)</span><span class="ft9">- procurar ajustar as condições de estágio, estabelecidas pela Instituição de Ensino, com as condições estabelecidas pela </span><span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO</span>, explicitando as principais atividades a serem desenvolvidas pelo estagiário, analisando sempre a compatibilidade com o contexto básico da profissão ao qual o curso se refere;</p>
<p class="p8 ft1"><span class="ft1">e)</span><span class="ft8">- enviar à </span><span class="ft5">UNIDADE CONCEDENTE DE ESTÁGIO</span>, estudantes cadastrados no <span class="ft3">NUDEP</span>, em condições de atenderem ao perfil da vaga de estágio existente;</p>
<p class="p8 ft1"><span class="ft1">f)</span><span class="ft9">- providenciar os instrumentos jurídicos exigidos no inciso II do caput do art. 3º, art 5º. Art 8º. e parágrafo único do art. 7º da Lei 11.788 /08, de 25/09/2008.</span></p>
<p class="p8 ft1"><span class="ft1">g)</span><span class="ft10">- desenvolver esforços para que, o Termo de Compromisso de Estágio e/ou Termo Aditivo seja assinado pelo ESTUDANTE, </span>UNIDADE CONCEDENTE DE ESTÁGIO <span class="ft1">e a interveniência da </span>INSTITUIÇÃO DE ENSINO<span class="ft1">;</span></p>
<p class="p8 ft1"><span class="ft1">h)</span><span class="ft10">- providenciar toda a documentação legal referente ao estágio, efetivando o respectivo Seguro Contra Acidentes Pessoais em favor dos estagiários, de acordo com apólice nº </span><span class="ft3">500390 </span>sob a responsabilidade do grupo Sul América Seguros de Vida e Previdência S/A.</p>
<p class="p19 ft3">COMPETE A UNIDADE CONCEDENTE DE ESTÁGIO:</p>
<p class="p3 ft1"><span class="ft3">CLÁUSULA 3ª: </span><span class="ft1">Para cumprir o estabelecido na Lei 11.788 de 25/09/2008 competirá à </span>UNIDADE CONCEDENTE DE ESTÁGIO<span class="ft1">:</span></p>
<p class="p30 ft1 ft3">Art 1º:</p>
<p class="p8 ft1"><span class="ft1">(a)</span><span class="ft10">- formalizar as vagas de estágio, estabelecendo junto ao </span><span class="ft3">NUDEP </span>as condições exigidas pela Instituição de Ensino;</p>
<p class="p8 ft1"><span class="ft1">(b)</span><span class="ft12">- atender os estudantes encaminhados pelo </span><span class="ft3">NUDEP</span>, <nobr>orientando-os</nobr> sobre as condições de realização do estágio;</p>
<p class="p8 ft1"><span class="ft1">(c)</span><span class="ft9">- relacionar os nomes dos estudantes selecionados, passando a informação para o </span><span class="ft3">NUDEP</span>;</p>
<p class="p8 ft1"><span class="ft1">(d)</span><span class="ft9">- firmar com o estudante o Termo de Compromisso de Estágio, com a interveniência e assinatura da Instituição de Ensino, conforme inciso II do caput do art. 3º;</span></p>
<p class="p8 ft1"><span class="ft1">(e)</span><span class="ft13">- sistematizar a forma de acompanhamento, supervisão e avaliação do estagiário, informando, quando necessário, dados diretamente às instituições de ensino ou através do </span><span class="ft3">NUDEP;</span></p>
<p class="p8 ft1"><span class="ft1">(f)</span><span class="ft14">- indicar um funcionário de seu quadro de pessoal com formação ou experiência profissional na área de conhecimento do curso do estagiário;</span></p>
<p class="p8 ft1"><span class="ft1">(g)</span><span class="ft8">- fornecer recesso proporcional remunerado, de acordo com o art. 13, da Lei 11.788/08, caso o estágio tenha duração igual ou superior a 1 (um) ano;</span></p>
<p class="p8 ft1"><span class="ft1">(h)</span><span class="ft9">- fornecer auxílio transporte ao estagiário na hipótese de estágio não obrigatório;</span></p>
<p class="p8 ft1"><span class="ft1">(i)</span><span class="ft9">- efetuar o pagamento da Bolsa - Auxílio do estagiário até o décimo dia do mês subsequente ao exercício do estágio;</span></p>
<p class="p8 ft1"><span class="ft1">(j)</span><span class="ft9">- transferir mensalmente ao </span><span class="ft11">NUDEP</span>, através de rede bancária, o valor de R$ 65,00 (sessenta e cinco reais) por estagiário. Em caso de atraso na efetivação do pagamento será acrescida a multa e juros sobre o valor total do boleto, conforme descriminado no mesmo.</p>
<p class="p8 ft1"><span class="ft1">(k)</span><span class="ft9">Empresas novas ou reativação de convênio com mais de 2 (dois) anos inativa, o primeiro mês é cobrado de forma integral ou retroativa a data de início do estágio, até o dia 30 do mês vigente.</span></p>
<div class="dclr"></div>
<p class="p29 ft1" style="margin-top: 20px;"><span class="ft3">Art 2º</span>- Feita a contratação do estagiário, caso haja rescisão do contrato por qualquer uma das partes, antes de trinta dias, a taxa administrativa será paga integralmente pela organização ao NUDEP.</p>
<p class="p30 ft1"><span class="ft3">Art 3º</span>- Fica ratificado que o estagiário só deverá iniciar o estágio na UNIDADE CONCEDENTE após a INSTITUIÇÃO DE ENSINO assinar as vias do TERMO DE COMPROMISSO DE ESTÁGIO. Caso a UNIDADE CONCEDENTE, permita que o estágio seja iniciado e surja algum problema que impeça a assinatura do termo, a taxa administrativa do NUDEP deve ser paga normalmente, uma vez que o mesmo cumpriu todas as obrigações legais constantes no convênio, legalmente assinado.</p>
<p class="p7 ft1"><span class="ft3">Parágrafo único - </span>O não cumprimento das letras “i” e “j” desta cláusula, por parte da unidade concedente, implicará na rescisão deste convênio e, assim, a unidade concedente perderá a cobertura legal pela manutenção de seus estagiários;</p>
<p class="p9 ft1"><span class="ft3">CLÁUSULA 4ª: </span>Fica a critério da unidade concedente, reajustar o valor da <nobr>Bolsa-Auxílio,</nobr> de acordo com a legislação em vigor,</p>
<p class="p8 ft1"><span class="ft3">CLÁUSULA 5ª: </span>O <span class="ft3">NUDEP </span>poderá executar outras atividades para a unidade concedente, de acordo com entendimentos mantidos entre si, estabelecidos mediante estudos específicos à configuração técnica, quantificação de recursos humanos e financeiros necessários.</p>
<p class="p8 ft1"><span class="ft3">CLÁUSULA 6ª: </span>O presente convênio tem prazo indeterminado, podendo a qualquer momento ser rescindido, por qualquer uma das partes, mediante comunicado por escrito, com antecedência mínima de 30 dias, ou automaticamente, em casos de descumprimento de qualquer uma das cláusulas aqui estabelecidas.</p>
<p class="p31 ft1"><span class="ft3">CLÁUSULA 7ª: </span>Qualquer débito existente entre as partes, deverão ser liquidados até a data do encerramento. Caso não exista a quitação dos débitos, fica o NUDEP no direito de encaminhar a situação para o jurídico tomar as decisões que serão orientadas e seguidas.</p>
<p class="p32 ft1">E por estarem de comum acordo as partes assinam o presente convênio em 2 (duas) vias de igual teor.</p>
<table cellpadding="0" cellspacing="0" class="t1">
<tbody><tr>
	<td class="td3"><p class="p1 ft5">____________________________________________</p></td>
	<td class="td4"><p class="p33 ft5">____________________________________________</p></td>
</tr>
<tr>
	<td class="tr8 td3"><p class="p34 ft18">UNIDADE CONCEDENTE</p></td>
	<td class="tr8 td4"><p class="p35 ft18">REPRESENTANTE NUDEP</p></td>
</tr>
<tr>
	<td colspan="2" class="tr9 td5"><p class="p36 ft19">TESTEMUNHAS</p></td>
</tr>
<tr>
	<td class="tr1 td4"><p class="p33 ft20">_____________________________________________________</p></td>
	<td class="tr1 td4"><p class="p33 ft20">_____________________________________________________</p></td>
</tr>
<tr>
	<td class="td3"><p class="p38 ft5">NOME</p></td>
	<td class="td4"><p class="p39 ft5">NOME</p></td>
</tr>
<tr>
<td class="tr1 td4"><p class="p33 ft20">_____________________________________________________</p></td>
<td class="tr1 td4"><p class="p33 ft20">_____________________________________________________</p></td>
</tr>
<tr>
	<td class="tr0 td3"><p class="p40 ft5">CPF</p></td>
	<td class="tr0 td4"><p class="p41 ft5">CPF</p></td>
</tr>
</tbody></table>
</div>


</body></html>