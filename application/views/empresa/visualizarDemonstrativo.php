	<?php 
// var_dump($estudante);die();
$mes = date('m'); // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0,0,0,$mes,'01',$ano)); // Mágica, plim!

// $lastmes = date('m', strtotime('-1 months')); // Não estava pegando.
$lastmes = date('m', strtotime('first day of -1 months'));

 ?>
	<!-- Both borders -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<?php if(checarPermissao('aAdministrativo')){ ?>
				<div class="col-md-3 col-sm-6 row">
					<a class="btn btn-success btn-lg" href="<?php echo base_url(); ?>relatorios/imprimirdemonstrativo/<?php echo $estudante[$contrato[0]->palualcodig]->pempemcodig; ?>">Imprimir <i class="icon-printer position-left"></i></a>
					<a class="btn btn-info btn-lg" href="<?php echo base_url(); ?>relatorios/imprimirdemonstrativo/<?php echo $estudante[$contrato[0]->palualcodig]->pempemcodig; ?>/true ">Enviar <i class="icon-envelop2 position-left"></i></a>
				</div>
			<?php } ?>
			<br/>
			<br/>			
			<h5 class="panel-title text-center">DEMONSTRATIVO DE ESTUDANTE EM ESTÁGIO</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>
		<br/>
		<br/>

		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarDemostrativoExe" method="post" enctype="multipart/form-data">
		<div class="panel-body">
			Discriminamos., através deste, o valor de <code><span id="totalServico"></span></code> ( <span id="totalExtenso"></span> ) recorrente à Empresa: <code><?php echo $contrato[0]->sempemrazao; ?></code>  referente à taxa alusiva aos estagiários relacionados abaixo.	     

		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Código</th>
						<th>Estagiário</th>
						<th>Periodo Cobrança</th>
						<th>Valor</th>
						<th>Início Contrato</th>
						<th>Término Contrato</th>
						<th>Vinculo/Dias</th>
					</tr>
				</thead>
				<tbody>

					<?php

						$contador = 1;
						$cobrado = 0;
						$ativo = 0; 
						$totalServico = 0;

					foreach ($contrato as $valor) { 

						// Contrato pós o mês atual não entra no demonstrativo - 30052022
						// if ($estudante[$valor->palualcodig]->dataInicio > date('Y-m-d') ) { 
						if (date('Y-m', strtotime($estudante[$valor->palualcodig]->dataInicio))  > date('Y-m') ) {
				    		continue;
						}	  

						if ($valor->contrestempr_status == 'Ativo') {						
						$ativo++;							
						} else {
						$cobrado++;						
						}

						$retVal = ($estudante[$valor->palualcodig]->valorTotal >= ($estudante[$valor->palualcodig]->sempemvalor * 2)) ? true : false ;

						if (!$retVal) {
						$ParVal = ($estudante[$valor->palualcodig]->valorTotal > ($estudante[$valor->palualcodig]->sempemvalor)) ? true : false ;
						}else{
						$ParVal = false;	
						}
						

						
						$totalServico = $totalServico + $estudante[$valor->palualcodig]->valorTotal;

						?>

						<?php if ($valor->palualcodig = $estudante[$valor->palualcodig]->palualcodig ) { ?>
						<tr>							
							<td><?php echo $contador++; ?></td> <!-- Código -->
							<?php if ($valor->contrestempr_status == 'Rescindido') { ?>			
							<td><code><?php echo $valor->salualnome; ?></code>
							<?php } else { ?>
							<td><?php echo $valor->salualnome; ?>
							<?php } ?>

							<?php if ($retVal OR $ParVal) { ?>
								<div class="btn-group">
									<a  class="badge badge-danger dropdown-toggle" data-toggle="dropdown">. <span class="caret"></span></a>

									<?php if ($ParVal) { ?>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="<?php echo base_url()?>empresa/criarDemonstrativo/<?php echo $valor->palualcodig.'/'.$estudante[$valor->palualcodig]->pempemcodig ?>"><span class="status-mark position-left border-warning"></span> Faturar Retroativo</a></li>
									</ul>
									<?php } ?>
									<?php if ($retVal) { ?>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="<?php echo base_url()?>empresa/criarDemonstrativo/<?php echo $valor->palualcodig.'/'.$estudante[$valor->palualcodig]->pempemcodig ?>"><span class="status-mark position-left border-danger"></span> Faturar Mês Anterior</a></li>
									</ul>
									<?php } ?>	
								</div>								
							<?php } ?>	
		
							</td> <!-- Estagiário -->
							<td><?php echo date('d/m/y', strtotime($estudante[$valor->palualcodig]->dataInicio)).' a '.date('d/m/y', strtotime($estudante[$valor->palualcodig]->datafim)); ?></td> <!-- Periodo contrato rescindido-->
							<td><?php echo 'R$ '. number_format($estudante[$valor->palualcodig]->valorTotal, 2, ".", ""); ?></td>  <!-- Valor -->							
							
							<td><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->contrestempr_data_ini )); ?></td>

							<?php if ($estudante[$valor->palualcodig]->termino < date('Y-m-d') OR $valor->contrestempr_status == 'Rescindido') { ?>
							    <td><code><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></code></td> <!-- Término para contrato (Vencido) -->
							<?php } else { ?> 
								<td><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></td> <!-- Término contrato -->
							<?php } ?> 	

							<?php 

							$dataInicio = $estudante[$valor->palualcodig]->contrestempr_data_ini;
							$dataFim = $estudante[$valor->palualcodig]->datafim;

							// Criar objetos DateTime a partir das strings de data
							$inicio = new DateTime($dataInicio);
							$fim = new DateTime($dataFim);

							// Calcular a diferença entre as datas
							$intervalo = $inicio->diff($fim);

							// Acessar o resultado em dias
							$intervaloEmDias = $intervalo->days + 1;
													
         					?>

         					<td><?php echo $intervaloEmDias.' Dias' ?></td>

        				</tr>
							
						<?php } ?>

					<?php } ?>	
						

				</tbody>
			</table>		
		</div>

		<div class="panel-body">
			<?php 
			$totalEstorno = 0;
			if (count($estorno) >= 1) {  ?>	

				<label>Estorno:</label><br />
				<?php				
				foreach ($estorno as $valor) {

				$totalEstorno = $totalEstorno + $valor->estorno;

				?>				
				<div class="form-control-static">
					<?php echo $valor->salualnome; ?> -  Estorno <span><code><?php echo 'R$ '. number_format($valor->estorno, 2, ".", ""); ?></span></code>, Foi cobrado mês: <?php echo $lastmes; ?> rescindido no dia: <?php echo date('d/m/Y', strtotime($valor->contrestempr_data_rescisao ));  ?> .<br />	     
				</div>
				<?php } ?>
			<?php } ?>
			
            
		</div>

        <div class="panel-body">
			<label  class="control-label col-lg-2">Nota Fiscal:</label>
            <input  type="text" placeholder="Nº nota Fiscal" class="form-control"  name="demonstrativoempr_notafiscal" id="demonstrativoempr_notafiscal" value="<?php echo $contrato[0]->demonstrativoempr_notafiscal; ?>">
          <?php echo form_error('demonstrativoempr_notafiscal'); ?>
		</div>

		<div class="panel-body">
			<label  class="control-label col-lg-2">Observações:</label>
            <textarea class="form-control" rows="4" id="demonstrativoempr_obs" name="demonstrativoempr_obs" ><?php echo $contrato[0]->demonstrativoempr_obs; ?></textarea>
            <?php echo form_error('demonstrativoempr_obs'); ?>
		</div>

	<!-- 	    <div class="text-right">
				<button type="submit" class="btn bg-teal">Faturar <i class="icon-arrow-right14 position-right"></i></button>
			</div> -->

			<div class="text-right">		

				<?php if ($contrato[0]->demonstrativoempr_faturado == '0') { ?>
				<a type="button" href="<?php echo base_url()?>empresa/faturarDemonstrativo/<?php echo $totalServico.'/'.$estudante[$valor->palualcodig]->pempemcodig ?>" class="btn bg-slate-400">Faturar <i class="icon-cash3 position-right"></i></a>	
				<?php } else{ ?>
				<button type="submit" disabled="false" class="btn bg-slate-400">Faturado <i class="icon-cash3 position-right"></i></button>
				<?php } ?>

				<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>

			</div>



	    <input  type="hidden" name="demonstrativoempr_qtd_estud_cobrado" value="<?php echo $ativo + $cobrado ?>" />
	    <input  type="hidden" name="demonstrativoempr_qtd_estud_ativo" value="<?php echo $ativo ?>" />
	    <input  type="hidden" name="demonstrativoempr_valor_taxa" value="<?php echo $contrato[0]->sempemvalor; ?>" />
	    <input  type="hidden" name="demonstrativoempr_valor_cobrado" value="<?php echo $totalServico; ?>" />
	    <input  type="hidden" name="demonstrativoempr_cod_estudante" value="<?php echo $contrato[0]->grupo; ?>" />
	    <input  type="hidden" name="demonstrativoempr_empresa_id" value="<?php echo $this->uri->segment(3); ?>" />	

		</form>
		<br/>
		<br/>


	</div>
	<!-- /both borders -->

	<?php   $Totalextenso =  extenso($totalServico - $totalEstorno); ?>

	<script type="text/javascript">

	var valor = '<?php echo  "R$". number_format($totalServico - $totalEstorno , 2, ",", ".");  ?>';
	var valorExtenso =  '<?php echo  $Totalextenso;  ?>';

	$("#totalServico").text(valor);
	$("#totalExtenso").text(valorExtenso);


	</script>