<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aEmpresa')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>Nome Fantasia</th>	
				<th>Cnpj</th>					
				<th>Contato</th>
				<th>Email</th>
				<th>Responsável</th>
				<th>Cadastro</th>
				<th>Situação</th>								
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { 
				 $dataOperacao = date(('d/m/Y'),strtotime($valor->date_operacao));
				 ?>
				
				<tr>     								
					<td><?php echo $valor->sempemfanta; ?></td>
					<td><?php echo $valor->sempemcnpj; ?></td> 					
					<td><?php echo $valor->sempemtel01; ?></td>
					<td><?php echo $valor->sempememail; ?></td>
					<td><?php echo $valor->sempemrespo; ?></td>

					<td><?php echo $dataOperacao; ?></td>

					<?php if($valor->situacao == 'Ativo'){ ?> 	
					<td><span class="label label-success">Ativa</span></td>
					<?php } else if($valor->situacao == 'Inativo'){ ?> 
					<td><span class="label label-danger">Inativo</span></td>
					<?php } else if($valor->situacao == 'Alerta'){ ?> 
					<td><span class="label label-info label-rounded"><?php echo $valor->total.' - ALERTA' ?> </span></td>
					<?php } ?>							
														
					<td>																				
						<ul class="icons-list">
								<?php if(checarPermissao('eEmpresa')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->pempemcodig; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dEmpresa')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->pempemcodig; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vEmpresa')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->pempemcodig; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>
								<?php if(checarPermissao("vEmpresa")){ ?>						
								<li class="text-teal-900"><a target="_blank" href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizarconvenio/<?php echo $valor->pempemcodig; ?>" data-popup="tooltip" title="Baixar Convenio"><i class="icon-download"></i></a></li>	
								<?php } ?>																					
							
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->