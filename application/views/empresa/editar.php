<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Empresa</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Empresa:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="pempemcodig" value="<?php echo $dados[0]->pempemcodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">CNPJ/CPF:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Cnpj/Cpf"  name="sempemcnpj" id="sempemcnpj" value="<?php echo $dados[0]->sempemcnpj; ?>">
					<?php echo form_error('sempemcnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Empresa:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Empresa / Fantasia" name="sempemfanta" id="sempemfanta" value="<?php echo $dados[0]->sempemfanta; ?>">
					<?php echo form_error('sempemfanta'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Razão Social:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Razão" name="sempemrazao" id="sempemrazao" value="<?php echo $dados[0]->sempemrazao; ?>">
					<?php echo form_error('sempemrazao'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Mantededora:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Mantededora" name="sempemmantededora" id="sempemmantededora" value="<?php echo $dados[0]->sempemmantededora; ?>">
					<?php echo form_error('sempemmantededora'); ?>
					</div>										
				</div>
				


				<legend class="text-bold">Dados Comerciais:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Rua/Av/Trav" name="sempemlogra" id="sempemlogra" value="<?php echo $dados[0]->sempemlogra; ?>">
					<?php echo form_error('sempemlogra'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempemnumer" id="sempemnumer" value="<?php echo $dados[0]->sempemnumer; ?>">
					<?php echo form_error('sempemnumer'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempemcompl" id="sempemcompl" value="<?php echo $dados[0]->sempemcompl; ?>">
					<?php echo form_error('sempemcompl'); ?>
					</div>										
				</div>
<!--
				<div class="form-group">
                	<label class="control-label col-lg-2">UF:</label>
                	<div class="col-lg-5">
                        <select disabled  class="form-control" name="estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sempemuf)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select disabled class="form-control" name="cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('cidade'); ?>	
                    </div>				                            	                            
				</div> -->

				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempembairr" id="sempembairr" value="<?php echo $dados[0]->sempembairr;  ?>">
					<?php echo form_error('sempembairr'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempemcidad" id="sempemcidad" value="<?php echo $dados[0]->sempemcidad; ?>">
					<?php echo form_error('sempemcidad'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Estado:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempemuf" id="sempemuf" value="<?php echo $dados[0]->sempemuf; ?>">
					<?php echo form_error('sempemuf'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="sempemcep" id="sempemcep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->sempemcep; ?>">
					<?php echo form_error('sempemcep'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel01" id="sempemtel01" value="<?php echo $dados[0]->sempemtel01; ?>">
					<?php echo form_error('sempemtel01'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular I:</label>
					<div class="col-lg-5">
						<input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel02" id="sempemtel02" value="<?php echo $dados[0]->sempemtel02; ?>">
					<?php echo form_error('sempemtel02'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel03" id="sempemtel03" value="<?php echo $dados[0]->sempemtel03; ?>">
					<?php echo form_error('sempemtel03'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Mala Direta:</label>
					<div class="col-lg-5">
						<input  type="text" placeholder="seu@email.com" class="form-control" name="sempememail" id="sempememail" value="<?php echo $dados[0]->sempememail; ?>">
					<?php echo form_error('sempememail'); ?>
					</div>										 
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Responsável:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="sempemrespo" id="sempemrespo" value="<?php echo $dados[0]->sempemrespo; ?>">
					<?php echo form_error('sempemrespo'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Cargo Responsável:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Cargo Responsável"  data-mask-selectonfocus="true" name="sempemcargo" id="sempemcargo" value="<?php echo $dados[0]->sempemcargo; ?>">
					<?php echo form_error('sempemcargo'); ?>
					</div>										
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Representante:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Representante"  data-mask-selectonfocus="true" name="sempemrepre" id="sempemrepre" value="<?php echo $dados[0]->sempemrepre; ?>">
					<?php echo form_error('sempemrepre'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Cargo do Representante:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Cargo do Representante"  data-mask-selectonfocus="true" name="sempemrecar" id="sempemrecar" value="<?php echo $dados[0]->sempemrecar; ?>">
					<?php echo form_error('sempemrecar'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">CPF do Representante:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="CPF do Representante"  data-mask="999.999.999-99" data-mask-selectonfocus="true" name="sempemrecpf" id="sempemrecpf" value="<?php echo $dados[0]->sempemrecpf; ?>">
					<?php echo form_error('sempemrecpf'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Ramo de Atividade:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Ramo de Atividade"  data-mask-selectonfocus="true" name="sempemativi" id="sempemativi" value="<?php echo $dados[0]->sempemativi; ?>">
					<?php echo form_error('sempemativi'); ?>
					</div>										
				</div>

				<legend class="text-bold">Dados Financeiro:</legend>

				<div class="form-group">
		          <label class="control-label col-lg-2">Cód Sacado:</label>
		          <div class="col-lg-5">
		            <input  type="text" placeholder="Código do sacado" class="form-control"  name="sempemcodsacado" id="sempemcodsacado" value="<?php echo $dados[0]->sempemcodsacado; ?>">
		          <?php echo form_error('sempemcodsacado'); ?>
		          </div>                     
		        </div>

				<div class="form-group">
		          <label class="control-label col-lg-2">Vecimento do Contrato:</label>
		          <div class="col-lg-5">
		            <input  type="number" maxlength="2" placeholder="Vecimento do contrato" class="form-control"  name="sempemvenccontrato" id="sempemvenccontrato" value="<?php echo $dados[0]->sempemvenccontrato; ?>">
		          <?php echo form_error('sempemvenccontrato'); ?>
		          </div>                     
		        </div>

		        <div class="form-group">
		          <label class="control-label col-lg-2">Valor UND Contrato:</label>
		          <div class="col-lg-5">
		            <input  type="text" placeholder="R$ und contrato" class="form-control"  name="sempemvalor" id="sempemvalor" value="<?php echo $dados[0]->sempemvalor; ?>">
		          <?php echo form_error('sempemvalor'); ?>
		          </div>                     
		        </div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Financeiro:</label>
					<div class="col-lg-5">
						<input  type="text" placeholder="seu@email.com" class="form-control" name="sempememailgestor" id="sempememailgestor" value="<?php echo $dados[0]->sempememailgestor; ?>">
					<?php echo form_error('sempememailgestor'); ?>
					</div>										 
				</div>						


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>


	<script type="text/javascript">

	$(document).ready(function(){

	  $("#sempemcnpj").focusout(function(){  

	      try {
	        $("#sempemcnpj").unmask();
	      } catch (e) {}
	      
	      var tamanho = $("#sempemcnpj").val().length;
	    
	      if(tamanho == 11 ){                                 
	          $("#sempemcnpj").mask("999.999.999-99");
	      } else if(tamanho == 14){
	          $("#sempemcnpj").mask("99.999.999/9999-99");
	      }            
	   
	  });


	});
	
	</script>
