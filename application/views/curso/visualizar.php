<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Curso</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>curso/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Curso:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="pcurcucodig" value="<?php echo $dados[0]->pcurcucodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Curso:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Curso" name="scurcunome" id="scurcunome" value="<?php echo $dados[0]->scurcunome; ?>">
					<?php echo form_error('scurcunome'); ?>
					</div>										
				</div>	

			<!-- 	<div class="form-group">
                	<label class="control-label col-lg-2">Nível:</label>
                	<div class="col-lg-5">
                        <select  disabled class="form-control" name="scurcunivel" id="scurcunivel">
                        	<option value="">Selecione</option>
                            <option value="Medio" <?php echo ($dados[0]->scurcunivel == 'Medio')?'selected':''; ?>>Médio</option>
                            <option value="Tecnico" <?php echo ($dados[0]->scurcunivel == 'Tecnico')?'selected':''; ?>>Técnico</option> 
                            <option value="Superior" <?php echo ($dados[0]->scurcunivel == 'Superior')?'selected':''; ?>>Superior</option>  
                            <option value="Pos" <?php echo ($dados[0]->scurcunivel == 'Pos')?'selected':''; ?>>Pós Graduação</option>                        
                                                      
                        </select>
                        <?php echo form_error('scurcunivel'); ?>
                    </div>
                </div>	 -->		
								
		
            </fieldset>  
		</form>
	</div>
</div>
