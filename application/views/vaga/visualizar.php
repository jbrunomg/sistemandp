<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Visualizar Vaga</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<!-- <li><a data-action="reload"></a></li> -->
			                		<!-- <li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url();?>vaga/editarExe" method="post" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Dados Vaga:</legend>

									<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

									<input disabled type="hidden" name="pestvacodig" value="<?php echo $dados[0]->pestvacodig; ?>" />
 
									<div class="form-group">
										<label class="control-label col-lg-2">Atividade:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" placeholder="Atividade" name="atividade" id="atividade" value="<?php echo $dados[0]->sestvaativi; ?>">
										<?php echo form_error('atividade'); ?>
										</div>										
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Carga Horária:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" placeholder="Carga Horária" name="carHoraria" id="carHoraria" value="<?php echo $dados[0]->sestvacarho; ?>">
										<?php echo form_error('carHoraria'); ?>
										</div>										
									</div>

									<div class="form-group">
			                        	<label class="control-label col-lg-2">UF:</label>
			                        	<div class="col-lg-5">
				                            <select disabled  class="form-control" name="estado" id="estado" >
				                            	<option value="">Selecione</option>
				                                <?php foreach ($estados as $valor) { ?>
				                                	<?php $selected = ($valor->id == '16')?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
				  		                        <?php } ?>
				                            </select>
				                            <?php echo form_error('estado'); ?>
			                            </div>			                            
			                        </div>			                        
									<div class="form-group">
										<label class="control-label col-lg-2">Cidade:</label>
										<div class="col-lg-5">
				                            <select disabled class="form-control" name="cidade" id="cidade">
				                                
				                                <?php foreach ($cidades as $valor) { ?>
				                                	<?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
				  		                        <?php } ?>
				                            </select>
			                            <?php echo form_error('cidade'); ?>	
			                            </div>				                            	                            
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Bairro:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" name="bairro" id="bairro" value="<?php echo $dados[0]->sestvabairr;  ?>">
										<?php echo form_error('bairro'); ?>
										</div>										
									</div>


									<div class="form-group">
			                        	<label class="control-label col-lg-2">Sexo:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="sexo" id="sexo">
				                            	<option value="">Selecione</option>
				                                <option value="Masculino" <?php echo ($dados[0]->sestvasexo == 'Masculino')?'selected':''; ?>>Masculino</option>
				                                <option value="Feminino" <?php echo ($dados[0]->sestvasexo == 'Feminino')?'selected':''; ?>>Feminino</option>
				                                <option value="Ambos" <?php echo ($dados[0]->sestvasexo == 'Ambos')?'selected':''; ?>>Ambos</option>

				                                <option value="Ambos" <?php echo ($dados[0]->sestvasexo == 'Feminino / Masc')?'selected':''; ?>>Feminino / Masc </option>
				                                                          
				                            </select>
				                            <?php echo form_error('sexo'); ?>
			                            </div>
			                        </div>	

									<div class="form-group">
										<label class="control-label col-lg-2">Observação:</label>
										<div class="col-lg-5">
											<input disabled type="text" name="observacaovaga" class="form-control" placeholder="Observação Vaga"  data-mask-selectonfocus="true" name="observacaovaga" id="observacaovaga" value="<?php echo $dados[0]->observacaovaga; ?>">
										<?php echo form_error('observacaovaga'); ?>
										</div>										
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Data para expirar:</label>
										<div class="col-lg-5">																						
											<input type="text" class="form-control classData" name="testvaexpir" id="testvaexpir" value="<?php echo date('d/m/Y', strtotime($dados[0]->testvaexpir)); ?>">
										<?php echo form_error('testvaexpir'); ?>
										</div>										
									</div>
							<!-- 		<div class="form-group">
										<label class="control-label col-lg-2">Periodo:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" placeholder="Periodo" name="sestvaperio" id="sestvaperio" value="<?php echo $dados[0]->sestvaperio; ?>">
										<?php echo form_error('sestvaperio'); ?>
										</div>										
									</div> -->	

									<div class="form-group">
							            <label class="control-label col-lg-2">A partir:</label>
							            <div class="col-lg-10">
							              <div class="row">
							                <div class="col-md-3">
							                  <input disabled type="text" class="form-control" name="sestvaserie" id="sestvaserie" value="<?php echo $dados[0]->sestvaserie; ?>">
							                  <span class="help-block text-center">Série</span>
							                </div>

							                <div class="col-md-3">
							                  <input disabled type="text" class="form-control" name="sestvamodul" id="sestvamodul" value="<?php echo $dados[0]->sestvamodul; ?>">
							                  <span class="help-block text-center">Módulo</span>
							                </div>

							                <div class="col-md-3">
							                  <input disabled type="text" class="form-control" name="sestvaperio" id="sestvaperio" value="<?php echo $dados[0]->sestvaperio; ?>">
							                  <span class="help-block text-center">Período</span>
							                </div>
							              </div>
							            </div>
							        </div>									
								<!-- Possível Botão de Form dinamico 
									<div class="text-center">
									<button type="button" class="btn btn-primary">Próximo<i class="icon-arrow-right14 position-right"></i></button>
								</div>
								-->
								<legend class="text-bold">Benefícios:</legend>

								<div class="row">
								   	<div class="col-md-2">
							    		<div class="panel panel-body border-top-teal text-center">
											<h6 class="no-margin text-semibold">Bolsa</h6>
											<label class="checkbox-inline">

												<input disabled class="styled" name="bolsa" type="checkbox" value="1" <?php if($dados[0]->eestvabolsa <> 0){ echo 'checked';}else{ echo '';} ?>>
												Checked
											</label>
										
										</div>
							    	</div>
							    	<div class="col-md-2">
							    		<div class="panel panel-body border-top-teal text-center">
											<h6 class="no-margin text-semibold">Vale transporte</h6>
											<label class="checkbox-inline">
												<input disabled class="styled" name="vale" type="checkbox" value="1"  <?php if($dados[0]->eestvavale <> 0){ echo 'checked';}else{ echo '';} ?>>
												Checked							
											</label>
										</div>
							    	</div>
							    	<div class="col-md-2">
							    		<div class="panel panel-body border-top-teal text-center">
											<h6 class="no-margin text-semibold">Refeição</h6>
											<label class="checkbox-inline">
												<input disabled class="styled" name="refeicao" type="checkbox" value="1"   <?php echo ($dados[0]->eestvarefei == '1')?'checked':''; ?>>
												Checked
											</label>
										</div>
							    	</div>
							    	<div class="col-md-2">
							    		<div class="panel panel-body border-top-teal text-center">
											<h6 class="no-margin text-semibold">Outros</h6>
											<label class="checkbox-inline">
												<input disabled class="styled" name="outros" type="checkbox" value="1"   <?php echo ($dados[0]->eestvaoutros == '1')?'checked':''; ?>>
												Checked
											</label>
										</div>
							    	</div>
							    </div>	

<!-- 								<div class="form-group">
									<div class="col-lg-5">
									    <div class="radio"> 
	                                      <label><input disabled type="checkbox" name="bolsa" class="icheck" value="1" <?php echo ($dados[0]->eestvabolsa == '1')?'checked':''; ?>> Bolsa</label>
	                                    </div>
	                                    <div class="radio"> 
	                                      <label><input disabled type="checkbox" name="vale" class="icheck" value="1" <?php echo ($dados[0]->eestvavale == '1')?'checked':''; ?>> Vale transporte</label> 
	                                    </div>
	                                    <div class="radio"> 
	                                      <label><input disabled type="checkbox" name="refeicao" class="icheck" value="1" <?php echo ($dados[0]->eestvarefei == '1')?'checked':''; ?>> Refeição</label> 
	                                    </div>	                                 
									</div>
								</div> -->







								<legend class="text-bold">Curso Disponível:</legend>
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Curso 1:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="curso1" id="curso1">
				                           		<option  value="">Selecione</option>
				                                <?php foreach ($curso as $valor) { ?>
				                                	  <?php $selected = ($valor->pcurcucodig == $cursosSelecionados[0])?'SELECTED': ''; ?>
				  		                              <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
				  		                        <?php } ?>
				                               
				                            </select>
			                            </div>
			                        </div>	
			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Curso 2:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="curso2" id="curso2">
				                           		<option  value="">Selecione</option>
				                                  <?php foreach ($curso as $valor) { ?>
				                                	  <?php $selected = ($valor->pcurcucodig == $cursosSelecionados[1])?'SELECTED': ''; ?>
				  		                              <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
				  		                        <?php } ?>
				                               
				                            </select>
			                            </div>
			                        </div>	
			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Curso 3:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="curso3" id="curso3">
				                           		<option  value="">Selecione</option>
				                                <?php foreach ($curso as $valor) { ?>
				                                	  <?php $selected = ($valor->pcurcucodig == $cursosSelecionados[2])?'SELECTED': ''; ?>
				  		                              <option  value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
				  		                        <?php } ?>
				                               
				                               
				                            </select>
			                            </div>
			                        </div>	
			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Curso 4:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="curso4" id="curso4">
				                           		<option  value="">Selecione</option>
				                                <?php foreach ($curso as $valor) { ?>
				                                	  <?php $selected = ($valor->pcurcucodig == $cursosSelecionados[3])?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
				  		                        <?php } ?>
				                               
				                               
				                            </select>
			                            </div>
			                        </div>
			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Curso 5:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="curso5" id="curso5">
				                           		<option value="">Selecione</option>
				                                <?php foreach ($curso as $valor) { ?>
				                                	  <?php $selected = ($valor->pcurcucodig == $cursosSelecionados[4])?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->pcurcucodig; ?>" <?php  echo $selected; ?>><?php echo $valor->scurcunome; ?></option>
				  		                        <?php } ?>
				                               				  		            		
				                            </select>


			                            </div>
			                        </div>		
			                    </fieldset>  
							</form>
						</div>
					</div>
