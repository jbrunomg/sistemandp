<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Vagas</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados da vaga:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

					
          <div class="form-group">
            <label class="control-label col-lg-2">Empresa:</label>
            <div class="col-lg-5">
                  <select class="form-control" name="empresa" id="empresa">
                    <option value="">Selecione</option>
                      <?php foreach ($empresa as $valor) { ?>
                          <option value="<?php echo $valor->pempemcodig; ?>"><?php echo $valor->sempemrazao; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>  

          <div class="form-group">
						<label class="control-label col-lg-2">Atividade:</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" placeholder="Atividade" name="atividade" id="atividade" value="<?php echo set_value('atividade'); ?>">
						<?php echo form_error('atividade'); ?>
						</div>										
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Carga Horária:</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" placeholder="Carga Horária" name="carHoraria" id="carHoraria" value="<?php echo set_value('carHoraria'); ?>">
						<?php echo form_error('carHoraria'); ?>
						</div>										
					</div>


					<div class="form-group">
                    	<label class="control-label col-lg-2">UF:</label>
                    	<div class="col-lg-5">
                            <select  class="form-control" name="estado" id="estado" >
                            	<option value="">Selecione</option>
                                <?php foreach ($estados as $estado) { ?>
  		                              <option value="<?php echo $estado->id; ?>"><?php echo $estado->nome; ?></option>
  		                        <?php } ?>
                            </select>
                            <?php echo form_error('estado'); ?>
                        </div>			                            
                    </div>			                        
					<div class="form-group">
						<label class="control-label col-lg-2">Cidade:</label>
						<div class="col-lg-5">
                            <select class="form-control" name="cidade" id="cidade">
                                <option value="">Selecione</option>
                            </select>
                        <?php echo form_error('cidade'); ?>	
                        </div>				                            	                            
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Bairro:</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" name="bairro" id="bairro" value="<?php echo set_value('bairro'); ?>">
						<?php echo form_error('bairro'); ?>
						</div>										
					</div>

					<div class="form-group">
          	<label class="control-label col-lg-2">Sexo:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="sexo" id="sexo">
                  	<option value="">Selecione</option>
                      <option value="Masculino">Masculino</option>
                      <option value="Feminino">Feminino</option>
                      <option value="Ambos">Ambos</option>
                  </select>
              </div>
          </div>			

                    
					<div class="form-group">
						<label class="control-label col-lg-2">Observação:</label>
						<div class="col-lg-5">
							<input type="text" name="observacaovaga" class="form-control" placeholder="Observação Vaga"  data-mask-selectonfocus="true" name="observacaovaga" id="observacaovaga" value="<?php echo set_value('observacaovaga'); ?>">
						<?php echo form_error('observacaovaga'); ?>
						</div>										
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Data para expirar:</label>
						<div class="col-lg-5">																						
							<input type="text" class="form-control classData" name="testvaexpir" id="testvaexpir" value="<?php echo set_value('testvaexpir'); ?>">
						<?php echo form_error('testvaexpir'); ?>
						</div>										
					</div>
<!-- 					<div class="form-group">
						<label class="control-label col-lg-2">Periodo:</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" placeholder="Periodo" name="sestvaperio" id="sestvaperio" value="<?php echo set_value('sestvaperio'); ?>">
						<?php echo form_error('sestvaperio'); ?>
						</div>										
					</div> -->

          <div class="form-group">
            <label class="control-label col-lg-2">A partir:</label>
            <div class="col-lg-10">
              <div class="row">
                <div class="col-md-3">
                  <input type="text" class="form-control" name="sestvaserie" id="sestvaserie" value="<?php echo set_value('sestvaserie'); ?>">
                  <span class="help-block text-center">Série</span>
                </div>

                <div class="col-md-3">
                  <input type="text" class="form-control" name="sestvamodul" id="sestvamodul" value="<?php echo set_value('sestvamodul'); ?>">
                  <span class="help-block text-center">Módulo</span>
                </div>

                <div class="col-md-3">
                  <input type="text" class="form-control" name="sestvaperio" id="sestvaperio" value="<?php echo set_value('sestvaperio'); ?>">
                  <span class="help-block text-center">Período</span>
                </div>
              </div>
            </div>
          </div>

				<legend class="text-bold">Benefícios:</legend>

        <div class="row">
          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Bolsa</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="bolsa" value="1">  
                Checked
              </label>
            </div>
          </div>

         <div class="col-md-2">
              <div class="panel panel-body border-top-teal text-center">
                <h6 class="no-margin text-semibold">Valor-Auxílio</h6>
                <input type="text"  class="form-control input-xs" name="valor_auxilio" id="valor_auxilio" value="" >
              </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Vale transporte</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="vale" value="1">   
                Checked
              </label>
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Refeição</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="refeicao" value="1">                 
                Checked
              </label>
            </div>
          </div>

          <div class="col-md-2">
            <div class="panel panel-body border-top-teal text-center">
              <h6 class="no-margin text-semibold">Outros</h6>
              <label class="checkbox-inline">
                <input type="checkbox" class="styled" name="outros" value="1">                 
                Checked
              </label>
            </div>
          </div>
        </div>


                <legend class="text-bold">Cursos/Nível:</legend>

                <div class="row">
                  
                   <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                            <h6 class="no-margin text-semibold">Curso 01</h6>
                            <select id="curso" name="curso" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>
                           
                            <h6 class="no-margin text-semibold">Nível</h6>            
                            <select id="nivel" name="nivel" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 02</h6>
                            <select id="curso2" name="curso2" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>                                     
                            <select id="nivel2" name="nivel2" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 03</h6>
                            <select id="curso3" name="curso3" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>
                            <select id="nivel3" name="nivel3" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-body border-top-teal text-center">
                          <h6 class="no-margin text-semibold">Curso 04</h6>
                            <select id="curso4" name="curso4" class="form-control">
                                <option value="">Selecione</option>
                                <?php foreach ($cursos as $valor) { ?>
                                    <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                                <?php } ?>
                            </select>

                            <h6 class="no-margin text-semibold">Nível</h6>
                            <select id="nivel4" name="nivel4" class="form-control">
                                <option value="">Selecione</option>
                                <option value="Medio">Médio</option>
                                <option value="Tecnico">Técnico</option>                  
                                <option value="Superior">Superior</option>
                                <option value="Pos">Pós Graduação</option>                   
                            </select>
                        </div>
                    </div>


                </div>
                

<!-- 			<legend class="text-bold">Curso Disponível:</legend>
					<div class="form-group">
          	<label class="control-label col-lg-2">Curso 1:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="curso1" id="curso1">
                 		<option value="">Selecione</option>
                      <?php foreach ($curso as $valor) { ?>
                          <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>	
          <div class="form-group">
          	<label class="control-label col-lg-2">Curso 2:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="curso2" id="curso2">
                 		<option value="">Selecione</option>
                      <?php foreach ($curso as $valor) { ?>
                          <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>	
          <div class="form-group">
          	<label class="control-label col-lg-2">Curso 3:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="curso3" id="curso3">
                 		<option value="">Selecione</option>
                      <?php foreach ($curso as $valor) { ?>
                          <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>	
          <div class="form-group">
          	<label class="control-label col-lg-2">Curso 4:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="curso4" id="curso4">
                 		<option value="">Selecione</option>
                      <?php foreach ($curso as $valor) { ?>
                          <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>
          <div class="form-group">
          	<label class="control-label col-lg-2">Curso 5:</label>
          	<div class="col-lg-5">
                  <select class="form-control" name="curso5" id="curso5">
                 		<option value="">Selecione</option>
                      <?php foreach ($curso as $valor) { ?>
                          <option value="<?php echo $valor->pcurcucodig; ?>"><?php echo $valor->scurcunome; ?></option>
                    <?php } ?>
                     
                  </select>
              </div>
          </div>	

 -->


                  		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
