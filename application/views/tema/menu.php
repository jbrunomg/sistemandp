<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html">
				<!-- <img src="<?php echo base_url(); ?>public/assets/images/logo_light.png" alt=""> -->

			<font color="#ffffff" style="font-size: 17px;" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<b>WDM</b>-Recrutamento</font>

			</a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">

			<ul class="nav navbar-nav">
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				<?php 
			
			    if (isset($dadostce[0]->total)) {
			      $totaltce = $dadostce[0]->total;                  
                } else {
                  $totaltce = 0; 
                }

                if (isset($dadosta[0]->total)) {
                  $totalta = $dadosta[0]->total;                 
                } else {
                   $totalta = 0;		
                }
				
				?>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-git-compare"></i>
						<span class="visible-xs-inline-block position-right">Notificações</span>
						<span class="badge bg-warning-400" id="somaNotificacao"></span>
					</a>
					
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-heading">
							Notificações
							<ul class="icons-list">
								<li><a href="#"><i class="icon-sync"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body width-350">
							
						<!--	<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTce" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>

								<div class="media-body">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTce">T.C.E </a> <span class="text-semibold">Pendente</span> no sistema.
									<div class="media-annotation">Total: <span id="tceNotificacao"></span></div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTa" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
								</div>
								
								<div class="media-body">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTa">T.A </a> <span class="text-semibold">Pendente</span> no sistema.
									<div class="media-annotation">Total: <span id="taNotificacao"></span></div>
								</div>
							</li> -->

							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>empresa/listar/vencido" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
								</div>
								
								<div class="media-body">									
									<a href="<?php echo base_url(); ?>empresa/listar/vencido">Contrato </a> <span class="text-semibold">Vencido</span> no sistema.
									<div class="media-annotation">Total <span id="contVencNotificacao"></span></div>
								</div>
							</li>

		 					<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>empresa/listar/avencer" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
								</div>
								
								<div class="media-body">
									<a href="<?php echo base_url(); ?>empresa/listar/avencer">Contrato</a> <span class="text-semibold">30 Dias</span> a vencer
									<div class="media-annotation">Total <span id="cont20aVencNotificacao"></span></div>
								</div>
							</li> 

							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>contratoestudante/solicitacaoContr" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>
								
								<div class="media-body">
									
									<a href="<?php echo base_url(); ?>contratoestudante/solicitacaoContr">Solicitação</a> <span class="text-semibold">Contrato</span> Empresa 
									<div class="media-annotation">Total <span id="soliContraNotificacao"></span></div>
								</div>
							</li> 

							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>recrutamento/solicitacaoRecrut" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
								</div>
								
								<div class="media-body">
									<a href="<?php echo base_url(); ?>recrutamento/solicitacaoRecrut">Solicitação </a> <span class="text-semibold">Recrutamento</span> Empresa.
									<div class="media-annotation">Total: <span id="soliRecrutNotificacao"></span></div>
								</div>
							</li>



						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>				
			</ul>



			<ul class="nav navbar-nav navbar-right">			
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>public/assets/images/placeholder.jpg" alt="">
						<span><?php echo $this->session->userdata('usuario_nome');?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> Meu Perfil</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Configuração</a></li>
						<li><a href="<?php echo base_url() ?>Auth/sair"><i class="icon-switch2"></i> Sair</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="<?php echo base_url(); ?>public/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $this->session->userdata('usuario_nome');?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-user-tie text-size-small"></i> &nbsp;<?php echo $this->session->userdata('permissao_nome');?>
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="<?php echo base_url(); ?>Dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>						
								<!-- /main -->

								<?php if(checarPermissao('vEstudante')){ ?>		
								<li>
									<a href=""><i class="icon-graduation"></i> <span>Estudantes</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>estudante/">Estudantes</a></li>					
										<li><a href="<?php echo base_url() ?>emails/">Mala Direta</a></li>
										<li><a href="<?php echo base_url() ?>precadastro/">Pré Cadastro</a></li>
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vCurso')){ ?>		
								<li>
									<a href=""><i class="icon-book"></i> <span>Cursos</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>curso/">Cursos</a></li>				
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vVaga')){ ?>		
								<li>
									<a href=""><i class="icon-briefcase"></i> <span>Vagas</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>vaga/">Vagas</a></li>				
									</ul>
								</li>
								<?php } ?>

								<li>
									<a href=""><i class="icon-users"></i> <span>Recrutamento</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>recrutamento">Recrutamento</a></li>
									</ul>
								</li>

								<?php if(checarPermissao('vArquivo')){ ?>		
								<li>
									<a href=""><i class="icon-server"></i> <span>Arquivos</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>arquivo/">Arquivos</a></li>				
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vEmpresa')){ ?>		
								<li>
									<a href=""><i class="icon-office"></i> <span>Empresa</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>empresa/">Empresa</a></li>
										<li><a href="<?php echo base_url() ?>empresa/recrutamento">Recrutamento</a></li>	
										<li><a href="<?php echo base_url() ?>emailsempresa/">Mala Direta</a></li>	
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vInstEnsino')){ ?>		
								<li>
									<a href=""><i class="icon-library2"></i> <span>Instituições de Ensino</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>instensino/">Instituições de Ensino</a></li>						
										<li><a href="<?php echo base_url() ?>emailsinstituicao/">Mala Direta</a></li>	
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vPatrimonio')){ ?>		
								<li>
									<a href=""><i class="icon-cabinet"></i> <span>Bens Patrimoniais</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>patrimonio/">Patrimônio</a></li>			
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('VFornecedor')){ ?>
									<li><a href="<?php echo base_url() ?>fornecedor/"><i class="icon-truck"></i> <span>Fornecedores</span></a></li>
								<?php } ?>	

								<?php if(checarPermissao('VAgenda')){ ?>	
									<li><a href="#"><i class="icon-calendar"></i> <span>Agenda</span></a></li>
								<?php } ?>

								

								<li>
									<a href=""><i class="icon-coins"></i> <span>Demonstrativo de Atividades</span></a>

									<?php if(checarPermissao('vDemonstrativoRecrut')){ ?>	
									<ul>
										<li><a href="<?php echo base_url() ?>demonstrativorecrut/">Recrutamento e Seleção</a></li>			
									</ul>
									<?php } ?>

									<?php if(checarPermissao('vDemonstrativoAdm')){ ?>	
									<ul>
										<li><a href="<?php echo base_url() ?>demonstrativoadm/">Administrativo</a></li>			
									</ul>
									<?php } ?>

									<?php if(checarPermissao('vDemonstrativoFin')){ ?>
									<ul>
										<li><a href="<?php echo base_url() ?>demonstrativofin/">Financeiro</a></li>			
									</ul>
									<?php } ?>

								</li>
								


								<?php if(checarPermissao('vFinanceiro')){ ?>		
									<li>
										<a href=""><i class="icon-coins"></i> <span>Financeiro</span></a>
										<ul>
											<li><a href="<?php echo base_url() ?>financeiro/">Contas</a></li>			
										</ul>
										<ul>
											<li><a href="<?php echo base_url() ?>categoriafinanceiro/">Centro de Custo</a></li>			
										</ul>
									</li>
								<?php } ?>
		
								
								<?php if(checarPermissao('vAdministrativo')){ ?>		
								<li>
									<a href=""><i class="icon-cabinet"></i> <span>Administrativo</span></a>
									<ul>
										<li><a href="<?php echo base_url() ?>contratoestudante/">Contratação</a></li>			
									</ul>
								</li>
								<?php } ?>

								<?php if(checarPermissao('vCrm')){ ?>
								<li><a href="<?php echo base_url(); ?>crm"><i class="icon-calendar3"></i> <span>CRM</span></a></li>	
								<?php } ?> 
							
							 	<?php if(checarPermissao('vNewsletter')){ ?>
								<li><a href="<?php echo base_url(); ?>newsletter"><i class="icon-envelope"></i> <span>Newsletter</span></a></li>	
								<?php } ?> 	

								<li>
									<a href="#"><i class="icon-printer4"></i> <span>Impressões</span></a>
									<ul>
										<?php if(checarPermissao('vCarta')){ ?>
											<li><a href="<?php echo base_url(); ?>arquivos/impressaoExe">Carta de Recomendação</a></li>
										<?php } ?>
										<?php if(checarPermissao('vContrato')){ ?>
										<li><a href="<?php echo base_url(); ?>arquivos/impressaoExe">Contrato</a></li>	
										<?php } ?>		
										<?php if(checarPermissao('vFicha')){ ?>
										<li><a href="<?php echo base_url(); ?>arquivos/impressaoExe">Ficha de Inscrição</a></li>
										<?php } ?>										
									</ul>
								</li>

								<li class="">
									<a href="#" class="has-ul"><i class="icon-stack"></i> Relatórios</a>

									<ul >								

										<?php if(checarPermissao('vEstudante')){ ?>
										<li class="">										
											<a href="#" class="has-ul"><i class="icon-graduation"></i> Estudantes</a>
											<ul class="hidden-ul"  >
												<li><a href="<?php echo base_url(); ?>relatorios/aniversariantes"><i class="icon-gift"></i> Aniversariantes</a></li>									
												
												<li><a href="<?php echo base_url(); ?>relatorios/estudanteAtivoInativo"><i class="icon-wrench"></i> Ativo/Inativo</a></li>
											</ul>
										</li>
										<?php } ?>

										<?php if(checarPermissao('vAdministrativo')){ ?>
										<li class="">										
											<a href="#" class="has-ul"><i class="icon-cash3"></i> Financeiro</a>
											<ul class="hidden-ul"  >
												<li><a href="<?php echo base_url(); ?>relatorios/financeiroNotaFiscal"><i class="icon-clippy"></i> Controle Nota Fiscais </a></li>									
												
												<li><a href="#"><i class="icon-wrench"></i> ....</a></li>
											</ul>
										</li>
										<?php } ?>
										
										<li class="">
											<a href="#" class="has-ul"><i class="icon-chart"></i> Demonstrativo</a>
											<ul class="hidden-ul">

												<?php if(checarPermissao('vDemonstrativoAdm')){ ?>
												<li><a href="<?php echo base_url(); ?>relatorios/demonstrativoview"><i class="icon-briefcase"></i> Atividade(s) </a></li>									
												<?php } ?>

											</ul>
										</li>									

										<?php if(checarPermissao('vEmpresa')){ ?>
										<li>
											<a href="#"><i class="icon-office"></i> Empresa</a>
											<ul class="hidden-ul">
												<li><a href="<?php echo base_url(); ?>relatorios/empresa"><i class="icon-briefcase"></i> Demonstrativo Empresa</a></li>								
												

											</ul>
										</li>
										<?php } ?>	
										<?php if(checarPermissao('vInstEnsino')){ ?>
										<li><a href="<?php echo base_url(); ?>relatorios/instituicao"><i class="icon-library2"></i> I. de Ensino</a></li>
										<?php } ?>
										<?php if(checarPermissao('vPatrimonio')){ ?>
										<li><a href="<?php echo base_url(); ?>relatorios/patrimonio"  ><i class="icon-cabinet"></i>Patrimônio</a></li>
										<?php } ?>
										<?php if(checarPermissao('vPatrimonio')){ ?>
										<li><a href="<?php echo base_url(); ?>pagamentosistema"  ><i class="icon-coins"></i>Pagamento do Sistema</a></li>
										<?php } ?>
									</ul>
								</li>	


								<li>
									<a href="#"><i class="icon-gear"></i> <span>Configurações</span></a>
									<ul>
										<?php if(checarPermissao('vUsuarios')){ ?>
											<li><a href="<?php echo base_url(); ?>usuario">Usuários</a></li>
										<?php } ?>
										<?php if(checarPermissao('cPermissoes')){ ?>
										<li><a href="<?php echo base_url(); ?>permissao">Permissões</a></li>	
										<?php } ?>		
										<?php if(checarPermissao('cEmitente')){ ?>
										<li><a href="<?php echo base_url(); ?>auth/emitente">Emitente</a></li>
										<?php } ?>	
										<?php if(checarPermissao('cSite')){ ?>
										<li><a href="<?php echo base_url(); ?>site">Site</a></li>
										<?php } ?>										
									</ul>
								</li>
													
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
			<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo ucfirst($this->uri->segment(1)); ?></span>  <?php echo ucfirst($this->uri->segment(2)); ?></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Estatísticas</span></a>								
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>Dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="<?php echo base_url($this->uri->segment(1)); ?>"><?php echo ucfirst($this->uri->segment(1)); ?></a></li>
							<li class="active"><?php echo ucfirst($this->uri->segment(2)); ?></li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>	
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">