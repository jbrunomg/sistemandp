
<div class="panel panel-flat">
<div class="panel-heading">
	<h5 class="panel-title text-center">Configuração Site</h5>							
</div>
	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>site/configurar/" method="post" enctype="multipart/form-data">

			<input type="hidden" name="site_produto_valor" value="0" />
				
			<div class="form-group">
				<label class="cursor-move">BANNER 01:</label>
				<input type="file" name="userfile1" class="file-styled">
			</div>	

			<div class="form-group">
				<label class="cursor-move">BANNER 02:</label>
				<input type="file" name="userfile2" class="file-styled">
			</div>

			<div class="form-group">
				<label class="cursor-move">BANNER 03:</label>
				<input type="file" name="userfile3" class="file-styled">
			</div>

			<div class="form-group">
				<label class="cursor-move">BANNER 04:</label>
				<input type="file" name="userfile4" class="file-styled">
			</div>

			<div class="text-right">
				<button type="submit" class="btn bg-teal-400">Salvar<i class="icon-arrow-right14 position-right"></i></button>
			</div>

		</form>
	</div>
</div>
