<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
        <?php if(checarPermissao('aEstudante')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>	
				<th>#</th>			
				<th>Empresa</th>
				<th>Aluno</th>
				<th>Data Envio</th>	
				<th>Status</th>	
				<th>Enviado</th>										
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>     								
					<td><?php echo $valor->precadastro_id; ?></td>
					<td><?php echo $valor->sempemrazao; ?></td>
					<td><?php echo $valor->precadastro_estudante; ?></td>

					<td><?php echo date('d/m/Y',strtotime($valor->precadastro_data_cadastro)); ?></td>
					<td><?php echo $valor->precadastro_status; ?></td>
					<td><?php echo $valor->precadastro_email_enviado; ?></td>
																	
					<td>																				
							<ul class="icons-list">
																		
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->precadastro_id; ?>"><i class="icon-trash"></i></a></li>
									
									<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->precadastro_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
																										
								
							</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->