<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Visualizar Pré-Registro</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">					

        <fieldset class="content-group">
          <legend class="text-bold">Dados de envio:</legend>

          <div class="form-group">
            <label class="control-label col-lg-2">Estudantes:</label>
            <div class="col-lg-5">
              <input  disabled type="text" class="form-control" placeholder="Nome Estudante" name="estudante"  value="<?php echo $dados[0]->precadastro_estudante; ?>">
              <?php  echo form_error('estudante'); ?>           
            </div>                    
          </div> 

          <div class="form-group">
            <label class="control-label col-lg-2">Email:</label>
            <div class="col-lg-5">
              <input  disabled type="email" class="form-control" placeholder="Email Estudante" name="email"  value="<?php echo $dados[0]->precadastro_email; ?>">
              <?php  echo form_error('email'); ?>           
            </div>                    
          </div> 

          <div class="form-group">
            <label class="control-label col-lg-2">Status:</label>
            <div class="col-lg-5">
              <input  disabled type="status" class="form-control" placeholder="Email Estudante" name="status"  value="<?php echo $dados[0]->precadastro_status; ?>">
              <?php  echo form_error('email'); ?>           
            </div>                    
          </div> 
          
          <div class="form-group">
            <label class="control-label col-lg-2">Empresa:</label>
              <div class="col-lg-5">
              <select disabled class="form-control" name="empresa" id="empresa">
                <option  value="">Selecione</option>
                  <?php foreach ($empresa as $valor) { ?>                                         
                    <?php $selected = ($valor->pempemcodig == $dados[0]->precadastro_empresa_id)?'SELECTED': ''; ?>
                    <option value="<?php echo $valor->pempemcodig; ?>" <?php echo $selected; ?>><?php echo $valor->sempemfanta; ?></option>
                <?php } ?>
                 
              </select>
              </div>                  
          </div>  

          <div class="form-group">
          <label class="control-label col-lg-2">Descrição:</label>
          <div class="col-lg-5">
            <textarea disabled rows="3" cols="60" class="form-control" name="precadastro_obs" id="precadastro_obs" ><?php echo $dados[0]->precadastro_obs; ?>            
            </textarea>            
             <?php echo form_error('precadastro_obs'); ?>           
          </div>                    
        </div> 

			</form>
		</div>
	</div>