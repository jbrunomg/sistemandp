<?php $p = unserialize($dados[0]->permissao_permissoes);?>

<!-- Table with togglable columns -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Configurar permissão <?php echo $dados[0]->permissao_nome; ?></h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<form action="<?php echo base_url(); ?>permissao/configurarExe/" method="post">

	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	<input type="hidden" value="<?php echo $dados[0]->permissao_id; ?>" name="id">
	<table class="table table-togglable table-hover">
		<thead>
			<tr>
				<th data-toggle="true">Atividade</th>
				<th data-hide="phone,tablet">Visualizar</th>
				<th data-hide="phone,tablet">Adicionar</th>
				<th data-hide="phone,tablet">Editar</th>
				<th data-hide="phone">Excluir</th>	
				<th data-hide="phone">Outros</th>									
			</tr>
		</thead>
		<tbody>	



	<tr>
		<td>Estudantes</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vEstudante" class="control-success" <?php echo (in_array('vEstudante',$p))? 'checked="checked"': '' ?>>
					Visualizar Estudante
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aEstudante" class="control-success" <?php echo (in_array('aEstudante',$p))? 'checked="checked"': '' ?>>
					Adicionar Estudante
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eEstudante" class="control-success" <?php echo (in_array('eEstudante',$p))? 'checked="checked"': '' ?>>
					Editar Estudante
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dEstudante" class="control-success" <?php echo (in_array('dEstudante',$p))? 'checked="checked"': '' ?>>
					Excluir Estudante
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="cEstudante" class="control-success" <?php echo (in_array('cEstudante',$p))? 'checked="checked"': '' ?>>
					Configurar Mala Direta
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Cursos</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vCurso" class="control-success" <?php echo (in_array('vCurso',$p))? 'checked="checked"': '' ?>>
					Visualizar Curso
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aCurso" class="control-success" <?php echo (in_array('aCurso',$p))? 'checked="checked"': '' ?>>
					Adicionar Curso
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eCurso" class="control-success" <?php echo (in_array('eCurso',$p))? 'checked="checked"': '' ?>>
					Editar Curso
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dCurso" class="control-success" <?php echo (in_array('dCurso',$p))? 'checked="checked"': '' ?>>
					Excluir Curso
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Vagas</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vVaga" class="control-success" <?php echo (in_array('vVaga',$p))? 'checked="checked"': '' ?>>
					Visualizar Vaga
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aVaga" class="control-success" <?php echo (in_array('aVaga',$p))? 'checked="checked"': '' ?>>
					Adicionar Vaga
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eVaga" class="control-success" <?php echo (in_array('eVaga',$p))? 'checked="checked"': '' ?>>
					Editar Vaga
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dVaga" class="control-success" <?php echo (in_array('dVaga',$p))? 'checked="checked"': '' ?>>
					Excluir Vaga
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Empresas</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vEmpresa" class="control-success" <?php echo (in_array('vEmpresa',$p))? 'checked="checked"': '' ?>>
					Visualizar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aEmpresa" class="control-success" <?php echo (in_array('aEmpresa',$p))? 'checked="checked"': '' ?>>
					Adicionar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eEmpresa" class="control-success" <?php echo (in_array('eEmpresa',$p))? 'checked="checked"': '' ?>>
					Editar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dEmpresa" class="control-success" <?php echo (in_array('dEmpresa',$p))? 'checked="checked"': '' ?>>
					Excluir Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="cEmpresa" class="control-success" <?php echo (in_array('cEmpresa',$p))? 'checked="checked"': '' ?>>
					Configurar Mala Direta
				</label>
			</div>
		</td>		
		<td></td>													
	</tr>

	<tr>
		<td>Instituições de Ensino</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vInstEnsino" class="control-success" <?php echo (in_array('vInstEnsino',$p))? 'checked="checked"': '' ?>>
					Visualizar Inst. de Ensino
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aInstEnsino" class="control-success" <?php echo (in_array('aInstEnsino',$p))? 'checked="checked"': '' ?>>
					Adicionar Inst. de Ensino
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eInstEnsino" class="control-success" <?php echo (in_array('eInstEnsino',$p))? 'checked="checked"': '' ?>>
					Editar Inst. de Ensino
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dInstEnsino" class="control-success" <?php echo (in_array('dInstEnsino',$p))? 'checked="checked"': '' ?>>
					Excluir Inst. de Ensino
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Usuários</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vUsuarios" class="control-success" <?php echo (in_array('vUsuarios',$p))? 'checked="checked"': '' ?>>
					Visualizar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aUsuarios" class="control-success" <?php echo (in_array('aUsuarios',$p))? 'checked="checked"': '' ?>>
					Adicionar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eUsuarios" class="control-success" <?php echo (in_array('eUsuarios',$p))? 'checked="checked"': '' ?>>
					Editar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dUsuarios" class="control-success" <?php echo (in_array('dUsuarios',$p))? 'checked="checked"': '' ?>>
					Excluir Usuarios
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Patrimônio</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vPatrimonio" class="control-success" <?php echo (in_array('vPatrimonio',$p))? 'checked="checked"': '' ?>>
					Visualizar Patrimônio
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aPatrimonio" class="control-success" <?php echo (in_array('aPatrimonio',$p))? 'checked="checked"': '' ?>>
					Adicionar Patrimônio
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="ePatrimonio" class="control-success" <?php echo (in_array('ePatrimonio',$p))? 'checked="checked"': '' ?>>
					Editar Patrimônio
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dPatrimonio" class="control-success" <?php echo (in_array('dPatrimonio',$p))? 'checked="checked"': '' ?>>
					Excluir Patrimônio
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Arquivo</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vArquivo" class="control-success" <?php echo (in_array('vArquivo',$p))? 'checked="checked"': '' ?>>
					Visualizar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aArquivo" class="control-success" <?php echo (in_array('aArquivo',$p))? 'checked="checked"': '' ?>>
					Adicionar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eArquivo" class="control-success" <?php echo (in_array('eArquivo',$p))? 'checked="checked"': '' ?>>
					Editar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dArquivo" class="control-success" <?php echo (in_array('dArquivo',$p))? 'checked="checked"': '' ?>>
					Excluir Arquivo
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Administrativo</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vAdministrativo" class="control-success" <?php echo (in_array('vAdministrativo',$p))? 'checked="checked"': '' ?>>
					Visualizar Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aAdministrativo" class="control-success" <?php echo (in_array('aAdministrativo',$p))? 'checked="checked"': '' ?>>
					Adicionar Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eAdministrativo" class="control-success" <?php echo (in_array('eAdministrativo',$p))? 'checked="checked"': '' ?>>
					Editar Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dAdministrativo" class="control-success" <?php echo (in_array('dAdministrativo',$p))? 'checked="checked"': '' ?>>
					Excluir Administrativo
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Demonstrativo Atividade Financeira</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vDemonstrativoFin" class="control-success" <?php echo (in_array('vDemonstrativoFin',$p))? 'checked="checked"': '' ?>>
					Visualizar Demonstrativo Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aDemonstrativoFin" class="control-success" <?php echo (in_array('aDemonstrativoFin',$p))? 'checked="checked"': '' ?>>
					Adicionar Demonstrativo Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eDemonstrativoFin" class="control-success" <?php echo (in_array('eDemonstrativoFin',$p))? 'checked="checked"': '' ?>>
					Editar Demonstrativo Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dDemonstrativoFin" class="control-success" <?php echo (in_array('dDemonstrativoFin',$p))? 'checked="checked"': '' ?>>
					Excluir Demonstrativo Financeiro
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Demonstrativo Atividade Administrativa</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vDemonstrativoAdm" class="control-success" <?php echo (in_array('vDemonstrativoAdm',$p))? 'checked="checked"': '' ?>>
					Visualizar Demonstrativo Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aDemonstrativoAdm" class="control-success" <?php echo (in_array('aDemonstrativoAdm',$p))? 'checked="checked"': '' ?>>
					Adicionar Demonstrativo Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eDemonstrativoAdm" class="control-success" <?php echo (in_array('eDemonstrativoAdm',$p))? 'checked="checked"': '' ?>>
					Editar Demonstrativo Administrativo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dDemonstrativoAdm" class="control-success" <?php echo (in_array('dDemonstrativoAdm',$p))? 'checked="checked"': '' ?>>
					Excluir Demonstrativo Administrativo
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Demonstrativo Recrutamento/Seleção</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vDemonstrativoRecrut" class="control-success" <?php echo (in_array('vDemonstrativoRecrut',$p))? 'checked="checked"': '' ?>>
					Visualizar Demonstrativo Recrutamento/Seleção
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aDemonstrativoRecrut" class="control-success" <?php echo (in_array('aDemonstrativoRecrut',$p))? 'checked="checked"': '' ?>>
					Adicionar Demonstrativo Recrutamento/Seleção
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eDemonstrativoRecrut" class="control-success" <?php echo (in_array('eDemonstrativoRecrut',$p))? 'checked="checked"': '' ?>>
					Editar Demonstrativo Recrutamento/Seleção
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dDemonstrativoRecrut" class="control-success" <?php echo (in_array('dDemonstrativoRecrut',$p))? 'checked="checked"': '' ?>>
					Excluir Demonstrativo Recrutamento/Seleção
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Financeiro</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vFinanceiro" class="control-success" <?php echo (in_array('vFinanceiro',$p))? 'checked="checked"': '' ?>>
					Visualizar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aFinanceiro" class="control-success" <?php echo (in_array('aFinanceiro',$p))? 'checked="checked"': '' ?>>
					Adicionar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eFinanceiro" class="control-success" <?php echo (in_array('eFinanceiro',$p))? 'checked="checked"': '' ?>>
					Editar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dFinanceiro" class="control-success" <?php echo (in_array('dFinanceiro',$p))? 'checked="checked"': '' ?>>
					Excluir Financeiro
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>CRM</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vCrm" class="control-success" <?php echo (in_array('vCrm',$p))? 'checked="checked"': '' ?>>
					Visualizar CRM
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aCrm" class="control-success" <?php echo (in_array('aCrm',$p))? 'checked="checked"': '' ?>>
					Adicionar CRM
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eCrm" class="control-success" <?php echo (in_array('eCrm',$p))? 'checked="checked"': '' ?>>
					Editar CRM
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dCrm" class="control-success" <?php echo (in_array('dCrm',$p))? 'checked="checked"': '' ?>>
					Excluir CRM
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Newsletter</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vNewsletter" class="control-success" <?php echo (in_array('vNewsletter',$p))? 'checked="checked"': '' ?>>
					Visualizar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aNewsletter" class="control-success" <?php echo (in_array('aNewsletter',$p))? 'checked="checked"': '' ?>>
					Adicionar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eNewsletter" class="control-success" <?php echo (in_array('eNewsletter',$p))? 'checked="checked"': '' ?>>
					Editar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dNewsletter" class="control-success" <?php echo (in_array('dNewsletter',$p))? 'checked="checked"': '' ?>>
					Excluir Newsletter
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>
    
	<tr>
		<td>Impressões</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vCarta" class="control-success" <?php echo (in_array('vCarta',$p))? 'checked="checked"': '' ?>>
					Baixar Carta
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vContrato" class="control-success" <?php echo (in_array('vContrato',$p))? 'checked="checked"': '' ?>>
					Baixar Cotrato
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vFicha" class="control-success" <?php echo (in_array('vFicha',$p))? 'checked="checked"': '' ?>>
					Baixar Ficha
				</label>
			</div>
		</td>
	
		<td></td>													
	</tr>

			<tr>

				<tr>
					<td>Permissões</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="vPermissoes" class="control-success" <?php echo (in_array('vPermissoes',$p))? 'checked="checked"': '' ?>>
								Visualizar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="aPermissoes" class="control-success" <?php echo (in_array('aPermissoes',$p))? 'checked="checked"': '' ?>>
								Adicionar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="ePermissoes" class="control-success" <?php echo (in_array('ePermissoes',$p))? 'checked="checked"': '' ?>>
								Editar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="dPermissoes" class="control-success" <?php echo (in_array('dPermissoes',$p))? 'checked="checked"': '' ?>>
								Excluir Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cPermissoes" class="control-success" <?php echo (in_array('cPermissoes',$p))? 'checked="checked"': '' ?>>
								Configurar Permissoes
							</label>
						</div>
					</td>		
					<td></td>													
				</tr>


				<tr>
					<td>Configuração</td>		
		
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cSite" class="control-success" <?php echo (in_array('cSite',$p))? 'checked="checked"': '' ?>>
								Configurar Site
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cEmitente" class="control-success" <?php echo (in_array('cEmitente',$p))? 'checked="checked"': '' ?>>
								Configurar Emitente
							</label>
						</div>
					</td>			
					<td></td>													
				</tr>
		
				<tr>
					<td colspan="6">
						<div class="text-right">
							<button type="submit" class="btn bg-teal">Configurar<i class="icon-arrow-right14 position-right"></i></button>
						</div>
					</td>
				</tr>
		
		</tbody>			
	</table>		
	</form>
</div>
<!-- /table with togglable columns -->