<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Demonstrativo</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Demonstrativo:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="demonstrativoadm_id" value="<?php echo $dados[0]->demonstrativoadm_id; ?>" />

				
			<div class="col-md-12">
				<div class="panel panel-body border-top-teal">
					<div class="text-center">
						<h6 class="no-margin text-semibold">Mês/Ano:</h6>                
					</div>              
					<div class="text-center">
						<input  type="Month" class="form-control" placeholder="mes_ano" name="mes_ano" id="mes_ano" value="<?php echo $dados[0]->demonstrativoadm_ano.'-'.$dados[0]->demonstrativoadm_mes; ?>">
						<?php echo form_error('mes_ano'); ?>                
					</div>           
				</div>
			</div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estagiários Contratado( TCEs feitos)</h6>
                <p></p>                
              </div>

              <input type="number" name= 'demonstrativoadm_contratados' value="<?php echo $dados[0]->demonstrativoadm_contratados; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estagiário Cobrado</h6>
                <p></p>                
              </div>

              <input type="number" name= 'demonstrativoadm_estag_cabrados' value="<?php echo $dados[0]->demonstrativoadm_estag_cabrados; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Empresas Ativas</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativoadm_empr_ativas' value="<?php echo $dados[0]->demonstrativoadm_empr_ativas; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estagiários Ativos</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativoadm_estag_ativos' value="<?php echo $dados[0]->demonstrativoadm_estag_ativos; ?>" class="form-control border-teal border-lg" placeholder="Quantidade">
              
            </div>
          </div>

          <div class="col-md-4">
            <div class="panel panel-body border-top-teal">
              <div class="text-center">
                <h6 class="no-margin text-semibold">Estagiários Rescindidos</h6>
                <p></p>
              </div>

              <input type="number" name= 'demonstrativoadm_estag_rescindidos' value="<?php echo $dados[0]->demonstrativoadm_estag_rescindidos; ?>" class="form-control border-teal border-lg" placeholder="Quantidade"  pattern="[0-9]+$">

            </div>
          </div>



                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
