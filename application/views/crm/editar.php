<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Edição de CRM</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados CRM:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="pcrmcodig" value="<?php echo $dados[0]->pcrmcodig; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Empresa:</label>
					<div class="col-lg-5">
					<input type="text" class="form-control" placeholder="Empresa" name="crm_empresa" id="crm_empresa" value="<?php echo $dados[0]->crm_empresa; ?>">
					<?php echo form_error('atividade'); ?>
					</div>										
				</div>			

				<div class="form-group">
					<label class="control-label col-lg-2">Contato 01:</label>
					<div class="col-lg-5">
					  <input type="text" class="form-control" placeholder="Contato 01" data-mask="(99) 9 9999-9999"  name="crm_contato01" id="crm_contato01" value="<?php echo $dados[0]->crm_contato01; ?>">
					<?php echo form_error('atividade'); ?>
					</div>                    
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Contato 02:</label>
					<div class="col-lg-5">
					  <input type="text" class="form-control" placeholder="Contato 02" data-mask="(99) 9 9999-9999" name="crm_contato02" id="crm_contato02" value="<?php echo $dados[0]->crm_contato02; ?>">
					<?php echo form_error('atividade'); ?>
					</div>                    
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Email:</label>
					<div class="col-lg-5">
					  <input type="email" class="form-control" placeholder="Email" name="crm_email" id="crm_email" value="<?php echo $dados[0]->crm_email; ?>">
					<?php echo form_error('atividade'); ?>
					</div>                    
				</div>



		 		<div class="form-group">
		            <label class="control-label col-lg-2">Status:</label>
		            <div class="col-lg-5">
		              <select class="form-control" name="crm_status" id="crm_status">  
		                <option value="Aberto" <?php echo ($dados[0]->crm_status == 'Aberto')?'selected':''; ?>>Aberto</option>   
		                <option value="Agendado" <?php echo ($dados[0]->crm_status == 'Agendado')?'selected':''; ?>>Agendado</option>   
		                <option value="Pendente" <?php echo ($dados[0]->crm_status == 'Pendente')?'selected':''; ?>>Pendente</option>   
		                <option value="Aprovado" <?php echo ($dados[0]->crm_status == 'Aprovado')?'selected':''; ?>>Aprovado</option> 
		              </select>
		            </div>
		        </div>



				<legend class="text-bold">Observações:</legend>

		        <div class="row">

		          <div class="col-md-4">
		              <div class="panel panel-body border-top-teal text-center">
		                <h6 class="no-margin text-semibold">Anotação 01</h6>
		                <input type="text" class="form-control" name="crm_data_anot01" id="crm_data_anot01" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo date('d/m/Y',strtotime( $dados[0]->crm_data_anot01)); ?>">
		                <br>
		                <textarea class="form-control" rows="3" cols="40" id="crm_anotacao01" name="crm_anotacao01" ><?php echo $dados[0]->crm_anotacao01; ?></textarea>                
		                <?php echo form_error('crm_anotacao01'); ?>                                 
		              </div>
		          </div>

		          <div class="col-md-4">
		              <div class="panel panel-body border-top-teal text-center">
		                <h6 class="no-margin text-semibold">Anotação 02</h6>
		                <input type="text" class="form-control" name="crm_data_anot02" id="crm_data_anot02" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo date('d/m/Y',strtotime( $dados[0]->crm_data_anot02)); ?>">
		                <br>
		                <textarea class="form-control" rows="3" cols="40" id="crm_anotacao02" name="crm_anotacao02" ><?php echo $dados[0]->crm_anotacao02; ?></textarea>
		                <?php echo form_error('crm_anotacao02'); ?>
		              </div>
		          </div>

		          <div class="col-md-4">
		              <div class="panel panel-body border-top-teal text-center">
		                <h6 class="no-margin text-semibold">Anotação 03</h6>
		                <input type="text" class="form-control" name="crm_data_anot03" id="crm_data_anot03" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo date('d/m/Y',strtotime( $dados[0]->crm_data_anot03)); ?>">
		                <br>
		                <textarea class="form-control" rows="3" cols="40" id="crm_anotacao03" name="crm_anotacao03" ><?php echo $dados[0]->crm_anotacao03; ?></textarea>
		                <?php echo form_error('crm_anotacao03'); ?>
		              </div>
		          </div>          

		        </div>


	            </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>



	<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">CRM - envio email</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarEmail" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Email:</legend>

				<input  type="hidden" name="pcrmcodig" value="<?php echo $dados[0]->pcrmcodig; ?>" />

				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

          <div class="form-group">
            <label class="control-label col-lg-2">Empresa:</label>
            <div class="col-lg-5">
       		<input readOnly type="text" class="form-control" placeholder="Assunto" name="crm_email" id="crm_email" value="<?php echo $dados[0]->crm_email; ?>">
          	<?php  echo form_error('crm_email'); ?>
            </div>                    
          </div>  
          						
				</fieldset>

        <fieldset class="content-group">
          <legend class="text-bold">Dados de envio:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Assunto:</label>
          <div class="col-lg-10">
            <input  type="text" class="form-control" placeholder="Assunto" name="assunto" id="assunto" value="<?php echo $dados[0]->crm_assunto;  ?>">
          <?php  echo form_error('assunto'); ?>
          </div>                    
        </div>    

        <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Descrição:</h5>             
            </div>

            <div class="panel-body">
              <div class="summernoteEmails"><?php echo $dados[0]->crm_mensagem;  ?>
               
              </div>
            </div>
        </div>


        <input type="hidden" id="descricao" name="descricao">

        		<?php if ($dados[0]->crm_email <> '' ) { ?>        			
        		
				<div class="text-right">
					<button type="submit" id="enviarMalaDireta" class="btn bg-teal">Enviar <i class="icon-arrow-right14 position-right"></i></button>
				</div>

				<?php } ?>
			</form>
		</div>
	</div>
