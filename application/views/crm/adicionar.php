<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de CRM</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">

					<legend class="text-bold">Dados da CRM:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

<!-- 
          <div class="form-group">
            <label class="control-label col-lg-2">Data Cadastro:</label>
            <div class="col-lg-5">                                            
              <input type="text" class="form-control classData" name="crm_data_cadastro" id="crm_data_cadastro" value="<?php echo set_value('crm_data_cadastro'); ?>" readonly>
            <?php echo form_error('crm_data_cadastro'); ?>
            </div>                    
          </div> -->

          <div class="form-group">
						<label class="control-label col-lg-2">Empresa:</label>
						<div class="col-lg-5">
							<input type="text" class="form-control" placeholder="Empresa" name="crm_empresa" id="crm_empresa" value="<?php echo set_value('crm_empresa'); ?>">
						<?php echo form_error('atividade'); ?>
						</div>										
					</div>

          <div class="form-group">
            <label class="control-label col-lg-2">Contato 01:</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="Contato 01" data-mask="(99) 9 9999-9999"  name="crm_contato01" id="crm_contato01" value="<?php echo set_value('crm_contato01'); ?>">
            <?php echo form_error('atividade'); ?>
            </div>                    
          </div>


          <div class="form-group">
            <label class="control-label col-lg-2">Contato 02:</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" placeholder="Contato 02" data-mask="(99) 9 9999-9999" name="crm_contato02" id="crm_contato02" value="<?php echo set_value('crm_contato02'); ?>">
            <?php echo form_error('atividade'); ?>
            </div>                    
          </div>


          <div class="form-group">
            <label class="control-label col-lg-2">Email:</label>
            <div class="col-lg-5">
              <input type="email" class="form-control" placeholder="Email" name="crm_email" id="crm_email" value="<?php echo set_value('crm_email'); ?>">
            <?php echo form_error('atividade'); ?>
            </div>                    
          </div>


          <div class="form-group">
            <label class="control-label col-lg-2">Status:</label>
            <div class="col-lg-5">
                  <select class="form-control" name="crm_status" id="crm_status">                    
                    <option value="Aberto">Aberto</option>
                    <option value="Agendado">Agendado</option>
                    <option value="Pendente">Pendente</option>
                    <option value="Aprovado">Aprovado</option>
                  </select>
              </div>
          </div>



				<legend class="text-bold">Observações:</legend>

        <div class="row">

          <div class="col-md-4">
              <div class="panel panel-body border-top-teal text-center">
                <h6 class="no-margin text-semibold">Anotação 01</h6>
                <input type="text" class="form-control" name="crm_data_anot01" id="crm_data_anot01" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo set_value('crm_data_anot01'); ?>">
                <br>
                <textarea rows="3" cols="40" name="crm_anotacao01" id="crm_anotacao01" value="<?php echo set_value('crm_anotacao01'); ?>">
                <?php echo form_error('crm_anotacao01'); ?>  
                </textarea>                 
              </div>
          </div>

          <div class="col-md-4">
              <div class="panel panel-body border-top-teal text-center">
                <h6 class="no-margin text-semibold">Anotação 02</h6>
                <input type="text" class="form-control" name="crm_data_anot02" id="crm_data_anot02" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo set_value('crm_data_anot02'); ?>">
                <br>
                <textarea rows="3" cols="40" name="crm_anotacao02" id="crm_anotacao02" value="<?php echo set_value('crm_anotacao02'); ?>">
                <?php echo form_error('crm_anotacao02'); ?>  
                </textarea>
              </div>
          </div>

          <div class="col-md-4">
              <div class="panel panel-body border-top-teal text-center">
                <h6 class="no-margin text-semibold">Anotação 03</h6>
                <input type="text" class="form-control" name="crm_data_anot03" id="crm_data_anot03" data-mask="99/99/9999" placeholder="00-00-0000" value="<?php echo set_value('crm_data_anot03'); ?>">
                <br>
                <textarea rows="3" cols="40" name="crm_anotacao03" id="crm_anotacao03" value="<?php echo set_value('crm_anotacao03'); ?>">
                <?php echo form_error('crm_anotacao03'); ?>  
                </textarea>
              </div>
          </div>          

        </div>






                  		
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
