<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Emailscron_model');
	}

	public function index()
	{
		$emailsEnviar = $this->Emailscron_model->pegarEmailsCron();
		

		foreach ($emailsEnviar as $email) {

			$envio = $this->enviarEmails($email['maladireta_enviar_email'],$email['maladireta_enviar_conteudo'],$email['maladireta_enviar_assunto']);

			$id = $email['maladireta_enviar_id'];			

			if ($envio) {
				$resultado = $this->Emailscron_model->editarcron($id,'SIM');
			} else {
				$resultado = $this->Emailscron_model->editarcron($id,'NAO');
			}
			
		}

		$envio = $this->enviarEmails('bruno@wdmtecnologia.com.br',$emailsEnviar['maladireta_enviar_conteudo'],$emailsEnviar['maladireta_enviar_assunto']); // Teste email Cron
	}


	public function enviarEmails($emails,$conteudo,$assunto)
	{
		// $this->load->library('email'); // inserido no AutoLoad
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);

		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		//if($this->email->send())
		if(true)
        {
            $this->session->set_flashdata('success','Email enviado com sucesso!');
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('error',$this->email->print_debugger());
            return false;
        }
	}

	

}

/* End of file Emails.php */
/* Location: ./application/controllers/Emails.php */