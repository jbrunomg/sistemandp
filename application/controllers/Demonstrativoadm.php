<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demonstrativoadm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Demonstrativoadm_model');
	}

	public function index()
	{

		$resultado = $this->Demonstrativoadm_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'demonstrativoadm/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Demonstrativoadm_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativoadm/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		
		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativoadm_ano'   => $dados[0],
		  'demonstrativoadm_mes'   => $dados[1],

		  'demonstrativoadm_contratados'     => $this->input->post('demonstrativoadm_contratados'),
		  'demonstrativoadm_estag_cabrados'  => $this->input->post('demonstrativoadm_estag_cabrados'),
		  'demonstrativoadm_empr_ativas'   => $this->input->post('demonstrativoadm_empr_ativas'),
		  'demonstrativoadm_estag_ativos'  => $this->input->post('demonstrativoadm_estag_ativos'),
		  'demonstrativoadm_estag_rescindidos'  => $this->input->post('demonstrativoadm_estag_rescindidos'),
		  
		  'demonstrativoadm_usuario_id'   => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativoadm_visivel'   => 1
		  
		);

       
		$resultado = $this->Demonstrativoadm_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Demonstrativoadm', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Demonstrativoadm_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Demonstrativoadm_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativoadm/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('demonstrativoadm_id');


		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativoadm_ano'   => $dados[0],
		  'demonstrativoadm_mes'   => $dados[1],

		  'demonstrativoadm_contratados'     => $this->input->post('demonstrativoadm_contratados'),
		  'demonstrativoadm_estag_cabrados'  => $this->input->post('demonstrativoadm_estag_cabrados'),
		  'demonstrativoadm_empr_ativas'   => $this->input->post('demonstrativoadm_empr_ativas'),
		  'demonstrativoadm_estag_ativos'  => $this->input->post('demonstrativoadm_estag_ativos'),
		  'demonstrativoadm_estag_rescindidos'  => $this->input->post('demonstrativoadm_estag_rescindidos'),
		  
		  'demonstrativoadm_usuario_id'   => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativoadm_visivel'   => 1
		  
		);


		$resultado = $this->Demonstrativoadm_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Demonstrativoadm', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'demonstrativoadm_visivel' => 0
						
					);

		$resultado = $this->Demonstrativoadm_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Demonstrativoadm_model->pegarPorId($id);			
		$dadosView['permissoes']  = $this->Demonstrativoadm_model->todasPermissoes();	

		$dadosView['meio']       = 'demonstrativoadm/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Curso.php */
/* Location: ./application/controllers/Curso.php */