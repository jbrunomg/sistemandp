<?php
ini_set('memory_limit', '1024M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa


defined('BASEPATH') OR exit('No direct script access allowed');

class Estudante extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Estudante_model');
	}

	public function index()
	{

		$resultado = $this->Estudante_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'estudante/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Estudante_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Estudante_model->todosEstados();
		$dadosView['curso']       = $this->Estudante_model->todosCursos();
		$dadosView['ensino']      = $this->Estudante_model->todosInstEnsino();
		$dadosView['permissoes'] = $this->Estudante_model->tadasPermissoes();
		$dadosView['meio']       = 'estudante/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

	    $Habilitacao = ($this->input->post('ealualhabil') == 1) ? '1' : '0';
        $Veiculo = ($this->input->post('ealualvepro') == 1) ? '1' : '0';


        $cpf =  str_replace("-","",str_replace(".","",$this->input->post('salualcpf')));

        // Capturar Coordenada Endereço, estudante.   
		$rua    = str_replace(',','',str_replace(' ', '+', strtolower($this->input->post('saluallogra'))));

		$bairro = str_replace(' ', '+', strtolower($this->input->post('salualbairr')));

		$endereco = $rua.','.$bairro.','.'pernambuco00,brasil';

		$logLat = $this->geolocationEstudante($endereco);	
		
		if(!$logLat){
			$logLat['latitude']   = '';
			$logLat['longitude']  = '';
		}



		$dados = array(

 		  'salualnome'      => $this->input->post('salualnome'),
		  'salualcpf'       => $cpf,
		  'salualrg'        => $this->input->post('salualrg'),
		  'salualrg_org_expd'     => $this->input->post('salualrg_org_expd'),
		  'talualniver'     => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('talualniver')))),
		  'salualnatur'     => $this->input->post('salualnatur'), 
		  'salualestci'     => $this->input->post('salualestci'),	
		  'ealualsexo'      => $this->input->post('ealualsexo'),
		  'ealualhabil'      => $Habilitacao,	  	  
	  	  'ealualvepro'      => $Veiculo,	  
		  

		  'salualpai'   => $this->input->post('salualpai'),	
		  'salualpprof'  => $this->input->post('salualpprof'), // Prof - Pai

		  'salualmae'   => $this->input->post('salualmae'),		  
		  'salualmprof'  => $this->input->post('salualmprof'), // Prof - Mãe	   

		  'saluallogra'   => $this->input->post('saluallogra'),
		  'salualnumer'   => $this->input->post('salualnumer'), 
		  'salualcompl'   => $this->input->post('salualcompl'),		  
		  'salualcidad'   => $this->input->post('salualcidad'),
		  'salualbairr'   => $this->input->post('salualbairr'),
		  'salualestad'   => $this->input->post('salualestad'),
		  'salualcep'     => $this->input->post('salualcep'),


		  'salualtel01'   => $this->input->post('salualtel01'),
		  'salualtel02'   => $this->input->post('salualtel02'),
		  'salualtel03'   => $this->input->post('salualtel03'),
		  'salualemail'   => $this->input->post('salualemail'), 	 

		  
		  'ialualcurso'      => $this->input->post('pcurcucodig'),
		  'ialualcurso_nivel'      => $this->input->post('ialualcurso_nivel'),

		  'ialualinsen'      => $this->input->post('pensencodig'),
		  'salualmatricula'  => $this->input->post('salualmatricula'),
		  'salualperio'      => $this->input->post('salualperio'),
		  'salualturno'      => $this->input->post('salualturno'),
		  'salualtermino'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('salualtermino')))),
		  'salualdispo'      => $this->input->post('salualdispo'),
		

	  	  'ealualwindo'      => ($this->input->post('ealualwindo') == 1) ? '1' : '0',
	  	  'ealualword'       => ($this->input->post('ealualword') == 1) ? '1' : '0',
	  	  'ealualexel'       => ($this->input->post('ealualexel') == 1) ? '1' : '0',
	  	  'ealualpower'      => ($this->input->post('ealualpower') == 1) ? '1' : '0',
	  	  'ealuallinux'      => ($this->input->post('ealuallinux') == 1) ? '1' : '0',
	  	  'ealualinter'      => ($this->input->post('ealualinter') == 1) ? '1' : '0',

	  	  'ealualingle'      => ($this->input->post('ealualingle') == 1) ? '1' : '0',
	  	  'ealualespan'      => ($this->input->post('ealualespan') == 1) ? '1' : '0',
	  	  'ealualfranc'      => ($this->input->post('ealualfranc') == 1) ? '1' : '0',
	  	  'ealualalema'      => ($this->input->post('ealualalema') == 1) ? '1' : '0',
	  	  'salualoutro'      => $this->input->post('salualoutro'),

	  	  'saluallogin'      => $cpf,
		  'salualsenha'      => md5($cpf),

	  	  'salualobser'      => $this->input->post('salualobser'),
	  	  'salualexper'      => $this->input->post('salualexper'),


		  'estudante_visivel'   => 1,

		  'date_operacao'    => date('Y-m-d H:i:s'),
		  'date_cadastro'    => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  
		  'latitude'         => $logLat['latitude'],
		  'longitude'        => $logLat['longitude']	

		  
		);

 		// echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Estudante_model->checarCpfEstudantes($cpf);

        if($resultado){
           print "<script>alert('CPF já cadastrado, favor verificar se o CPF foi digitado corretamente!');</script>";
           print "<script>history.back();</script>";
          exit;
        }
	
		$resultado = $this->Estudante_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Estudante', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		

		$dadosView['dados']      = $this->Estudante_model->pegarPorId($id);
		$dadosView['curso']      = $this->Estudante_model->todosCursos();
		$dadosView['ensino']     = $this->Estudante_model->todosInstEnsino();
		$dadosView['cidades']    = $this->Estudante_model->todasCidadesIdEstado('16');
		$dadosView['estados']    = $this->Estudante_model->todosEstados();		
		$dadosView['permissoes'] = $this->Estudante_model->tadasPermissoes();
		$dadosView['meio']       = 'estudante/editar';



		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('palualcodig');
		$Habilitacao = ($this->input->post('ealualhabil') == 1) ? '1' : '0';
        $Veiculo = ($this->input->post('ealualvepro') == 1) ? '1' : '0';

        $cpf =  str_replace("-","",str_replace(".","",$this->input->post('salualcpf')));

        // Capturar Coordenada Endereço, estudante.   
		$rua    = str_replace(',','',str_replace(' ', '+', strtolower($this->input->post('saluallogra'))));

		$bairro = str_replace(' ', '+', strtolower($this->input->post('salualbairr')));

		$endereco = $rua.','.$bairro.','.'pernambuco00,brasil';

		$logLat = false; //$this->geolocationEstudante($endereco);	
		
		if(!$logLat){
			$logLat['latitude']   = '';
			$logLat['longitude']  = '';
		}

		//var_dump($this->input->post('talualniver'));die();
		$dados = array(
		  
		  'salualnome'   => $this->input->post('salualnome'),
		  'salualcpf'    => $cpf,
		  'salualrg'     => $this->input->post('salualrg'),
		  'salualrg_org_expd'     => $this->input->post('salualrg_org_expd'),
		  'talualniver'      => date('Y-m-d', strtotime($this->input->post('talualniver'))),
		  'salualnatur'      => $this->input->post('salualnatur'), 
		  'salualestci'      => $this->input->post('salualestci'),	
		  'ealualsexo'       => $this->input->post('ealualsexo'),
		  'ealualhabil'      => $Habilitacao,	  	  
	  	  'ealualvepro'      => $Veiculo,	  
		  

		  'salualpai'   => $this->input->post('salualpai'),	
		  'salualpprof'  => $this->input->post('salualpprof'), // Prof - Pai

		  'salualmae'   => $this->input->post('salualmae'),		  
		  'salualmprof'  => $this->input->post('salualmprof'), // Prof - Mãe	  

		  

		  'saluallogra'   => $this->input->post('saluallogra'),
		  'salualnumer'   => $this->input->post('salualnumer'), 
		  'salualcompl'   => $this->input->post('salualcompl'),		  
		  'salualcidad'   => $this->input->post('salualcidad'),
		  'salualbairr'   => $this->input->post('salualbairr'),
		  'salualestad'   => $this->input->post('salualestad'),
		  'salualcep'     => $this->input->post('salualcep'),


		  'salualtel01'   => $this->input->post('salualtel01'),
		  'salualtel02'   => $this->input->post('salualtel02'),
		  'salualtel03'   => $this->input->post('salualtel03'),
		  'salualemail'   => $this->input->post('salualemail'), 	 

		  
		  'ialualcurso'        => $this->input->post('pcurcucodig'),
		  'salualmatricula'    => $this->input->post('salualmatricula'),
		  'ialualcurso_nivel'  => $this->input->post('ialualcurso_nivel'),
		  'ialualinsen'      => $this->input->post('pensencodig'),
		  'salualperio'      => $this->input->post('salualperio'),
		  'salualturno'      => $this->input->post('salualturno'),
		  'salualtermino'    => date('Y-m-d', strtotime($this->input->post('salualtermino'))),
		  'salualdispo'      => $this->input->post('salualdispo'),
		

	  	  'ealualwindo'      => ($this->input->post('ealualwindo') == 1) ? '1' : '0',
	  	  'ealualword'       => ($this->input->post('ealualword') == 1) ? '1' : '0',
	  	  'ealualexel'       => ($this->input->post('ealualexel') == 1) ? '1' : '0',
	  	  'ealualpower'      => ($this->input->post('ealualpower') == 1) ? '1' : '0',
	  	  'ealuallinux'      => ($this->input->post('ealuallinux') == 1) ? '1' : '0',
	  	  'ealualinter'      => ($this->input->post('ealualinter') == 1) ? '1' : '0',

	  	  'ealualingle'      => ($this->input->post('ealualingle') == 1) ? '1' : '0',
	  	  'ealualespan'      => ($this->input->post('ealualespan') == 1) ? '1' : '0',
	  	  'ealualfranc'      => ($this->input->post('ealualfranc') == 1) ? '1' : '0',
	  	  'ealualalema'      => ($this->input->post('ealualalema') == 1) ? '1' : '0',
	  	  'salualoutro'      => $this->input->post('salualoutro'),

		  'saluallogin'      => $cpf,
		  'salualsenha'      => md5($cpf),

	  	  'salualobser'      => $this->input->post('salualobser'),
	  	  'salualexper'      => $this->input->post('salualexper'),

		  'estudante_visivel'   => 1,

		  'date_operacao'    => date('Y-m-d H:i:s'),
		  //'date_cadastro'    => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  
		  'latitude'         => $logLat['latitude'],
		  'longitude'        => $logLat['longitude']	
		  
		);

		//p($dados);

		$resultado = $this->Estudante_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Estudante', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'estudante_visivel' => 0
						
					);

		$resultado = $this->Estudante_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Estudante_model->pegarPorId($id);
		$dadosView['curso']       = $this->Estudante_model->todosCursos();
		$dadosView['ensino']      = $this->Estudante_model->todosInstEnsino();
		$dadosView['cidades']     = $this->Estudante_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Estudante_model->todosEstados();		
		$dadosView['permissoes']  = $this->Estudante_model->tadasPermissoes();	

		$dadosView['meio']       = 'estudante/visualizar';

		$this->load->view('tema/tema',$dadosView);

	}

	public function visualizarficha()
	{			
		if((!$this->session->userdata('usuario_id')) || (!$this->session->userdata('permissao')) ){
        		redirect('Estudante', 'refresh');
        }



		$id = $this->uri->segment(3);

		$dadosView['dados']         = $this->Estudante_model->pegarPorId($id);

		$dadosView['contratos']     = $this->Estudante_model->pegarContratoPorId($id);
		$dadosView['recrutamento']  = $this->Estudante_model->pegarRecrutamentoPorId($id);

		// var_dump($dadosView['contratos'] );die();

		$dadosView['dados'][0]->cpf = $this->Mask("###.###.###-##",$this->soNumero($dadosView['dados'][0]->salualcpf));
		$dadosView['dados'][0]->cep = $this->Mask("#####-###",$this->soNumero($dadosView['dados'][0]->salualcep));
	  //$dadosView['dados'][0]->fone1 = $this->Mask("(##)####-####",$this->soNumero($dadosView['dados'][0]->salualtel01));
	  //$dadosView['dados'][0]->fone2 = $this->Mask("(##)####-####",$this->soNumero($dadosView['dados'][0]->salualtel02));

		//var_dump($dadosView['dados'][0]->fone2);die();
		// https://pt.stackoverflow.com/questions/25651/tem-como-colocar-mascara-em-php-dinamicamente		

		$dadosView['curso']       = $this->Estudante_model->todosCursos();
		$dadosView['ensino']      = $this->Estudante_model->todosInstEnsino();
		$dadosView['cidades']     = $this->Estudante_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Estudante_model->todosEstados();		
		$dadosView['permissoes']  = $this->Estudante_model->tadasPermissoes();
		
		$dadosView['emitente']    = $this->Estudante_model->agenciaEmitente();	

		//$dadosView['meio']       = 'estudante/fichainscricao';
		$dadosView['meio']        = 'estudante/fichainscricaobranco';

		//$this->load->view('tema/tema',$dadosView);

		$html = $this->load->view('estudante/fichainscricaobranco',$dadosView,TRUE);		
		//gerarPdf($html,'ficha',false);
		echo $html;


	}


    public function soNumero($str)
    {
        return preg_replace("/[^0-9]/", "", $str);
    }

	public function Mask($mask,$str){
    $str = str_replace(" ","",$str);
    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

	}


	public function baixarcv()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Estudante_model->pegarPorId($id);

	}

    public function listaAjax()
	{
		// DB table to use
		$table = 'vw_listaEstudante';
		// $table = 'vw_estudante';
		 
		// Table's primary key
		$primaryKey = 'palualcodig';


		 
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
		    array( 'db' => 'salualnome', 'dt' => 0 ),
		    array( 'db' => 'salualemail', 'dt' => 1 ),
		    array( 'db' => 'salualcpf',  'dt' => 2 ),
		    array( 'db' => 'scurcunome', 'dt' => 3 ),		    		   
		    array( 'db' => 'date_operacao',   'dt' => 4 ),
		    // array( 'db' => 'contrato', 'dt' => 5 ),		    
		
		    array(
		        'db'        => 'palualcodig',
		        'dt'        => 5,
		        'formatter' => function( $d, $row ) {
		            return '<ul class="icons-list">
		            			<?php if(checarPermissao("eEstudante")){ ?>
								<li class="text-primary-600"><a href="'.base_url().$this->uri->segment(1).'/editar/'.$d.'" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao("dEstudante")){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="'.$this->uri->segment(1).'/excluir" registro="'.$d.'"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao("vEstudante")){ ?>						
								<li class="text-teal-600"><a href="'.base_url().$this->uri->segment(1).'/visualizar/'.$d.'" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>
								<?php if(checarPermissao("vEstudante")){ ?>						
								<li class="text-info-700"><a target="_blank" href="'.base_url().$this->uri->segment(1).'/visualizarficha/'.$d.'" data-popup="tooltip" title="Ficha de Inscrição"><i class="icon-book"></i></a></li>	
								<?php } ?>
								<?php if(checarPermissao("vEstudante")){ ?>														
								<li class="text-teal-900"><a target="_blank" href="'.base_url().$this->uri->segment(1).'/visualizarcv/'.$d.'" data-popup="tooltip" title="Baixar Curriculo"><i class="icon-download"></i></a></li>	
								<?php } ?>
							</ul>';
		        }
		    )
		);


		// SQL server connection information
		$sql_details = array(
		    'user' => $this->db->username,
		    'pass' => $this->db->password,
		    'db'   => $this->db->database,
		    'host' => $this->db->hostname
		);
		 
		 
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		require( APPPATH.'/libraries/ssp.class.php' );
		 
		echo json_encode(
		    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	}


	public function visualizarCv()
	{
		$id = $this->uri->segment(3);

		$html = 'Estudante sem Curriculo';

		$valor = $this->Estudante_model->pegarPorId($id);

		// var_dump($valor);die();		

		if (!empty($valor[0]->url)) {

			$html =  str_replace("sistema.","iframe.",base_url()).$valor[0]->url;

			echo anchor($html);
		} else {

			echo 'Estudante sem Curriculo';

		}			
		
	
	}


    public function geolocationEstudante($address)
	{

		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');

		$output= json_decode($geocode);

		//v($output);

		if($output->status <> 'OVER_QUERY_LIMIT'){ 

		if($output->status <> 'ZERO_RESULTS'){

			//v($output);die();
		
		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;

	    $dados['latitude']   = $lat;
	    $dados['longitude']  = $long;

	    return $dados;

		} else
		{
			return false;
		}


	} else{
		echo 'You have exceeded your daily request quota for this API';
	}

	}

	public function selecionarCidadeEstudantes()
	{		
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Estudante_model->selecionarCidadeEstudantes($termo['term']);
		echo json_encode($ret);
	}

    public function selecionarCursos()
	{		
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Estudante_model->selecionarCursos($termo['term']);
		echo json_encode($ret);
	}

}

/* End of file Estudante.php */
/* Location: ./application/controllers/Estudante.php */