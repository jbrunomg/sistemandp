<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Precadastro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Precadastro_model');
	}

	public function index()
	{
		$resultado = $this->Precadastro_model->listar();		
		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'precadastro/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{	
	    $dadosView['empresa']   = $this->Precadastro_model->todasEmpresas();	
		$dadosView['meio']       = 'precadastro/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$empresaid = explode('|', $this->input->post('empresa'));

		if (CLIENTE == 'nudep') {
			# code...
			$msgEmail = 'Olá, '.$this->input->post('estudante').'!</br> Precisamos do seu cadastro no nosso banco de dados para darmos continuidade no seu contrato com a empresa: '.$empresaid[1].'.</br> Acesse o link e preencha as informações solicitadas: </br> <a href="https://www.'.CLIENTE.'.com.br/estudantes/cadastroEstudantes?email='.$this->input->post('email').'" target="_blank" >https://www.'.CLIENTE.'.com.br/estudantes/cadastroEstudantes/</a> <br><br><strong>'.$this->input->post('precadastro_obs').'</strong>';
		} else {
			$msgEmail = 'Olá, '.$this->input->post('estudante').'!</br> Precisamos do seu cadastro no nosso banco de dados para darmos continuidade no seu contrato com a empresa: '.$empresaid[1].'.</br> Acesse o link e preencha as informações solicitadas: </br> <a href="https://www.'.CLIENTE.'.com.br/estudante/carregarPerfil/?email='.$this->input->post('email').'" target="_blank" >https://www.'.CLIENTE.'.com.br/estudantes/cadastroEstudantes/</a> <br><br><strong>'.$this->input->post('precadastro_obs').'</strong>';
		}

	    $dados = array(		  	  		  
		  'precadastro_estudante'  => $this->input->post('estudante'),	
		  'precadastro_empresa_id' => $empresaid[0],
		  'precadastro_email'      => $this->input->post('email'),
		  'precadastro_status'     => 'Preparado',
		  'precadastro_assunto'    => 'Cadastro - estágio',
		  'precadastro_mensagem'   => $msgEmail,
		  'precadastro_obs'        => $this->input->post('precadastro_obs'),	
		  'precadastro_data_cadastro' => date('Y-m-d H:i:s')
		);



		// var_dump($dados['precadastro_email']);die();

		$resultado = $this->Precadastro_model->inserir($dados);

		$email    = $dados['precadastro_email'];
		$assunto  = $dados['precadastro_assunto'];
		$conteudo = $dados['precadastro_mensagem'];

		$resultado = $this->enviarEmails($email,$conteudo,$assunto);

		// foreach ($emailsBanco as $email) {

		// 	$dados = array(		  	  		  
		// 	  'maladireta_enviar_email'     => $email->salualemail,			
		// 	  'maladireta_enviar_assunto'   => $assunto,
		// 	  'maladireta_enviar_conteudo'  => $conteudo
		
		// 	);

		// 	$resultado = $this->Precadastro_model->inserircron($dados);

			
		// }
		

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('precadastro', 'refresh');
	}



	public function excluir()
	{	
		//var_dump($this->input->post('id'));die();
		$id = $this->input->post('id');

		$dados = array(
						'precadastro_visivel' => 0						
					);

		$resultado = $this->Precadastro_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['empresa']   = $this->Precadastro_model->todasEmpresas();	
		$dadosView['dados']      = $this->Precadastro_model->pegarPorId($id);			
		$dadosView['meio']       = 'precadastro/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

	public function EnviarCron()
	{
		$emailsEnviar = $this->Precadastro_model->pegarEmailsCron();
		

		foreach ($emailsEnviar as $email) {

			$envio = $this->enviarEmails($email['precadastro_email'],$email['precadastro_mensagem'],$email['precadastro_assunto']);

			$id = $email['precadastro_id'];
			$qtd = '1' + $email['precadastro_email_enviado'];			

			if ($envio) {
				$resultado = $this->Precadastro_model->editarcron($id,$qtd);
			} else {
				$resultado = $this->Precadastro_model->editarcron($id,'0');
			}
			
		}

		//$envio = $this->enviarEmails('bruno@wdmtecnologia.com.br',$emailsEnviar['maladireta_enviar_conteudo'],$emailsEnviar['maladireta_enviar_assunto']); // Teste email Cron
	}


	public function enviarEmails($emails,$conteudo,$assunto)
	{
		//die('AQUI');

		$this->load->library('email'); // inserido no AutoLoad
		// $this->email->from('selecao@nudep.com.br', 'Informativos');
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);

		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		//v($this->email->send());
		
		if($this->email->send())
		//if(true)
        {
            $this->session->set_flashdata('sucesso','Email enviado com sucesso!');
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('erro',$this->email->print_debugger());
            return false;
        }
	}

	public function selecionarEstudantes()
	{
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Precadastro_model->selecionarEstudantes($termo['term']);
		echo json_encode($ret);
	}

}

/* End of file Emails.php */
/* Location: ./application/controllers/Emails.php */