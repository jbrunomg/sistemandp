<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demonstrativofin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Demonstrativofin_model');
	}

	public function index()
	{

		$resultado = $this->Demonstrativofin_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'demonstrativofin/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Demonstrativofin_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativofin/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		
		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativofin_ano'   => $dados[0],
		  'demonstrativofin_mes'   => $dados[1],


		  'demonstrativofin_faturamento' => $this->input->post('demonstrativofin_faturamento'),
		  'demonstrativofin_credito'     => $this->input->post('demonstrativofin_credito'),
		  'demonstrativofin_debito'      => $this->input->post('demonstrativofin_debito'),
		  'demonstrativofin_lucro_prejuizo' => $this->input->post('demonstrativofin_lucro_prejuizo'),
		  'demonstrativofin_cc_desc_serv'   => $this->input->post('demonstrativofin_cc_desc_serv'),
		  'demonstrativofin_cp_saldo_inicial' => $this->input->post('demonstrativofin_cp_saldo_inicial'),
		  'demonstrativofin_cp_renda_credito' => $this->input->post('demonstrativofin_cp_renda_credito'),
		  'demonstrativofin_cp_desc_taxas'    => $this->input->post('demonstrativofin_cp_desc_taxas'),
		  'demonstrativofin_cp_mov_debito'    => $this->input->post('demonstrativofin_cp_mov_debito'),
		  'demonstrativofin_cp_mov_credito'   => $this->input->post('demonstrativofin_cp_mov_credito'),
		  'demonstrativofin_cp_saldo_final'   => $this->input->post('demonstrativofin_cp_saldo_final'),


		  'demonstrativofin_usuario_id'       => $this->session->userdata('usuario_id'), // ID na Sessão	
		  'demonstrativofin_visivel' => 1
		  
		);

        // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Demonstrativofin_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Demonstrativofin', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Demonstrativofin_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Demonstrativofin_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativofin/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('demonstrativofin_id');


		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativofin_ano'   => $dados[0],
		  'demonstrativofin_mes'   => $dados[1],

		  'demonstrativofin_faturamento' => $this->input->post('demonstrativofin_faturamento'),
		  'demonstrativofin_credito'     => $this->input->post('demonstrativofin_credito'),
		  'demonstrativofin_debito'      => $this->input->post('demonstrativofin_debito'),
		  'demonstrativofin_lucro_prejuizo' => $this->input->post('demonstrativofin_lucro_prejuizo'),
		  'demonstrativofin_cc_desc_serv'   => $this->input->post('demonstrativofin_cc_desc_serv'),
		  'demonstrativofin_cp_saldo_inicial' => $this->input->post('demonstrativofin_cp_saldo_inicial'),
		  'demonstrativofin_cp_renda_credito' => $this->input->post('demonstrativofin_cp_renda_credito'),
		  'demonstrativofin_cp_desc_taxas'    => $this->input->post('demonstrativofin_cp_desc_taxas'),
		  'demonstrativofin_cp_mov_debito'    => $this->input->post('demonstrativofin_cp_mov_debito'),
		  'demonstrativofin_cp_mov_credito'   => $this->input->post('demonstrativofin_cp_mov_credito'),
		  'demonstrativofin_cp_saldo_final'   => $this->input->post('demonstrativofin_cp_saldo_final'),

		  
		  'demonstrativofin_usuario_id'   => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativofin_visivel'   => 1
		  
		);


		$resultado = $this->Demonstrativofin_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Demonstrativofin', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'demonstrativofin_visivel' => 0
						
					);

		$resultado = $this->Demonstrativofin_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Demonstrativofin_model->pegarPorId($id);			
		$dadosView['permissoes']  = $this->Demonstrativofin_model->todasPermissoes();	

		$dadosView['meio']       = 'demonstrativofin/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Curso.php */
/* Location: ./application/controllers/Curso.php */