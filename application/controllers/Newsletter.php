<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Newsletter_model');
	}

	public function index()
	{

		$resultado = $this->Newsletter_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'newsletter/listar';
		$this->load->view('tema/tema',$dadosView);
	}



	public function adicionar()
	{		
		//$dadosView['newsletter'] = $this->Newsletter_model->todosNewsletters();
		$dadosView['permissoes'] = $this->Newsletter_model->todasPermissoes();
		$dadosView['meio']       = 'newsletter/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	
		$dados = array(

		  'newsemail'    => $this->input->post('newsemail'),
		  		    	  
		  'newsletter_visivel'   => 1
		  
		);

        // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Newsletter_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Newsletter', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Newsletter_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Newsletter_model->todasPermissoes();
		$dadosView['meio']       = 'newsletter/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('newsid');

		$dados = array(
		  
		  'newsemail'    => $this->input->post('newsemail'),
		  		    	  
		  'newsletter_visivel'   => 1
		  
		);


		$resultado = $this->Newsletter_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Newsletter', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'newsletter_visivel' => 0
						
					);

		$resultado = $this->Newsletter_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Newsletter_model->pegarPorId($id);				
		$dadosView['permissoes']  = $this->Newsletter_model->todasPermissoes();	

		$dadosView['meio']       = 'newsletter/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Newsletter.php */
/* Location: ./application/controllers/Newsletter.php */