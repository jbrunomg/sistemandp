<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Curso_model');
	}

	public function index()
	{

		$resultado = $this->Curso_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'curso/listar';
		$this->load->view('tema/tema',$dadosView);
	}



	public function adicionar()
	{		
		//$dadosView['curso']      = $this->Curso_model->todosCursos();
		$dadosView['permissoes'] = $this->Curso_model->tadasPermissoes();
		$dadosView['meio']       = 'curso/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	
		$dados = array(

		  'scurcunome'    => $this->input->post('scurcunome'),
		  // 'scurcunivel'    => $this->input->post('scurcunivel'),
		  'fcurcuadmin'   => $this->session->userdata('usuario_id'), // ID na Sessão
		    	  
		  'curso_visivel'   => 1
		  
		);

  	    // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Curso_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Curso', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Curso_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Curso_model->tadasPermissoes();
		$dadosView['meio']       = 'curso/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('pcurcucodig');

		$dados = array(
		  
		  'scurcunome'    => $this->input->post('scurcunome'),
		  // 'scurcunivel'    => $this->input->post('scurcunivel'),
	      'fcurcuadmin'   => $this->session->userdata('usuario_id'), // ID na Sessão

    	  'curso_visivel'   => 1
		  
		);


		$resultado = $this->Curso_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Curso', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'curso_visivel' => 0
						
					);

		$resultado = $this->Curso_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Curso_model->pegarPorId($id);
		$dadosView['cidades']     = $this->Curso_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Curso_model->todosEstados();		
		$dadosView['permissoes']  = $this->Curso_model->tadasPermissoes();	

		$dadosView['meio']       = 'curso/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Curso.php */
/* Location: ./application/controllers/Curso.php */