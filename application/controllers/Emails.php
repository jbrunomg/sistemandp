<?php
ini_set('memory_limit', '2048M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa

defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Emails_model');
	}

	public function index()
	{
		$resultado = $this->Emails_model->listar();
		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'emails/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{	
		//$dadosView['estudantes'] = $this->Emails_model->listarEstudantes();		
		$dadosView['cursos']     = $this->Emails_model->listarCursos();	
		$dadosView['meio']       = 'emails/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

		$cursos     = '';
		$estudantes = '';
		$sexo       = '';
		$estudanteBanco = '';
		$cursosBanco = '';

		if($this->input->post('cursos')){
			$cursos = $this->input->post('cursos');
			$cursosBanco = implode(',', $cursos);
		}

		if($this->input->post('estudante')){
			$estudantes = $this->input->post('estudante');
			$estudanteBanco = implode(',', $estudantes);
		}

		if($this->input->post('sexo')){
			$sexo = $this->input->post('sexo');
		}

		$emailsBanco = $this->Emails_model->pegarEmails($cursos, $estudantes, $sexo);

		$dados = array(		  	  		  
		  'maladireta_usuario_envio'=> $this->session->userdata('usuario_id'),	
		  'maladireta_estudantes'   => $estudanteBanco,
		  'maladireta_cursos'       => $cursosBanco,
		  'maladireta_sexo'         => $this->input->post('sexo'),
		  'maladireta_assunto'     => $this->input->post('assunto'),
		  'maladireta_mensagem'     => $this->input->post('descricao'),	
		  'maladireta_data_envio'   => date('Y-m-d H:i:s'),
		  'maladireta_atualizacao'  => date('Y-m-d H:i:s')
		);

		$resultado = $this->Emails_model->inserir($dados);

		//$emails   = array('jbrunomg@hotmail.com','jbrunomg@gmail.com');
		$assunto  = $this->input->post('assunto');
		$conteudo = $this->input->post('descricao');

		//$this->enviarEmails($emails,$conteudo,$assunto);

		foreach ($emailsBanco as $email) {

			$dados = array(		  	  		  
			  'maladireta_enviar_email'     => $email->salualemail,			
			  'maladireta_enviar_assunto'   => $assunto,
			  'maladireta_enviar_conteudo'  => $conteudo
		
			);

			$resultado = $this->Emails_model->inserircron($dados);

			//$envio = $this->enviarEmails($email->salualemail,$conteudo,$assunto);
		}
		

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('emails', 'refresh');
	}



	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'maladireta_visivel' => 0,
						'maladireta_atualizacao' => date('Y-m-d H:i:s')
						
					);

		$resultado = $this->Emails_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Emails_model->pegarPorId($id);			
		$dadosView['meio']       = 'emails/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

	public function EnviarCron()
	{
		$emailsEnviar = $this->Emails_model->pegarEmailsCron();


		
    	foreach ($emailsEnviar as $email) {

			$listaEmail = explode(',', $email['maladireta_enviar_email']);

			$x = 0;

			do {

			  $envio = $this->enviarEmails($listaEmail[$x],$email['maladireta_enviar_conteudo'],$email['maladireta_enviar_assunto']);

			  $x++;
			} while ($x <= count($listaEmail));					

			
			$id = $email['maladireta_enviar_id'];			

			if ($envio) {
				$resultado = $this->Emails_model->editarcron($id,'SIM');
			} else {
				$resultado = $this->Emails_model->editarcron($id,'NAO');
			}
			
		}

		//$envio = $this->enviarEmails('bruno@wdmtecnologia.com.br',$emailsEnviar['maladireta_enviar_conteudo'],$emailsEnviar['maladireta_enviar_assunto']); // Teste email Cron
	}


	public function enviarEmails($emails,$conteudo,$assunto)
	{
		$this->load->library('email'); // inserido no AutoLoad
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);

		$this->email->to($emails); 
		//$this->email->bcc($emails); // Copia Oculta
		
		$this->email->message($conteudo);

		//v($emails);
		
		if($this->email->send())
		//if(true)
        {
            $this->session->set_flashdata('success','Email enviado com sucesso!');
            echo "TRUE";
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('error',$this->email->print_debugger());
            echo $this->email->print_debugger();
            return false;
        }
	}

	public function selecionarEstudantes()
	{
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Emails_model->selecionarEstudantes($termo['term']);
		echo json_encode($ret);
	}

}

/* End of file Emails.php */
/* Location: ./application/controllers/Emails.php */