<?php
ini_set('memory_limit', '2048M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa

defined('BASEPATH') or exit('No direct script access allowed');

class Recrutamento extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Vaga_model');
        $this->load->model('Recrutamento_model');
        $this->load->model('Curso_model');

    }

    public function index()
    {
        $dadosView['meio']    = 'recrutamento/index';
        $dadosView['dados']   = $this->Recrutamento_model->listar();
        $this->load->view('tema/tema', $dadosView);
    }

    public function adicionar()
    {   

        $dadosView['meio']    = 'recrutamento/adicionar';
        $dadosView['empresa'] = $this->Vaga_model->todasEmpresas();
        $dadosView['cursos']  = $this->Vaga_model->todosCursos();
        // $dadosView['cursos']  = $this->Curso_model->listar();
        $this->load->view('tema/tema', $dadosView);
    }

    public function adicionarExe()
    {
        
        // $respCursos = ";";
        // if ($_POST['curso'] != 0)
        // $respCursos .= $this->input->post('curso').';';
        // if ($_POST['curso2'] != 0)
        // $respCursos .= $this->input->post('curso2').';';
        // if ($_POST['curso3'] != 0)
        // $respCursos .= $this->input->post('curso3').';';
        // if ($_POST['curso4'] != 0)
        // $respCursos .= $this->input->post('curso4').';';
        // if ($_POST['curso5'] != 0)
        // $respCursos .= $this->input->post('curso5').';';

        $todosOsDadosDoPost = $this->input->post();

        $bolsa = ($this->input->post('bolsa') == 1) ? '1' : '0';
        $vale  = ($this->input->post('vale') == 1) ? '1' : '0';
        $refeicao = ($this->input->post('refeicao') == 1) ? '1' : '0';
        $outros = ($this->input->post('outros') == 1) ? '1' : '0';


        $dadosParaInserir = [
            'recrut_empr_id'  => $todosOsDadosDoPost['empresa'],
            'recrut_end_esta' => $todosOsDadosDoPost['logradouro'],            
            'recrut_bairro'   => $todosOsDadosDoPost['bairro'],  
            'recrut_cidade'   => $todosOsDadosDoPost['cidade'],
            'recrut_uf'       => $todosOsDadosDoPost['estado'],          
            'recrut_cep'      => $todosOsDadosDoPost['sempemcep'],
            'recrut_inicio'   => $todosOsDadosDoPost['inicio'],
            'recrut_duracao'  => $todosOsDadosDoPost['duracao'],
            'recrut_horario'  => $todosOsDadosDoPost['horario'],
            'recrut_bolsa_auxilio' => $bolsa,
            'recrut_valor_auxilio' => $todosOsDadosDoPost['valor_auxilio'],
            'recrut_transporte'    => $vale,
            'recrut_valor_transporte' => $todosOsDadosDoPost['recrut_valor_transporte'],
            'recrut_refei'     => $refeicao,
            'recrut_valor_refei' => $todosOsDadosDoPost['recrut_valor_refei'],            
            'recrut_outro'     => $outros,
            'recrut_valor_outro' => $todosOsDadosDoPost['recrut_valor_outro'],
            'recrut_atividade' => $todosOsDadosDoPost['atv-previstas'],
            'recrut_respo'     => $todosOsDadosDoPost['responsavel'],
            'recrut_hs_entrev' => $todosOsDadosDoPost['h-entrevista'],

            'recrut_curso'   => $todosOsDadosDoPost['curso'],            
            'recrut_nivel'   => $todosOsDadosDoPost['nivel'],
            'recrut_curso2'  => $todosOsDadosDoPost['curso2'],            
            'recrut_nivel2'  => $todosOsDadosDoPost['nivel2'],
            'recrut_curso3'  => $todosOsDadosDoPost['curso3'],            
            'recrut_nivel3'  => $todosOsDadosDoPost['nivel3'],
            'recrut_curso4'  => $todosOsDadosDoPost['curso4'],            
            'recrut_nivel4'  => $todosOsDadosDoPost['nivel4'],

            'recrut_grau_serie'    => $todosOsDadosDoPost['recrut_grau_serie'],
            'recrut_grau_modulo'   => $todosOsDadosDoPost['recrut_grau_modulo'],            
            'recrut_grau_periodo'  => $todosOsDadosDoPost['recrut_grau_periodo'],

            'recrut_periodo' => $todosOsDadosDoPost['periodo'],
            'recrut_sexo'    => $todosOsDadosDoPost['sexo'],
            'recrut_indicado'  => $todosOsDadosDoPost['indicado'],            
            'recrut_requisito' => $todosOsDadosDoPost['requisitosExigidos'],
            'recrut_divul'     => $todosOsDadosDoPost['divulgacao'],

            'recrut_observacao' => $todosOsDadosDoPost['recrut_observacao'], 
            'recrut_dataexpir'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('recrut_dataexpir')))),

            'recrut_data_status' => date('Y-m-d'),
            'recrut_data_cadastro' =>  date("Y-m-d H:i:s", time())
        ];

        // echo '<pre>';
        // var_dump($dadosParaInserir);die();

        
        if (is_numeric($id = $this->Recrutamento_model->add('tbrecrutamento', $dadosParaInserir, true)) ) {
            if ($todosOsDadosDoPost['indicado'] == '0') {
                // code...                
            $vaga_id =  $this->Recrutamento_model->inserirVaga($id);

            $sql = " UPDATE tbrecrutamento SET vaga_id = ? WHERE `recrut_id` = ? ";

            $this->db->query($sql, array($vaga_id, $id));

            //var_dump($res);die();
            }

            $this->session->set_flashdata('sucesso','Registro inserido com sucesso!');
            redirect('Recrutamento/editar/'.$id);

        } else {
            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
            // $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
        }

        redirect('Recrutamento', 'refresh');
    }

    public function pegarEmpresaPeloID()
    {
        $todosOsDadosDoPost = $this->input->post();
        if (isset($todosOsDadosDoPost['id']) && $todosOsDadosDoPost['id'] > 0) {
            echo json_encode($this->Vaga_model->pegarDadosDaEmpresa($todosOsDadosDoPost['id']));
        } else {
            echo false;
        }
    }

    public function excluir()
    {
        $id = $this->input->post('id');

        $dados = array(
            'recrutamento_visivel' => '0'
        );

        $resultado = $this->Recrutamento_model->excluir($id, $dados);

        if ($resultado) {
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function visualizar()
    {
        $id = $this->uri->segment(3);
        $dadosView['meio']       = 'recrutamento/visualizar';
        $dadosView['data']       = $this->Recrutamento_model->pegarPorId($id);
        
        $dadosView['recrut']     = $this->Recrutamento_model->pegarRecrutFinalPorId($id);

        $dadosView['empresa']    = $this->Vaga_model->todasEmpresas();
        $dadosView['cursos']     = $this->Vaga_model->todosCursos();
        //$dadosView['cursos']     = $this->Curso_model->listar();
        $this->load->view('tema/tema', $dadosView);
    }

    public function editar()
    {
        $id = $this->uri->segment(3);
       
        $dadosView['data']     = $this->Recrutamento_model->pegarPorId($id);
        $dadosView['recrut']   = $this->Recrutamento_model->pegarRecrutPorId($id);      

        $dadosView['empresa']  = $this->Vaga_model->todasEmpresas();
        $dadosView['cursos']   = $this->Vaga_model->todosCursos();

        // $cursos = explode(';', $dadosView['data'][0]->recrut_curso);
        // foreach($cursos as $c) {
        //   if (!empty($c)) {
        //     $dadosView['cursosSelecionados'][] = $c;
        //   }
        // }

        $dadosView['meio']     = 'recrutamento/editar';
        $this->load->view('tema/tema', $dadosView);
    }

    public function editarExe()
    {

        // $respCursos = ";";
        // if ($_POST['curso'] != 0)
        // $respCursos .= $this->input->post('curso').';';
        // if ($_POST['curso2'] != 0)
        // $respCursos .= $this->input->post('curso2').';';
        // if ($_POST['curso3'] != 0)
        // $respCursos .= $this->input->post('curso3').';';
        // if ($_POST['curso4'] != 0)
        // $respCursos .= $this->input->post('curso4').';';
        // if ($_POST['curso5'] != 0)
        // $respCursos .= $this->input->post('curso5').';';


        $todosOsDadosDoPost = $this->input->post();


        //var_dump($todosOsDadosDoPost);die();

        $bolsa = (isset($todosOsDadosDoPost['bolsa']) == 1) ? '1' : '0';
        $vale  = (isset($todosOsDadosDoPost['vale']) == 1) ? '1' : '0';
        $refeicao = (isset($todosOsDadosDoPost['refeicao']) == 1) ? '1' : '0';
        $outros = (isset($todosOsDadosDoPost['outro']) == 1) ? '1' : '0';



        $dadosParaInserir01 = [
            'recrut_empr_id'  => $todosOsDadosDoPost['empresa'],
            'recrut_end_esta' => $todosOsDadosDoPost['logradouro'],            
            'recrut_bairro'   => $todosOsDadosDoPost['bairro'], 
            'recrut_cidade'   => $todosOsDadosDoPost['cidade'],
            'recrut_uf'       => $todosOsDadosDoPost['estado'],           
            'recrut_cep'      => $todosOsDadosDoPost['sempemcep'],
            'recrut_inicio'   => $todosOsDadosDoPost['inicio'],
            'recrut_duracao'  => $todosOsDadosDoPost['duracao'],
            'recrut_horario'  => $todosOsDadosDoPost['horario'],         

            'recrut_bolsa_auxilio'    => $bolsa,
            'recrut_valor_auxilio'    => $todosOsDadosDoPost['valor_auxilio'],
            'recrut_transporte'       => $vale,
            'recrut_valor_transporte' => $todosOsDadosDoPost['recrut_valor_transporte'],
            'recrut_refei'       => $refeicao,
            'recrut_valor_refei' => $todosOsDadosDoPost['recrut_valor_refei'],            
            'recrut_outro'       => $outros,
            'recrut_valor_outro' => $todosOsDadosDoPost['recrut_valor_outro'],
            
            'recrut_atividade' => $todosOsDadosDoPost['atv-previstas'],
            'recrut_respo'     => $todosOsDadosDoPost['responsavel'],
            'recrut_hs_entrev' => $todosOsDadosDoPost['h-entrevista'],
            // 'recrut_curso' => $respCursos,
            // 'recrut_nivel' => $todosOsDadosDoPost['nivel'],

            'recrut_curso'   => $todosOsDadosDoPost['curso'],            
            'recrut_nivel'   => $todosOsDadosDoPost['nivel'],
            'recrut_curso2'  => $todosOsDadosDoPost['curso2'],            
            'recrut_nivel2'  => $todosOsDadosDoPost['nivel2'],
            'recrut_curso3'  => $todosOsDadosDoPost['curso3'],            
            'recrut_nivel3'  => $todosOsDadosDoPost['nivel3'],
            'recrut_curso4'  => $todosOsDadosDoPost['curso4'],            
            'recrut_nivel4'  => $todosOsDadosDoPost['nivel4'],
            'recrut_grau_serie'    => $todosOsDadosDoPost['recrut_grau_serie'],
            'recrut_grau_modulo'   => $todosOsDadosDoPost['recrut_grau_modulo'],
            'recrut_grau_periodo'  => $todosOsDadosDoPost['recrut_grau_periodo'],

            'recrut_periodo'       => $todosOsDadosDoPost['periodo'],
            'recrut_sexo'          => $todosOsDadosDoPost['sexo'],
            'recrut_indicado'      => $todosOsDadosDoPost['indicado'],     
            

            'recrut_requisito'     => $todosOsDadosDoPost['requisitosExigidos'],
            'recrut_divul'         => $todosOsDadosDoPost['divulgacao'],

            'recrut_solicitacao_site' => 1,

            'recrut_observacao'   => $todosOsDadosDoPost['recrut_observacao'], 
            'recrut_dataexpir'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('recrut_dataexpir')))) 

        ];

        if ($todosOsDadosDoPost['recrut_status'] <> $todosOsDadosDoPost['recrut_status_old']) {
            # code...       

            $dadosParaInserir02 = [

            'recrut_status'        => $todosOsDadosDoPost['recrut_status'],
            'recrut_data_status' => date('Y-m-d')

            ];

        } else {            

            $dadosParaInserir02 = [

            'recrut_status'        => $todosOsDadosDoPost['recrut_status']

            ];
        }


        $dadosParaInserir = array_merge($dadosParaInserir01,$dadosParaInserir02);

       // var_dump($dadosParaInserir);die();

        $resultado = $this->Recrutamento_model->editar($todosOsDadosDoPost['idEdicao'], $dadosParaInserir);

        if ($resultado) {


            if ($todosOsDadosDoPost['recrut_status'] == 'AGUARDANDO' AND $todosOsDadosDoPost['vaga_id'] <> '')  {
                // Retirar do site a vaga que está aguardando...
                $dados = array(                
                    'testvaexpir'  =>  date('Y-m-d',strtotime('-1 day'))  
                ); 
            }elseif ($todosOsDadosDoPost['recrut_status'] == 'CANCELADO'  AND $todosOsDadosDoPost['vaga_id'] <> '') {
                 // Retirar do site a vaga que está cancelada 
                $dados = array(                   
                    'vaga_visivel'  => '0'                    

                ); 
            }


            if ($todosOsDadosDoPost['recrut_status'] == 'ABERTO' AND $todosOsDadosDoPost['vaga_id'] <> '')  {
                // 
                $dados = array(  
                    'vaga_visivel'  => '1',               
                    'testvaexpir'  =>  date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('recrut_dataexpir'))))  
                ); 
            }
            
            
            $this->Recrutamento_model->editarVaga($todosOsDadosDoPost['vaga_id'],$todosOsDadosDoPost['idEdicao'],$dados);
         
            $this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
        }else{
            $this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
        }

        redirect('Recrutamento', 'refresh');
    }

    public function recrutar($id)
    {

        // fazer busca do perfil estudante conforme solicitação cliente

        $dadosView['meio']     = 'recrutamento/editar';
        $dadosView['data']     = $this->Recrutamento_model->pegarPorId($id);      

        // $cursos = explode(';', $dadosView['data'][0]->recrut_curso);
        // foreach($cursos as $c) {
        //   if (!empty($c)) {
        //     $dadosView['cursosSelecionados'][] = $c;
        //   }
        // }


        //$curso    = $dadosView['cursosSelecionados'];
        $curso    = $dadosView['data'][0]->recrut_curso;
        $nivel    = $dadosView['data'][0]->recrut_nivel;  
        $curso2   = $dadosView['data'][0]->recrut_curso2;
        $nivel2   = $dadosView['data'][0]->recrut_nivel2; 

        $curso3   = $dadosView['data'][0]->recrut_curso3;
        $nivel3   = $dadosView['data'][0]->recrut_nivel3; 
        $curso4   = $dadosView['data'][0]->recrut_curso4;
        $nivel4   = $dadosView['data'][0]->recrut_nivel4; 

        $disponib = $dadosView['data'][0]->recrut_periodo;   
        $sexo     = $dadosView['data'][0]->recrut_sexo;

        $horaDispon = explode('/', $disponib);

        // var_dump($horaDispon);die();

        

        if (count($horaDispon) >= 2) {
            $disponib = array_merge($horaDispon,array($disponib),array('Integral')); 
        }else{
            $disponib = array_merge($horaDispon,array('Integral'));
        }


       // var_dump($disponib);die();


          


        switch ($sexo) {
               case 'Masculino':
                   // code...
                   $sexo = '0'; 
                   break;

                case 'Feminino':
                   // code...
                   $sexo = '1'; 
                   break;   
               
               default:
                   // code...
                   $sexo = array('0', '1'); // "0,1"; 
                   break;
           }      


          // var_dump($sexo);die();    


        $recrut   = $this->Recrutamento_model->pegarEstudante($curso,$nivel, $curso2,$nivel2, $curso3,$nivel3, $curso4,$nivel4, $sexo, $disponib);

        //var_dump($recrut);die();  

        if ($recrut){

        $emailEstudante = ''; 
        $i=0;  
        
        foreach ($recrut as $valor) {       

        $dados = array(                   
          'recrutalu_estud_id'  => $valor->palualcodig,  
          'recrutalu_recrut_id' => $id,          
        ); 


        if ($i < 19) { // montar uma lista de até 20 contas email
            $emailEstudante = $emailEstudante. $valor->salualemail.','; 
        }

    
        $this->Recrutamento_model->add('tbrecrutaluno', $dados);       
        $i++;

        } 


        // var_dump($emailEstudante);die();

        // Enviar lista de email para os estudantes 
        if ($dadosView['data'][0]->recrut_vaga_email == 0) {
  
        $agencia = $this->Recrutamento_model->getEmitente();
        $agencia_email = $agencia[0]->email;
        $agencia_nome  = $agencia[0]->nome;
        $agencia_cnpj  = $agencia[0]->cnpj;

        $msg_vaga   = '<strong> Ei, psiu! </strong>! <br>';
        $msg_vaga  .= 'Surgiu uma vaga de acordo com teu perfil! <br>';
        $msg_vaga  .= 'Tem interesse? Vem aqui!<br>';
		
		
        if (CLIENTE == 'nudep') {
           $msg_vaga  .= '<a href="https://www.'.CLIENTE.'.com.br/login-estudantes/' .'" target="_blank" >Clique aqui para acessar o nosso site</a> <br>';
        } else {
            $msg_vaga  .= '<a href="https://www.'.CLIENTE.'.com.br/estudantes/estudanteLogin/' .'" target="_blank" >Clique aqui para acessar o nosso site</a> <br>'; 
        }      
               
        $msg_vaga  .= 'Lembrando que é necessário manter o cadastro sempre atualizado. <br>';        
        $msg_vaga  .= 'Qualquer dúvida, por favor, nos contate através do e-mail: <br>'.$agencia_email.'.';

       
        $dados = array(                   
          'maladireta_enviar_email'    => $emailEstudante,         
          'maladireta_enviar_assunto'  => 'Olá! Você tem uma nova mensagem - '.$agencia_nome,
          'maladireta_enviar_conteudo' => $msg_vaga,                             
        );

        // $tabela = 'wdm_wdmgestor.maladireta_enviar'; // localhost
        // $tabela = 'wdmte596_wdmgestor.maladireta_enviar'; // Produção

        $resultado = $this->Recrutamento_model->inserircron('tbmaladireta_enviar',$dados); 

        if ( $resultado) {
            # Salvar para garantir não enviar email novamente para os alunos a mesma vaga
            $dados = array(                   
              'recrut_vaga_email'  => 1                        
            ); 

            $this->Recrutamento_model->editar($id, $dados);

            }

        }
     

        redirect('Recrutamento/editar/'.$id);


        }else{
            
        $dadosView['recrut']   =  '';

        }
        
        $dadosView['empresa']  = $this->Vaga_model->todasEmpresas();
        $dadosView['cursos']   = $this->Curso_model->listar();
        $this->load->view('tema/tema', $dadosView);


    }


    public function limpar()
    {


        $id = $this->uri->segment(3);
        $dadosView['meio']     = 'recrutamento/editar';
        $dadosView['data']     = $this->Recrutamento_model->pegarPorId($id);

        $dadosView['recrut']   = '';

        //limpando busca recrutamento anterior
        $this->db->where('recrutalu_recrut_id', $id);
        $this->db->where('recrutalu_selec', '0');
        $this->db->where('recrutalu_aprov', '0');
        $this->db->delete('tbrecrutaluno');
        
        $this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');

        $dadosView['empresa']  = $this->Vaga_model->todasEmpresas();
        $dadosView['cursos']   = $this->Curso_model->listar();
        $this->load->view('tema/tema', $dadosView);

    }

    public function finalizar()
    {
        // code...
        $id = $this->uri->segment(3);
        $resultado = $this->Recrutamento_model->pegarRecrutFinalPorId($id); 

        // var_dump($resultado[0]->vaga_id);die(); 

        if (count($resultado) >= 1) {


            $agencia = $this->Recrutamento_model->getEmitente();

           // var_dump($agencia);die(); 

            $agencia_email = $agencia[0]->email;
            $agencia_nome  = $agencia[0]->nome;

            foreach ($resultado as  $value) {
                // var_dump($value->recrutalu_aprov);die();

            if ($value->recrutalu_aprov == '1') {
                // parabens vc foi aprovado    

                $msg_posit   = 'Olá: <strong>'.$value->salualnome.'</strong>! <br>';
                $msg_posit  .= 'Nós da '.$value->sempemfanta.' , queremos lhe parabenizar pela aprovação! <br>';
                $msg_posit  .= 'Entraremos em contato em breve para informar sobre o processo de contratação.<br>';

             
                $msg_posit  .= 'Lembrando que é necessário manter o cadastro atualizado. <br>';
                $msg_posit  .= '<a href="https://www.'.CLIENTE.'.com.br/estudantes/' .'" target="_blank" >Clique aqui para acessar o nosso site</a> <br>';
                $msg_posit  .= 'Qualquer dúvida, por favor, nos contate através do e-mail '.$agencia_email.'.';

                $dadosEmail = array(
                    'aluno' => $value->salualnome ,
                    'email_destino' => $value->salualemail ,
                    'empresa' => $value->sempemrazao ,
                    'msg'     => $msg_posit,
                     );                    
                

                } else {

                $msg_neg  = 'Prezado Estudante,<br>

                Seu perfil foi muito bem classificado no processo, porém nesse momento o optamos por outro estudante que a empresa julgou ser adequado para a oportunidade em questão.<br>
                Tomamos então a liberdade de manter o seu Curriculum Vitae em nosso Banco de Dados para futuras oportunidades.<br>
                Agradecemos toda confiança depositada nesta empresa e desejamos muito sucesso em sua carreira.<br>
                Cordialmente,<br>
                Setor de Recrutamento.<br>

                <a href="https://www.'.CLIENTE.'.com.br/estudantes/' .'" target="_blank" >Lembrando que é necessário manter o cadastro atualizado.</a>
                ';
    

                // não foi dessa vez
                $dadosEmail = array(
                    'aluno' => $value->salualnome ,
                    'email_destino' => $value->salualemail ,
                    'empresa' => $value->sempemrazao ,
                    'msg'     => $msg_neg,
                     );

                }




                if ($dadosEmail){

               // $this->email($dadosEmail,$agencia_email,$agencia_nome); // Enviar email para os alunos selecionado 

                $dados = array(                   
                    'recrut_status'  => 'FINALIZADO',
                    'recrut_dataexpir' => date('Y-m-d')           
                );

                $this->Recrutamento_model->editar($id,$dados);

                $dadosVaga = array( 
                    'testvaexpir'    => date('Y-m-d', strtotime(date('yyy-mm-dd').' -1 day'))              
                );

                $this->load->model('Vaga_model');
                $this->Vaga_model->editar($resultado[0]->vaga_id,$dadosVaga); 

               $this->session->set_flashdata('sucesso', 'Recrutamento finalizado com sucesso!');   
                                
                }

            }



   

  

        } else {
            $this->session->set_flashdata('erro', 'Para finalizar o recrutamento é necessário ter no mimimo 01 Aprovado!');
        }

        redirect('Recrutamento', 'refresh');

    }



    public function enviarSelecionado()
    {
        // code...
        $id = $this->uri->segment(3);
        $resultado = $this->Recrutamento_model->pegarRecrutSelecionadoPorId($id); 

        // var_dump($resultado);die(); 

        //var_dump($resultado[0]->sempemrazao);die();

        if (count($resultado) >= 1) {


            $agencia = $this->Recrutamento_model->getEmitente();

           // var_dump($agencia);die(); 

            $agencia_email = $agencia[0]->email;
            $agencia_nome  = $agencia[0]->nome;
            $agencia_logo  = $agencia[0]->url_logo;

            $msg_selec = '
            <html>
    <head>

        <style>body{margin: 0;padding: 0;}@media only screen and (max-width: 640px){table{ width:100% !important; min-width: 200px !important; } img[class="partial-image"]{ width:100% !important; min-width: 120px !important; }}</style>
        <style>
        .table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        .td, .th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }

        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
    </head>
   <body topmargin="0" leftmargin="0">
      <!--?xml encoding="utf-8" ?-->
      <table style="border-collapse: collapse; border-spacing: 0; min-height: 418px;" cellpadding="0" cellspacing="0" width="100%" bgcolor="#E8E8E8">
         <tbody>
            <tr>
               <td align="center" style="border-collapse: collapse; padding-top: 30px; padding-bottom: 30px;">
                  <table cellpadding="5" cellspacing="5" width="600" bgcolor="white" style="border-collapse: collapse; border-spacing: 0;">
                     <tbody>
                        <tr>
                           <td style="border-collapse: collapse; width: 600px; padding: 0px; text-align: center;">
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background: rgb(122,122,122);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <h3 style="color: rgb(255,255,255);"><a style="text-decoration: none; display: inline-block; font-family: arial; box-sizing: border-box; width: 100%; text-align: left; color: rgb(255,255,255); font-size: 12px; cursor: text;" target="_blank"><span style="font-weight: normal; color: rgb(255,255,255);">Recrutamento</span></a></h3>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; max-width: 600px; text-align: center;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; font-family: Arial; line-height: 0px; mso-line-height-rule: exactly; padding: 0px;">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td align="center" style="border-collapse: collapse; padding: 0; line-height: 0px; mso-line-height-rule: exactly;"><a target="_blank" style="text-decoration: none; font-family: arial; box-sizing: border-box; display: block;"><img class="partial-image" src="'. $agencia_logo .'" width="160" style="min-width: 160px; box-sizing: border-box; display: block; max-width: 160px;"></a></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; max-width: 600px; text-align: center;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; font-family: Arial; line-height: 0px; mso-line-height-rule: exactly; padding: 0px;">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td align="center" style="border-collapse: collapse; padding: 0; line-height: 0px; mso-line-height-rule: exactly;"><a target="_blank" style="text-decoration: none; font-family: arial; box-sizing: border-box; display: block;"><img class="partial-image" src="'. base_url().'public/assets/templates/images/banner.jpg" width="600" style="min-width: 160px; box-sizing: border-box; display: block; max-width: 600px;"></a></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box; font-family: Arial; font-size: 25px; text-align: center; padding-top: 20px; padding-bottom: 20px; vertical-align: middle;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background-color: rgb(192,192,192);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <h3 style="font-weight: normal; padding: 0px; margin: 0px; word-wrap: break-word; font-size: 25px; color: rgb(255,252,252);"><a style="text-decoration: none; display: inline-block; font-family: arial; box-sizing: border-box; word-wrap: break-word; width: 100%; text-align: center; color: rgb(255,252,252); font-size: 25px;" target="_blank"><span style="font-size: inherit; width: 100%; text-align: center; color: rgb(255,252,252);">LISTAGEM RECRUTAMENTO</span></a></h3>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background-color: rgb(192,192,192);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; text-align: left; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <div style="font-family: Arial; font-size: 15px; line-height: 170%; text-align: left; font-weight: normal; word-wrap: break-word; color: rgb(69,69,69);"><font color="#666666" style="color: rgb(77,77,77);"><span style="line-height: 0; display: none;"></span><span style="line-height: 0; display: none;"></span><font size="2">
                                                      Olá, <strong>'.$resultado[0]->sempemrazao.'</strong> tudo bem? Selecionamos alguns estudantes que se enquadram no perfil solicitado.
                                                      </font></font><br style="color: rgb(77,77,77);"></div>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>                              
                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box; display: table;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background-color: rgb(192,192,192);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <hr style="border-style: dashed; border-color: rgb(192,192,192);">
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>



                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box;">
                                 <tbody>
                                    <tr>
                                       <td>

                                        <div class="container">
                                          <h2>Estudante(s) Selecionados</h2>
                                          <!-- <p></p>  -->          
                                          <table class="table table-bordered">
                                            <thead>
                                              <tr>
                                                <th class="th">Estudante</th>
                                                <th class="th">Faculdade</th>
                                                <th class="th">E-mail</th>
                                                <th class="th">Curriculum</th>                                                
                                              </tr>
                                            </thead>
                                            <tbody> ';




              

                                                foreach($resultado as $v ) {  

                                                $msg_selec = $msg_selec . '<tr>';

                                                $msg_selec = $msg_selec . '<td class="td">' .$v->salualnome. '</td>';

                                                $msg_selec = $msg_selec . '<td class="td">' .$v->sensennome. '</td>';

                                                $msg_selec = $msg_selec . '<td class="td">' .$v->salualemail. '</td>';

                                                $msg_selec = $msg_selec . '<td class="td"><a href="http://'.CLIENTE.'.com.br/'.$v->url. '" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Exibir Curriculum</a></td>';

                                                $msg_selec = $msg_selec . '</tr>';

                                                }; 

                                    $msg_selec = $msg_selec . '          
                                            </tbody>
                                          </table>
                                        </div>


                                       </td>
                                    </tr>
                                 </tbody>
                              </table> 

                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background-color: rgb(192,192,192);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <hr style="border-style: dashed; border-color: rgb(255,255,255);">
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>

                            


                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial; background-color: rgb(192,192,192);">
                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td style="border-collapse: collapse;">
                                                      <hr style="border-style: dashed; border-color: rgb(255,255,255);">
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>


     


                              <table style="border-collapse: collapse; border-spacing: 0; position: relative; min-height: 40px; width: 100%; box-sizing: border-box; padding: 30px 0px;">
                                 <tbody>
                                    <tr>
                                       <td style="border-collapse: collapse; padding: 10px 15px; font-family: Arial;">
                                       
                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" valign="top" bgcolor="#FFFFFF" style="background-color:#FFFFFF; vertical-align:top"><tbody><tr><td align="center" style="font-size:0px; padding:10px 25px; word-break:break-word"><div style="font-family: OpenSans, Helvetica, Tahoma, Arial, sans-serif, serif, EmojiFont; font-size: 14px; font-weight: bold; line-height: 24px; text-align: center; color: rgb(79, 79, 79);">FIQUE POR DENTRO: </div></td></tr></tbody>
                                       </table>
                                       
                                       <!--REDE SOCIAL -->
                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" valign="top" bgcolor="#FFFFFF" style="background-color: #ffffff; vertical-align: top;">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="font-size: 0px; padding: 0 0 20px 0; word-break: break-word;">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="20" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="42"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/HTB1M.I3aECF3KVjSZJn762nHFXaq.png" height="20" width="20" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="20" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="43"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/HTB1mFo3axiH3KVjSZPf760BiVXaq.png" height="20" width="20" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="24" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 24px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="44"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/Hdcee24d3349a413cbb57701a2afdbf59c.png" height="22" width="24" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="20" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="45"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/HTB1nYk4aEGF3KVjSZFv762_nXXah.png" height="20" width="20" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="20" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="47"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/H129b739af72944f096c75bb5feb8916cL.png" height="20" width="20" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" bgcolor="#FFFFFF" style="background-color: #ffffff; float: none; display: inline-table;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding: 0 14px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="20" bgcolor="#FFFFFF" style="background-color: #ffffff; border-radius: 3px; width: 20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 0; height: 20px; vertical-align: middle; width: 20px;">
                                                                                        <a
                                                                                            href="#"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer"
                                                                                            data-auth="NotApplicable"
                                                                                            data-linkindex="48"
                                                                                        >
                                                                                            <img data-imagetype="External" src="https://ae01.alicdn.com/kf/H6d88de9fb83b4b59890f733b7001cf76N.png" height="20" width="20" style="border-radius: 3px; display: block;" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- FIM REDE SOCIAL -->

                                          <table width="100%" style="border-collapse: collapse; border-spacing: 0; font-family: Arial;">
                                             <tbody>
                                                <tr>
                                                   <td align="center" style="border-collapse: collapse;">
                                                   <a href="https://www.'.CLIENTE.'.com.br/' .'" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="text-decoration:none; color:#843C8E" data-linkindex="38">Acesse nosso site.</a></strong>.
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>                  
                               
                                          
                                          
                                       </td>
                                    </tr>

                                    <tr>
                                      <td align="center">
                                        <p style="margin:0; padding:0; margin-bottom:0; font-size:11px; line-height:11px; color:#33273b; display:inline; font-family:"trebuchet ms",sans-serif">O e-mail enviado é um serviço prestado para você pela WDM-Tecnologia.<strong>
                                        <a href="http://www.wdmtecnologia.com.br" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="text-decoration:none; color:#843C8E" data-linkindex="38">Acesse este link </a></strong> para mais informações.
                                        </p>
                                      </td>
                                    </tr>
                                 </tbody>
                              </table>

                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>
';


                $dadosEmail = array(
                    'aluno' => $resultado[0]->salualnome ,
                    'email_destino' => $resultado[0]->sempememail,
                    'empresa' => $resultado[0]->sempememail ,
                    'msg'     => $msg_selec,
                     );


                // $dadosEmail = array(
                //     'aluno' => $resultado[0]->salualnome ,
                //     'email_destino' => 'jbrunomg@hotmail.com',
                //     'empresa' => $resultado[0]->sempememail ,
                //     'msg'     => $msg_selec,
                //      );

                
                
                if ($dadosEmail){

                $this->email($dadosEmail,$agencia_email,$agencia_nome); // Enviar email para os alunos selecionado     
                $this->session->set_flashdata('sucesso', 'Selecionado(s) enviado com sucesso!'); 

                }
 

  

        } else {

            $this->session->set_flashdata('erro', 'Para finalizar o recrutamento é necessário ter no mimimo 01 Aprovado!');
        }

        redirect('Recrutamento', 'refresh');

    }


    public function recrutSelecAprov()
    {
        // code...
        $idEstudante   = $this->uri->segment(3);
        $idRecrutament = $this->uri->segment(4);
        $selecAprov    = $this->uri->segment(5);
    
        if ( $selecAprov == 'S') {
            // Selecionado
            $dados = array(                   
                'recrutalu_selec'  => '1'          
            ); 
        }else{
            // Aprovado
            $dados = array(                   
                'recrutalu_aprov'  => '1'          
            ); 


        }
     
        
        $this->Recrutamento_model->recrutSelecionando($idEstudante,$idRecrutament,$dados);

        redirect('Recrutamento/editar/'. $idRecrutament);



    }


    public function EnviarCron() // Enviar email para os recrutamento que está em STATUS = AGUARDANDO
    {

        $agencia = $this->Recrutamento_model->getEmitente();
        $agencia_email = $agencia[0]->email;
        $agencia_nome  = $agencia[0]->nome;
        $agencia_cnpj  = $agencia[0]->cnpj;   


        $emailsEnviar = $this->Recrutamento_model->pegarRecrutAguardando();

        // var_dump($emailsEnviar);die();
        if (CLIENTE == 'nudep') {   
            $agencia_email = 'recrutamento@nudep.com.br';
        }
        

        foreach ($emailsEnviar as $email) {       

            $msg_recrut   = '<strong> Olá! '.$email->sempemfanta.' </strong>! <br>';
            $msg_recrut  .= 'Enviamos alguns currículos de candidatos interessados na vaga:'.$email->recrut_atividade.' <br>';
     
            $msg_recrut  .= 'Ficamos no aguardo do retorno para finalizarmos a vaga em nosso sistema ou darmos continuidade no processo seletivo de novos interessados. <br>';        
            
            $msg_recrut  .= 'Qualquer dúvida, por favor, nos contate através do e-mail: <br>'.$agencia_email.'.';


            $dadosEmail = array(                
                'email_destino' => 'jbrunomg@hotmail.com', // $email['sempememail'],
                'empresa'       => $email->sempemfanta,
                'msg'           => $msg_recrut,
                 );

            $envio = $this->email($dadosEmail, $agencia_email, $agencia_nome);       
            
        }
        
    }


    public function email($dados, $agencia_email,$agencia_nome)
    {  
        // var_dump($dados);die();

      
        $this->load->library('email');
        $this->email->from('emailsite@wdmtecnologia.com.br', $agencia_nome);
        $this->email->to($dados['email_destino']);         
        $this->email->subject('Olá! Você tem uma nova mensagem.');
        $this->email->reply_to($agencia_email); // Responder email
        $this->email->message($dados['msg']);

        
        //$this->email->attach('/path/to/file1.png');
        //$this->email->attach('/path/to/file2.pdf');
        if ($this->email->send()){
            //die('foi');
            //echo json_encode(array('status' => true, 'msg' => 'E-mail enviado com sucesso!'));
        }else{
            //die('não foi');
            //echo json_encode(array('status' => false, 'msg' => 'Falha ao enviar o email, favor tente mais tarde!'));
        }    
    }

    public function solicitacaoRecrut()
    {
        $resultado = $this->Recrutamento_model->solicitacaoRecrut();

        // echo '<pre>';
        // var_dump($resultado);die();

        $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'recrutamento/index';
        $this->load->view('tema/tema',$dadosView);
    }




}
