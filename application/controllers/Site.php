<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('site_model');
    }


    public function index()
    {
        // $resultado = $this->Site_model->getConfiguracaosite();

        // $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'configurarsite/editarSite';
        $this->load->view('tema/tema',$dadosView);
    }

 

    public function configurar()
    {
        if($this->input->post()){

            $data['site_produto_valor'] = ($this->input->post('site_produto_valor'))?1:0;
            $data['site_mapa']      = ($this->input->post('site_mapa'))?1:0;
            $data['site_logomarca'] = ($this->input->post('site_logomarca'))?1:0;
            $data['site_sobre']     = '';         
            $data['site_facebook']  = '';
            $data['site_instagram'] = '';
            $data['site_twitter']   = '';
            $data['site_googleplus'] = '';
            $data['site_whatsapp']   = '';
            // NOME MENU
            $data['site_coluna1'] = '';
            $data['site_coluna2'] = '';
            $data['site_coluna3'] = '';

            
            $banner1    = $_FILES['userfile1'];
            $banner2    = $_FILES['userfile2'];
            $banner3    = $_FILES['userfile3'];
            $banner4    = $_FILES['userfile4'];

            // var_dump(base_url());die();
            
            if($banner1['name']){
                $image1 = $this->uploadImagem('userfile1');
                $data['site_banner1'] = base_url().'public/assets/images/slider/'.$image1;   
            }

            if($banner2['name']){
                $image2 = $this->uploadImagem('userfile2');
                $data['site_banner2'] = base_url().'public/assets/images/slider/'.$image2;   
            }

            if($banner3['name']){
                $image3 = $this->uploadImagem('userfile3');
                $data['site_banner3'] = base_url().'public/assets/images/slider/'.$image3;   
            }

            if($banner4['name']){
                $image4 = $this->uploadImagem('userfile4');
                $data['site_banner4'] = base_url().'public/assets/images/slider/'.$image4;   
            }

            $resultado = $this->site_model->atualizarConfiguracaosite($data);


            if($resultado){
                $this->session->set_flashdata('success','Alterações realizadas com sucesso!');
            }else{
                 $this->session->set_flashdata('error','Error ao atualizar os dados, ou nenhum dado foi alterado!');
            }

            redirect('Site', 'refresh');

        }


        $dadosView['meio']  = 'configurarsite/editarSite';
        $this->load->view('tema/tema',$dadosView);
    }

    function uploadImagem($arquivo){
  
        $this->load->library('upload');

        $image_upload_folder = FCPATH . 'public/assets/images/slider';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }

        $this->upload_config = array(
            'upload_path'   => $image_upload_folder,
            'allowed_types' => 'png|jpg|jpeg|bmp|gif',
            'max_size'      => 2048,
            'remove_space'  => TRUE,
            'encrypt_name'  => TRUE,
        );

        $this->upload->initialize($this->upload_config);

        if (!$this->upload->do_upload($arquivo)) {
            $upload_error = $this->upload->display_errors();
            /*print_r($upload_error);
            exit();*/
            return false;
        } else {
            $file_info = array($this->upload->data());
            return $file_info[0]['file_name'];
        }

    }
}

