<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Crm_model');
	}

	public function index()
	{
		$resultado = $this->Crm_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'crm/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{
		$dadosView['permissoes'] = $this->Crm_model->todasPermissoes();
		$dadosView['meio']       = 'crm/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

		$dados = array(

		  'crm_empresa'    => $this->input->post('crm_empresa'),
		  'crm_contato01'  => $this->input->post('crm_contato01'),
		  'crm_contato02'  => $this->input->post('crm_contato02'),		  
		  'crm_email'      => $this->input->post('crm_email'),
		  'crm_status'     => $this->input->post('crm_status'),

		  'crm_data_anot01'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot01')))),
		  'crm_anotacao01'   => $this->input->post('crm_anotacao01'),
		 
		  'crm_data_anot02'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot02')))),
		  'crm_anotacao02'   => $this->input->post('crm_anotacao02'),	

		  'crm_data_anot03'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot03')))),	
		  'crm_anotacao03'   => $this->input->post('crm_anotacao03'),

	  	  'crm_data_cadastro' => date("Y-m-d H:i:s"),
		  'crm_visivel'       => 1
		  
		);

   //       echo '<pre>';
		 // var_dump($dados);die();

		$resultado = $this->Crm_model->inserir('tbcrm',$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Crm', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Crm_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Crm_model->todasPermissoes();
	
		$dadosView['meio']       = 'crm/editar';
     	$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('pcrmcodig');

		$dados = array(

		  'crm_empresa'    => $this->input->post('crm_empresa'),
		  'crm_contato01'  => $this->input->post('crm_contato01'),
		  'crm_contato02'  => $this->input->post('crm_contato02'),		  
		  'crm_email'      => $this->input->post('crm_email'),
		  'crm_status'     => $this->input->post('crm_status'),

		  'crm_data_anot01'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot01')))),
		  'crm_anotacao01'   => $this->input->post('crm_anotacao01'),
		 
		  'crm_data_anot02'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot02')))),
		  'crm_anotacao02'   => $this->input->post('crm_anotacao02'),	

		  'crm_data_anot03'  => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('crm_data_anot03')))),	
		  'crm_anotacao03'   => $this->input->post('crm_anotacao03'),

	  	  'crm_data_cadastro' => date("Y-m-d H:i:s"),
		  'crm_visivel'       => 1
		  
		);

		$resultado = $this->Crm_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Crm', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'crm_visivel' => 0
						
					);

		$resultado = $this->Crm_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Crm_model->pegarPorId($id);
		$dadosView['permissoes']  = $this->Crm_model->todasPermissoes();
		$dadosView['meio']       = 'crm/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}


	public function adicionarEmail()
	{	
		$crm_id   = $this->input->post('pcrmcodig');
		$email    = $this->input->post('crm_email');
		$assunto  = $this->input->post('assunto');
		$conteudo = $this->input->post('descricao');


		$dados = array(		  	  		  
		  'crm_email'       => $email,		  
		  'crm_assunto'     => $assunto,
		  'crm_mensagem'         => $conteudo,
		  'crm_id'               => $crm_id, 	
		  'crm_email_data_envio' => date('Y-m-d H:i:s')
		);


		$resultado = $this->Crm_model->inserir('tbcrm_email',$dados);

		$resultado = $this->enviarEmails($email,$conteudo,$assunto);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro enviado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para enviar o registro!');
		}

		redirect('Crm', 'refresh');


	}

	public function enviarEmails($emails,$conteudo,$assunto)
	{
		// $this->load->library('email'); // inserido no AutoLoad
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);


		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		if($this->email->send())
        {
            $this->session->set_flashdata('success','Email enviado com sucesso!');
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('error',$this->email->print_debugger());
            return false;
        }
	}

}

/* End of file Crm.php */
/* Location: ./application/controllers/Crm.php */