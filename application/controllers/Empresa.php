<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Empresa_model');
	}

	public function index()
	{

		$resultado = $this->Empresa_model->listar();
		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'empresa/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function listar($notificacao)
	{
		
		$resultado = $this->Empresa_model->listarNotificacao($notificacao);
		$dadosView['dados'] = $resultado;	

		// var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'empresa/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Empresa_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Empresa_model->todosEstados();
		$dadosView['curso']      = $this->Empresa_model->todosCursos();
		$dadosView['permissoes'] = $this->Empresa_model->tadasPermissoes();
		$dadosView['meio']       = 'empresa/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$Cnpj =  str_replace("-","",str_replace("/","",str_replace(".","",$this->input->post('sempemcnpj')))); 

		$rua    = str_replace(',','',str_replace(' ', '+', strtolower($this->input->post('sempemlogra'))));

		$bairro = str_replace(' ', '+', strtolower($this->input->post('sempembairr')));

		$endereco = $rua.','.$bairro.','.'pernambuco00,brasil';

		$logLat = $this->geolocationEmpresa($endereco);
			
		if(!$logLat){
			$logLat['latitude']   = '';
			$logLat['longitude']  = '';
		}		

		$dados = array(

		   'sempemcnpj'    => $Cnpj,

		   'sempemrazao'   => $this->input->post('sempemrazao'),
		   'sempemfanta'   => $this->input->post('sempemfanta'),
		   'sempemmantededora'   => $this->input->post('sempemmantededora'),

		   'sempemlogra'   => $this->input->post('sempemlogra'),		  
		   'sempemnumer'   => $this->input->post('sempemnumer'),
		   'sempemcompl'   => $this->input->post('sempemcompl'),

		   'sempembairr'   => $this->input->post('sempembairr'),
		   'sempemcidad'   => $this->input->post('sempemcidad'),
		   'sempemuf'      => $this->input->post('sempemuf'), 
		   'sempemcep'     => $this->input->post('sempemcep'),		  
		   'sempemrespo'   => $this->input->post('sempemrespo'),
		   'sempemcargo'   => $this->input->post('sempemcargo'),


		   'sempemtel01'   => $this->input->post('sempemtel01'),
		   'sempemtel02'   => $this->input->post('sempemtel02'),
		   'sempemtel03'   => $this->input->post('sempemtel03'), 
		   'sempememail'   => $this->input->post('sempememail'),
		   'sempememailgestor'   => $this->input->post('sempememailgestor'),		  
		   'sempemrepre'   => $this->input->post('sempemrepre'),
		   'sempemrecar'   => $this->input->post('sempemrecar'),
		   'sempemrecpf'   => $this->input->post('sempemrecpf'),
		   'sempemativi'   => $this->input->post('sempemativi'),
		   'sempemie'      => $this->input->post('sempemie'),

		   'sempemcodsacado' => $this->input->post('sempemcodsacado'),
		   'sempemvalor'     => str_replace(",", ".", $this->input->post('sempemvalor')),
		   'sempemvenccontrato' => $this->input->post('sempemvenccontrato'),
	  	  
		    'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
			'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro

			'empresa_visivel'   => 1,

			'latitude'   => $logLat['latitude'],
			'longitude'  => $logLat['longitude']
		  
		);

  
		$resultadoCnpj = $this->Empresa_model->pegarPorCnpj($Cnpj);

		// echo '<pre>';
		// var_dump($resultadoCnpj[0]->sempemrazao);die();

		if ($resultadoCnpj) {	

			$this->session->set_flashdata('erro', "A empresa: ".'\n' .$resultadoCnpj[0]->sempemrazao.'\n'. " já está cadastrado em nosso sistema!");

		} else {

			$resultado = $this->Empresa_model->inserir($dados);

			if ($resultado) {			
				$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
			}
			
		}

	

		redirect('Empresa', 'refresh');
	}


	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Empresa_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Empresa_model->todasCidadesIdEstado('16');
		$dadosView['estados']    = $this->Empresa_model->todosEstados();		
		$dadosView['permissoes'] = $this->Empresa_model->tadasPermissoes();
		$dadosView['meio']       = 'empresa/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$Cnpj =  str_replace("-","",str_replace("/","",str_replace(".","",$this->input->post('sempemcnpj'))));


		$rua    = str_replace(',','',str_replace(' ', '+', strtolower($this->input->post('sempemlogra'))));

		$bairro = str_replace(' ', '+', strtolower($this->input->post('sempembairr')));

		$endereco = $rua.','.$bairro.','.'pernambuco00,brasil';

		//$logLat = $this->geolocationEmpresa($endereco);	
	
		// if(!$logLat){
		// 	$logLat['latitude']   = '';
		// 	$logLat['longitude']  = '';
		// } 

		$id = $this->input->post('pempemcodig');

		$dados = array(
		  
		   'sempemcnpj'    => $Cnpj,

		   'sempemrazao'   => $this->input->post('sempemrazao'),
		   'sempemfanta'   => $this->input->post('sempemfanta'),
		   'sempemmantededora'   => $this->input->post('sempemmantededora'),
		    
		   'sempemlogra'   => $this->input->post('sempemlogra'),		  
		   'sempemnumer'   => $this->input->post('sempemnumer'),
		   'sempemcompl'   => $this->input->post('sempemcompl'),

		   'sempembairr'   => $this->input->post('sempembairr'),
		   'sempemcidad'   => $this->input->post('sempemcidad'),
		   'sempemuf'      => $this->input->post('sempemuf'), 
		   'sempemcep'     => $this->input->post('sempemcep'),		  
		   'sempemrespo'   => $this->input->post('sempemrespo'),
		   'sempemcargo'   => $this->input->post('sempemcargo'),


		   'sempemtel01'   => $this->input->post('sempemtel01'),
		   'sempemtel02'   => $this->input->post('sempemtel02'),
		   'sempemtel03'   => $this->input->post('sempemtel03'), 
		   'sempememail'   => $this->input->post('sempememail'),
		   'sempememailgestor'   => $this->input->post('sempememailgestor'),		  
		   'sempemrepre'   => $this->input->post('sempemrepre'),
		   'sempemrecar'   => $this->input->post('sempemrecar'),
		   'sempemrecpf'   => $this->input->post('sempemrecpf'),
		   'sempemativi'   => $this->input->post('sempemativi'),
		   'sempemie'      => $this->input->post('sempemie'),


		   'sempemcodsacado' => $this->input->post('sempemcodsacado'),
		   'sempemvalor'     => str_replace(",", ".", $this->input->post('sempemvalor')),
		   'sempemvenccontrato' => $this->input->post('sempemvenccontrato'),
		   	  
	  	  
		    //'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
			'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro

			'empresa_visivel'   => 1,

			'latitude'   => '', //$logLat['latitude'],
			'longitude'  => '' //$logLat['longitude']
		  
		);


		$resultado = $this->Empresa_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Empresa', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

        $sql = "
				    iempemcodig AS idEmpresa FROM `tbcontrestempr` 
				WHERE  tbcontrestempr.`contrato_visivel` = 1
				    AND tbcontrestempr.ialualcodig <> 0
				    AND iempemcodig = ".$id." 
				   
				 UNION ALL
				 
				SELECT `recrut_empr_id` AS idEmpresa FROM `tbrecrutamento`
				WHERE `recrutamento_visivel` = 1
				    AND `recrut_status` IN ('ABERTO','AGUARDANDO')
				    AND recrut_empr_id = ".$id.";"; 

        $this->db->select($sql);			

		$resultado = $this->db->get()->result();	    


		if (!empty($resultado)) {			
			// Não deixar remover a empresa.
			$this->session->set_flashdata('erro', 'Existe contrato ou recrutamento vinculado a essa empresa!');
			echo json_encode(array('status' => false));
		}else{

			$dados = array(
						'empresa_visivel' => 0
						
					  );

			$resultado = $this->Empresa_model->excluir($id,$dados);

			if ($resultado) {			
				echo json_encode(array('status' => true));
			}else{
				echo json_encode(array('status' => false));
			}
		}




	}
	
	public function visualizar()
	{

		$id = $this->uri->segment(3);

		$dadosView['contrato'] = $this->Empresa_model->pegarContratoPorId($id);

		// echo "<pre>";
		// var_dump($dadosView['contrato']);die();
				 

		$dadosView['dados']       = $this->Empresa_model->pegarPorId($id);
		$dadosView['cidades']     = $this->Empresa_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Empresa_model->todosEstados();		
		$dadosView['permissoes']  = $this->Empresa_model->tadasPermissoes();	

		$dadosView['meio']       = 'empresa/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

	public function criarDemonstrativo($id,$empresa)
	{	
		// $lastmes = date('m', strtotime('-1 months')); // Descontinuado 25032022
		$lastmes = date('m', strtotime('first day of -1 months'));		
		$ano = date('Y'); // Ano atual

		

		// Checar existencia de contrato caso não tenha criar (mes/ano/empresa_id/empresa_$$)
		$resultado = $this->Empresa_model->checarDemonstrativoId($lastmes,$ano,$empresa);

		// var_dump(	$resultado);die();

		if (count($resultado) > 0 ) {
			// update para estudante no contrato anterior  `demonstrativoempr_cod_estudante`

        $sql = " UPDATE `tbdemonstrativoempr` SET
         `demonstrativoempr_cod_estudante` =   concat(IF(`demonstrativoempr_cod_estudante` IS NULL, '', `demonstrativoempr_cod_estudante`), ',',".$id.") ,
         `date_operacao` = '".date('Y-m-d H:i:s')."', 
         `demonstrativoempr_usuario_id` = ".$this->session->userdata('usuario_id').",
         `demonstrativoempr_visivel` = 1 
         WHERE `demonstrativoempr_mes` = '".$lastmes."' 
         AND `demonstrativoempr_ano` = '".$ano."' 
         AND `demonstrativoempr_empresa_id` = ".$empresa.";  ";
        
        $resultado = $this->db->query($sql);


		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		} else {

			$dados = array(
				'demonstrativoempr_mes'    => $lastmes,
				'demonstrativoempr_ano'    => $ano, //Criar uma regra para mes de janeiro e ano anterior 
				'demonstrativoempr_empresa_id'    => $empresa,
				'demonstrativoempr_cod_estudante' => $id,
				'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  		'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro
				
			);
			
			$this->Empresa_model->cadastrarDemonstrativo($dados); // Cadastra um registro depois carrega o demonstrativo.
	}


	$this->visualizardemonstrativo($empresa);



    }

	public function editarDemostrativoExe()
	{	

		// OBS: Verificar algoritimo na virada entre dezembro e Janeiro de cada ano.		
		// $lastmes = date('m', strtotime('-1 months'));
		$mes = date('m');
		$ano = date('Y'); // Ano atual
		$empresa = $this->input->post('demonstrativoempr_empresa_id');


		$dados = array(
	  		  	
		  'demonstrativoempr_notafiscal'     => $this->input->post('demonstrativoempr_notafiscal'),
		  'demonstrativoempr_obs'            => $this->input->post('demonstrativoempr_obs'),
		  'demonstrativoempr_cod_estudante'  => $this->input->post('demonstrativoempr_cod_estudante'),
		  'demonstrativoempr_qtd_estud_cobrado' => $this->input->post('demonstrativoempr_qtd_estud_cobrado'),
		  'demonstrativoempr_qtd_estud_ativo'   => $this->input->post('demonstrativoempr_qtd_estud_ativo'),		  
		  'demonstrativoempr_valor_taxa'        => $this->input->post('demonstrativoempr_valor_taxa'),
		  'demonstrativoempr_valor_cobrado'        => $this->input->post('demonstrativoempr_valor_cobrado'),

		  //'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro
		  
		  'demonstrativoempr_usuario_id'  => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativoempr_visivel'     => 1
		  
		);

  		// echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Empresa_model->editarDemonstrativo($mes,$ano,$empresa,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Empresa/visualizardemonstrativo/'.$empresa, 'refresh');
	}

	public function visualizardemonstrativo($empresa = null)
	{	

		$mes = date('m');
		$ano = date('Y'); // Ano atual
		$lastmes = date('m', strtotime('first day of -1 months'));
		$lastano = date('Y', strtotime('first day of -1 months'));
		$valorTotalPagar = 0;
	
		//var_dump($lastmes);die();
		$id =  ($empresa == null) ? $this->uri->segment(3) : $empresa ;  

		// Checar existencia de contrato caso não tenha criar (mes/ano/empresa_id/empresa_$$)
		$resultado = $this->Empresa_model->checarDemonstrativoId($mes,$ano,$id);


		if (count($resultado) > 0 ) {			

		   $dadosView['contrato'] = $this->Empresa_model->pegarDemonstrativoId($id); // carrega o contrato em modo edição;

		  // var_dump($dadosView['contrato']);die();
		
		   if ($lastano == $ano) {
		   	# de Fevereiro a Dezembro
		   	$dadosView['contrato_anteriror'] = $this->Empresa_model->checarDemonstrativoId($lastmes,$ano,$id);
		   } else {
		   	# Só em Janeiro
		   	$dadosView['contrato_anteriror'] = $this->Empresa_model->checarDemonstrativoId($lastmes,$lastano,$id);

		   }
		   

		  // var_dump($dadosView['contrato']);die();

		} else {

			$dados = array(
				'demonstrativoempr_mes'    => $mes,
				'demonstrativoempr_ano'    => $ano,
				'demonstrativoempr_empresa_id'    => $id,
				'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  		'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro
				
			);
			
			$this->Empresa_model->cadastrarDemonstrativo($dados); // Cadastra um registro depois carrega o demonstrativo.

			$dadosView['contrato'] = $this->Empresa_model->pegarDemonstrativoId($id); // carrega o contrato em modo edição;

			$dadosView['contrato_anteriror'] = $this->Empresa_model->checarDemonstrativoId($lastmes,$ano,$id);
		}	


		// v($dadosView['contrato']);
			
		
		$contrato = explode(',',  $dadosView['contrato'][0]->grupo);
		if ($dadosView['contrato_anteriror']) {	
		$contrato_anteriror = explode(',',  $dadosView['contrato_anteriror'][0]->demonstrativoempr_cod_estudante);
		$dadosView['contratoAnterior'] = 'True';
		}else{
		$contrato_anteriror = array();
		$dadosView['contratoAnterior'] = 'False';	
		}
		

		$_array = $contrato_anteriror;
		$arrays = $contrato;
		$result = array_diff( $arrays , $_array );


		$estudantes = array();	

		foreach ($dadosView['contrato'] as $estudante) {


				$data_term  = $estudante->contrestempr_data_term;	
				$data_renI  = $estudante->contrestempr_data_renI;
				$data_renII = $estudante->contrestempr_data_renII;
				$data_renIII = $estudante->contrestempr_data_renIII;
				$data_rescisao = $estudante->contrestempr_data_rescisao;	

		
			if(in_array($estudante->palualcodig, $result)){			 

				$valor = $estudante->sempemvalor;	
				$dataInicio = $estudante->contrestempr_data_ini;

				if (($estudante->contrestempr_data_rescisao <> '0000-00-00') and ($estudante->contrestempr_data_rescisao <> '1970-01-01')) {					
					$datafim = $estudante->contrestempr_data_rescisao;					
					$Rescindido = true;
				} else {
					if ($id == '325') {
						$datafim = date('Y').'-'.$mes.'-'.'19';
					} else {
						if ($mes == '02'){ // Adaptado para o mes de fevereiro. Bruno - 220223
							$datafim = date('Y').'-'.$mes.'-'.'29';
						}else{
							$datafim = date('Y').'-'.$mes.'-'.'30';	
						}
					}
					
					$Rescindido = false;
				}				

				$contrestempr_data_ini = $dataInicio;
				$contrestempr_data_fim = $datafim;
				
				$valorPagar = $this->calcularValor($valor,$dataInicio,$datafim ,true,$Rescindido);
				$valorTotalPagar = $valorTotalPagar + $valorPagar;
				$dataTermino = $this->terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao);

			} else {

				$valor = $estudante->sempemvalor;	
				if ($id == '325') {	
					$dataInicio =  date('Y').'-'.$lastmes.'-'.'20';
		    	} else {
		    		$dataInicio =  date('Y').'-'.$mes.'-'.'01';	
		    	}

				if (($estudante->contrestempr_data_rescisao <> '0000-00-00') and ($estudante->contrestempr_data_rescisao <> '1970-01-01')) {
					$datafim = $estudante->contrestempr_data_rescisao;
					$Rescindido = true;
				} else {
					if ($id == '325') {
						$datafim = date('Y').'-'.$mes.'-'.'19';
					} else {
						if ($mes == '02'){ // Adaptado para o mes de fevereiro. Bruno - 220223
							$datafim = date('Y').'-'.$mes.'-'.'29';
						}else{
							$datafim = date('Y').'-'.$mes.'-'.'30';	
						}
						
					}
					$Rescindido = false;
				}

				$contrestempr_data_ini = $dataInicio;
				$contrestempr_data_fim = $datafim;

				$valorPagar = $this->calcularValor($valor,$dataInicio,$datafim ,false,$Rescindido);
				// var_dump($valorPagar);die();
				$valorTotalPagar = $valorTotalPagar + $valorPagar;
				$dataTermino = $this->terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao);							
			}

		

			$estudante->dataInicio  =  $contrestempr_data_ini;
			$estudante->datafim     =  $contrestempr_data_fim;
			$estudante->valorTotal  =  $valorPagar;
			$estudante->SomaTotal   =  $valorTotalPagar;
			$estudante->termino     =  $dataTermino;
			$estudantes[$estudante->palualcodig] = $estudante;

		}

		// var_dump($estudantes);die();

		// Checar existencia de contrato caso não tenha criar (mes/ano/empresa_id)
		$estorno = $this->Empresa_model->estornoDemonstrativoId($lastmes,$ano,$id);

		$dadosView['estorno'] = $estorno;

		$dadosView['estudante'] = $estudantes;	

		$dadosView['permissoes']  = $this->Empresa_model->tadasPermissoes();	

		$dadosView['meio']       = 'empresa/visualizarDemonstrativo';		

		$this->load->view('tema/tema',$dadosView);


	}

	public function calcularValor($valor,$dataInicio,$datafim,$devedor,$Rescindido)
	{	
		// $dataInicio = date_create($dataInicio);
		// $datetime1 = (object) date_format($dataInicio, 'Ymd H:i:s');

		// $datafim = date_create($datafim); 
		// $datetime2 = (object) date_format($datafim, 'Ymd H:i:s'); 

		// $interval = $datetime1->diff($datetime2)->format("%d days, %h hours and %i minuts"); 

		$datetime1 = date_create($dataInicio);
		$datetime2 = date_create($datafim);
		$interval  = date_diff($datetime1,$datetime2);

		

		$diasGeral = $interval->format('%a');
		$meses = $interval->format('%m');

		//var_dump($meses);die();
 	

		if ($meses <> '0') {
			// Definimos o intervalo de 1 ano
			$interval = new DateInterval('P1M');
			 
			// Resgatamos datas de cada ano entre data de início e fim
			$period = new DatePeriod($datetime1, $interval, $datetime2); 

			//var_dump($period);die();

			$arr = array();
			foreach($period as $date) {				
				$dataArray = $date->format("m");			
				$arr[] = $dataArray;
			}

			$fruta = array_shift($arr); // Remover 1º array
			$fruta = array_pop($arr); // Remover 1º ultimo	

			if ( in_array('02',$arr)) {
				$dias = ((($diasGeral)+2) - (30 * $meses));	
			} else {
				$dias = ((($diasGeral)+1) - (30 * $meses));
			}
										
		} else {
			$dias = $interval->format('%d')+1; 
			//var_dump($dias);die();
		}
	

		if($devedor){

			//die('AQUI 1'.$Rescindido.'ok');
			if ($Rescindido) {				
				//return  number_format(((round($valor/30,2)) * $dias), 2, ".", ""); // round(70/30,2)*30) = 69,90 ERRADO
				return  number_format((round(($valor/30)* $dias,2)), 2, ".", "");
			}	
			
			// if($dias <> '26'){
			// v($interval);	
			// v($dataInicio .' <-> '. $datafim .' <-> '.  $dias);
			// v($dias);	}

	// return number_format(((round($valor/30,2) * $dias) + ($valor * $meses) ), 2, ".", ""); // Bruno 270422 - quebrando resultado
			if ($dataInicio == '2024-02-01') {				
				# quando for contrato novo no mes de Fevereiro contar 30dias 
				return number_format((round(($valor/30) * 30,2) + ($valor * $meses) ), 2, ".", "");
			}else{
				if (date('m') == '02') {
				# Quando for Fevereiro calcular conforme os dias ex: 28 ou 29
				return number_format((round(($valor/29) * $dias,2) + ($valor * $meses) ), 2, ".", "");
				}else{
				return number_format((round(($valor/30) * $dias,2) + ($valor * $meses) ), 2, ".", "");		
				}
			}
		
			
		}else{
			// die('AQUI 2'.$Rescindido);
			if ($Rescindido) {

				if (date('m') == '02') {
					# Quando for Fevereiro calcular conforme os dias ex: 28 ou 29
					return  number_format((round(($valor/29)* $dias,2)), 2, ".", "");

				}else{

					return  number_format((round(($valor/30)* $dias,2)), 2, ".", "");
				}
				

				
			}
			   return number_format($valor, 2, ".", "");

		}
	}

	public function terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao)
	{
		if(($data_rescisao <> '0000-00-00') and ($data_rescisao <> '1970-01-01')) {
			return $data_rescisao;

		} else if(($data_renIII <> '0000-00-00') and ($data_renIII <> '1970-01-01')) {
			return $data_renIII;

		} else if(($data_renII <> '0000-00-00') and ($data_renII <> '1970-01-01')) {
			return $data_renII;

		} else if(($data_renI <> '0000-00-00') and ($data_renI <> '1970-01-01')) {
			return $data_renI; 

		} else if(($data_term <> '0000-00-00') and ($data_term <> '1970-01-01')) {	
			return $data_term;

	    }
    }

	

	public function recrutamento()
	{		
		$dadosView['vagas']   = $this->Empresa_model->pegarVagas();
		// $dadosView['cidades'] = $this->Empresa_model->todasCidadesIdEstado(16);
		// $dadosView['cidades'] = $this->Empresa_model->todasCidadesAlunos();
		$dadosView['meio']    = 'empresa/recrutamento';
		$this->load->view('tema/tema',$dadosView);
	}

	public function recrutar()
	{
		$this->session->set_userdata('cidadeRecutamento',$this->input->post('cidade'));
		$this->session->set_userdata('cursosRecutamento',$this->input->post('vagas'));

		
		
		$cidade  = $this->input->post('cidade');
		$curso   = $this->input->post('curso');
		$sexo    = $this->input->post('sexo');
		$sexo    = explode(',',$this->input->post('sexo'));

		// v($cidade);

		// $cidade    = implode(',',explode(';',substr($this->input->post('cidade'),1, -1)));	
		// $cursos    = implode(',',explode(';',substr($this->input->post('vagas'),1, -1)));

		$resultado = $this->Empresa_model->pegarAlunosVagasCidade($cidade, $curso, $sexo);
			
		$dadosView['vagas']   = $this->Empresa_model->pegarVagas();
		$dadosView['cidades'] = $this->Empresa_model->todasCidadesIdEstado(16);
		$dadosView['mapa']    = $resultado;
		$dadosView['meio']    = 'empresa/recrutamento';
		$this->load->view('tema/tema',$dadosView);
	}

	public function getBairroCidadeVaga()
    {
        $cidade = $this->input->post('cidadeVaga');

        //V($cidade);

        $cidades = $this->Empresa_model->getBairroCidadeEstado($cidade);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->nome."'>".$cidade->nome."</option>";
        }

    }


    public function visualizarconvenio()
	{

		$id = $this->uri->segment(3);

		$resultado = $this->Empresa_model->pegarPorId($id);
		
				
		$dadosView['dados'] = $resultado;

		$dadosView['dados'][0]->sempemcnpj = $this->Mask("##.###.###/####-##",$this->soNumero($resultado[0]->sempemcnpj));
		// $dadosView['dados'][0]->salualcpf = $this->Mask("###.###.###-##",$this->soNumero($resultado[0]->salualcpf));



		$dadosView['emitente'] = $this->Empresa_model->agenciaEmitente();

		
		$dadosView['meio']       = 'empresa/'.CLIENTE.'/convenio';
		
		if (CLIENTE == 'nudep') {
			$folter = true;
		}else{
			$folter = false;
		}
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'convenio', false, $folter);
		
	}


	public function faturarDemonstrativo()
	{

		# code...
        $valorTotal = $this->uri->segment(3); // Valor
        $empresa    = $this->uri->segment(4); // IdEmpresa

        $mes = date('m');
		$ano = date('Y'); // Ano atual

      

        // pegar os getId junto com cliente_rastreador

        $this->data['result'] = $this->Empresa_model->pegarPorId($empresa);


        // var_dump($this->data['result'][0]->sempemvenccontrato );die();

        
        $vencimento = $ano.'-'.$mes.'-'. $this->data['result'][0]->sempemvenccontrato;

        $chaveMes = $mes;
        $chaveAno = $ano;
        $chaveId  = $empresa;

             

            $data = array(
                'financeiro_descricao'    => 'Fatura Demonstrativo - #'.$empresa,
                'financeiro_valor'        => $valorTotal,
                'data_vencimento'         => $vencimento,                
                'financeiro_baixado'      => '0',
                'financeiro_forn_empr'    => $this->data['result'][0]->sempemrazao,
                'financeiro_forn_empr_id' => $empresa,
                'financeiro_forma_pgto'   => 'Boleto Bancário',               
                'financeiro_tipo'        => 'receita',
                'demonstrativofin_id'    =>  $chaveMes.'_'.$chaveAno.'_'.$chaveId
            );

            //var_dump($data);die();

            if ($this->Empresa_model->add('financeiro',$data) == TRUE) {              

                $this->db->set('demonstrativoempr_faturado',1);
                
                $this->db->where('demonstrativoempr_mes', $chaveMes);
                $this->db->where('demonstrativoempr_ano', $chaveAno);
                $this->db->where('demonstrativoempr_empresa_id', $chaveId);

                $this->db->update('tbdemonstrativoempr');             

                $this->session->set_flashdata('sucesso', 'Demonstrativo Faturado com sucesso!');
               

                // $json = array('result'=>  true);
                // echo json_encode($json);
                // die();
            } else {
                $this->session->set_flashdata('erro', 'Tivemos problema para Faturar o registro!');
               
                
                // $json = array('result'=>  false);
                // echo json_encode($json);
                //die();
            }

            redirect('Empresa/visualizardemonstrativo/'.$empresa, 'refresh');

 


	}


	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $documento = $this->input->post('documento');
        $documento = str_replace($caracteres, "", $documento);        
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$documento);
        
        echo $json;
    }

    public function geolocationEmpresa($address)
	{

		// v('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');

		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');



		$output= json_decode($geocode);

		//v($output);

		if($output->status <> 'OVER_QUERY_LIMIT'){ 

		if($output->status <> 'ZERO_RESULTS'){

		//v($output);die();
		
		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;


	    $dados['latitude']   = $lat;
	    $dados['longitude']  = $long;

	    return $dados;

		} else
		{
			return false;
		}


	} else{
		echo 'You have exceeded your daily request quota for this API';
	}

	}	


	public function Mask($mask,$str){

	    $str = str_replace(" ","",$str);

	    for($i=0;$i<strlen($str);$i++){
	        $mask[strpos($mask,"#")] = $str[$i];
	    }

	    return $mask;

	}

	public function soNumero($str)
    {
        return preg_replace("/[^0-9]/", "", $str);
    }

	
}

/* End of file Empresa.php */
/* Location: ./application/controllers/Empresa.php */