<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instensino extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Instensino_model');
	}

	public function index()
	{

		$resultado = $this->Instensino_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'instensino/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Instensino_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['permissoes'] = $this->Instensino_model->tadasPermissoes();
		$dadosView['meio']       = 'instensino/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	
		$dados = array(

		  'sensencnpj'    => $this->input->post('sensencnpj'),
		  
		  'sensennome'   => $this->input->post('sensennome'),

		  'sensenmatricula'    => $this->input->post('sensenmatricula'),
		  'sensenrazao'    => $this->input->post('sensenrazao'),
		  'sensenmantededora'    => $this->input->post('sensenmantededora'),

		  'sensenlogra'   => $this->input->post('sensenlogra'),		  
		  'sensennumer'   => $this->input->post('sensennumer'),
		  'sensencompl'   => $this->input->post('sensencompl'),
		  'sensenbairr'   => $this->input->post('sensenbairr'),
		  'sensencidad'   => $this->input->post('sensencidad'),
		  'sensenuf'      => $this->input->post('sensenuf'), 
		  'sensencep'     => $this->input->post('sensencep'),

		  'sensentel01'   => $this->input->post('sensentel01'),

		  'sensenrepre'   => $this->input->post('sensenrepre'),
		  'sensencargo'   => $this->input->post('sensencargo'),		  
	  	  
		  'instensino_visivel'   => 1
		  
		);

        // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Instensino_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Instensino', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Instensino_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Instensino_model->todasCidadesIdEstado('16');
		$dadosView['estados']    = $this->Instensino_model->todosEstados();		
		$dadosView['permissoes'] = $this->Instensino_model->tadasPermissoes();
		$dadosView['meio']       = 'instensino/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('pensencodig');

		$dados = array(
		  
    	  'sensencnpj'    => $this->input->post('sensencnpj'),
		  
		  'sensennome'   => $this->input->post('sensennome'), 

		  'sensenmatricula'    => $this->input->post('sensenmatricula'),
		  'sensenrazao'    => $this->input->post('sensenrazao'),
		  'sensenmantededora'    => $this->input->post('sensenmantededora'),
		  
		  'sensenlogra'   => $this->input->post('sensenlogra'),		  
		  'sensennumer'   => $this->input->post('sensennumer'),
		  'sensencompl'   => $this->input->post('sensencompl'),
		  'sensenbairr'   => $this->input->post('sensenbairr'),
		  'sensencidad'   => $this->input->post('sensencidad'),
		  'sensenuf'      => $this->input->post('sensenuf'), 
		  'sensencep'     => $this->input->post('sensencep'),

		  'sensentel01'   => $this->input->post('sensentel01'),

		  'sensenrepre'   => $this->input->post('sensenrepre'),
		  'sensencargo'   => $this->input->post('sensencargo'),		  
	  	  
		  'instensino_visivel'   => 1


		);


		$resultado = $this->Instensino_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Instensino', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'instensino_visivel' => 0
						
					);

		$resultado = $this->Instensino_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Instensino_model->pegarPorId($id);
		$dadosView['cidades']     = $this->Instensino_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Instensino_model->todosEstados();		
		$dadosView['permissoes']  = $this->Instensino_model->tadasPermissoes();	

		$dadosView['meio']       = 'instensino/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Instensino.php */
/* Location: ./application/controllers/Instensino.php */