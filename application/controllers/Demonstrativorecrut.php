<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demonstrativorecrut extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Demonstrativorecrut_model');
	}

	public function index()
	{

		$resultado = $this->Demonstrativorecrut_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'demonstrativorecrut/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Demonstrativorecrut_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativorecrut/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		
		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativorecrut_ano'   => $dados[0],
		  'demonstrativorecrut_mes'   => $dados[1],

		  'demonstrativorecrut_estud_inscr'     => $this->input->post('demonstrativorecrut_estud_inscr'),
		  'demonstrativorecrut_estud_encamin'  => $this->input->post('demonstrativorecrut_estud_encamin'),
		  'demonstrativorecrut_estud_selec'   => $this->input->post('demonstrativorecrut_estud_selec'),
		  'demonstrativorecrut_vagas_pree'  => $this->input->post('demonstrativorecrut_vagas_pree'),
		  'demonstrativorecrut_vagas_canc'  => $this->input->post('demonstrativorecrut_vagas_canc'),	
		  'demonstrativorecrut_vagas_susp'  => $this->input->post('demonstrativorecrut_vagas_susp'),			  
		  
		  'demonstrativorecrut_usuario_id'   => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativorecrut_visivel'   => 1
		  
		);

        // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Demonstrativorecrut_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Demonstrativorecrut', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Demonstrativorecrut_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Demonstrativorecrut_model->todasPermissoes();
		$dadosView['meio']       = 'demonstrativorecrut/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('demonstrativorecrut_id');


		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativorecrut_ano'   => $dados[0],
		  'demonstrativorecrut_mes'   => $dados[1],

		  'demonstrativorecrut_estud_inscr'     => $this->input->post('demonstrativorecrut_estud_inscr'),
		  'demonstrativorecrut_estud_encamin'  => $this->input->post('demonstrativorecrut_estud_encamin'),
		  'demonstrativorecrut_estud_selec'   => $this->input->post('demonstrativorecrut_estud_selec'),		  
		  'demonstrativorecrut_vagas_pree'  => $this->input->post('demonstrativorecrut_vagas_pree'),
		  'demonstrativorecrut_vagas_canc'  => $this->input->post('demonstrativorecrut_vagas_canc'),	
		  'demonstrativorecrut_vagas_susp'  => $this->input->post('demonstrativorecrut_vagas_susp'),
		  
		  
		  'demonstrativorecrut_usuario_id'   => $this->session->userdata('usuario_id'), // ID na Sessão		    	  
		  'demonstrativorecrut_visivel'   => 1
		  
		);


		$resultado = $this->Demonstrativorecrut_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Demonstrativorecrut', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'demonstrativorecrut_visivel' => 0
						
					);

		$resultado = $this->Demonstrativorecrut_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Demonstrativorecrut_model->pegarPorId($id);			
		$dadosView['permissoes']  = $this->Demonstrativorecrut_model->todasPermissoes();	

		$dadosView['meio']       = 'demonstrativorecrut/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Curso.php */
/* Location: ./application/controllers/Curso.php */