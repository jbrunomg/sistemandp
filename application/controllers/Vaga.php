<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaga extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Vaga_model');
	}

	public function index()
	{

		$resultado = $this->Vaga_model->listar();

		// echo '<pre>';
		// var_dump($resultado);die();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'vaga/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Vaga_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Vaga_model->todosEstados();
		$dadosView['cursos']     = $this->Vaga_model->todosCursos();
		$dadosView['empresa']    = $this->Vaga_model->todasEmpresas();
		$dadosView['permissoes'] = $this->Vaga_model->tadasPermissoes();
		$dadosView['meio']       = 'vaga/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

	  // $respCursos = ";";
	  // if ($_POST['curso1'] != 0)
	  //   $respCursos .= $this->input->post('curso1').';';
	  // if ($_POST['curso2'] != 0)
	  //   $respCursos .= $this->input->post('curso2').';';
	  // if ($_POST['curso3'] != 0)
	  //   $respCursos .= $this->input->post('curso3').';';
	  // if ($_POST['curso4'] != 0)
	  //   $respCursos .= $this->input->post('curso4').';';
	  // if ($_POST['curso5'] != 0)
	  //   $respCursos .= $this->input->post('curso5').';';

		$bolsa = ($this->input->post('bolsa') == 1) ? '1' : '0';
		$vale  = ($this->input->post('vale') == 1) ? '1' : '0';
		$refeicao = ($this->input->post('refeicao') == 1) ? '1' : '0';
		$outros = ($this->input->post('outros') == 1) ? '1' : '0';

		$dados = array(

		  'sestvaempresa'  => $this->input->post('empresa'),
		 // 'sestvacurso'    => $respCursos,

		  'sestvaserie'    => $this->input->post('sestvaserie'),
		  'sestvamodul'    => $this->input->post('sestvamodul'),
		  'sestvaperio'    => $this->input->post('sestvaperio'),
		  'sestvaativi'    => $this->input->post('atividade'),
		  'sestvacarho'    => $this->input->post('carHoraria'),

		  'sestvauf'     => $this->input->post('estado'),

		  'sestvacidad'    => $this->input->post('cidade'),		  
		  'sestvabairr'    => $this->input->post('bairro'),

		  'testvaexpir'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('testvaexpir')))), 
		  'eestvabolsa'    => $bolsa,
		  'eestva_valorauxilio' => $this->input->post('valor_auxilio'),
		  'eestvavale'     => $vale,
		  'eestvarefei'    => $refeicao,
		  'eestvaoutros'   => $outros,	
		  'sestvasexo'     => $this->input->post('sexo'),
		  'observacaovaga' => $this->input->post('observacaovaga'),

		  'vaga_curso'   => $this->input->post('curso'),            
		  'vaga_nivel'   => $this->input->post('nivel'),
		  'vaga_curso2'  => $this->input->post('curso2'),            
		  'vaga_nivel2'  => $this->input->post('nivel2'),
		  'vaga_curso3'  => $this->input->post('curso3'),            
		  'vaga_nivel3'  => $this->input->post('nivel3'),
		  'vaga_curso4'  => $this->input->post('curso4'),            
		  'vaga_nivel4'  => $this->input->post('nivel4'),
		 	  	  
		  'vaga_visivel'   => 1
		  
		);

         // echo '<pre>';
		 // var_dump($dados);die();

		$resultado = $this->Vaga_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Vaga', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Vaga_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Vaga_model->todasCidadesIdEstado('16');
		$dadosView['estados']    = $this->Vaga_model->todosEstados();
		$dadosView['empresa']    = $this->Vaga_model->todasEmpresas();
		$dadosView['cursos']     = $this->Vaga_model->todosCursos();
		$dadosView['permissoes'] = $this->Vaga_model->tadasPermissoes();
		$dadosView['meio']       = 'vaga/editar';

		$cursos = explode(';', $dadosView['dados'][0]->sestvacurso);
		foreach($cursos as $c) {
		  if (!empty($c)) {
		    $dadosView['cursosSelecionados'][] = $c;
		  }
		}

		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('pestvacodig');

		 // $respCursos = ";";
	  // if ($_POST['curso1'] != 0)
	  //   $respCursos .= $this->input->post('curso1').';';
	  // if ($_POST['curso2'] != 0)
	  //   $respCursos .= $this->input->post('curso2').';';
	  // if ($_POST['curso3'] != 0)
	  //   $respCursos .= $this->input->post('curso3').';';
	  // if ($_POST['curso4'] != 0)
	  //   $respCursos .= $this->input->post('curso4').';';
	  // if ($_POST['curso5'] != 0)
	  //   $respCursos .= $this->input->post('curso5').';';


		$bolsa = ($this->input->post('bolsa') == 1) ? '1' : '0';
		$vale  = ($this->input->post('vale') == 1) ? '1' : '0';
		$refeicao = ($this->input->post('refeicao') == 1) ? '1' : '0';
		$outros   = ($this->input->post('outros') == 1) ? '1' : '0';


		$dados = array(

		  'sestvaempresa'  => $this->input->post('empresa'),
		 // 'sestvacurso'    => $respCursos,

		  'sestvaserie'    => $this->input->post('sestvaserie'),
		  'sestvamodul'    => $this->input->post('sestvamodul'),
		  'sestvaperio'    => $this->input->post('sestvaperio'),
		  'sestvaativi'    => $this->input->post('atividade'),
		  'sestvacarho'    => $this->input->post('carHoraria'),

		 // 'vaga_estado'     => $this->input->post('estado'),

		  'sestvacidad'    => $this->input->post('cidade'),		  
		  'sestvabairr'    => $this->input->post('bairro'),
		  'testvaexpir'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('testvaexpir')))), 
		  'eestvabolsa'    => $bolsa,
		  'eestva_valorauxilio' => $this->input->post('valor_auxilio'),
		  'eestvavale'     => $vale,
		  'eestvarefei'    => $refeicao,
		  'eestvaoutros'   => $outros,	
		  'sestvasexo'     => $this->input->post('sexo'),
		  'observacaovaga' => $this->input->post('observacaovaga'),

		  'vaga_curso'   => $this->input->post('curso'),            
		  'vaga_nivel'   => $this->input->post('nivel'),
		  'vaga_curso2'  => $this->input->post('curso2'),            
		  'vaga_nivel2'  => $this->input->post('nivel2'),
		  'vaga_curso3'  => $this->input->post('curso3'),            
		  'vaga_nivel3'  => $this->input->post('nivel3'),
		  'vaga_curso4'  => $this->input->post('curso4'),            
		  'vaga_nivel4'  => $this->input->post('nivel4'),
		 	  	  
		  'vaga_visivel'   => 1
	
		  
		);


		$resultado = $this->Vaga_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Vaga', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'vaga_visivel' => 0
						
					);

		$resultado = $this->Vaga_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Vaga_model->pegarPorId($id);
		$dadosView['cidades']     = $this->Vaga_model->todasCidadesIdEstado('16');
		$dadosView['estados']     = $this->Vaga_model->todosEstados();
		$dadosView['curso']       = $this->Vaga_model->todosCursos();
		$dadosView['permissoes']  = $this->Vaga_model->tadasPermissoes();

		$cursos = explode(';', $dadosView['dados'][0]->sestvacurso);
		foreach($cursos as $c) {
		  if (!empty($c)) {
		    $dadosView['cursosSelecionados'][] = $c;
		  }
		}

		$dadosView['meio']       = 'vaga/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Vaga.php */
/* Location: ./application/controllers/Vaga.php */