<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratoestudante extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Contratoestudante_model');
	}

	public function index()
	{

		$resultado = $this->Contratoestudante_model->listar();

		// echo '<pre>';
		// var_dump($resultado);die();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'contratacao/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function notificacaoTce()
	{
		$resultadoTce  = $this->Contratoestudante_model->tcePendenteListar();
		
		$dadosView['dados'] = $resultadoTce;
			
		$dadosView['meio']  = 'contratacao/listar';
		$this->load->view('tema/tema',$dadosView);	 	

	}

	public function notificacaoTa()
	{
		$resultadoTa  = $this->Contratoestudante_model->taPendenteListar();
		
		$dadosView['dados'] = $resultadoTa;
			
		$dadosView['meio']  = 'contratacao/listar';
		$this->load->view('tema/tema',$dadosView);	 	

	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Contratoestudante_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['estudante']  = $this->Contratoestudante_model->todosEstudante();
		$dadosView['empresas']   = $this->Contratoestudante_model->todasEmpresas();

		// var_dump($dadosView['empresas']);die();

		$dadosView['vaga']       = $this->Contratoestudante_model->todasVagas();
		$dadosView['permissoes'] = $this->Contratoestudante_model->todasPermissoes();
		$dadosView['meio']       = 'contratacao/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

		$renovacaoI   = $this->input->post('contrestempr_data_renI');
		$renovacaoII  = $this->input->post('contrestempr_data_renII');
		$renovacaoIII = $this->input->post('contrestempr_data_renIII');

		//var_dump($renovacaoI);die();

		$recisao = $this->input->post('contrestempr_data_rescisao');

		$termo_c_e  = $this->input->post('contrestempr_termo_C_E');
		$termo_a_I  = $this->input->post('contrestempr_termo_A_I');
		$termo_a_II = $this->input->post('contrestempr_termo_A_II');
		$termo_a_III = $this->input->post('contrestempr_termo_A_III');	

		$contrestempr_seguro = $this->input->post('contrestempr_seguro');	


		$dados = array(

		  'iempemcodig'  => $this->input->post('empresas'),
		  'ialualcodig'  => $this->input->post('estudante'),
		  'iestvacodig'  => $this->input->post('vaga'),
		  'contrestempr_estagioobg'    => $this->input->post('contrestempr_estagioobg'),	  
		  
		  'contrestempr_atividade'     => $this->input->post('contrestempr_atividade'),
		  'contrestempr_bolsa_Auxilio' => $this->input->post('contrestempr_bolsa_Auxilio'),

		  'contrestempr_beneficios'      => $this->input->post('contrestempr_beneficios'),
		  'contrestempr_cargahoraria'    => $this->input->post('contrestempr_cargahoraria'),
		  'contrestempr_duracao_estagio' => $this->input->post('contrestempr_duracao_estagio'),

		  'contrestempr_setor'      => $this->input->post('contrestempr_setor'),
		  'contrestempr_orientador' => $this->input->post('contrestempr_orientador'),		   
		  'contrestempr_cargo'      => $this->input->post('contrestempr_cargo'),
		  'contrestempr_formacao'   => $this->input->post('contrestempr_formacao'),


		  // 'contrestempr_represent_legal' => $this->input->post('contrestempr_represent_legal'),
		  // 'contrestempr_repr_cargo'      => $this->input->post('contrestempr_repr_cargo'),
		  // 'contrestempr_repr_cpf'        => $this->input->post('contrestempr_repr_cpf'),
		  		
		  'contrestempr_data_ini'     => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('contrestempr_data_ini')))),		 
		  'contrestempr_data_term'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('contrestempr_data_term')))),

		  'contrestempr_data_renI'    =>  $renovacaoI == '0000-00-00' ? $renovacaoI : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoI))),	
		  'contrestempr_data_renII'   =>  $renovacaoII == '0000-00-00' ? $renovacaoII : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoII))),
		  'contrestempr_data_renIII'  => $renovacaoIII == '0000-00-00' ? $renovacaoIII : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoIII))),

		  'contrestempr_data_rescisao'    => $recisao = '0000-00-00' ? $recisao : date('Y-m-d', strtotime(str_replace('/', '-',$recisao))),
		  
		  'contrestempr_termo_C_E'    => $termo_c_e == null ? 0 : $termo_c_e,
		  'contrestempr_termo_A_I'    => $termo_a_I == null ? 0 : $termo_a_I,
		  'contrestempr_termo_A_II'   => $termo_a_II == null ? 0 : $termo_a_II,
		  'contrestempr_termo_A_III'  => $termo_a_III == null ? 0 : $termo_a_III,

		  'contrestempr_seguro'      => $contrestempr_seguro == null ? 0 : $contrestempr_seguro,
		  
		  'contrestempr_status'   => $this->input->post('situacao'),
		  'contrestempr_obs'   => $this->input->post('contrestempr_obs'),
		  
		  'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  'date_operacao' => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro

	  	  
		  'ultimo_usuario_id' => $this->session->userdata('usuario_id'),
		  'contrato_visivel'   => 1
		  
		);

        if ( is_numeric($id = $this->Contratoestudante_model->inserir($dados,true)) ) {
        	$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso, você já pode baixar T.C.E ou T.A no momento que quiser! ');
   
            redirect('Contratoestudante/editar/'.$id);

        } else {
            
            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        	redirect('Contratoestudante', 'refresh');
        }

		// $resultado = $this->Contratoestudante_model->inserir($dados);

		// if ($resultado) {			
		// 	$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		// }else{
		// 	$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		// }

		redirect('Contratoestudante', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Contratoestudante_model->pegarPorId($id);
		$estudante = $dadosView['dados'][0]->ialualcodig;		
		$dadosView['estudante']  = $this->Contratoestudante_model->pegarEstudante($estudante);
		$dadosView['empresas']   = $this->Contratoestudante_model->todasEmpresas();
		$dadosView['vaga']       = $this->Contratoestudante_model->todasVagas();
		$dadosView['permissoes'] = $this->Contratoestudante_model->todasPermissoes();

		// echo "<pre>";
		// var_dump($dadosView['dados']);die();

		$dadosView['meio']       = 'contratacao/editar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('contrestempr_id');


		$renovacaoI   = $this->input->post('contrestempr_data_renI');
		$renovacaoII  = $this->input->post('contrestempr_data_renII');
		$renovacaoIII = $this->input->post('contrestempr_data_renIII');

		$recisao = $this->input->post('contrestempr_data_rescisao');

		$termo_c_e = $this->input->post('contrestempr_termo_C_E');
		$termo_a_I = $this->input->post('contrestempr_termo_A_I');
		$termo_a_II = $this->input->post('contrestempr_termo_A_II');
		$termo_a_III = $this->input->post('contrestempr_termo_A_III');

		$contrestempr_seguro = $this->input->post('contrestempr_seguro');


		$estorno = $this->input->post('contrestempr_estorno');

		//var_dump($estorno_dias);die();


		$dados = array(

		  'iempemcodig'  => $this->input->post('empresas'),
		  'ialualcodig'  => $this->input->post('estudante'),
		  'iestvacodig'  => $this->input->post('vaga'),
		  
		  'contrestempr_atividade'    => $this->input->post('contrestempr_atividade'),
		  'contrestempr_bolsa_Auxilio'    => $this->input->post('contrestempr_bolsa_Auxilio'),

		  'contrestempr_beneficios'    => $this->input->post('contrestempr_beneficios'),
		  'contrestempr_cargahoraria'  => $this->input->post('contrestempr_cargahoraria'),
		  'contrestempr_duracao_estagio'  => $this->input->post('contrestempr_duracao_estagio'),

		  'contrestempr_setor'      => $this->input->post('contrestempr_setor'),
		  'contrestempr_orientador' => $this->input->post('contrestempr_orientador'),		   
		  'contrestempr_cargo'      => $this->input->post('contrestempr_cargo'),
		  'contrestempr_formacao'   => $this->input->post('contrestempr_formacao'),

		  // 'contrestempr_represent_legal' => $this->input->post('contrestempr_represent_legal'),
		  // 'contrestempr_repr_cargo'      => $this->input->post('contrestempr_repr_cargo'),
		  // 'contrestempr_repr_cpf'        => $this->input->post('contrestempr_repr_cpf'),


		  'contrestempr_data_ini'     => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('contrestempr_data_ini')))),		 
		  'contrestempr_data_term'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('contrestempr_data_term')))),

		  'contrestempr_data_renI'     =>  $renovacaoI == '00-00-0000' ?  $renovacaoI : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoI))),
		  'contrestempr_data_renII'    =>  $renovacaoII == '00-00-0000' ?  $renovacaoII : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoII))),
		  'contrestempr_data_renIII'   =>  $renovacaoIII == '00-00-0000' ?  $renovacaoIII : date('Y-m-d', strtotime(str_replace('/', '-',$renovacaoIII))),

		  				  
		  'contrestempr_data_rescisao' => $recisao = '0000-00-00' ? date('Y-m-d', strtotime(str_replace('/', '-',$recisao))) : $recisao,
		  'contrestempr_status'        => $this->input->post('situacao'),		  

		  'contrestempr_termo_C_E'     => $termo_c_e == 1 ?  $termo_c_e : 0,
		  'contrestempr_termo_A_I'     => $termo_a_I == null ? 0 : $termo_a_I,
		  'contrestempr_termo_A_II'    => $termo_a_II == null ? 0 : $termo_a_II,
		  'contrestempr_termo_A_III'   => $termo_a_III == null ? 0 : $termo_a_III,

		  'contrestempr_seguro'        => $contrestempr_seguro == null ? 0 : $contrestempr_seguro,

		  'contrestempr_estorno'         => $estorno == null ? 0 : $estorno,
		  'contrestempr_estorno_dias'    => $this->input->post('contrestempr_estorno_dias'),		 
		  'contrestempr_obs'              => $this->input->post('contrestempr_obs'),
		  'contrestempr_solicitacao_site' => 1, 	
		  
		  //'date_cadastro' => date('Y-m-d H:i:s'), // Data do 1º dia de cadastro
		  'date_operacao'     => date('Y-m-d H:i:s'), // Data sempre quando Atualizar cadastro

		  'ultimo_usuario_id' => $this->session->userdata('usuario_id'),
		  'contrato_visivel'  => 1
		  
		);


		// echo "<pre>";
	    // var_dump($dados);die();	


		$resultado = $this->Contratoestudante_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Contratoestudante', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'contrato_visivel' => 0
						
					);

		$resultado = $this->Contratoestudante_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Contratoestudante_model->pegarPorId($id);
		$estudante = $dadosView['dados'][0]->ialualcodig;		
		$dadosView['estudante']  = $this->Contratoestudante_model->pegarEstudante($estudante);
		$dadosView['empresas']   = $this->Contratoestudante_model->todasEmpresas();
		$dadosView['vaga']       = $this->Contratoestudante_model->todasVagas();
		$dadosView['permissoes'] = $this->Contratoestudante_model->todasPermissoes();

	

		$dadosView['meio']       = 'contratacao/visualizar';
		$this->load->view('tema/tema',$dadosView);


	}


	public function carregarContrato()
	{
		$id = $this->uri->segment(3);

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
		
				
		$dadosView['dados'] = $resultado;

		$dadosView['dados'][0]->sempemcnpj = $this->Mask("##.###.###/####-##",$this->soNumero($resultado[0]->sempemcnpj));
		$dadosView['dados'][0]->salualcpf = $this->Mask("###.###.###-##",$this->soNumero($resultado[0]->salualcpf));

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();


		// $dadosView['meio']       = 'empresa/'.CLIENTE.'/visualizarTCE';


		$this->load->view('empresa/'.CLIENTE.'/visualizarTCE', $dadosView);
		
	}

	public function carregarContrato_OLD()
	{

		$id = $this->uri->segment(3);

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
		
				
		$dadosView['dados'] = $resultado;

		$dadosView['dados'][0]->sempemcnpj = $this->Mask("##.###.###/####-##",$this->soNumero($resultado[0]->sempemcnpj));
		$dadosView['dados'][0]->salualcpf = $this->Mask("###.###.###-##",$this->soNumero($resultado[0]->salualcpf));

		// echo "<pre>";
		// var_dump($dadosView['dados']);die();

		
		// $emitente  = $this->Contratoestudante_model->agenciaEmitente();

		// $dadosView['emitente'] = $emitente[0];

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		
		$dadosView['meio']       = 'empresa/'.CLIENTE.'/visualizarTCE';
		
		if (CLIENTE == 'nudep') {
			$folter = true;
		}else{
			$folter = false;
		}
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'tce', false, $folter);
		
	}

	public function carregarContratoAditivo()
	{
		$id = $this->uri->segment(3);

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$dadosView['dados'] = $resultado;


		$this->load->view('empresa/'.CLIENTE.'/visualizarTA', $dadosView);
	}

	public function carregarContratoAditivo_old()
	{

		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
	
		$dadosView['dados'] = $resultado;

		$dadosView['dados'][0]->sempemcnpj = $this->Mask("##.###.###/####-##",$this->soNumero($resultado[0]->sempemcnpj));
		$dadosView['dados'][0]->salualcpf = $this->Mask("###.###.###-##",$this->soNumero($resultado[0]->salualcpf));

		$dadosView['meio']  = 'empresa/'.CLIENTE.'/visualizarTA';
		
		if (CLIENTE == 'nudep') {
			$folter = true;
		}else{
			$folter = false;
		}
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'tce', false, $folter);
		
	}

	public function carregarDeclaracaoEstagio()
	{
		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);

		$dadosView['dados'] = $resultado;

		$this->load->view('empresa/' . CLIENTE . '/visualizarDE',$dadosView);
	}

	public function carregarDeclaracaoEstagio_old()
	{
		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
	
		$dadosView['dados'] = $resultado;

		$dadosView['meio']  = 'empresa/'.CLIENTE.'/visualizarDE';
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'declaracao_estagio', false, false);
		

	}

	public function carregarTermoRecisaoEstagio()
	{
		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);

		$dadosView['dados'] = $resultado;

		$this->load->view('empresa/'.CLIENTE.'/visualizarTR', $dadosView);
	}

	public function carregarTermoRecisaoEstagio_old()
	{
		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
	
		$dadosView['dados'] = $resultado;

		$dadosView['meio']  = 'empresa/'.CLIENTE.'/visualizarTR';
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'termo_recisao', false, false);
		

	}

	public function carregarFichaAcompanhamentoEstagio()
	{
		$id = $this->uri->segment(3);

		$dadosView['emitente'] = $this->Contratoestudante_model->agenciaEmitente();

		$resultado = $this->Contratoestudante_model->carregarContratoId($id);
	
		$dadosView['dados'] = $resultado;

		$dadosView['meio']  = 'empresa/'.CLIENTE.'/visualizarFAE';		
		
		$html = $this->load->view($dadosView['meio'],$dadosView, true);
		gerarPdf($html,'ficha_acompanhamento_estagio', false, false);
		

	}

	public function duplicidade()
    {
        $estudante =  $this->input->post('estudante');
        $empresas   =  $this->input->post('empresas');

        $dados['result']  = $this->Contratoestudante_model->duplicidade($estudante,$empresas);
		
		$retorno['status'] = false;

		if($dados['result']){
			$retorno['status'] = true;
		}
		//var_dump($dados);die();
		
		echo json_encode($retorno);

    }

    public function soNumero($str)
    {
        return preg_replace("/[^0-9]/", "", $str);
    }

	public function Mask($mask,$str){
    $str = str_replace(" ","",$str);
    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

	}

	public function selecionarEmpresas()
	{
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Contratoestudante_model->selecionarEmpresas($termo['term']);
		echo json_encode($ret);
	}


	public function pegarVagaPeloID()
	{
		$vagaId =  $this->input->post('id');

  //       $ret['results'] = $this->Contratoestudante_model->pegarVagaPeloID($vagaId);
		// echo json_encode($ret);

		if (isset($vagaId) && $vagaId > 0) {
            echo json_encode($this->Contratoestudante_model->pegarVagaPeloID($vagaId));
        } else {
            echo false;
        }

	}


	public function solicitacaoContr()
	{
		$resultado = $this->Contratoestudante_model->solicitacaoContr();

		// echo '<pre>';
		// var_dump($resultado);die();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'contratacao/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	

}

/* End of file Vaga.php */
/* Location: ./application/controllers/Vaga.php */
