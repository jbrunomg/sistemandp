<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patrimonio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Patrimonio_model');
	}

	public function index()
	{

		$resultado = $this->Patrimonio_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'patrimonio/listar';
		$this->load->view('tema/tema',$dadosView);
	}



	public function adicionar()
	{		
		$dadosView['patrimonio']      = $this->Patrimonio_model->todosPatrimonios();
		$dadosView['permissoes'] = $this->Patrimonio_model->todasPermissoes();
		$dadosView['meio']       = 'patrimonio/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	
		$dados = array(

                      'patrimonio_documento'         => $this->input->post('documento'),
					  'patrimonio_nome'              => $this->input->post('descricao'),

					  'patrimonio_aquisicao'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('dataAquisicao')))),

					  'patrimonio_estado_conservacao' => $this->input->post('situacao'),					  
					  'patrimonio_valor'              => $this->input->post('valor'),
					  'patrimonio_etiqueta'           => $this->input->post('etiqueta'),
					  'patrimonio_tipo'               => $this->input->post('tipo'),
					  'patrimonio_data_atualizacao'   => date('Y-m-d H:i:s'),
					  'patrimonio_visivel'            => 1
		  
		);

        // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Patrimonio_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Patrimonio', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Patrimonio_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Patrimonio_model->todasPermissoes();
		$dadosView['meio']       = 'patrimonio/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('patrimonio_id');

		$dados = array(
		  
					   'patrimonio_documento'         => $this->input->post('documento'),
					   'patrimonio_nome'               => $this->input->post('descricao'),

					   'patrimonio_aquisicao'    => date('Y-m-d', strtotime(str_replace('/', '-',$this->input->post('dataAquisicao')))),	

					   'patrimonio_estado_conservacao' => $this->input->post('conservacao'),					  
					   'patrimonio_valor'              => $this->input->post('valor'),
					   'patrimonio_etiqueta'           => $this->input->post('etiqueta'),
					   'patrimonio_tipo'               => $this->input->post('tipo'),
					   'patrimonio_data_atualizacao'   => date('Y-m-d H:i:s'),
					   'patrimonio_visivel'            => 1
		  
		);

		// var_dump($dados);die();

		$resultado = $this->Patrimonio_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Patrimonio', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'patrimonio_visivel' => 0
						
					);

		$resultado = $this->Patrimonio_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Patrimonio_model->pegarPorId($id);		
		$dadosView['permissoes']  = $this->Patrimonio_model->todasPermissoes();	

		$dadosView['meio']       = 'patrimonio/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}

}

/* End of file Patrimonio.php */
/* Location: ./application/controllers/Patrimonio.php */