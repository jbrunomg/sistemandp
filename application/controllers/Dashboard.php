<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');
	}

	public function index()
	{
		$noticias = $this->carregarnoticias();		
		
		$resultadoEst  = $this->Dashboard_model->graficoEstudante();
		$resultadoEmpr = $this->Dashboard_model->graficoEmpresa();
		$resultadoVag = $this->Dashboard_model->graficoVaga();
		

		$data['noticias']   = $noticias;
		
		$data['dadosestudante'] = $resultadoEst;
		$data['dadosempresa'] = $resultadoEmpr;
		$data['dadosvaga'] = $resultadoVag;
		

		
		$data['meio'] = 'dashboard/listar';
		$this->load->view('tema/tema',$data);
	 	

	}

	public function notificacoes()
	{
		$dados['resultadoTce']      = $this->Dashboard_model->tcePendente();
		$dados['resultadoTa']       = $this->Dashboard_model->taPendente();
		$dados['resultadoContVenc'] = $this->Dashboard_model->contratoVencido(false);
		$dados['resultadoCont20aVenc'] = $this->Dashboard_model->contrato20Vencer(false);
		$dados['resultadoSolicContra'] = $this->Dashboard_model->solicContra();
		$dados['resultadoSolicRecrut'] = $this->Dashboard_model->solicRecrut();

		// var_dump($dados['resultadoCont20aVenc']);die();

		$resposta['ta']  = '0';  //$dados['resultadoTa'][0]->total;
		$resposta['tce'] = '0'; //$dados['resultadoTce'][0]->total;
		$resposta['contvenc'] = $dados['resultadoContVenc'][0]->total;
		$resposta['cont20avenc'] = $dados['resultadoCont20aVenc'][0]->total;
		$resposta['solicContra'] = $dados['resultadoSolicContra'][0]->total;
		$resposta['solicRecrut'] = $dados['resultadoSolicRecrut'][0]->total;

		echo json_encode($resposta);
	}


	public function carregarnoticias()
	{


		$xml = simplexml_load_file("http://g1.globo.com/dynamo/pernambuco/educacao/rss2.xml");

		$quant = 4;
		$noticias = array();

		for($i=0;$i<$quant;$i++) 
		{
			$hora                       = explode(" ",$xml->channel->item[$i]->pubDate);
		
			$noticias[$i]['titulo']     = $xml->channel->item[$i]->title;
			$noticias[$i]['link']       = $xml->channel->item[$i]->link;
			$noticias[$i]['dia']       = $hora[1];
			$noticias[$i]['mes']       = $hora[2];			
			$fimimg                    = strpos($xml->channel->item[$i]->description, "<br />");			
			$semimg                    = substr($xml->channel->item[$i]->description, 6 + $fimimg);
			//var_dump($novo);die();
			$noticias[$i]['descricao']  = $semimg;
				
		}

	 
	    return $noticias;


	}


	public function contratoVencidoEnviarCron()
	{
		$emailsEnviar = $this->Dashboard_model->contratoVencido(true);

		foreach ($emailsEnviar as $email) {

	

	       		$num = $email['dias_vencido'];
	            // Só envia se tiver com 05 dias em atraso;     
	            // é múltiplo de 5!
	            if (($num % 5) == 0) {

					$msg_   = '<strong>CONTRATOS VENCIDOS: </strong>! <br>';
					$msg_  .= 'Prezado(a) estagiário(a),' .$email['salualnome']. '<br>';
			        $msg_  .= 'Esperamos que esta mensagem o(a) encontre bem. <br>';
					$msg_  .= 'Gostaríamos de lembrá-lo(a) da importância de manter seus documentos de estágio atualizados em nosso sistema. Verificamos que a declaração de vínculo com sua instituição de ensino para o semestre atual ainda está pendente.';
					$msg_  .= 'Este documento é fundamental para a regularização do seu contrato de estágio na <strong>'.$email['sempemfanta'].'</strong>, que atualmente encontra-se <strong>desatualizado desde '. date('d/m/Y', strtotime($email['data_term'])) .'</strong> <br><br>';
					$msg_  .= 'Solicitamos, por gentileza, que nos envie a declaração de vínculo o mais breve possível para o e-mail nudep@nudep.com.br. Assim, poderemos garantir a continuidade do seu estágio sem interrupções.<br><br>';
					$msg_  .= 'Agradecemos sua atenção e colaboração.<br><br>';

					$msg_  .= 'Atenciosamente, <br>';
					$msg_  .= 'Nudep Estágios <br>';

					$envio = $this->enviarEmails($email['salualemail'],$msg_ ,'CONTRATOS VENCIDOS - ESTÁGIOS');	

	            }    
		}

		//$envio = $this->enviarEmails('bruno@wdmtecnologia.com.br',$emailsEnviar['maladireta_enviar_conteudo'],$emailsEnviar['maladireta_enviar_assunto']); // Teste email Cron
	}


	public function contratoAvencerEnviarCron()
	{
		$emailsEnviar = $this->Dashboard_model->contrato20Vencer(true);

		foreach ($emailsEnviar as $email) {

	

	       		$num = $email['dias_vencido'];
	            // Só envia se tiver com 05 dias em atraso;     
	            // é múltiplo de 5!
	            if (($num % 5) == 0) {

					$msg_   = '<strong>CONTRATOS A VENCER: </strong>! <br>';
					$msg_  .= 'Prezado(a) estagiário(a),' .$email['salualnome']. '<br>';
			        $msg_  .= 'Esperamos que esta mensagem o(a) encontre bem. <br>';
			        $msg_  .= 'Gostaríamos de lembrá-lo(a) da importância de manter seus documentos de estágio atualizados em nosso sistema.';
			        $msg_  .= 'Verificamos que a declaração de vínculo com sua instituição de ensino para o semestre atual ainda está pendente.';
					$msg_  .= 'Este documento é fundamental para a regularização do seu contrato de estágio na <strong>'.$email['sempemfanta'].'</strong>, que atualmente encontra-se desatualizado desde <strong>'. date('d/m/Y', strtotime($email['data_term'])) .'</strong> <br><br>';
					
					$msg_  .= 'Solicitamos, por gentileza, que nos envie a declaração de vínculo o mais breve possível para o e-mail nudep@nudep.com.br. Assim, poderemos garantir a continuidade do seu estágio sem interrupções..<br>';
					$msg_  .= 'Agradecemos sua atenção e colaboração.<br><br>';

					$msg_  .= 'Atenciosamente, <br>';
					$msg_  .= 'Nudep Estágios <br>';

					$envio = $this->enviarEmails($email['salualemail'],$msg_ ,'CONTRATOS VENCIDOS - ESTÁGIOS');	

	            }    
		}

		//$envio = $this->enviarEmails('bruno@wdmtecnologia.com.br',$emailsEnviar['maladireta_enviar_conteudo'],$emailsEnviar['maladireta_enviar_assunto']); // Teste email Cron
	}



	public function enviarEmails($emails,$conteudo,$assunto)
	{
		if (CLIENTE == 'nudep') {
			$emailEmit = 'selecao@nudep.com.br';
		}else{
			$emailEmit = 'contato@semprehumana.com.br';
		}

		$this->load->library('email'); // inserido no AutoLoad
		// $this->email->from('selecao@nudep.com.br', 'Informativos');
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);

		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		//v($this->email->send());

		$this->email->reply_to($emailEmit); // Responder email
        $this->email->bcc($emailEmit); // Copia Oculta
		
		if($this->email->send())
		//if(true)
        {
            $this->session->set_flashdata('sucesso','Email enviado com sucesso!');
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('erro',$this->email->print_debugger());
            return false;
        }
	}



}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */