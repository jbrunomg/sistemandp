<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailsinstituicao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Emailsinstituicao_model');
		$this->load->model('Emails_model');
	}

	public function index()
	{
		$resultado = $this->Emailsinstituicao_model->listar();
		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'emailsinstituicao/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{	
		//$dadosView['estudantes'] = $this->Emailsinstituicao_model->listarEstudantes();		
		$dadosView['instituicao']     = $this->Emailsinstituicao_model->listarInstituicao();	
		$dadosView['meio']       = 'emailsinstituicao/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

		$instituicao     = '';


		if($this->input->post('instituicao')){
			$instituicao = $this->input->post('instituicao');
		}

		$emailsBanco = $this->Emailsinstituicao_model->pegarEmails($instituicao);



		$dados = array(		  	  		  
		  'maladireta_usuario_envio'=> $this->session->userdata('usuario_id'),		  
		  'maladireta_instituicao'     => implode(',',$instituicao),		 
		  'maladireta_assunto'     => $this->input->post('assunto'),
		  'maladireta_mensagem'     => $this->input->post('descricao'),	
		  'maladireta_data_envio'   => date('Y-m-d H:i:s'),
		  'maladireta_atualizacao'  => date('Y-m-d H:i:s')
		);

		$resultado = $this->Emailsinstituicao_model->inserir($dados);
		
		$assunto  = $this->input->post('assunto');
		$conteudo = $this->input->post('descricao');

		
		foreach ($emailsBanco as $email) {

			$dados = array(		  	  		  
			  'maladireta_enviar_email'     => $email->salualemail,			
			  'maladireta_enviar_assunto'   => $assunto,
			  'maladireta_enviar_conteudo'  => $conteudo
		
			);

			$resultado = $this->Emails_model->inserircron($dados);
			
		}
		

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('emails', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'maladireta_visivel' => 0,
						'maladireta_atualizacao' => date('Y-m-d H:i:s')
						
					);

		$resultado = $this->Emailsinstituicao_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Emailsinstituicao_model->pegarPorId($id);			
		$dadosView['meio']       = 'emailsempresa/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}
	public function enviarEmails($emails,$conteudo,$assunto)
	{
		// $this->load->library('email'); // inserido no AutoLoad
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);


		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		if($this->email->send())
        {
            $this->session->set_flashdata('success','Email enviado com sucesso!');
            return true;
            
        }
        else
        {
            $this->session->set_flashdata('error',$this->email->print_debugger());
            return false;
        }
	}

}

/* End of file Emails.php */
/* Location: ./application/controllers/Emails.php */